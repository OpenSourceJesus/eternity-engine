using System;
using UnityEngine;
using _EternityEngine;
using UnityEngine.Networking;
using System.Threading.Tasks;

namespace AICommand
{
static class OpenAIUtil
{
    static string CreateChatRequestBody(string prompt)
    {
        var msg = new OpenAI.RequestMessage();
        msg.role = "user";
        msg.content = prompt;

        var req = new OpenAI.Request();
        req.model = "gpt-3.5-turbo";
        req.messages = new [] { msg };

        return JsonUtility.ToJson(req);
    }

    public async static Task<string> InvokeChat (string prompt, uint timeout = 0)
    {
        using var post = UnityWebRequest.Post(OpenAI.Api.Url, CreateChatRequestBody(prompt), "application/json");
        post.timeout = (int) timeout;
        post.SetRequestHeader("Authorization", "Bearer " + EternityEngine.OPENAI_API_KEY);
        var request = post.SendWebRequest();
        while (!request.isDone)
            await Task.Yield();
        var json = post.downloadHandler.text;
        var data = JsonUtility.FromJson<OpenAI.Response>(json);
        return data.choices[0].message.content;
    }

    class WaitForResponseUpdater : IUpdatable
    {
        Action onDone;
        UnityWebRequest unityWebRequest;

        public void DoUpdate ()
        {


        }
    }
}
} // namespace AICommand