#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

namespace _EternityEngine
{
	public class SetInputDevice : EditorScript
	{
		const string PATH_TO_INPUT_MANAGER = "Assets/Resources/Prefabs/Managers (Prefabs)/Input Manager.prefab";

		[MenuItem("Game/Use keyboard and mouse")]
		static void SetToKeyboardAndMouse ()
		{
			Set (InputManager.InputDevice.KeyboardAndMouse);
		}

		[MenuItem("Game/Use VR")]
		static void SetToVR ()
		{
			Set (InputManager.InputDevice.VR);
		}

		public static void Set (InputManager.InputDevice inputDevice)
		{
			InputManager inputManager = (InputManager) AssetDatabase.LoadAssetAtPath(PATH_TO_INPUT_MANAGER, typeof(InputManager));
			inputManager.inputDevice = inputDevice;
			PrefabUtility.SavePrefabAsset(inputManager.gameObject);
		}
	}
}
#else
namespace _EternityEngine
{
	public class SetInputDevice : EditorScript
	{
	}
}
#endif