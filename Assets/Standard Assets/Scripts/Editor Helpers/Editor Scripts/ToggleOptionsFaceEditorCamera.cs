#if UNITY_EDITOR
using UnityEditor;

namespace _EternityEngine
{
	public class TogglesFaceEditorCamera : EditorScript
	{
		public static bool optionsFaceEditorCamera;

		public override void Do ()
		{
			_Do ();
		}

		[MenuItem("Game/Toggle Options facing editor Camera")]
		static void _Do ()
		{
			optionsFaceEditorCamera = !optionsFaceEditorCamera;
		}
	}
}
#else
namespace _EternityEngine
{
	public class TogglesFaceEditorCamera : EditorScript
	{
		public override void Do ()
		{
		}
	}
}
#endif