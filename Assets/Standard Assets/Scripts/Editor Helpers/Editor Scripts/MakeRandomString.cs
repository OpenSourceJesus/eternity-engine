using Extensions;

namespace _EternityEngine
{
	public class MakeRandomString : EditorScript
	{
		public int length;

		public override void Do ()
		{
			print(StringExtensions.Random(length, "qwertyuiopasdfghjklzxcvbnm1234567890`-= []\\;',./!@#$%^&*()~_+{}|:\"<>?QWERTYUIOPASDFGHJKLZXCVBNM"));
		}
	}
}