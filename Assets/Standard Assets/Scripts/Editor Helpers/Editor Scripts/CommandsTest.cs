using UnityEngine;
using _EternityEngine;
using System.Collections.Generic;

public class CommandsTest : EditorScript
{
	public string command;
	public bool clearMemory;
	public const uint MAX_COMMANDS = 1000;

	public override void Do ()
	{
#if UNITY_EDITOR
		if (!Application.isPlaying)
			return;
#endif
		if (clearMemory)
		{
			EternityEngine.codeCommandObjectsDict.Clear();
			EternityEngine.previousCommands.Clear();
		}
		CodeRunner.Init ();
		EternityEngine.instance.RunCommand (command);
		CodeRunner.DoUpdate ();
	}
}