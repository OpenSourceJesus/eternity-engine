#if UNITY_EDITOR
using System;
using System.IO;
using Extensions;
using System.Text;
using UnityEngine;
using UnityEditor;
using System.Reflection;
using System.Collections;
using UnityEditor.Callbacks;
using System.Collections.Generic;
using Object = UnityEngine.Object;

namespace _EternityEngine
{
	public class MakeNewOption : EditorScript
	{
		public string optionScriptName;
		public string optionPrefabPath;
		[HideInInspector]
		public Material optionMaterial;
		[HideInInspector]
		public string eternityEngineScriptPath;
		public List<string> optionScriptsFoldersPaths;
		public string inheritedOptionScriptName;
		public bool isSpawnable;
		public string[] includeReferencesToOptionsScriptsNames;
		const string SCRIPTS_FOLDER_SUFFIX = "s (Scripts)";
		const string POST_OPTION_PREFABS_INDICATOR = "// Insert new Option prefabs before this line";
		const string OPTION_PREFABS_WITH_CHILDREN_INDICATOR = "// Insert new Option prefabs with children after this line";

		public override void Do ()
		{
			optionScriptName = optionScriptName.Replace(".cs", "");
			StringBuilder expectedEndOfOptionFolderPath = new StringBuilder(inheritedOptionScriptName);
			expectedEndOfOptionFolderPath[0] = Char.ToUpper(expectedEndOfOptionFolderPath[0]);
			for (int i = 1; i < expectedEndOfOptionFolderPath.Length; i ++)
			{
				char c = expectedEndOfOptionFolderPath[i];
				if (Char.IsUpper(c))
				{
					expectedEndOfOptionFolderPath = expectedEndOfOptionFolderPath.Insert(i, " ");
					i ++;
				}
			}
			string optionScriptPath = "";
			expectedEndOfOptionFolderPath.Append(SCRIPTS_FOLDER_SUFFIX);
			for (int i = 0; i < optionScriptsFoldersPaths.Count; i ++)
			{
				optionScriptPath = optionScriptsFoldersPaths[i];
				if (optionScriptPath.EndsWith(expectedEndOfOptionFolderPath.ToString()))
					break;
			}
			optionScriptPath += "/" + optionScriptName + ".cs";
			if (File.Exists(optionScriptPath))
				return;
			EditorPrefs.SetString("New Option script path", optionScriptPath);
			File.WriteAllText(optionScriptPath, GetOptionScriptText(optionScriptName, inheritedOptionScriptName, includeReferencesToOptionsScriptsNames));
			string eternityEngineScriptText = File.ReadAllText(eternityEngineScriptPath);
			int indexOfPostOptionPrefabsIndicator = eternityEngineScriptText.IndexOf(POST_OPTION_PREFABS_INDICATOR);
			eternityEngineScriptText = eternityEngineScriptText.Insert(indexOfPostOptionPrefabsIndicator, "public " + optionScriptName + " " + GetOptionPrefabVariableName(optionScriptName) + ";\n");
			File.WriteAllText(eternityEngineScriptPath, eternityEngineScriptText);
			if (isSpawnable)
				MakeSpawnable ();
			AssetDatabase.Refresh();
		}

		// [DidReloadScripts]
		static void OnScriptsReloaded ()
		{
			MakeNewOption makeNewOption = FindObjectOfType<MakeNewOption>(true);
			if (!File.Exists(EditorPrefs.GetString("New Option script path")) || File.Exists(makeNewOption.optionPrefabPath))
				return;
			Option inheritedOptionPrefab = (Option) EternityEngine.Instance.GetType().GetField(GetOptionPrefabVariableName(makeNewOption.inheritedOptionScriptName)).GetValue(EternityEngine.instance);
			Option inheritedOption = Instantiate(inheritedOptionPrefab);
			inheritedOption.SetData ();
			Option.Data inheritedData = inheritedOption._Data;
			string typeName = "_EternityEngine." + makeNewOption.optionScriptName;
			Option newOption = (Option) inheritedOption.gameObject.AddComponent(Type.GetType(typeName));
			newOption.prefabIndex = ObjectPool.Instance.spawnEntries.Length;
			newOption._Data = inheritedData;
			newOption.Awake ();
			newOption._Data.Apply (newOption);
			GameObject newOptionGo = newOption.gameObject;
			newOption.valueType = inheritedOption.valueType;
			newOption.notActivatableColorOffset = inheritedOption.notActivatableColorOffset;
			newOption.selectedColorOffset = inheritedOption.selectedColorOffset;
			newOption.renderer.sharedMaterial = makeNewOption.optionMaterial;
			DestroyImmediate(inheritedOption);
			GameObject newOptionPrefabGo = PrefabUtility.SaveAsPrefabAssetAndConnect(newOptionGo, makeNewOption.optionPrefabPath, InteractionMode.AutomatedAction);
			makeNewOption._OnScriptsReloadedNextEditorUpdate (newOptionGo);
		}
		
		void _OnScriptsReloadedNextEditorUpdate (GameObject go)
		{
			EditorApplication.update += () => { _OnScriptsReloaded (go); };
		}

		void _OnScriptsReloaded (GameObject go)
		{
			EditorApplication.update = null;
			GameObject prefab = PrefabUtility.GetCorrespondingObjectFromSource(go);
			DestroyImmediate(go);
			Option option = (Option) prefab.GetComponent(optionScriptName);
			EternityEngine.instance.GetType().GetField(GetOptionPrefabVariableName(optionScriptName)).SetValue(EternityEngine.instance, option);
			ObjectPool.SpawnEntry spawnEntry = new ObjectPool.SpawnEntry();
			spawnEntry.prefab = prefab;
			spawnEntry.trs = option.trs;
			ObjectPool.instance.spawnEntries = ObjectPool.instance.spawnEntries.Add(spawnEntry);
		}

		static string GetOptionScriptText (string optionScriptName, string inheritedOptionScriptName, params string[] includeReferencesToOptionsScriptsNames)
		{
			string optionVariableName = GetOptionVariableName(optionScriptName);
			string optionPrefabVariableName = GetOptionPrefabVariableName(optionScriptName);
			string output = @"
using System;

namespace _EternityEngine
{
	public class " + optionScriptName + " : " + inheritedOptionScriptName + @"
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}";
		for (int i = 0; i < includeReferencesToOptionsScriptsNames.Length; i ++)
		{
			string referenceOptionScriptName = includeReferencesToOptionsScriptsNames[i];
			output += "\npublic " + referenceOptionScriptName + " " + GetOptionVariableName(referenceOptionScriptName) + ";";
		}
		output += @"

		public override void OnActivate (EternityEngine.Hand hand)
		{
			
			// base.OnActivate (hand);
		}

		public override bool SetValue (string value)
		{
			bool output = base.SetValue(value);

			return output;
		}

		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();";
		for (int i = 0; i < includeReferencesToOptionsScriptsNames.Length; i ++)
		{
			string referenceOptionScriptName = includeReferencesToOptionsScriptsNames[i];
			output += "\nSet" + referenceOptionScriptName + "NameOfData ();";
		}
		output += @"
		}

		";
		for (int i = 0; i < includeReferencesToOptionsScriptsNames.Length; i ++)
		{
			string referenceOptionScriptName = includeReferencesToOptionsScriptsNames[i];
			output += @"
		public void Set" + referenceOptionScriptName + @"NameOfData ()
		{
			if (Exists(" + GetOptionVariableName(referenceOptionScriptName) + @"))
				_Data." + GetOptionVariableName(referenceOptionScriptName) + "Name = " + GetOptionVariableName(referenceOptionScriptName) + @".name;
		}

		public void Set" + referenceOptionScriptName + @"NameFromData ()
		{
			if (_Data." + GetOptionVariableName(referenceOptionScriptName) + @"Name != null)
				" + GetOptionVariableName(referenceOptionScriptName) + " = EternityEngine.GetOption<" + referenceOptionScriptName + @">(_Data." + GetOptionVariableName(referenceOptionScriptName) + @"Name);
		}
";
		}
		output += @"
		[Serializable]
		public class Data : " + inheritedOptionScriptName + @".Data
		{";
			for (int i = 0; i < includeReferencesToOptionsScriptsNames.Length; i ++)
			{
				string referenceOptionScriptName = includeReferencesToOptionsScriptsNames[i];
				output += "\npublic string " + GetOptionVariableName(referenceOptionScriptName) + "Name = null;";
			}
			output += @"

			public override object MakeAsset ()
			{
				" + optionScriptName + " " + optionVariableName + " = ObjectPool.instance.SpawnComponent<" + optionScriptName + ">(EternityEngine.instance." + optionPrefabVariableName + @".prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (" + optionVariableName + @");
				" + optionVariableName + @".Init ();
				return " + optionVariableName + @";
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				" + optionScriptName + " " + optionVariableName + " = (" + optionScriptName + @") asset;
				" + optionVariableName + @"._Data = this;";
				for (int i = 0; i < includeReferencesToOptionsScriptsNames.Length; i ++)
				{
					string referenceOptionScriptName = includeReferencesToOptionsScriptsNames[i];
					output += "\n " + optionVariableName + ".Set" + referenceOptionScriptName + "NameFromData ();";
				}
				output += @"
			}
		}
	}
}";
			return output;
		}

		void MakeSpawnable ()
		{
			
		}

		public static string GetOptionVariableName (string optionScriptName)
		{
			return Char.ToLower(optionScriptName[0]) + optionScriptName.Substring(1);
		}

		static string GetOptionPrefabVariableName (string optionScriptName)
		{
			return GetOptionVariableName(optionScriptName) + "Prefab";
		}

		static string GetOptionPrefabWithChildrenVariableName (string optionScriptName)
		{
			return GetOptionPrefabVariableName(optionScriptName) + "WithChildren";
		}
	}
}
#else
namespace _EternityEngine
{
	public class MakeNewOption : EditorScript
	{
	}
}
#endif