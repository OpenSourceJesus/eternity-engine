using System;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class EternityScript : EditorScript
	{
		public string script;
		public SerializableDictionary<CommandType, StringGroup> _commandsIndicatorsDict = new SerializableDictionary<CommandType, StringGroup>();
		// public SerializableDictionary<CommandType, CommandTypeGroup> _expectedCommandTypesDict = new SerializableDictionary<CommandType, CommandTypeGroup>();
		static Dictionary<CommandType, string[]> commandsIndicatorsDict = new Dictionary<CommandType, string[]>();
		// static Dictionary<CommandType, CommandType[]> expectedCommandTypesDict = new Dictionary<CommandType, CommandType[]>();
		// static Stack<string> commands = new Stack<string>();
		// static Stack<CommandType> commandTypes = new Stack<CommandType>();
		// static Stack<CommandType> expectedCommandTypes = new Stack<CommandType>();
		static Stack<(string, CommandType)> commandsInParentheses = new Stack<(string, CommandType)>();
		static CommandType[] allCommandTypes = new CommandType[0];
		static int scriptCharacterIndex;
		const int maxIterations = 100;

		public override void Do ()
		{
			// commands.Clear();
			// commandTypes.Clear();
			// expectedCommandTypes.Clear();
			Array _allCommandTypes = Enum.GetValues(typeof(CommandType));
			allCommandTypes = new CommandType[_allCommandTypes.Length];
			_allCommandTypes.CopyTo(allCommandTypes, 0);
			commandsIndicatorsDict.Clear();
			for (int i = 0; i < _commandsIndicatorsDict.keys.Count; i ++)
				commandsIndicatorsDict.Add(_commandsIndicatorsDict.keys[i], _commandsIndicatorsDict.values[i].strings);
			// expectedCommandTypesDict.Clear();
			// for (int i = 0; i < _expectedCommandTypesDict.keys.Count; i ++)
			// 	expectedCommandTypesDict.Add(_expectedCommandTypesDict.keys[i], _expectedCommandTypesDict.values[i].commandTypes);
			scriptCharacterIndex = 0;
			int iteration = 0;
			while (scriptCharacterIndex < script.Length)
			{
				(string commandIndicator, CommandType commandType) command = ExtractCommand(script.Substring(scriptCharacterIndex));
				CommandType commandType = command.commandType;
				if (commandType != CommandType.None)
				{
					int commandIndicatorLength = command.commandIndicator.Length;
					DoCommand (commandType, script.Substring(scriptCharacterIndex + commandIndicatorLength));
					scriptCharacterIndex += commandIndicatorLength;
				}
				iteration ++;
				if (iteration == maxIterations)
					break;
			}
		}

		public static (string commandIndicator, CommandType commandType) ExtractCommand (string text)
		{
			for (int i = 0; i < allCommandTypes.Length; i ++)
			{
				CommandType commandType = allCommandTypes[i];
				string[] commandIndicators;
				if (commandsIndicatorsDict.TryGetValue(commandType, out commandIndicators))
				{
					string commandIndicator = text.StartsWithAny(commandIndicators);
					if (commandIndicator != null)
					{
						// CommandType[] _expectedCommandTypes;
						// if (expectedCommandTypesDict.TryGetValue(commandType, out _expectedCommandTypes))
						// {
						// 	for (int i2 = 0; i2 < _expectedCommandTypes.Length; i2 ++)
						// 	{
						// 		CommandType expectedCommandType = _expectedCommandTypes[i2];
						// 		expectedCommandTypes.Push(expectedCommandType);
						// 	}
						// }
						print(commandType);
						return (commandIndicator, commandType);
					}
				}
			}
			print(CommandType.None);
			return (null, CommandType.None);
		}

		public static void DoCommand (CommandType commandType, string commandArguments)
		{
			if (commandType == CommandType.Print)
				DoPrintCommand (commandArguments);
			else if (commandType == CommandType.Space)
				commandArguments = TrimCommandStart(commandArguments);
			else if (commandType == CommandType.StartParenthesis)
			{
				commandArguments = TrimCommandStart(commandArguments);
				(string commandIndicator, CommandType commandType) command = ExtractCommand(commandArguments);
				commandType = command.commandType;
				if (commandType != CommandType.EndParenthesis)
					commandsInParentheses.Push((command.commandIndicator, commandType));
			}
			// commandTypes.Push(commandType);
		}

		static void DoPrintCommand (string commandArguments)
		{
			commandArguments = TrimCommandStart(commandArguments);
			(string commandIndicator, CommandType commandType) command = ExtractCommand(commandArguments);
			CommandType _commandType = command.commandType;
			if (_commandType == CommandType.String)
			{
				int commandIndicatorLength = command.commandIndicator.Length;
				commandArguments = commandArguments.Substring(commandIndicatorLength);
				scriptCharacterIndex += commandIndicatorLength;
			}
			else if (_commandType == CommandType.StartParenthesis)
			{
				commandArguments = commandArguments.Substring(1);
				scriptCharacterIndex ++;
				
			}
			else
			{
				Debug.LogError("Expected a string to come after the print statement but a " + _commandType + " came after instead");
				return;
			}
			string[] stringIndicators = commandsIndicatorsDict[CommandType.String];
			(int index, string indicator) stringEnd = commandArguments.IndexOfFirstAndFirst(stringIndicators);
			int stringEndIndex = stringEnd.index;
			if (stringEndIndex == -1)
			{
				Debug.LogError("You didn't mark the end of the string");
				return;
			}
			string printText = commandArguments.Remove(stringEndIndex);
			string stringEndIndicator = stringEnd.indicator;
			int addToIndex = stringEndIndex + stringEndIndicator.Length;
			commandArguments = commandArguments.Substring(addToIndex);
			scriptCharacterIndex += addToIndex;
			commandArguments = TrimCommandStart(commandArguments);
			string[] addIndicators = commandsIndicatorsDict[CommandType.Add];
			string addIndicator = commandArguments.StartsWithAny(addIndicators);
			if (addIndicator != null)
			{
				while (true)
				{
					int addIndicatorLength = addIndicator.Length;
					bool hasDecimal;
					CommandType commandType;
					commandArguments = commandArguments.Substring(addIndicatorLength);
					scriptCharacterIndex += addIndicatorLength;
					commandArguments = TrimCommandStart(commandArguments);
					string stringStartIndicator = commandArguments.StartsWithAny(stringIndicators);
					if (stringStartIndicator == null)
					{
						command = ExtractCommand(commandArguments);
						commandType = command.commandType;
						hasDecimal = commandType == CommandType.Period;
						if (hasDecimal)
						{
							printText += ".";
							commandArguments = commandArguments.Substring(1);
							scriptCharacterIndex ++;
							command = ExtractCommand(commandArguments);
							commandType = command.commandType;
							if (commandType == CommandType.Period)
							{
								Debug.LogError("Can't have a float with multiple decimal symbols");
								return;
							}
							else if (commandType == CommandType.None || commandType == CommandType.Print || commandType == CommandType.String)
							{
								Debug.LogError("Found " + commandArguments[0] + " immediately after a digit not in a string or variable name");
								return;
							}
						}
						while (commandType == CommandType.Digit)
						{
							printText += command.commandIndicator;
							if (commandArguments.Length > 1)
							{
								commandArguments = commandArguments.Substring(1);
								scriptCharacterIndex ++;
								command = ExtractCommand(commandArguments);
								commandType = command.commandType;
								if (hasDecimal && commandType == CommandType.Period)
								{
									Debug.LogError("Can't have a float with multiple decimal symbols");
									return;
								}
								else if (commandType == CommandType.None || commandType == CommandType.Print || commandType == CommandType.String)
								{
									Debug.LogError("Found '" + commandArguments[0] + "' immediately after a digit not in a string or variable name");
									return;
								}
							}
							else
								break;
						}
						if ((commandType == CommandType.None || commandType == CommandType.Print) && commandArguments.Length > 1)
						{
							Debug.LogError("Can't add a " + commandType + " to a string");
							return;
						}
					}
					else
					{
						stringEnd = commandArguments.IndexOfFirstAndFirst(stringStartIndicator.Length, stringIndicators);
						stringEndIndex = stringEnd.index;
						if (stringEndIndex == -1)
						{
							Debug.LogError("You didn't mark the end of the string");
							return;
						}
						printText += commandArguments.Substring(stringStartIndicator.Length, stringEndIndex - stringStartIndicator.Length);
						stringEndIndicator = stringEnd.indicator;
						addToIndex = stringEndIndex + stringEndIndicator.Length;
						commandArguments = commandArguments.Substring(addToIndex);
						scriptCharacterIndex += addToIndex;
					}
					commandArguments = TrimCommandStart(commandArguments);
					command = ExtractCommand(commandArguments);
					commandType = command.commandType;
					if (commandType == CommandType.Add)
						addIndicator = command.commandIndicator;
					else
					{
						hasDecimal = commandType == CommandType.Period;
						if (hasDecimal)
						{
							printText += ".";
							commandArguments = commandArguments.Substring(1);
							scriptCharacterIndex ++;
							command = ExtractCommand(commandArguments);
							commandType = command.commandType;
							if (commandType == CommandType.Period)
							{
								Debug.LogError("Can't have a float with multiple decimal symbols");
								return;
							}
							else if (commandType == CommandType.None || commandType == CommandType.Print || commandType == CommandType.String)
							{
								Debug.LogError("Found " + commandArguments[0] + " immediately after a digit not in a string or variable name");
								return;
							}
						}
						while (commandType == CommandType.Digit)
						{
							printText += command.commandIndicator;
							if (commandArguments.Length > 1)
							{
								commandArguments = commandArguments.Substring(1);
								scriptCharacterIndex ++;
								command = ExtractCommand(commandArguments);
								commandType = command.commandType;
								if (hasDecimal && commandType == CommandType.Period)
								{
									Debug.LogError("Can't have a float with multiple decimal symbols");
									return;
								}
								else if (commandType == CommandType.None || commandType == CommandType.Print || commandType == CommandType.String)
								{
									Debug.LogError("Found '" + commandArguments[0] + "' immediately after a digit not in a string or variable name");
									return;
								}
							}
							else
								break;
						}
						break;
					}
				}
			}
			print(printText);
			// expectedCommandTypes.Pop();
		}

		static string TrimCommandStart (string commandArguments)
		{
			int previousCommandArgumentsLength = commandArguments.Length;
			commandArguments = commandArguments.TrimStart();
			scriptCharacterIndex += previousCommandArgumentsLength - commandArguments.Length;
			return commandArguments;
		}

		[Serializable]
		public struct StringGroup
		{
			public string[] strings;
		}

		[Serializable]
		public struct CommandTypeGroup
		{
			public CommandType[] commandTypes;
		}

		public enum CommandType
		{
			None,
			Print,
			String,
			Space,
			Add,
			Digit,
			Period,
			StartParenthesis,
			EndParenthesis,
		}
	}
}