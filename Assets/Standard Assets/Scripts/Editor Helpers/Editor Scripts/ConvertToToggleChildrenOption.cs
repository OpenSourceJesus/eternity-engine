#if UNITY_EDITOR
using Extensions;
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
// using Unity.EditorCoroutines.Editor;

namespace _EternityEngine
{
	[ExecuteInEditMode]
	public class ConvertToToggleChildrenOption : EditorScript
	{
		public Option option;

		public override void Do ()
		{
			if (option == null)
				option = GetComponent<Option>();
			_Do (option);
		}

		public static void _Do (Option option)
		{
			GameObject go = option.gameObject;
			List<Option> children = new List<Option>(option.children);
			option.childOptionsParent.DetachChildren();
			GameManager.DestroyOnNextEditorUpdate (option);
			Option newOption = go.AddComponent<ToggleChildrenOption>();
			newOption.collidable = true;
			newOption.activatable = true;
			newOption.Awake ();
			for (int i = 0; i < children.Count; i ++)
			{
				Option child = children[i];
				child.trs.SetParent(newOption.childOptionsParent);
			}
			// EditorCoroutineUtility.StartCoroutine(SetChildrenRoutine (newOption), newOption);
		}

		// static IEnumerator SetChildrenRoutine (Option option)
		// {
		// 	yield return null;
		// 	option.children = new List<Option>(option.childOptionsParent.GetComponentsInChildren<Option>());
		// }

		[MenuItem("Game/Convert selected Options to ToggleChildrenOptions")]
		static void _Do ()
		{
			Option[] selectedOptions = SelectionExtensions.GetSelected<Option>();
			for (int i = 0; i < selectedOptions.Length; i ++)
			{
				Option option = selectedOptions[i];
				_Do (option);
			}
		}
	}
}
#else
namespace _EternityEngine
{
	public class ConvertToToggleChildrenOption : EditorScript
	{
	}
}
#endif