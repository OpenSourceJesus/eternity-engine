#if UNITY_EDITOR
using Extensions;
using UnityEditor;

namespace _EternityEngine
{
	public class ToggleChildToOption : EditorScript
	{
		public Option parent;
		public Option child;

		public override void Do ()
		{
			_Do (parent, child);
		}

		static void _Do (Option parent, Option child)
		{
			if (!parent.children.Contains(child))
			{
				parent.AddParentingArrowToChild (child);
				parent.AddChild (child);
			}
			else
				parent.RemoveChild (child);
		}

		[MenuItem("Game/Toggle first selected Option as child to second selected Option %#t")]
		static void _DoToSelected ()
		{
			Option parent = SelectionExtensions.GetSelected<Option>()[0];
			Option child = SelectionExtensions.GetSelected<Option>()[1];
			_Do (parent, child);
		}

		[MenuItem("Game/Toggle second selected Option as child to first selected Option")]
		static void _DoToSelected2 ()
		{
			Option parent = SelectionExtensions.GetSelected<Option>()[1];
			Option child = SelectionExtensions.GetSelected<Option>()[0];
			_Do (parent, child);
		}
	}
}
#else
namespace _EternityEngine
{
	public class ToggleChildToOption : EditorScript
	{
	}
}
#endif