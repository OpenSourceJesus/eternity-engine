#if UNITY_EDITOR
using Extensions;
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	[ExecuteInEditMode]
	public class RenameOption_EditorScript : EditorScript
	{
		public Option option;

		public override void Do ()
		{
			if (option == null)
				option = GetComponent<Option>();
			_Do (option);
		}

		public static void _Do (Option option)
		{
			string name = option.text.text;
			string value = null;
			int indexOfNameToValueSeparator = name.IndexOf(option.NameToValueSeparator);
			if (indexOfNameToValueSeparator != -1)
			{
				value = name.StartAfter(option.NameToValueSeparator);
				name = name.Remove(indexOfNameToValueSeparator);
			}
			int indexOfSpaceAndLeftParenthesis = name.LastIndexOf(" (");
			if (indexOfSpaceAndLeftParenthesis != -1)
			{
				int indexOfRightParenthesis = name.LastIndexOf(")");
				name = name.RemoveStartEnd(indexOfSpaceAndLeftParenthesis, indexOfRightParenthesis);
			}
			option.name = name;
			string nameAndValue = name;
			if (value != null)
				nameAndValue += option.NameToValueSeparator + value;
			option.text.text = nameAndValue;
			EditorUtility.SetDirty(option);
			EditorUtility.SetDirty(option.text);
		}

		[MenuItem("Game/Rename selected Options")]
		static void _DoToSelected ()
		{
			Option[] options = SelectionExtensions.GetSelected<Option>();
			for (int i = 0; i < options.Length; i ++)
			{
				Option option = options[i];
				_Do (option);
			}
		}

		[MenuItem("Game/Rename all Options")]
		static void _DoToAll ()
		{
			Option[] options = FindObjectsOfType<Option>(true);
			for (int i = 0; i < options.Length; i ++)
			{
				Option option = options[i];
				_Do (option);
			}
		}
	}
}
#else
namespace _EternityEngine
{
	public class RenameOption_EditorScript : EditorScript
	{
	}
}
#endif