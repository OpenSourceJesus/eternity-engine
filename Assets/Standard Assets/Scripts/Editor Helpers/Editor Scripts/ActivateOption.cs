#if UNITY_EDITOR
using Extensions;
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	[ExecuteInEditMode]
	public class ActivateOption : EditorScript
	{
		public Option option;

		public override void Do ()
		{
			if (option == null)
				option = GetComponent<Option>();
			_Do (option);
		}

		public static void _Do (Option option)
		{
			if (option.activatable && option.gameObject.activeSelf)
				option.Activate (null);
		}

		[MenuItem("Game/Activate selected Options %#a")]
		static void _Do ()
		{
			Option[] selectedOptions = SelectionExtensions.GetSelected<Option>();
			for (int i = 0; i < selectedOptions.Length; i ++)
			{
				Option option = selectedOptions[i];
				_Do (option);
			}
		}
	}
}
#else
namespace _EternityEngine
{
	public class ActivateOption : EditorScript
	{
	}
}
#endif