#if UNITY_EDITOR
using Extensions;
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	[ExecuteInEditMode]
	public class SetOptionChildren : EditorScript
	{
		public Option option;

		public override void Do ()
		{
			if (option == null)
				option = GetComponent<Option>();
			_Do (option);
		}

		public static void _Do (Option option)
		{
			option.children.Clear();
			for (int i = 0; i < option.childOptionsParent.childCount; i ++)
			{
				Transform child = option.childOptionsParent.GetChild(i);
				Option childOption = child.GetComponent<Option>();
				option.children.Add(childOption);
			}
		}

		[MenuItem("Game/Set selected Options' children")]
		static void _Do ()
		{
			Option[] selectedOptions = SelectionExtensions.GetSelected<Option>();
			for (int i = 0; i < selectedOptions.Length; i ++)
			{
				Option option = selectedOptions[i];
				_Do (option);
			}
		}
	}
}
#else
namespace _EternityEngine
{
	public class SetOptionChildren : EditorScript
	{
	}
}
#endif