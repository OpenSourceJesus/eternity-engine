#if UNITY_EDITOR
using System;
using Extensions;
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class MakeTypingOptions : EditorScript
	{
		public string characters;
		public Option parentOption;
		
		public override void Do ()
		{
			foreach (char character in characters)
			{
				TypingOption typingOption = null;
				for (int i = 0; i < EternityEngine.Instance.optionNamesDict.Count; i ++)
				{
					Option option = EternityEngine.instance.optionNamesDict.keys[i];
					TypingOption typingOption2 = option as TypingOption;
					if (typingOption != null && typingOption2.charToType == character)
					{
						typingOption = typingOption2;
						break;
					}
				}
				if (typingOption == null)
					typingOption = Instantiate(EternityEngine.instance.typingOptionPrefab, parentOption.childOptionsParent);
				typingOption.name = "" + character;
				typingOption.text.text = typingOption.name;
				typingOption.charToType = character;
			}
		}
	}
}
#else
namespace _EternityEngine
{
	public class MakeTypingOptions : EditorScript
	{
	}
}
#endif