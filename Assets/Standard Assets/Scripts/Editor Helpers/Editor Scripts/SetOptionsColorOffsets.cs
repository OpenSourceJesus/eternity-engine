#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Extensions;

namespace _EternityEngine
{
	[ExecuteInEditMode]
	public class SetOptionsColorOffsets : EditorScript
	{
		public Option[] options = new Option[0];
		public ColorOffset notActivatableColorOffset;
		public ColorOffset selectedColorOffset;
		public ColorOffset selectedEmissionOffset;

		public override void Do ()
		{
			_Do (options, notActivatableColorOffset, selectedColorOffset);
		}

		public static void _Do (Option[] options, ColorOffset notActivatableColorOffset, ColorOffset selectedColorOffset)
		{
			for (int i = 0; i < options.Length; i ++)
			{
				Option option = options[i];
				option.notActivatableColorOffset = notActivatableColorOffset;
				option.selectedColorOffset = selectedColorOffset;
				option.enabled = !option.enabled;
				option.enabled = !option.enabled;
			}
		}
	}
}
#else
namespace _EternityEngine
{
	public class SetOptionsColorOffsets : EditorScript
	{
	}
}
#endif