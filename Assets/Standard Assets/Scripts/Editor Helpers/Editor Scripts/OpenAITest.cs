using AICommand;
using Extensions;
using UnityEngine;
using _EternityEngine;
using System.Threading.Tasks;

public class OpenAITest : EditorScript
{
	public string commandName;
	[Multiline]
	public string commandPrefix;
	[Multiline]
	public string commandSuffix;

	public override void Do ()
	{
#if UNITY_EDITOR
		if (!Application.isPlaying)
			return;
#endif
		_Do ();
	}

	async void _Do ()
	{
		Task<string> codeTask = OpenAIUtil.InvokeChat(commandPrefix + commandSuffix);
		await codeTask;
		print(codeTask.Result);
		int indexOfClassDeclaration = codeTask.Result.IndexOfFirst("public ", "class ");
		string usingStatements = codeTask.Result.Remove(indexOfClassDeclaration);
		string extraMethods = codeTask.Result;
		extraMethods = extraMethods.Substring(extraMethods.IndexOf("{", indexOfClassDeclaration) + 1);
		extraMethods = extraMethods.Remove(extraMethods.LastIndexOf("}"));
		print(extraMethods);
		CodeRunner.Init ();
		CodeRunner.RunCodeCommandOnce (commandName, "Do ();", extraMethods, usingStatements);
		CodeRunner.DoUpdate ();
	}
}