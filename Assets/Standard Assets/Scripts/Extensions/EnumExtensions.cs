﻿using System;

namespace Extensions
{
	public static class EnumExtensions
	{
		public static bool _HasFlag (this Enum _enum, Enum flag)
		{
			int value = _enum.GetHashCode();
			Array enumValues = Enum.GetValues(_enum.GetType());
			for (int i = enumValues.Length - 1; i >= 0; i --)
			{
				Enum flagValue = (Enum) enumValues.GetValue(i);
				int newValue = value - flagValue.GetHashCode();
				if (newValue >= 0)
				{
					value = newValue;
					if (flagValue.Equals(flag))
						return true;
				}
			}
			return false;
		}
	}
}