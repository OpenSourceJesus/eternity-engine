﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Extensions
{
	public static class StringExtensions
	{
		public static string SubstringStartEnd (this string str, int startIndex, int endIndex)
		{
			return str.Substring(startIndex, endIndex - startIndex + 1);
		}
		
		public static string RemoveEach (this string str, string remove)
		{
			return str.Replace(remove, "");
		}
		
		public static string StartAfter (this string str, string startAfter)
		{
			return str.Substring(str.IndexOf(startAfter) + startAfter.Length);
		}

		public static string RemoveStartEnd (this string str, int startIndex, int endIndex)
		{
			return str.Remove(startIndex, endIndex - startIndex + 1);
		}

		public static string RemoveStartAt (this string str, string remove)
		{
			return str.Remove(str.IndexOf(remove));
		}

		public static string RemoveAfter (this string str, string remove)
		{
			return str.Remove(str.IndexOf(remove) + remove.Length);
		}

		public static string RemoveStartEnd (this string str, string startString, string endString)
		{
			string output = str;
			int indexOfStartString = str.IndexOf(startString);
			if (indexOfStartString != -1)
			{
				string startOfStr = str.Substring(0, indexOfStartString);
				str = str.Substring(indexOfStartString + startString.Length);
				output = startOfStr + str.RemoveStartEnd(0, str.IndexOf(endString) + endString.Length);
			}
			return output;
		}

		public static bool ContainsOnlyInstancesOf (this string str, string instance)
		{
			for (int i = 0; i < str.Length; i ++)
			{
				if (str.StartsWith(instance))
					str = str.Remove(0, instance.Length);
			}
			return str.Length > 0;
		}
		
		public static string Random (int iterations, params string[] choices)
		{
			string output = "";
			for (int i = 0; i < iterations; i ++)
				output += choices[UnityEngine.Random.Range(0, choices.Length)];
			return output;
		}

		public static string Random (int iterations, string choices)
		{
			string output = "";
			for (int i = 0; i < iterations; i ++)
				output += choices[UnityEngine.Random.Range(0, choices.Length)];
			return output;
		}

		public static int GetCount (this string str, string findStr)
		{
			int output = -1;
			int indexOfFindStr = 0;
			do
			{
				output ++;
				indexOfFindStr = str.IndexOf(findStr, indexOfFindStr + 1);
			} while (indexOfFindStr != -1);
			return output;
		}

		public static int IndexOfFirst (this string str, params string[] findStrings)
		{
			int output = -1;
			for (int i = 0; i < findStrings.Length; i ++)
			{
				string findString = findStrings[i];
				if (output == -1)
					output = str.IndexOf(findString);
				else
				{
					int index = str.IndexOf(findString, 0, output);
					if (index != -1)
						output = index;
				}
			}
			return output;
		}

		public static int IndexOfFirst (this string str, int startIndex, params string[] findStrings)
		{
			int output = -1;
			for (int i = 0; i < findStrings.Length; i ++)
			{
				string findString = findStrings[i];
				if (output == -1)
					output = str.IndexOf(findString, startIndex);
				else
				{
					int index = str.IndexOf(findString, startIndex, output);
					if (index != -1)
						output = index;
				}
			}
			return output;
		}

		public static (int, string) IndexOfFirstAndFirst (this string str, params string[] findStrings)
		{
			(int index, string str) output = (-1, null);
			int index = -1;
			for (int i = 0; i < findStrings.Length; i ++)
			{
				string findString = findStrings[i];
				if (index == -1)
					output = (str.IndexOf(findString), findString);
				else
				{
					index = str.IndexOf(findString, 0, index);
					if (index != -1)
						output = (index, findString);
				}
			}
			return output;
		}

		public static (int, string) IndexOfFirstAndFirst (this string str, int startIndex, params string[] findStrings)
		{
			(int index, string str) output = (-1, null);
			int index = -1;
			for (int i = 0; i < findStrings.Length; i ++)
			{
				string findString = findStrings[i];
				if (index == -1)
					output = (str.IndexOf(findString, startIndex), findString);
				else
				{
					index = str.IndexOf(findString, startIndex, index);
					if (index != -1)
						output = (index, findString);
				}
			}
			return output;
		}

		public static int IndexAfterFirst (this string str, params string[] findStrings)
		{
			string findString = "";
			int output = -1;
			for (int i = 0; i < findStrings.Length; i ++)
			{
				string _findString = findStrings[i];
				if (output == -1)
				{
					output = str.IndexOf(_findString);
					findString = _findString;
				}
				else
				{
					int index = str.IndexOf(_findString, 0, output);
					if (index != -1)
					{
						output = index;
						findString = _findString;
					}
				}
			}
			if (output == -1)
				return -1;
			else
				return output + findString.Length;
		}

		public static int IndexAfterFirst (this string str, int startIndex, params string[] findStrings)
		{
			string findString = "";
			int output = -1;
			for (int i = 0; i < findStrings.Length; i ++)
			{
				string _findString = findStrings[i];
				if (output == -1)
				{
					output = str.IndexOf(_findString, startIndex);
					findString = _findString;
				}
				else
				{
					int index = str.IndexOf(_findString, startIndex, output);
					if (index != -1)
					{
						output = index;
						findString = _findString;
					}
				}
			}
			if (output == -1)
				return -1;
			else
				return output + findString.Length;
		}

		public static string StartsWithAny (this string str, params string[] findStrings)
		{
			for (int i = 0; i < findStrings.Length; i ++)
			{
				string findString = findStrings[i];
				if (str.StartsWith(findString))
					return findString;
			}
			return null;
		}
	}
}