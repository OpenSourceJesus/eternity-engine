using System;
using System.IO;
using UnityEngine;
using UnityEngine.Rendering;
using System.Collections.Generic;

namespace Extensions
{
	public static class MaterialExtensions
	{
		public static string[] IMAGE_FILE_EXTENSIONS = new string[] {
			".jpg", ".jpeg", ".jpe", ".jif", ".jfif", ".jfi",
			".png",
			".gif",
			".webp",
			".tiff", ".tif",
			".psd",
			".raw", ".arw", ".cr2", ".nrw", ".k25",
			".bmp", ".dib",
			".heif", ".heic",
			".ind", ".indd", ".indt",
			".jp2", ".j2k", ".jpf", ".jpx", ".jpm", ".mj2",
			".svg", ".svgz",
			".ai",
			".eps",
			".pdf"
		};
		public const string GUID_INDICATOR = "guid: ";
		public const string SHADER_KEYWORDS_INDICATOR = "m_ShaderKeywords: ";
		public const string DOUBLE_SIDED_GI_INDICATOR = "m_DoubleSidedGI: ";
		public const string NAME_INDICATOR = "m_Name: ";
		public const string RENDER_QUEUE_INDICATOR = "m_Name: ";
		public const string ENABLE_INSTANCING_INDICATOR = "m_EnableInstancingVariants: ";
		public const string GLOBAL_ILLUMINATION_FLAGS_INDICATOR = "m_LightmapFlags: ";
		public const string TEXTURES_INDICATOR = "m_TexEnvs:";
		public const string PROPERTY_INDICATOR = "- ";
		public const string X_INDICATOR = "x: ";
		public const string Y_INDICATOR = "y: ";
		public const string FLOATS_INDICATOR = "m_Floats:";
		public const string COLORS_INDICATOR = "m_Colors:";
		public const string RED_INDICATOR = "r: ";
		public const string GREEN_INDICATOR = "g: ";
		public const string BLUE_INDICATOR = "b: ";
		public const string ALPHA_INDICATOR = "a: ";
		public const string META_FILE_EXTENSION = ".meta";
		public const string MATERIAL_FILE_EXTENSION = ".mat";
		public const string SHADER_FILE_EXTENSION = ".shader";

		public static Material RecreateMaterial (string folderPath, Shader shader)
		{
			try
			{
				Dictionary<string, Texture2D> textureGuids = new Dictionary<string, Texture2D>();
				List<string> filePaths = new List<string>(Directory.GetFiles(folderPath));
				for (int i = 0; i < filePaths.Count; i ++)
				{
					string filePath = filePaths[i];
					filePaths.RemoveAt(i);
					i --;
					for (int i2 = 0; i2 < IMAGE_FILE_EXTENSIONS.Length; i2 ++)
					{
						string imageFileExtension = IMAGE_FILE_EXTENSIONS[i2];
						if (filePath.EndsWith(imageFileExtension + META_FILE_EXTENSION))
						{
							string[] fileLines = File.ReadAllLines(filePath);
							for (int i3 = 0; i3 < fileLines.Length; i3 ++)
							{
								string fileLine = fileLines[i3];
								int indexOfGuidIndicator = fileLine.IndexOf(GUID_INDICATOR);
								if (indexOfGuidIndicator != -1)
								{
									string guid = fileLine.Substring(indexOfGuidIndicator + GUID_INDICATOR.Length);
									filePath = filePath.Remove(filePath.Length - META_FILE_EXTENSION.Length);
									Texture2D texture = new Texture2D(1, 1);
									ImageConversion.LoadImage(texture, File.ReadAllBytes(filePath));
									textureGuids.Add(guid, texture);
									break;
								}
							}
						}
					}
				}
				Material material = new Material(shader);
				for (int i = 0; i < filePaths.Count; i ++)
				{
					string filePath = filePaths[i];
					filePaths.RemoveAt(i);
					i --;
					if (filePath.EndsWith(MATERIAL_FILE_EXTENSION))
					{
						List<string> fileLines = new List<string>(File.ReadAllLines(filePath));
						for (int i2 = 0; i2 < fileLines.Count; i2 ++)
						{
							string fileLine = fileLines[i2];
							fileLine = fileLine.TrimStart();
							int indexOfShaderKeywordsIndicator = fileLine.IndexOf(SHADER_KEYWORDS_INDICATOR);
							if (indexOfShaderKeywordsIndicator != -1)
							{
								fileLine = fileLine.Substring(indexOfShaderKeywordsIndicator + SHADER_KEYWORDS_INDICATOR.Length);
								material.shaderKeywords = fileLine.Split(new string[] { " " }, StringSplitOptions.None);
							}
							else
							{
								bool doubleSidedGI = false;
								if (GetBool(fileLine, DOUBLE_SIDED_GI_INDICATOR, out doubleSidedGI))
									material.doubleSidedGI = doubleSidedGI;
								else
								{
									string name = "";
									if (GetString(fileLine, NAME_INDICATOR, out name))
										material.name = name;
									else
									{
										int renderQueue = 0;
										if (GetInt(fileLine, RENDER_QUEUE_INDICATOR, out renderQueue))
											material.renderQueue = renderQueue;
										else
										{
											bool enableInstancing = false;
											if (GetBool(fileLine, ENABLE_INSTANCING_INDICATOR, out enableInstancing))
												material.enableInstancing = enableInstancing;
											else
											{
												MaterialGlobalIlluminationFlags globalIlluminationFlags = default(MaterialGlobalIlluminationFlags);
												if (GetEnum<MaterialGlobalIlluminationFlags>(fileLine, GLOBAL_ILLUMINATION_FLAGS_INDICATOR, out globalIlluminationFlags))
													material.globalIlluminationFlags = globalIlluminationFlags;
												else
												{
													int indexOfTexturesIndicator = fileLine.IndexOf(TEXTURES_INDICATOR);
													if (indexOfTexturesIndicator != -1)
													{
														i2 ++;
														for (int i3 = i2; i3 < fileLines.Count; i3 += 4)
														{
															string fileLine1 = fileLines[i3];
															int indexOfProertyIndicator = fileLine1.IndexOf(PROPERTY_INDICATOR);
															if (indexOfProertyIndicator != -1)
															{
																fileLine1 = fileLine1.Substring(indexOfProertyIndicator + PROPERTY_INDICATOR.Length);
																fileLine1 = fileLine1.Remove(fileLine1.Length - 1);
																string fileLine2 = fileLines[i3 + 1];
																int indexOfGuidIndicator = fileLine2.IndexOf(GUID_INDICATOR);
																string guid = fileLine2.Substring(indexOfGuidIndicator + GUID_INDICATOR.Length);
																guid = guid.RemoveStartAt(",");
																material.SetTexture(fileLine1, textureGuids[guid]);
																string fileLine3 = fileLines[i3 + 2];
																float x = float.Parse(fileLine3.SubstringStartEnd(fileLine3.IndexOf(X_INDICATOR), fileLine3.IndexOf(",") - 1));
																float y = float.Parse(fileLine3.SubstringStartEnd(fileLine3.IndexOf(Y_INDICATOR), fileLine3.IndexOf("}") - 1));
																material.SetTextureScale(fileLine1, new Vector2(x, y));
																string fileLine4 = fileLines[i3 + 3];
																x = float.Parse(fileLine4.SubstringStartEnd(fileLine4.IndexOf(X_INDICATOR), fileLine4.IndexOf(",") - 1));
																y = float.Parse(fileLine4.SubstringStartEnd(fileLine4.IndexOf(Y_INDICATOR), fileLine4.IndexOf("}") - 1));
																material.SetTextureOffset(fileLine1, new Vector2(x, y));
																i2 += 4;
															}
															else
																break;
														}
													}
													else
													{
														int indexOfFloatsIndicator = fileLine.IndexOf(FLOATS_INDICATOR);
														if (indexOfTexturesIndicator != -1)
														{
															i2 ++;
															for (int i3 = i2; i3 < fileLines.Count; i3 ++)
															{
																string fileLine1 = fileLines[i3];
																int indexOfProertyIndicator = fileLine1.IndexOf(PROPERTY_INDICATOR);
																if (indexOfProertyIndicator != -1)
																{
																	string nameAndValueSeparator = ": ";
																	int indexOfNameAndValueSeparator = fileLine1.IndexOf(nameAndValueSeparator);
																	string floatName = fileLine1.SubstringStartEnd(indexOfProertyIndicator + PROPERTY_INDICATOR.Length, indexOfNameAndValueSeparator - 1);
																	material.SetFloat(floatName, float.Parse(fileLine1.Substring(indexOfNameAndValueSeparator + nameAndValueSeparator.Length)));
																	i2 ++;
																}
																else
																	break;
															}
														}
														else
														{
															int indexOfColorsIndicator = fileLine.IndexOf(COLORS_INDICATOR);
															if (indexOfColorsIndicator != -1)
															{
																i2 ++;
																for (int i3 = i2; i3 < fileLines.Count; i3 ++)
																{
																	string fileLine1 = fileLines[i3];
																	int indexOfProertyIndicator = fileLine1.IndexOf(PROPERTY_INDICATOR);
																	if (indexOfProertyIndicator != -1)
																	{
																		string nameAndValueSeparator = ": ";
																		int indexOfNameAndValueSeparator = fileLine1.IndexOf(nameAndValueSeparator);
																		string colorName = fileLine1.SubstringStartEnd(indexOfProertyIndicator + PROPERTY_INDICATOR.Length, indexOfNameAndValueSeparator - 1);
																		int indexOfRedIndicator = fileLine1.IndexOf(RED_INDICATOR);
																		float r = float.Parse(fileLine1.SubstringStartEnd(indexOfRedIndicator + RED_INDICATOR.Length, fileLine1.IndexOf(",") - 1));
																		int indexOfGreenIndicator = fileLine1.IndexOf(GREEN_INDICATOR);
																		float g = float.Parse(fileLine1.SubstringStartEnd(indexOfGreenIndicator + GREEN_INDICATOR.Length, fileLine1.IndexOf(",") - 1));
																		int indexOfBlueIndicator = fileLine1.IndexOf(BLUE_INDICATOR);
																		float b = float.Parse(fileLine1.SubstringStartEnd(indexOfBlueIndicator + BLUE_INDICATOR.Length, fileLine1.IndexOf(",") - 1));
																		int indexOfAlphaIndicator = fileLine1.IndexOf(ALPHA_INDICATOR);
																		float a = float.Parse(fileLine1.SubstringStartEnd(indexOfAlphaIndicator + ALPHA_INDICATOR.Length, fileLine1.IndexOf("}") - 1));
																		material.SetColor(colorName, new Color(r, g, b, a));
																		i2 ++;
																	}
																	else
																		break;
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
				return material;
			}
			catch (Exception e)
			{
				return null;
			}
		}

		public static bool GetBool (string fileLine, string indicator, out bool value)
		{
			value = false;
			int indexOfIndicator = fileLine.IndexOf(indicator);
			if (indexOfIndicator != -1)
			{
				fileLine = fileLine.Substring(indexOfIndicator + indicator.Length);
				value = fileLine == "1";
				return true;
			}
			else
				return false;
		}

		public static bool GetInt (string fileLine, string indicator, out int value)
		{
			value = 0;
			int indexOfIndicator = fileLine.IndexOf(indicator);
			if (indexOfIndicator != -1)
			{
				fileLine = fileLine.Substring(indexOfIndicator + indicator.Length);
				value = int.Parse(fileLine);
				return true;
			}
			else
				return false;
		}

		public static bool GetString (string fileLine, string indicator, out string value)
		{
			value = "";
			int indexOfIndicator = fileLine.IndexOf(indicator);
			if (indexOfIndicator != -1)
			{
				value = fileLine.Substring(indexOfIndicator + indicator.Length);
				return true;
			}
			else
				return false;
		}

		public static bool GetEnum<T> (string fileLine, string indicator, out T value)
		{
			value = default(T);
			int indexOfIndicator = fileLine.IndexOf(indicator);
			if (indexOfIndicator != -1)
			{
				fileLine = fileLine.Substring(indexOfIndicator + indicator.Length);
				value = (T) Enum.ToObject(typeof(T), int.Parse(fileLine));
				return true;
			}
			else
				return false;
		}

		public static void ChangeMaterialTransparency (this Material material, bool shouldMakeTransparent)
		{
			if (shouldMakeTransparent)
			{
				material.SetFloat("_Surface", (float) SurfaceType.Transparent);
				material.SetFloat("_Blend", (float) BlendMode.Alpha);
			}
			else
				material.SetFloat("_Surface", (float) SurfaceType.Opaque);
			SetupMaterialBlendMode (material);
		}

		public static void SetupMaterialBlendMode (this Material material)
		{
			if (material == null)
				throw new ArgumentNullException("material");
			bool alphaClip = material.GetFloat("_AlphaClip") == 1;
			if (alphaClip)
				material.EnableKeyword("_ALPHATEST_ON");
			else
				material.DisableKeyword("_ALPHATEST_ON");
			SurfaceType surfaceType = (SurfaceType) material.GetFloat("_Surface");
			if (surfaceType == 0)
			{
				material.SetOverrideTag("RenderType", "");
				material.SetInt("_SrcBlend", (int) UnityEngine.Rendering.BlendMode.One);
				material.SetInt("_DstBlend", (int) UnityEngine.Rendering.BlendMode.Zero);
				material.SetInt("_ZWrite", 1);
				material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
				material.renderQueue = -1;
				material.SetShaderPassEnabled("ShadowCaster", true);
			}
			else
			{
				BlendMode blendMode = (BlendMode)material.GetFloat("_Blend");
				switch (blendMode)
				{
					case BlendMode.Alpha:
						material.SetOverrideTag("RenderType", "Transparent");
						material.SetInt("_SrcBlend", (int) UnityEngine.Rendering.BlendMode.SrcAlpha);
						material.SetInt("_DstBlend", (int) UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
						material.SetInt("_ZWrite", 0);
						material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
						material.renderQueue = (int) UnityEngine.Rendering.RenderQueue.Transparent;
						material.SetShaderPassEnabled("ShadowCaster", false);
						break;
					case BlendMode.Premultiply:
						material.SetOverrideTag("RenderType", "Transparent");
						material.SetInt("_SrcBlend", (int) UnityEngine.Rendering.BlendMode.One);
						material.SetInt("_DstBlend", (int) UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
						material.SetInt("_ZWrite", 0);
						material.EnableKeyword("_ALPHAPREMULTIPLY_ON");
						material.renderQueue = (int) UnityEngine.Rendering.RenderQueue.Transparent;
						material.SetShaderPassEnabled("ShadowCaster", false);
						break;
					case BlendMode.Additive:
						material.SetOverrideTag("RenderType", "Transparent");
						material.SetInt("_SrcBlend", (int) UnityEngine.Rendering.BlendMode.One);
						material.SetInt("_DstBlend", (int) UnityEngine.Rendering.BlendMode.One);
						material.SetInt("_ZWrite", 0);
						material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
						material.renderQueue = (int) UnityEngine.Rendering.RenderQueue.Transparent;
						material.SetShaderPassEnabled("ShadowCaster", false);
						break;
					case BlendMode.Multiply:
						material.SetOverrideTag("RenderType", "Transparent");
						material.SetInt("_SrcBlend", (int) UnityEngine.Rendering.BlendMode.DstColor);
						material.SetInt("_DstBlend", (int) UnityEngine.Rendering.BlendMode.Zero);
						material.SetInt("_ZWrite", 0);
						material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
						material.renderQueue = (int) UnityEngine.Rendering.RenderQueue.Transparent;
						material.SetShaderPassEnabled("ShadowCaster", false);
						break;
				}
			}
		}
		
		public enum SurfaceType
		{
			Opaque,
			Transparent
		}

		public enum BlendMode
		{
			Alpha,
			Premultiply,
			Additive,
			Multiply
		}
	}
}