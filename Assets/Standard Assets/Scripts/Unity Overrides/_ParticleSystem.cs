using UnityEngine;
using _EternityEngine;
using System.Collections;
using System.Collections.Generic;

[ExecuteInEditMode]
public class _ParticleSystem : Spawnable
{
	public ParticleSystem particleSystem;

// #if UNITY_EDITOR
// 	public override void Start ()
// 	{
// 		base.Start ();
// 		if (!Application.isPlaying)
// 		{
// 			if (particleSystem == null)
// 				particleSystem = GetComponent<ParticleSystem>();
// 			return;
// 		}
// 	}
// #endif
}