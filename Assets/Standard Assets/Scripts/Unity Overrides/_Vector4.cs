using System;
using UnityEngine;

[Serializable]
public struct _Vector4
{
	public float x;
	public float y;
	public float z;
	public float w;

	public _Vector4 (float x, float y, float z, float w)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		this.w = w;
	}
	
	public static _Vector4 FromVec4 (Vector4 v)
	{
		return new _Vector4(v.x, v.y, v.z, v.w);
	}

	public Vector4 ToVec4 ()
	{
		return new Vector4(x, y, z, w);
	}

	public static _Vector4 operator+ (_Vector4 v, _Vector4 v2)
	{
		return new _Vector4(v.x + v2.x, v.y + v2.y, v.z + v2.z, v.w + v2.w);
	}

	public static _Vector4 operator+ (_Vector4 v, Vector4 v2)
	{
		return new _Vector4(v.x + v2.x, v.y + v2.y, v.z + v2.z, v.w + v2.w);
	}

	public static _Vector4 operator- (_Vector4 v, _Vector4 v2)
	{
		return new _Vector4(v.x - v2.x, v.y - v2.y, v.z - v2.z, v.w - v2.w);
	}

	public static _Vector4 operator- (_Vector4 v, Vector4 v2)
	{
		return new _Vector4(v.x - v2.x, v.y - v2.y, v.z - v2.z, v.w - v2.w);
	}

	public static _Vector4 operator* (_Vector4 v, _Vector4 v2)
	{
		return new _Vector4(v.x * v2.x, v.y * v2.y, v.z * v2.z, v.w * v2.w);
	}

	public static _Vector4 operator* (_Vector4 v, Vector4 v2)
	{
		return new _Vector4(v.x * v2.x, v.y * v2.y, v.z * v2.z, v.w * v2.w);
	}

	public static _Vector4 operator/ (_Vector4 v, _Vector4 v2)
	{
		return new _Vector4(v.x / v2.x, v.y / v2.y, v.z / v2.z, v.w / v2.w);
	}

	public static _Vector4 operator/ (_Vector4 v, Vector4 v2)
	{
		return new _Vector4(v.x / v2.x, v.y / v2.y, v.z / v2.z, v.w / v2.w);
	}

	public static _Vector4 operator^ (_Vector4 v, _Vector4 v2)
	{
		return new _Vector4(Mathf.Pow(v.x, v2.x), Mathf.Pow(v.y, v2.y), Mathf.Pow(v.z, v2.z), Mathf.Pow(v.w, v2.w));
	}

	public static _Vector4 operator^ (_Vector4 v, Vector4 v2)
	{
		return new _Vector4(Mathf.Pow(v.x, v2.x), Mathf.Pow(v.y, v2.y), Mathf.Pow(v.z, v2.z), Mathf.Pow(v.w, v2.w));
	}

	public static _Vector4 operator% (_Vector4 v, _Vector4 v2)
	{
		return new _Vector4(v.x % v2.x, v.y % v2.y, v.z % v2.z, v.w % v2.w);
	}

	public static _Vector4 operator% (_Vector4 v, Vector4 v2)
	{
		return new _Vector4(v.x % v2.x, v.y % v2.y, v.z % v2.z, v.w % v2.w);
	}

	public static bool operator== (_Vector4 v, _Vector4 v2)
	{
		return v.x == v2.x && v.y == v2.y && v.z == v2.z && v.w == v2.w;
	}

	public static bool operator!= (_Vector4 v, _Vector4 v2)
	{
		return v.x != v2.x || v.y != v2.y || v.z != v2.z || v.w != v2.w;
	}
}