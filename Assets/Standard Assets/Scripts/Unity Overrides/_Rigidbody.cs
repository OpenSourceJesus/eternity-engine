using System;
using Extensions;
using UnityEngine;

[Serializable]
public struct _Rigidbody
{
	public float mass;
	public _Vector3 velocity;
	public _Vector3 angularVelocity;
	public _Vector3 position;
	public _Vector3 eulerAngles;
	public ConstrainType constrainType;

	public _Rigidbody (float mass, _Vector3 velocity, _Vector3 angularVelocity, _Vector3 position, _Vector3 eulerAngles, ConstrainType constrainType)
	{
		this.mass = mass;
		this.velocity = velocity;
		this.angularVelocity = angularVelocity;
		this.position = position;
		this.eulerAngles = eulerAngles;
		this.constrainType = constrainType;
	}
	
	public static _Rigidbody FromRigid (Rigidbody rigid)
	{
		ConstrainType constrainType = (ConstrainType) Enum.ToObject(typeof(ConstrainType), rigid.constraints.GetHashCode());
		return new _Rigidbody(rigid.mass, _Vector3.FromVec3(rigid.linearVelocity), _Vector3.FromVec3(rigid.angularVelocity), _Vector3.FromVec3(rigid.position), _Vector3.FromVec3(rigid.rotation.ToEuler()), constrainType);
	}

	public void ToRigid (Rigidbody rigid)
	{
		ApplyConstrainType (rigid);
		rigid.mass = mass;
		rigid.linearVelocity = velocity.ToVec3();
		rigid.angularVelocity = angularVelocity.ToVec3();
		rigid.position = position.ToVec3();
		rigid.rotation = Quaternion.Euler(eulerAngles.ToVec3());
	}

	public void ApplyConstrainType (Rigidbody rigid)
	{
		ApplyConstrainType (rigid, constrainType);
	}

	public static void ApplyConstrainType (Rigidbody rigid, ConstrainType constrainType)
	{
		rigid.constraints = (RigidbodyConstraints) Enum.ToObject(typeof(RigidbodyConstraints), constrainType.GetHashCode());
		rigid.isKinematic = rigid.constraints == RigidbodyConstraints.FreezeAll;
	}

	[Flags]
	public enum ConstrainType
	{
		FreezePositionX = 2,
		FreezePositionY = 4,
		FreezePositionZ = 8,
		FreezeRotationX = 16,
		FreezeRotationY = 32,
		FreezeRotationZ = 64
	}
}