using System;
using UnityEngine;

[Serializable]
public struct _Vector2Int
{
	public int x;
	public int y;

	public _Vector2Int (int x, int y)
	{
		this.x = x;
		this.y = y;
	}
	
	public static _Vector2Int FromVec2Int (Vector2Int v)
	{
		return new _Vector2Int(v.x, v.y);
	}

	public Vector2Int ToVec2Int ()
	{
		return new Vector2Int(x, y);
	}

	public static _Vector2Int operator+ (_Vector2Int v, _Vector2Int v2)
	{
		return new _Vector2Int(v.x + v2.x, v.y + v2.y);
	}

	public static _Vector2Int operator+ (_Vector2Int v, Vector2Int v2)
	{
		return new _Vector2Int(v.x + v2.x, v.y + v2.y);
	}

	public static _Vector2Int operator- (_Vector2Int v, _Vector2Int v2)
	{
		return new _Vector2Int(v.x - v2.x, v.y - v2.y);
	}

	public static _Vector2Int operator- (_Vector2Int v, Vector2Int v2)
	{
		return new _Vector2Int(v.x - v2.x, v.y - v2.y);
	}

	public static _Vector2Int operator* (_Vector2Int v, _Vector2Int v2)
	{
		return new _Vector2Int(v.x * v2.x, v.y * v2.y);
	}

	public static _Vector2Int operator* (_Vector2Int v, Vector2Int v2)
	{
		return new _Vector2Int(v.x * v2.x, v.y * v2.y);
	}

	public static _Vector2Int operator/ (_Vector2Int v, _Vector2Int v2)
	{
		return new _Vector2Int(v.x / v2.x, v.y / v2.y);
	}

	public static _Vector2Int operator/ (_Vector2Int v, Vector2Int v2)
	{
		return new _Vector2Int(v.x / v2.x, v.y / v2.y);
	}

	public static _Vector2Int operator^ (_Vector2Int v, _Vector2Int v2)
	{
		return new _Vector2Int((int) Mathf.Pow(v.x, v2.x), (int) Mathf.Pow(v.y, v2.y));
	}

	public static _Vector2Int operator^ (_Vector2Int v, Vector2Int v2)
	{
		return new _Vector2Int((int) Mathf.Pow(v.x, v2.x), (int) Mathf.Pow(v.y, v2.y));
	}

	public static _Vector2Int operator% (_Vector2Int v, _Vector2Int v2)
	{
		return new _Vector2Int(v.x % v2.x, v.y % v2.y);
	}

	public static _Vector2Int operator% (_Vector2Int v, Vector2Int v2)
	{
		return new _Vector2Int(v.x % v2.x, v.y % v2.y);
	}

	public static bool operator== (_Vector2Int v, _Vector2Int v2)
	{
		return v.x == v2.x && v.y == v2.y;
	}

	public static bool operator!= (_Vector2Int v, _Vector2Int v2)
	{
		return v.x != v2.x || v.y != v2.y;
	}
}