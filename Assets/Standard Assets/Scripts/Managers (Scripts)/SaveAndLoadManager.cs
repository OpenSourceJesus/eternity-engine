﻿using System;
using System.IO;
using Extensions;
using UnityEngine;
// using FullSerializer;
using _EternityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;

[ExecuteInEditMode]
public class SaveAndLoadManager : SingletonMonoBehaviour<SaveAndLoadManager>
{
	public static SaveData saveData;
	public static string MostRecentSaveFileName
	{
		get
		{
			return PlayerPrefs.GetString("Most recent save file name", null);
		}
		set
		{
			PlayerPrefs.SetString("Most recent save file name", value);
		}
	}
	// static fsSerializer serializer = new fsSerializer();
	
	public void Start ()
	{
#if UNITY_EDITOR
		if (!Application.isPlaying)
			return;
#endif
		if (!string.IsNullOrEmpty(MostRecentSaveFileName))
			LoadMostRecent ();
		else
			EternityEngine.instance.FirstTimeInit ();
	}

	void OnAboutToSave ()
	{
		EternityEngine.instance.UpdateMostRecentInstrument (EternityEngine.instance.leftHand);
		EternityEngine.instance.UpdateMostRecentInstrument (EternityEngine.instance.rightHand);
		Asset[] assets = EternityEngine.instance.sceneTrs.GetComponentsInChildren<Asset>(true);
		GameManager.instance.assetsData.Clear();
		for (int i = 0; i < assets.Length; i ++)
		{
			Asset asset = assets[i];
			asset.SetData ();
			GameManager.instance.assetsData.Add(asset._Data);
		}
	}
	
	public void Save (string fileName)
	{
#if OCULUS_STORE
		if (!GameManager.ModifierIsActive("Can save"))
		{
			IAPManager.instance.saveCapability.StartCheckout ();
			return;
		}
#endif
		ActivationBehaviourOption.ActivateChildrenIfShould (Option.TriggerType.Save, false);
		OnAboutToSave ();
		saveData.assetsData = GameManager.instance.assetsData.ToArray();
		if (DRMManager.useSaveFileDRM)
		{
			if (string.IsNullOrEmpty(DRMManager.licenseId))
			{
				DRMManager.licenseId = StringExtensions.Random(DRMManager.LICENSE_ID_LENGTH, "qwertyuiopasdfghjklzxcvbnm1234567890`-= []\\;',./!@#$%^&*()~_+{}|:\"<>?QWERTYUIOPASDFGHJKLZXCVBNM");
				saveData.usersData = new string[] { DRMManager.GetUserData() };
			}
			else
			{
				List<string> usersData = new List<string>(saveData.usersData);
				string userData = DRMManager.GetUserData();
				if (!usersData.Contains(userData))
				{
					usersData.Add(userData);
					saveData.usersData = usersData.ToArray();
				}
			}
		}
		saveData.licenseId = DRMManager.licenseId;
		saveData.usersPerLicense = DRMManager.usersPerLicense;
		SpawnOrientationOption spawnOrientationOption = EternityEngine.instance.currentSpawnOrientationOption;
		if (Option.Exists(spawnOrientationOption))
			saveData.currentSpawnOrientationOptionName = spawnOrientationOption.name;
		saveData.currentSpawnBehaviourOptionsNames = new string[EternityEngine.instance.currentSpawnBehaviourOptionsDict.Count];
		for (int i = 0; i < EternityEngine.instance.currentSpawnBehaviourOptionsDict.Count; i ++)
			saveData.currentSpawnBehaviourOptionsNames[i] = EternityEngine.instance.currentSpawnBehaviourOptionsDict.values[i].name;
		saveData.modulationDistanceRange = EternityEngine.instance.modulationDistanceRange;
		saveData.modulationSliderLength = EternityEngine.instance.modulationSliderLength;
		saveData.modulationRotationAngleRange = EternityEngine.instance.modulationRotationAngleRange;
		saveData.modulationControlMethod = EternityEngine.instance.modulationControlMethod;
		saveData.modulationBehaviourWhenUncontrolled = EternityEngine.instance.modulationBehaviourWhenUncontrolled;
		saveData.modulationKeyframeDataTemplateWhenControlled = EternityEngine.instance.modulationKeyframeDataTemplateWhenControlled;
		saveData.potentialModulationKeyframeDataTemplateWhenUncontrolled = EternityEngine.instance.potentialModulationKeyframeDataTemplateWhenUncontrolled;
		saveData.leftHandActiveInstrumentIndex = (byte) EternityEngine.instance.leftHand.currentInstrumentIndex;
		saveData.rightHandActiveInstrumentIndex = (byte) EternityEngine.instance.rightHand.currentInstrumentIndex;
		saveData.leftHandInstruments = new Instrument[EternityEngine.leftHandInstruments.Length];
		for (int i = 0; i < EternityEngine.leftHandInstruments.Length; i ++)
		{
			Instrument instrument = EternityEngine.leftHandInstruments[i];
			saveData.leftHandInstruments[i] = instrument;
		}
		saveData.rightHandInstruments = new Instrument[EternityEngine.rightHandInstruments.Length];
		for (int i = 0; i < EternityEngine.rightHandInstruments.Length; i ++)
		{
			Instrument instrument = EternityEngine.rightHandInstruments[i];
			saveData.rightHandInstruments[i] = instrument;
		}
		// File.WriteAllText(fileName, Serialize(saveData, typeof(SaveData)));
		FileStream fileStream = new FileStream(fileName, FileMode.Create);
		BinaryFormatter binaryFormatter = new BinaryFormatter();
		binaryFormatter.Serialize(fileStream, saveData);
		fileStream.Close();
		MostRecentSaveFileName = fileName;
		ActivationBehaviourOption.ActivateChildrenIfShould (Option.TriggerType.Save, true);
	}
	
	public void Load (string fileName)
	{
		ActivationBehaviourOption.ActivateChildrenIfShould (Option.TriggerType.Load, false);
		// saveData = (SaveData) Deserialize(File.ReadAllText(fileName), typeof(SaveData));
		FileStream fileStream = new FileStream(fileName, FileMode.Open);
		BinaryFormatter binaryFormatter = new BinaryFormatter();
		saveData = (SaveData) binaryFormatter.Deserialize(fileStream);
		fileStream.Close();
		if (!string.IsNullOrEmpty(saveData.licenseId))
		{
			if (!saveData.usersData.Contains(DRMManager.GetUserData()))
			{
				DRMManager.licenseId = saveData.licenseId;
				DRMManager.usersPerLicense = saveData.usersPerLicense;
				DRMManager.StartLoginProcess ();
				StartCoroutine(ValidateDRRoutine (fileName));
			}
			else
				OnLoad (fileName);
		}
		else
			OnLoad (fileName);
	}

	void OnLoad (string fileName)
	{
		GameManager.instance.assetsData = new List<Asset.Data>(saveData.assetsData);
		if (saveData.futureParentingArrowsColorHasBeenSet)
		{
			// EternityEngine.instance.parentingArrowPrefab.lineRenderer.sharedMaterial.color = saveData.futureParentingArrowsColor.ToColor();
			Material material = new Material(EternityEngine.instance.parentingArrowPrefab.lineRenderer.sharedMaterial);
			material.color = saveData.futureParentingArrowsColor.ToColor();
			EternityEngine.instance.parentingArrowPrefab.lineRenderer.sharedMaterial = material;
		}
		Material skybox = saveData.skyboxMaterial.ToMat();
		if (skybox != null)
			RenderSettings.skybox = skybox;
		EternityEngine.instance.modulationDistanceRange = saveData.modulationDistanceRange;
		EternityEngine.instance.modulationSliderLength = saveData.modulationSliderLength;
		EternityEngine.instance.modulationRotationAngleRange = saveData.modulationRotationAngleRange;
		EternityEngine.instance.modulationControlMethod = saveData.modulationControlMethod;
		EternityEngine.instance.modulationBehaviourWhenUncontrolled = saveData.modulationBehaviourWhenUncontrolled;
		EternityEngine.instance.modulationKeyframeDataTemplateWhenControlled = saveData.modulationKeyframeDataTemplateWhenControlled;
		EternityEngine.instance.potentialModulationKeyframeDataTemplateWhenUncontrolled = saveData.potentialModulationKeyframeDataTemplateWhenUncontrolled;
		EternityEngine.instance.leftHand.currentInstrumentIndex = (int) saveData.leftHandActiveInstrumentIndex;
		EternityEngine.instance.rightHand.currentInstrumentIndex = (int) saveData.rightHandActiveInstrumentIndex;
		EternityEngine.instance.leftHandStraightInstrument = (StraightInstrument) saveData.leftHandInstruments[0];
		EternityEngine.instance.leftHandCurveInstrument = (CurveInstrument) saveData.leftHandInstruments[1];
		EternityEngine.instance.leftHandTrailInstrument = (TrailInstrument) saveData.leftHandInstruments[2];
		EternityEngine.instance.rightHandStraightInstrument = (StraightInstrument) saveData.rightHandInstruments[0];
		EternityEngine.instance.rightHandCurveInstrument = (CurveInstrument) saveData.rightHandInstruments[1];
		EternityEngine.instance.rightHandTrailInstrument = (TrailInstrument) saveData.rightHandInstruments[2];
		MostRecentSaveFileName = fileName;
		OnLoaded ();
		ActivationBehaviourOption.ActivateChildrenIfShould (Option.TriggerType.Load, true);
	}

	IEnumerator ValidateDRRoutine (string fileName)
	{
		yield return new WaitUntil(() => (DRMManager.isLoggedIn));
		OnLoad (fileName);
	}

	void OnLoaded ()
	{
		for (int i = 0; i < EternityEngine.instance.optionNamesDict.Count; i ++)
		{
			Option option = EternityEngine.instance.optionNamesDict.keys[i];
			if (option != null)
				DestroyImmediate(option.gameObject);
		}
		EternityEngine.instance.optionNamesDict.Clear();
		for (int i = 0; i < GameManager.instance.assetsData.Count; i ++)
		{
			Asset.Data assetData = GameManager.instance.assetsData[i];
			if (!EternityEngine.instance.optionNamesDict.ContainsValue(assetData.name))
				assetData.MakeAsset ();
		}
		EternityEngine.instance.currentSpawnOrientationOption = EternityEngine.GetOption<SpawnOrientationOption>(saveData.currentSpawnOrientationOptionName);
		for (int i = 0; i < saveData.currentSpawnBehaviourOptionsNames.Length; i ++)
		{
			string currentSpawnBehaviourOptionName = saveData.currentSpawnBehaviourOptionsNames[i];
			EternityEngine.instance.currentSpawnBehaviourOptionsDict.values[i] = EternityEngine.GetOption<SpawnBehaviourOption>(currentSpawnBehaviourOptionName);
		}
		EternityEngine.instance.leftHand.HandleUpdateInstrument ();
		EternityEngine.instance.rightHand.HandleUpdateInstrument ();
	}
	
	public void LoadMostRecent ()
	{
		Load (MostRecentSaveFileName);
	}

	// public static string Serialize (object value, Type type)
	// {
	// 	fsData data;
	// 	serializer.TrySerialize(type, value, out data).AssertSuccessWithoutWarnings();
	// 	return fsJsonPrinter.CompressedJson(data);
	// }
	
	// public static object Deserialize (string serializedState, Type type)
	// {
	// 	fsData data = fsJsonParser.Parse(serializedState);
	// 	object deserialized = null;
	// 	serializer.TryDeserialize(data, type, ref deserialized).AssertSuccessWithoutWarnings();
	// 	return deserialized;
	// }
	
	[Serializable]
	public struct SaveData
	{
		public Asset.Data[] assetsData;
		public string licenseId;
		public int usersPerLicense;
		public string[] usersData;
		public _Material skyboxMaterial;
		public string currentSpawnOrientationOptionName;
		public string[] currentSpawnBehaviourOptionsNames;
		public _Color futureParentingArrowsColor;
		public bool futureParentingArrowsColorHasBeenSet;
		public FloatRange modulationDistanceRange;
		public float modulationSliderLength;
		public FloatRange modulationRotationAngleRange;
		public Modulator.ControlMethod modulationControlMethod;
		public Modulator.BehaviourWhenUncontrolled modulationBehaviourWhenUncontrolled;
		public ModulationCurve.KeyframeData modulationKeyframeDataTemplateWhenControlled;
		public ModulationCurve.KeyframeData potentialModulationKeyframeDataTemplateWhenUncontrolled;
		public byte leftHandActiveInstrumentIndex;
		public byte rightHandActiveInstrumentIndex;
		public Instrument[] leftHandInstruments;
		public Instrument[] rightHandInstruments;
	}
}
