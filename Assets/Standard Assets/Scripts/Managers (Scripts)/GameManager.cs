﻿using TMPro;
using System;
using System.IO;
using UnityEngine;
// using System.Collections;
using UnityEngine.InputSystem;
using System.Collections.Generic;
using Object = UnityEngine.Object;
using UnityEngine.SceneManagement;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace _EternityEngine
{
	public class GameManager : SingletonMonoBehaviour<GameManager>
	{
		[SaveAndLoadValue]
		public List<Asset.Data> assetsData = new List<Asset.Data>();
		// public GameObject[] registeredGos = new GameObject[0];
		// [SaveAndLoadValue]
		// static string enabledGosString = "";
		// [SaveAndLoadValue]
		// static string disabledGosString = "";
		public TMP_Text notificationText;
		public SerializableDictionary<string, GameModifier> gameModifiersDict = new SerializableDictionary<string, GameModifier>();
		public static bool paused;
		public static IUpdatable[] updatables = new IUpdatable[0];
		public static int framesSinceLevelLoaded;
		public static bool isQuitting;
		public static float pausedTime;
		public static float TimeSinceLevelLoad
		{
			get
			{
				return Time.timeSinceLevelLoad - pausedTime;
			}
		}

		public override void Awake ()
		{
			base.Awake ();
			if (instance != this)
				return;
			CodeRunner.Init ();
			gameModifiersDict.Init ();
			Application.wantsToQuit += OnWantToQuit;
			SceneManager.sceneLoaded += OnSceneLoaded;
		}

		void OnDestroy ()
		{
			if (instance == this)
				SceneManager.sceneLoaded -= OnSceneLoaded;
		}
		
		void OnSceneLoaded (Scene scene = new Scene(), LoadSceneMode loadMode = LoadSceneMode.Single)
		{
			StopAllCoroutines();
			framesSinceLevelLoaded = 0;
			pausedTime = 0;
		}

		void Update ()
		{
			for (int i = 0; i < updatables.Length; i ++)
			{
				IUpdatable updatable = updatables[i];
				updatable.DoUpdate ();
			}
			CodeRunner.DoUpdate ();
			// if (Time.deltaTime > 0)
			// 	Physics.Simulate(Time.deltaTime);
			if (ObjectPool.Instance != null && ObjectPool.instance.enabled)
				ObjectPool.instance.DoUpdate ();
			InputSystem.Update ();
			framesSinceLevelLoaded ++;
			if (paused)
				pausedTime += Time.unscaledDeltaTime;
		}

		public static void Quit ()
		{
			isQuitting = true;
#if UNITY_EDITOR
			EditorApplication.isPlaying = false;
#endif
			Application.Quit();
		}

		bool OnWantToQuit ()
		{
			isQuitting = true;
			// PlayerPrefs.DeleteAll();
			SaveAndLoadManager.instance.Save (Application.persistentDataPath + Path.DirectorySeparatorChar + "Auto Save.txt");
			Application.wantsToQuit -= OnWantToQuit;
			return EternityEngine.OnWantToQuit();
		}

		public void ToggleGameObject (GameObject go)
		{
			go.SetActive(!go.activeSelf);
		}

		public void DestroyChildren (Transform trs)
		{
			for (int i = 0; i < trs.childCount; i ++)
				Destroy(trs.GetChild(i).gameObject);
		}

		public void DestroyChildrenImmediate (Transform trs)
		{
			for (int i = 0; i < trs.childCount; i ++)
				DestroyImmediate(trs.GetChild(i).gameObject);
		}
		
#if UNITY_EDITOR
		public static void DestroyOnNextEditorUpdate (Object obj)
		{
			EditorApplication.update += () => { if (obj == null) return; DestroyObject (obj); };
		}

		static void DestroyObject (Object obj)
		{
			if (obj == null)
				return;
			EditorApplication.update -= () => { DestroyObject (obj); };
			DestroyImmediate(obj);
		}
#endif
		
		public static bool ModifierExistsAndIsActive (string name)
		{
			GameModifier gameModifier;
			if (instance.gameModifiersDict.TryGetValue(name, out gameModifier))
				return gameModifier.isActive;
			else
				return false;
		}

		public static bool ModifierIsActive (string name)
		{
			return instance.gameModifiersDict[name].isActive;
		}

		public static bool ModifierExists (string name)
		{
			return instance.gameModifiersDict.ContainsKey(name);
		}

		public static void SetModifierActive (string name, bool isActive)
		{
			GameModifier gameModifier = instance.gameModifiersDict[name];
			gameModifier.isActive = isActive;
			instance.gameModifiersDict[name] = gameModifier;
		}

		[Serializable]
		public struct GameModifier
		{
			public string name;
			public bool isActive;
		}
	}
}