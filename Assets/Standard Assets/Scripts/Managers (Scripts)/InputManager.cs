﻿using System;
using Extensions;
using UnityEngine;
using System.Collections;
using UnityEngine.InputSystem;
using System.Collections.Generic;
using UnityEngine.InputSystem.XR;

namespace _EternityEngine
{
	public class InputManager : SingletonMonoBehaviour<InputManager>
	{
		public InputDevice inputDevice;
		public InputSettings settings;
		public InputActionAsset inputActionAsset;
		public static Vector2? LeftThumbstick
		{
			get
			{
				return Vector2.ClampMagnitude(leftHandThumbstick.ReadValue<Vector2>(), 1);
			}
		}
		public static Vector2? RightThumbstick
		{
			get
			{
				return Vector2.ClampMagnitude(rightHandThumbstick.ReadValue<Vector2>(), 1);
			}
		}
		public static bool LeftGripInput
		{
			get
			{
				return leftHandGrip.ReadValue<float>() > Instance.settings.defaultDeadzoneMin;
			}
		}
		public static bool RightGripInput
		{
			get
			{
				return rightHandGrip.ReadValue<float>() > Instance.settings.defaultDeadzoneMin;
			}
		}
		public static bool LeftTriggerInput
		{
			get
			{
				return leftHandTrigger.ReadValue<float>() > Instance.settings.defaultDeadzoneMin;
			}
		}
		public static bool RightTriggerInput
		{
			get
			{
				return rightHandTrigger.ReadValue<float>() > Instance.settings.defaultDeadzoneMin;
			}
		}
		public static bool LeftPrimaryButtonInput
		{
			get
			{
				return leftHandPrimaryButton.ReadValue<float>() > Instance.settings.defaultDeadzoneMin;
			}
		}
		public static bool RightPrimaryButtonInput
		{
			get
			{
				return rightHandPrimaryButton.ReadValue<float>() > Instance.settings.defaultDeadzoneMin;
			}
		}
		public static bool LeftSecondaryButtonInput
		{
			get
			{
				return leftHandSecondaryButton.ReadValue<float>() > Instance.settings.defaultDeadzoneMin;
			}
		}
		public static bool RightSecondaryButtonInput
		{
			get
			{
				return rightHandSecondaryButton.ReadValue<float>() > Instance.settings.defaultDeadzoneMin;
			}
		}
		public static bool LeftThumbstickClickedInput
		{
			get
			{
				return leftHandThumbstickClicked.ReadValue<float>() > Instance.settings.defaultDeadzoneMin;
			}
		}
		public static bool RightThumbstickClickedInput
		{
			get
			{
				return rightHandThumbstickClicked.ReadValue<float>() > Instance.settings.defaultDeadzoneMin;
			}
		}
		public static Vector3? HeadPosition
		{
			get
			{
				if (Hmd != null)
					return Hmd.devicePosition.ReadValue();
				else
					return null;
			}
		}
		public static Quaternion? HeadRotation
		{
			get
			{
				if (Hmd != null)
					return Hmd.deviceRotation.ReadValue();
				else
					return null;
			}
		}
		public static Vector3? LeftHandPosition
		{
			get
			{
				return leftHandPosition.ReadValue<Vector3>();
			}
		}
		public static Quaternion? LeftHandRotation
		{
			get
			{
				return leftHandRotation.ReadValue<Quaternion>();
			}
		}
		public static Vector3? RightHandPosition
		{
			get
			{
				return rightHandPosition.ReadValue<Vector3>();
			}
		}
		public static Quaternion? RightHandRotation
		{
			get
			{
				return rightHandRotation.ReadValue<Quaternion>();
			}
		}
		public static XRHMD Hmd
		{
			get
			{
				return InputSystem.GetDevice<XRHMD>();
			}
		}
		static InputAction leftHandPosition;
		static InputAction rightHandPosition;
		static InputAction leftHandRotation;
		static InputAction rightHandRotation;
		static InputAction leftHandTrigger;
		static InputAction rightHandTrigger;
		static InputAction leftHandGrip;
		static InputAction rightHandGrip;
		static InputAction leftHandPrimaryButton;
		static InputAction rightHandPrimaryButton;
		static InputAction leftHandSecondaryButton;
		static InputAction rightHandSecondaryButton;
		static InputAction leftHandThumbstickClicked;
		static InputAction rightHandThumbstickClicked;
		static InputAction leftHandThumbstick;
		static InputAction rightHandThumbstick;

		public override void Awake ()
		{
			base.Awake ();
			leftHandPosition = inputActionAsset.FindAction("Left Hand Position");
			leftHandPosition.Enable();
			rightHandPosition = inputActionAsset.FindAction("Right Hand Position");
			rightHandPosition.Enable();
			leftHandRotation = inputActionAsset.FindAction("Left Hand Rotation");
			leftHandRotation.Enable();
			rightHandRotation = inputActionAsset.FindAction("Right Hand Rotation");
			rightHandRotation.Enable();
			leftHandTrigger = inputActionAsset.FindAction("Left Hand Trigger");
			leftHandTrigger.Enable();
			rightHandTrigger = inputActionAsset.FindAction("Right Hand Trigger");
			rightHandTrigger.Enable();
			leftHandGrip = inputActionAsset.FindAction("Left Hand Grip");
			leftHandGrip.Enable();
			rightHandGrip = inputActionAsset.FindAction("Right Hand Grip");
			rightHandGrip.Enable();
			leftHandPrimaryButton = inputActionAsset.FindAction("Left Hand Primary Button");
			leftHandPrimaryButton.Enable();
			rightHandPrimaryButton = inputActionAsset.FindAction("Right Hand Primary Button");
			rightHandPrimaryButton.Enable();
			leftHandSecondaryButton = inputActionAsset.FindAction("Left Hand Secondary Button");
			leftHandSecondaryButton.Enable();
			rightHandSecondaryButton = inputActionAsset.FindAction("Right Hand Secondary Button");
			rightHandSecondaryButton.Enable();
			leftHandThumbstickClicked = inputActionAsset.FindAction("Left Hand Thumbstick Clicked");
			leftHandThumbstickClicked.Enable();
			rightHandThumbstickClicked = inputActionAsset.FindAction("Right Hand Thumbstick Clicked");
			rightHandThumbstickClicked.Enable();
			leftHandThumbstick = inputActionAsset.FindAction("Left Hand Thumbstick");
			leftHandThumbstick.Enable();
			rightHandThumbstick = inputActionAsset.FindAction("Right Hand Thumbstick");
			rightHandThumbstick.Enable();
		}
		
		public enum InputDevice
		{
			KeyboardAndMouse,
			VR
		}
	}
}