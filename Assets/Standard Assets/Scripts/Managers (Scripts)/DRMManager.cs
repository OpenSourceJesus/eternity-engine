using System;
using System.IO;
using Extensions;
using UnityEngine;
using PlayerIOClient;
using System.Collections;
using System.Globalization;
using System.Collections.Generic;
#if OCULUS_STORE
using Message = Oculus.Platform.Message;
#endif
using Application = UnityEngine.Application;

namespace _EternityEngine
{
	public class DRMManager : SingletonMonoBehaviour<DRMManager>
	{
		public const int LICENSE_ID_LENGTH = 20;
		public static int usersPerLicense;
		public static bool useSaveFileDRM;
		public static string licenseId;
		public static bool isLoggedIn;
		static Client client;

		void Start ()
		{
#if UNITY_EDITOR
			_SceneManager.instance.LoadSceneWithoutTransition ("Game");
			return;
#endif
#if OCULUS_STORE
			Core.Initialize();
			Entitlements.IsUserEntitledToApplication().OnComplete(OnDRCheckComplete);
#else
			_SceneManager.instance.LoadSceneWithoutTransition ("Game");
#endif
		}

#if OCULUS_STORE
		void OnDRCheckComplete (Message msg)
		{
			if (!msg.IsError)
			{
				print("Entitlement check passed");
				_SceneManager.instance.LoadSceneWithoutTransition ("Game");
			}
			else
			{
				print("Error: " + msg.GetError().Message);
				Application.Quit();
			}
		}
#endif

		public static void StartLoginProcess ()
		{
			PlayerIO.UseSecureApiRequests = true;
			PlayerIO.Authenticate("eternity-engine-b72xdygce6ae7mrupk1ea", // The game id
				"public", // The connection id
				new Dictionary<string, string> { // Authentication arguments
					{ "licenseId", licenseId },
				},
				null, // PlayerInsight segments
				OnAuthenticateSucess,
				OnAuthenticateFail
			);
		}

		static void OnAuthenticateSucess (Client client)
		{
			print("OnAuthenticateSucess");
			DRMManager.client = client;
			DRMManager.client.BigDB.Load("PlayerObjects", "DRM Manager", OnDBObjectLoadSuccess, OnDBObjectLoadFail);
		}

		static void OnAuthenticateFail (PlayerIOError error)
		{
			print("OnAuthenticateError: " + error);
			Application.Quit();
		}

		static void OnDBObjectLoadSuccess (DatabaseObject dbObject)
		{
			print("OnDBObjectLoadSuccess");
			DatabaseArray licenses = dbObject.GetArray("licenses");
			DatabaseObject license;
			DatabaseArray usersData;
		    string userData = SystemInfo.deviceUniqueIdentifier;
			if (userData == SystemInfo.unsupportedIdentifier)
				userData = Environment.UserDomainName + ", " + Environment.UserName;
			print(userData);
			for (int i = 0; i < licenses.Count; i ++)
			{
				license = licenses.GetObject(i);
				if (license.GetString("id") == licenseId)
				{
					usersData = license.GetArray("usersData");
					if (!Contains(usersData, userData))
					{
						if (usersData.Count >= usersPerLicense)
						{
							Application.Quit();
							return;
						}
						usersData.Add(userData);
						dbObject.Save(true, false, OnDBObjectSaveSuccess, OnDBObjectSaveFail);
						return;
					}
					isLoggedIn = true;
					return;
				}
			}
			license = new DatabaseObject();
			license.Set("id", licenseId);
			usersData = new DatabaseArray();
			usersData.Add(userData);
			license.Set("usersData", usersData);
			licenses.Add(license);
			dbObject.Save(true, false, OnDBObjectSaveSuccess, OnDBObjectSaveFail);
		}

		static void OnDBObjectLoadFail (PlayerIOError error)
		{
			print("OnDBObjectLoadError: " + error);
			Application.Quit();
		}

		static void OnDBObjectSaveSuccess ()
		{
			print("OnDBSaveSuccess");
			isLoggedIn = true;
		}

		static void OnDBObjectSaveFail (PlayerIOError error)
		{
			print("OnDBSaveFail: " + error);
			StartLoginProcess ();
		}

		static bool Contains (DatabaseArray dbArray, string str)
		{
			for (int i = 0; i < dbArray.Count; i ++)
			{
				if (dbArray.GetString(i) == str)
					return true;
			}
			return false;
		}

		public static string GetUserData ()
		{
		    string userData = SystemInfo.deviceUniqueIdentifier;
			if (userData == SystemInfo.unsupportedIdentifier)
				userData = Environment.UserDomainName + ", " + Environment.UserName;
			return userData;
		}
	}
}