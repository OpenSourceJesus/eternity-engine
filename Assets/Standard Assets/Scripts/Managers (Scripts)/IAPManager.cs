#if OCULUS_STORE
using System;
using UnityEngine;
using _EternityEngine;
using Oculus.Platform;
using Oculus.Platform.Models;
using System.Collections.Generic;

public class IAPManager : SingletonMonoBehaviour<IAPManager>
{
	public List<Purchase> purchases = new List<Purchase>();
	public SaveCapability saveCapability;

	void Start ()
	{
		purchases.Add(saveCapability);
		Core.Initialize();
		GetPurchaseList ();
		GetPastPurchases ();
	}

	void GetPurchaseList ()
	{
		string[] skus = new string[purchases.Count];
		for (int i = 0; i < purchases.Count; i ++)
		{
			Purchase purchase = purchases[i];
			skus[i] = purchase.sku;
		}
		IAP.GetProductsBySKU(skus).OnComplete(OnGetPurchaseListComplete);
	}

	void OnGetPurchaseListComplete (Message<ProductList> message)
	{
		if (message.IsError)
		{
			print("Error: " + message.GetError().Message);
			return;
		}
		foreach (Product purchase in message.GetProductList())
			print(string.Format("Product: sku:{0} name:{1} price:{2}", purchase.Sku, purchase.Name, purchase.FormattedPrice));
	}

	void GetPastPurchases ()
	{
		IAP.GetViewerPurchases().OnComplete(OnGetPastPurchasesComplete);
	}

	void OnGetPastPurchasesComplete (Message<PurchaseList> message)
	{
		if (message.IsError)
		{
			print("Error: " + message.GetError().Message);
			return;
		}
		List<Purchase> _purchases = new List<Purchase>();
		foreach (Oculus.Platform.Models.Purchase purchase in message.GetPurchaseList())
		{
			print(string.Format("Purchased: sku:{0} granttime:{1} id:{2}", purchase.Sku, purchase.GrantTime, purchase.ID));
			for (int i = 0; i < _purchases.Count; i ++)
			{
				Purchase _purchase = _purchases[i];
				if (_purchase.sku == purchase.Sku)
				{
					_purchase.ApplyEffect ();
					_purchases.RemoveAt(i);
					break;
				}
			}
		}
	}
}
#else
public class IAPManager : SingletonMonoBehaviour<IAPManager>
{
}
#endif