using Extensions;
using UnityEngine;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class RecordStartOfCollisions : MonoBehaviour
	{
		public Option parentOption;

		void OnCollisionEnter (Collision coll)
		{
			CollisionOption collisionOption = EternityEngine.instance.SpawnOption<CollisionOption> (EternityEngine.instance.collisionOptionPrefab, (CollisionOption collisionOption) => { collisionOption.Init (coll); });
			if (Option.Exists(parentOption))
			{
				collisionOption.trs.SetParent(parentOption.childOptionsParent);
				parentOption.AddChild(collisionOption);
			}
		}
	}
}