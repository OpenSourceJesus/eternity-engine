﻿using System;
using Extensions;
using UnityEngine;
using System.Collections;
using UnityEngine.Rendering;
using UnityEngine.InputSystem;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class VRCameraRig : SingletonUpdateWhileEnabled<VRCameraRig>
	{
		public new static VRCameraRig instance;
		public new static VRCameraRig Instance
		{
			get
			{
				if (instance == null)
					instance = FindObjectOfType<VRCameraRig>();
				return instance;
			}
		}
		public Camera camera;
		public Transform trackingSpaceTrs;
		public Transform eyesTrs;
		public Transform leftHandTrs;
		public Transform rightHandTrs;
		public Transform bothHandsAverageTrs;
		public Transform trs;
		public float rotateHands;
		public float lookRate;
		
		public override void OnEnable ()
		{
			instance = this;
			Init ();
			base.OnEnable ();
			RenderPipelineManager.beginFrameRendering += OnBeginFrameRendering;
			RenderPipelineManager.endFrameRendering += OnEndFrameRendering;
		}

		public override void OnDisable ()
		{
			base.OnDisable ();
			RenderPipelineManager.beginFrameRendering -= OnBeginFrameRendering;
			RenderPipelineManager.endFrameRendering -= OnEndFrameRendering;
		}

		void OnBeginFrameRendering (ScriptableRenderContext context, Camera[] cameras)
		{
			ActivationBehaviourOption.ActivateChildrenIfShould (Option.TriggerType.Vision, false);
		}

		void OnEndFrameRendering (ScriptableRenderContext context, Camera[] cameras)
		{
			ActivationBehaviourOption.ActivateChildrenIfShould (Option.TriggerType.Vision, true);
		}

		public void Init ()
		{
			trs.SetParent(null);
		}

		public override void DoUpdate ()
		{
			if (InputManager.Instance.inputDevice == InputManager.InputDevice.VR)
				UpdateTransforms ();
			else
			{
				Vector2 rotaInput = Mouse.current.delta.ReadValue().FlipY() * lookRate * Time.unscaledDeltaTime;
				if (rotaInput != Vector2.zero)
				{
					trackingSpaceTrs.RotateAround(trackingSpaceTrs.position, trackingSpaceTrs.right, rotaInput.y);
					trackingSpaceTrs.RotateAround(trackingSpaceTrs.position, Vector3.up, rotaInput.x);
				}
			}
		}

		void UpdateTransforms ()
		{
			if (InputManager.HeadPosition != null)
				eyesTrs.localPosition = (Vector3) InputManager.HeadPosition;
			if (InputManager.HeadRotation != null)
				eyesTrs.localRotation = (Quaternion) InputManager.HeadRotation;
			if (InputManager.LeftHandPosition != null)
				leftHandTrs.localPosition = (Vector3) InputManager.LeftHandPosition;
			if (InputManager.LeftHandRotation != null)
			{
				leftHandTrs.localRotation = (Quaternion) InputManager.LeftHandRotation;
				leftHandTrs.Rotate(Vector3.right * rotateHands);
			}
			if (InputManager.RightHandPosition != null)
				rightHandTrs.localPosition = (Vector3) InputManager.RightHandPosition;
			if (InputManager.RightHandRotation != null)
			{
				rightHandTrs.localRotation = (Quaternion) InputManager.RightHandRotation;
				rightHandTrs.Rotate(Vector3.right * rotateHands);
			}
			bothHandsAverageTrs.position = (leftHandTrs.position + rightHandTrs.position) / 2;
			bothHandsAverageTrs.rotation = Quaternion.Slerp(leftHandTrs.rotation, rightHandTrs.rotation, 0.5f);
			bothHandsAverageTrs.SetWorldScale (Vector3.one * Vector3.Distance(leftHandTrs.position, rightHandTrs.position));
		}
	}
}