using System;
using UnityEngine;

namespace _EternityEngine
{
	public class ModulationCurveTweaker : SingletonUpdateWhileEnabled<ModulationCurveTweaker>
	{
		public override void DoUpdate ()
		{
			if (EternityEngine.instance.useSeparateTweakingDisplayersForModulationCurvesBoolOption.value)
			{
				for (int i = 0; i < ModulationOption.displayingModulationOptionCurves.Count; i ++)
				{
					ModulationOption modulationOption = ModulationOption.displayingModulationOptionCurves[i];
					TweakModulationOption (modulationOption);
				}
			}
			else
			{
				ModulationOption modulationOption = ModulationOption.displayingModulationOptionCurves[0];
				TweakModulationOption (modulationOption);
			}
		}

		void TweakModulationOption (ModulationOption modulationOption)
		{
			
		}
	}
}