using UnityEngine;
using _EternityEngine;

public class TestFramerate : SingletonUpdateWhileEnabled<TestFramerate>
{
	float totalTime;
	int frameCount;
	const int LAGGY_FRAME_COUNT_ON_LOAD_SCENE = 2;

	public override void DoUpdate ()
	{
		if (Time.frameCount <= LAGGY_FRAME_COUNT_ON_LOAD_SCENE)
			return;
		float deltaTime = Time.unscaledDeltaTime;
		totalTime += deltaTime;
		frameCount ++;
		print("Delta time: " + deltaTime + " Average delta time: " + totalTime / frameCount);
	}
}