using System;
using System.IO;
using Extensions;
using TriLibCore;
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Text.RegularExpressions;
#if UNITY_EDITOR
using UnityEditor;
using UnityEngine.InputSystem;
#endif

namespace _EternityEngine
{
	[ExecuteInEditMode]
	public class EternityEngine : SingletonUpdateWhileEnabled<EternityEngine>
	{
		public SpawnOrientationOption currentSpawnOrientationOption;
		public SerializableDictionary<int, SpawnBehaviourOption> currentSpawnBehaviourOptionsDict = new SerializableDictionary<int, SpawnBehaviourOption>();
		public DeltaTimeOption deltaTimeOption;
		public RandomValueOption randomValueOption;
		public ImportModelWithFileNameOption importModelWithFileNameOption;
		public ImportAudioWithFileNameOption importAudioWithFileNameOption;
		public ActivationBehaviourOption activationBehaviourOptionPrefab;
		public Option optionPrefab;
		public TypingTargetOption typingTargetOptionPrefab;
		public InstrumentOption instrumentOptionPrefab;
		public FileOption fileOptionPrefab;
		public FileInstanceOption fileInstanceOptionPrefab;
		public SoundFileOption soundFileOptionPrefab;
		public SoundFileInstanceOption soundFileInstanceOptionPrefab;
		public ModulationOption modulationOptionPrefab;
		public ModulationTargetValueRangeOption modulationTargetValueRangeOptionPrefab;
		public BoolOption boolOptionPrefab;
		public EnumOption enumOptionPrefab;
		public CodeCommandOption codeCommandOptionPrefab;
		public CodeCommandRunOption codeCommandRunOptionPrefab;
		public AutomationOption automationOptionPrefab;
		public AutomationSequenceEntryOption automationSequenceEntryOptionPrefab;
		public ConditionalOption conditionalOptionPrefab;
		public DeltaTimeOption deltaTimeOptionPrefab;
		public RandomValueOption randomValueOptionPrefab;
		public TransformOption transformOptionPrefab;
		public ModelFileOption modelFileOptionPrefab;
		public ModelFileInstanceOption modelFileInstanceOptionPrefab;
		public SpawnOrientationOption spawnOrientationOptionPrefab;
		public SetterOption setterOptionPrefab;
		public Vector3Option vector3OptionPrefab;
		public OneInputOperationsOption oneInputOperationsOptionPrefab;
		public TwoInputsOperationsOption twoInputsOperationsOptionPrefab;
		public ColorOption colorOptionPrefab;
		public ToneOption toneOptionPrefab;
		public SetTextColorOption setTextColorOptionPrefab;
		public DuplicateChildrenOption duplicateChildrenOptionPrefab;
		public InputBoolOption inputBoolOptionPrefab;
		public ImageFileOption imageFileOptionPrefab;
		public ImageFileInstanceOption imageFileInstanceOptionPrefab;
		public ImportImageWithFileNameOption importImageWithFileNameOptionPrefab;
		public ImageFilePathOption imageFilePathOptionPrefab;
		public CollisionOption collisionOptionPrefab;
		public ContactPointOption contactPointOptionPrefab;
		public GetChildrenTransformValuesOption getChildrenTransformValuesOptionPrefab;
		public InputTransformOption inputTransformOptionPrefab;
		public InputTransformTypeOption inputTransformTypeOptionPrefab;
		public ToggleChildrenOption toggleChildrenOptionPrefab;
		public ToggleSelfAndChildrenOption toggleSelfAndChildrenOptionPrefab;
		public DeleteChildrenOption deleteChildrenOptionPrefab;
		public DeleteSelfAndChildrenOption deleteSelfAndChildrenOptionPrefab;
		public ShowChildrenOption showChildrenOptionPrefab;
		public ShowSelfAndChildrenOption showSelfAndChildrenOptionPrefab;
		public HideChildrenOption hideChildrenOptionPrefab;
		public HideSelfAndChildrenOption hideSelfAndChildrenOptionPrefab;
		public ToggleChildrenCollidableOption toggleChildrenCollidableOptionPrefab;
		public SetChildrenCollidableOption setChildrenCollidableOptionPrefab;
		public SetChildrenUncollidableOption setChildrenUncollidableOptionPrefab;
		public QuitOption quitOptionPrefab;
		public TypingOption typingOptionPrefab;
		public BackspaceOption backspaceOptionPrefab;
		public MinBounceSpeedOption minBounceSpeedOptionPrefab;
		public FutureParentingArrowsColorOption futureParentingArrowsColorOptionPrefab;
		public RedOption redOptionPrefab;
		public GreenOption greenOptionPrefab;
		public BlueOption blueOptionPrefab;
		public AlphaOption alphaOptionPrefab;
		public SetVector3XOption setVector3XOptionPrefab;
		public SetVector3YOption setVector3YOptionPrefab;
		public SetVector3ZOption setVector3ZOptionPrefab;
		public GetVector3XOption getVector3XOptionPrefab;
		public GetVector3YOption getVector3YOptionPrefab;
		public GetVector3ZOption getVector3ZOptionPrefab;
		public TransformPositionOption transformPositionOptionPrefab;
		public TransformRotationOption transformRotationOptionPrefab;
		public TransformSizeOption transformSizeOptionPrefab;
		public UseDRMOption useDRMOptionPrefab;
		public EnumValueOption enumValueOptionPrefab;
		public EnumFlagOption enumFlagOptionPrefab;
		public InputBoolTypeOption inputBoolTypeOptionPrefab;
		public InputBoolUseLeftHandOption inputBoolUseLeftHandOptionPrefab;
		public PhysicsModeOption physicsModeOptionPrefab;
		public RecordStartOfCollisionsOption recordStartOfCollisionsOptionPrefab;
		public RecordContinuedCollisionsOption recordContinuedCollisionsOptionPrefab;
		public RecordEndOfCollisionsOption recordEndOfCollisionsOptionPrefab;
		public PhysicsObjectVisibleOption physicsObjectVisibleOptionPrefab;
		public PhysicsObjectVelocityOption physicsObjectVelocityOptionPrefab;
		public MassOption massOptionPrefab;
		public StaticFrictionOption staticFrictionOptionPrefab;
		public DynamicFrictionOption dynamicFrictionOptionPrefab;
		public BouncinessOption bouncinessOptionPrefab;
		public DragOption dragOptionPrefab;
		public AngularDragOption angularDragOptionPrefab;
		public SetChildrenMeshesOption setChildrenMeshesOptionPrefab;
		public MakeFileInstanceOption makeFileInstanceOptionPrefab;
		public DeleteFileInstancesOption deleteFileInstancesOptionPrefab;
		public DeleteSystemComponentOption deleteSystemComponentOptionPrefab;
		public RenameSystemComponentOption renameSystemComponentOptionPrefab;
		public ActivationBehaviourTriggerTypeOption activationBehaviourTriggerTypeOptionPrefab;
		public ActivateBeforeEventOption activateBeforeEventOptionPrefab;
		public ActivateAfterEventOption activateAfterEventOptionPrefab;
		public OneInputOperationTypeOption oneInputOperationTypeOptionPrefab;
		public OneInputOperationsTooManyChildrenBehaviourOption oneInputOperationsTooManyChildrenBehaviourOptionPrefab;
		public TwoInputsOperationTypeOption twoInputsOperationTypeOptionPrefab;
		public TwoInputsOperationsTooManyChildrenBehaviourOption twoInputsOperationsTooManyChildrenBehaviourOptionPrefab;
		public AutomationTimeMultiplierOption automationTimeMultiplierOptionPrefab;
		public PauseAutomationRecordingOption pauseAutomationRecordingOptionPrefab;
		public PauseAutomationPlaybackOption pauseAutomationPlaybackOptionPrefab;
		public AutomationReferenceNewOptionsOption automationReferenceNewOptionsOptionPrefab;
		public AutomationRecordLeftHandOption automationRecordLeftHandOptionPrefab;
		public InvalidOptionFixBehaviourOption invalidOptionFixBehaviourOptionPrefab;
		public InvalidOptionContinueSequenceBehaviourOption invalidOptionContinueSequenceBehaviourOptionPrefab;
		public DuplicateSequenceEntryIndexBehaviourOption duplicateSequenceEntryIndexBehaviourOptionPrefab;
		public AutomationSequenceEntryTimeOption automationSequenceEntryTimeOptionPrefab;
		public AutomationSequenceEntryIndexOption automationSequenceEntryIndexOptionPrefab;
		public CodeCommandContentsOption codeCommandContentsOptionPrefab;
		public CodeCommandRunRepeatedlyOption codeCommandRunRepeatedlyOptionPrefab;
		public ConditionalTooManyChildrenBehaviourOption conditionalTooManyChildrenBehaviourOptionPrefab;
		public RenameOption renameOptionPrefab;
		public NewNameOption newNameOptionPrefab;
		public InstrumentRadiusOption instrumentRadiusOptionPrefab;
		public InstrumentMaxLengthOption instrumentMaxLengthOptionPrefab;
		public NewProjectOption newProjectOptionPrefab;
		public LoadProjectOption loadProjectOptionPrefab;
		public LoadProjectPathOption loadProjectPathOptionPrefab;
		public SaveProjectOption saveProjectOptionPrefab;
		public SaveProjectPathOption saveProjectPathOptionPrefab;
		public MaxUsersOption maxUsersOptionPrefab;
		public StartAutomationPlaybackOption startAutomationPlaybackOptionPrefab;
		public EndAutomationPlaybackOption endAutomationPlaybackOptionPrefab;
		public AutomationRecordRightHandOption automationRecordRightHandOptionPrefab;
		public AutomationRecordInvalidHandOption automationRecordInvalidHandOptionPrefab;
		public StartAutomationRecordingOption startAutomationRecordingOptionPrefab;
		public EndAutomationRecordingOption endAutomationRecordingOptionPrefab;
		public ImportModelWithFileNameOption importModelWithFileNameOptionPrefab;
		public ModelFilePathOption modelFilePathOptionPrefab;
		public ImportAudioWithFileNameOption importAudioWithFileNameOptionPrefab;
		public AudioFilePathOption audioFilePathOptionPrefab;
		public DuplicateChildrenKeepParentingArrowsBehaviourOption duplicateChildrenKeepParentingArrowsBehaviourOptionPrefab;
		public SetterInvalidChildBehaviourOption setterInvalidChildBehaviourOptionPrefab;
		public SetterTooManyChildrenBehaviourOption setterTooManyChildrenBehaviourOptionPrefab;
		public ConditionalComparisonTypeOption conditionalComparisonTypeOptionPrefab;
		public StartAudioRecordingOption startAudioRecordingOptionPrefab;
		public PhysicsObjectAngularVelocityOption physicsObjectAngularVelocityOptionPrefab;
		public SetMeshColliderOption setMeshColliderOptionPrefab;
		public ConstrainTypeOption constrainTypeOptionPrefab;
		public FrictionCombineModeOption frictionCombineModeOptionPrefab;
		public BouncinessCombineModeOption bouncinessCombineModeOptionPrefab;
		public ShaderNameOption shaderNameOptionPrefab;
		public SkyboxOption skyboxOptionPrefab;
		public MaterialFolderPathOption materialFolderPathOptionPrefab;
		public SoundPitchOption soundPitchOptionPrefab;
		public SoundVolumeOption soundVolumeOptionPrefab;
		public SoundPanOption soundPanOptionPrefab;
		public MaxDepenetrationSpeedOption maxDepenetrationSpeedOptionPrefab;
		public ContactOffsetOption contactOffsetOptionPrefab;
		public SpawnOrientationDeleteCurrentBehaviourOption spawnOrientationDeleteCurrentBehaviourOptionPrefab;
		public SpawnOrientationMakeCurrentOption spawnOrientationMakeCurrentOptionPrefab;
		public InstrumentGraphicsAreVisibleOption instrumentGraphicsAreVisibleOptionPrefab;
		public SpawnBehaviourOption spawnBehaviourOptionPrefab;
		public SpawnBehaviourMakeCurrentOption spawnBehaviourMakeCurrentOptionPrefab;
		public SpawnBehaviourDeleteCurrentBehaviourOption spawnBehaviourDeleteCurrentBehaviourOptionPrefab;
		public FolderOption folderOptionPrefab;
		public SystemComponentOption systemComponentOptionPrefab;
		public ArbitraryFloatOption arbitraryFloatOptionPrefab;
		public ArbitraryIntOption arbitraryIntOptionPrefab;
		public ToggleParentingArrowsOption toggleParentingArrowsOptionPrefab;
		public MakeParentingArrowsOption makeParentingArrowsOptionPrefab;
		public DeleteParentingArrowsOption deleteParentingArrowsOptionPrefab;
		public ImportFolderOption importFolderOptionPrefab;
		public ImportFolderPathOption importFolderPathOptionPrefab;
		public SetTextSizeOption setTextSizeOptionPrefab;
		public TransformMeshVerticesOption transformMeshVerticesOptionPrefab;
		public DeleteMeshVerticesOption deleteMeshVerticesOptionPrefab;
		public DeleteMeshFacesOption deleteMeshFacesOptionPrefab;
		public DuplicateAndTransformMeshFacesOption duplicateAndTransformMeshFacesOptionPrefab;
		public StartEmittingSoundOption startEmittingSoundOptionPrefab;
		public StopEmittingSoundOption stopEmittingSoundOptionPrefab;
		public SoundMinDistanceOption soundMinDistanceOptionPrefab;
		public SoundMaxDistanceOption soundMaxDistanceOptionPrefab;
		public LightOption lightOptionPrefab;
		public LightTypeOption lightTypeOptionPrefab;
		public LightColorOption lightColorOptionPrefab;
		public LightIntensityOption lightIntensityOptionPrefab;
		public LightRangeOption lightRangeOptionPrefab;
		public LightRenderModeOption lightRenderModeOptionPrefab;
		public LightInnerSpotAngleOption lightInnerSpotAngleOptionPrefab;
		public LightOuterSpotAngleOption lightOuterSpotAngleOptionPrefab;
		public DivOption divOptionPrefab;
		public URLOption urlOptionPrefab;
		public DivIdOption divIdOptionPrefab;
		public DivClassNameOption divClassNameOptionPrefab;
		public SpritePivotOption spritePivotOptionPrefab;
		public DownloadTextOption downloadTextOptionPrefab;
		public SetMeshVertexColorsOption setMeshVertexColorsOptionPrefab;
		public InputVector3Option inputVector3OptionPrefab;
public VertexDataOption vertexDataOptionPrefab;
public VertexDataColorOption vertexDataColorOptionPrefab;
public VertexDataPositionOption vertexDataPositionOptionPrefab;
public VertexDataNormalOption vertexDataNormalOptionPrefab;
public VertexDataTangentOption vertexDataTangentOptionPrefab;
public CollisionLayerOption collisionLayerOptionPrefab;
public GetAndSetChildrenMeshVertexDataInSphereOption getAndSetChildrenMeshVertexDataInSphereOptionPrefab;
// Insert new Option prefabs before this line
		// Insert new Option prefabs with children after this line
		public ModelFileOption modelFileOptionWithChildrenPrefab;
		public ModelFileInstanceOption modelFileInstanceOptionWithChildrenPrefab;
		public SoundFileOption soundFileOptionWithChildrenPrefab;
		public SoundFileInstanceOption soundFileInstanceOptionWithChildrenPrefab;
		public ImageFileOption imageFileOptionWithChildrenPrefab;
		public ImageFileInstanceOption imageFileInstanceOptionWithChildrenPrefab;
		public VertexDataOption vertexDataOptionWithChildrenPrefab;
		public CodeCommandOption codeCommandOptionWithChildrenPrefab;
		public Transform actOnMeshVerticesFromTrs;
		public Transform actOnMeshVerticesToTrs;
		public AudioSource audioSource;
		public int recordingLength;
		public int recordingFrequency;
		public StraightInstrument leftHandStraightInstrument;
		public CurveInstrument leftHandCurveInstrument;
		public TrailInstrument leftHandTrailInstrument;
		public StraightInstrument rightHandStraightInstrument;
		public CurveInstrument rightHandCurveInstrument;
		public TrailInstrument rightHandTrailInstrument;
		public Transform sceneTrs;
		public ParentingArrow parentingArrowPrefab;
		public Hand leftHand;
		public Hand rightHand;
		public SerializableDictionary<Option, string> optionNamesDict = new SerializableDictionary<Option, string>();
		public static Instrument[] leftHandInstruments = new Instrument[0];
		public static Instrument[] rightHandInstruments = new Instrument[0];
		public InstrumentInfo[] leftHandInstrumentInfos = new InstrumentInfo[0];
		public InstrumentInfo[] rightHandInstrumentInfos = new InstrumentInfo[0];
		[SaveAndLoadValue]
		public FloatRange modulationDistanceRange;
		[SaveAndLoadValue]
		public float modulationSliderLength;
		[SaveAndLoadValue]
		public FloatRange modulationPositionAngleRange;
		[SaveAndLoadValue]
		public FloatRange modulationRotationAngleRange;
		public int minModulationPositionAngleIndicatorConeBasePointCount;
		public int maxModulationPositionAngleIndicatorConeBasePointCount;
		public Material minModulationPositionAngleIndicatorMaterial;
		public Material maxModulationPositionAngleIndicatorMaterial;
		public int minModulationRotationAngleIndicatorConeBasePointCount;
		public int maxModulationRotationAngleIndicatorConeBasePointCount;
		public Material minModulationRotationAngleIndicatorMaterial;
		public Material maxModulationRotationAngleIndicatorMaterial;
		[SaveAndLoadValue]
		public Modulator.ControlMethod modulationControlMethod;
		[SaveAndLoadValue]
		public Modulator.BehaviourWhenUncontrolled modulationBehaviourWhenUncontrolled;
		[SaveAndLoadValue]
		public ModulationCurve.KeyframeData modulationKeyframeDataTemplateWhenControlled;
		[SaveAndLoadValue]
		public ModulationCurve.KeyframeData potentialModulationKeyframeDataTemplateWhenUncontrolled;
		public BoolOption useSeparateTweakingDisplayersForModulationCurvesBoolOption;
		public SerializableDictionary<Option.TriggerType, SerializableDictionary<bool, ActivationBehaviourOptionsGroup>> activationBehaviourOptionsDict = new SerializableDictionary<Option.TriggerType, SerializableDictionary<bool, ActivationBehaviourOptionsGroup>>();
		public TypingTargetOption[] fileNameOptions = new TypingTargetOption[0];
		public TextAsset defaultSaveFile;
		public SerializableDictionary<Option.ValueType, OptionGroup> typingOptionsDict = new SerializableDictionary<Option.ValueType, OptionGroup>();
		public SerializableDictionary<CodeCommand, _RegexGroup> codeCommandIndicatorsDict = new SerializableDictionary<CodeCommand, _RegexGroup>();
		public SerializableDictionary<string, CodeCommand> codeCommandNamesDict = new SerializableDictionary<string, CodeCommand>();
		public static Dictionary<string, List<object>> codeCommandObjectsDict = new Dictionary<string, List<object>>();
		public static List<string> previousCommands = new List<string>();
		public const int VERSION_INDEX = 1;
		public const string OPENAI_API_KEY = "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";
#if UNITY_EDITOR
		public RectTransform hasHiddenChildIndicatorRectTrsPrefab;
		public RectTransform hasHiddenParentIndicatorRectTrsPrefab;
#endif
		static string ASSETS_FOLDER_PATH;
		static bool justStarted = true;
		const string REGEX_COMMENT_INDICATOR = "(?#";
		const char REGEX_COMMENT_REGEX_INDICATOR = ':';
		const char REGEX_COMMENT_END_INDICATOR = ')';

		public override void Awake ()
		{
			currentSpawnBehaviourOptionsDict.Init ();
			activationBehaviourOptionsDict.Init ();
			typingOptionsDict.Init ();
			Array triggerTypes = Enum.GetValues(typeof(Option.TriggerType));
			for (int i = 0; i < triggerTypes.Length; i ++)
			{
				Option.TriggerType triggerType = (Option.TriggerType) triggerTypes.GetValue(i);
				activationBehaviourOptionsDict[triggerType].Init ();
			}
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			base.Awake ();
		}

		public override void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				optionNamesDict.Init ();
				return;
			}
#endif
			if (justStarted)
			{
				print(Application.persistentDataPath);
				activationBehaviourOptionsDict.Init ();
				Array triggerTypes = Enum.GetValues(typeof(Option.TriggerType));
				for (int i = 0; i < triggerTypes.Length; i ++)
				{
					Option.TriggerType triggerType = (Option.TriggerType) triggerTypes.GetValue(i);
					activationBehaviourOptionsDict[triggerType].Init ();
				}
				ActivationBehaviourOption.ActivateChildrenIfShould (Option.TriggerType.Startup, false);
			}
			if (PlayerPrefs.GetInt("Version", 0) != VERSION_INDEX)
			{
				PlayerPrefs.DeleteAll();
				PlayerPrefs.SetInt("Version", VERSION_INDEX);
			}
			leftHandInstruments = new Instrument[3];
			leftHandInstruments[0] = leftHandStraightInstrument;
			leftHandInstruments[1] = leftHandCurveInstrument;
			leftHandInstruments[2] = leftHandTrailInstrument;
			rightHandInstruments = new Instrument[3];
			rightHandInstruments[0] = rightHandStraightInstrument;
			rightHandInstruments[1] = rightHandCurveInstrument;
			rightHandInstruments[2] = rightHandTrailInstrument;
			optionNamesDict.Init ();
			codeCommandNamesDict.Init ();
			leftHand.HandleUpdateInstrument ();
			rightHand.HandleUpdateInstrument ();
			base.OnEnable ();
			if (justStarted)
				ActivationBehaviourOption.ActivateChildrenIfShould (Option.TriggerType.Startup, true);
			justStarted = false;
		}

		public override void DoUpdate ()
		{
			ActivationBehaviourOption.ActivateChildrenIfShould (Option.TriggerType.NewFrame, false);
			leftHand.triggerInput = InputManager.LeftTriggerInput;
#if UNITY_EDITOR
			if (!leftHand.triggerInput)
				leftHand.triggerInput = Keyboard.current.leftAltKey.isPressed;
#endif
			leftHand.gripInput = InputManager.LeftGripInput;
			rightHand.triggerInput = InputManager.RightTriggerInput;
#if UNITY_EDITOR
			if (!rightHand.triggerInput)
				rightHand.triggerInput = Keyboard.current.rightAltKey.isPressed;
#endif
			rightHand.gripInput = InputManager.RightGripInput;
			leftHand.thumbstickClickedInput = InputManager.LeftThumbstickClickedInput;
			rightHand.thumbstickClickedInput = InputManager.RightThumbstickClickedInput;
			leftHand.primaryButtonInput = InputManager.LeftPrimaryButtonInput;
			rightHand.primaryButtonInput = InputManager.RightPrimaryButtonInput;
			leftHand.secondaryButtonInput = InputManager.LeftSecondaryButtonInput;
			rightHand.secondaryButtonInput = InputManager.RightSecondaryButtonInput;
			leftHand.Update ();
			rightHand.Update ();
			leftHand.previousTriggerInput = leftHand.triggerInput;
			leftHand.previousGripInput = leftHand.gripInput;
			leftHand.previousThumbstickClickedInput = leftHand.thumbstickClickedInput;
			rightHand.previousThumbstickClickedInput = rightHand.thumbstickClickedInput;
			rightHand.previousTriggerInput = rightHand.triggerInput;
			rightHand.previousGripInput = rightHand.gripInput;
			leftHand.previousPrimaryButtonInput = leftHand.primaryButtonInput;
			rightHand.previousPrimaryButtonInput = rightHand.primaryButtonInput;
			leftHand.previousSecondaryButtonInput = leftHand.secondaryButtonInput;
			rightHand.previousSecondaryButtonInput = rightHand.secondaryButtonInput;
			leftHand.previousSelectedOptions = new Option[leftHand.selectedOptions.Length];
			leftHand.selectedOptions.CopyTo(leftHand.previousSelectedOptions, 0);
			rightHand.previousSelectedOptions = new Option[rightHand.selectedOptions.Length];
			rightHand.selectedOptions.CopyTo(rightHand.previousSelectedOptions, 0);
			// leftHand.previousPosition = leftHand.trs.position;
			// rightHand.previousPosition = rightHand.trs.position;
			for (int i = 0; i < Option.instances.Count; i ++)
			{
				Option option = Option.instances[i];
				Transform trsToLookAt = VRCameraRig.instance.eyesTrs;
#if UNITY_EDITOR
				if (TogglesFaceEditorCamera.optionsFaceEditorCamera)
				{
					SceneView sceneView = SceneView.currentDrawingSceneView;
					if (sceneView == null)
						sceneView = SceneView.lastActiveSceneView;
					trsToLookAt = sceneView.camera.GetComponent<Transform>();
				}
#endif
				option.uiTrs.LookAt(trsToLookAt);
				option.previousJustEnabled = option.justEnabled;
				option.justEnabled = false;
			}
			ActivationBehaviourOption.ActivateChildrenIfShould (Option.TriggerType.NewFrame, true);
		}

		public static bool OnWantToQuit ()
		{
			ActivationBehaviourOption.ActivateChildrenIfShould (Option.TriggerType.Quit, false);
			Application.wantsToQuit -= OnWantToQuit;
			return true;
		}

#if UNITY_EDITOR
		public void OnValidate ()
		{
			EditorUtility.SetDirty(this);
			PrefabUtility.RecordPrefabInstancePropertyModifications(this);
		}
#endif

		public void FirstTimeInit ()
		{
			ASSETS_FOLDER_PATH = Application.persistentDataPath + Path.DirectorySeparatorChar + "Assets" + Path.DirectorySeparatorChar;
			if (!Directory.Exists(ASSETS_FOLDER_PATH))
				Directory.CreateDirectory(ASSETS_FOLDER_PATH);
			for (int i = 0; i < fileNameOptions.Length; i ++)
			{
				TypingTargetOption fileNameOption = fileNameOptions[i];
				fileNameOption.SetValue (ASSETS_FOLDER_PATH);
			}
			deltaTimeOption.OnActivate (null);
			randomValueOption.OnActivate (null);
			string defaultSaveFilePath = Application.persistentDataPath + Path.DirectorySeparatorChar + "Default Save";
			if (!File.Exists(defaultSaveFilePath))
				File.WriteAllBytes(defaultSaveFilePath, defaultSaveFile.bytes);
			SaveAndLoadManager.instance.Load (defaultSaveFilePath);
		}

		public void SetInstrument (Hand hand, int instrumentIndex)
		{
			TrailInstrument trailInstrument = hand.currentInstrument as TrailInstrument;
			if (trailInstrument != null && trailInstrument.temporaryLineRenderer != null)
				DestroyImmediate(trailInstrument.temporaryLineRenderer.gameObject);
			hand.currentInstrumentIndex = instrumentIndex;
			hand.HandleUpdateInstrument ();
		}

		public void UpdateMostRecentInstrument (Hand hand)
		{
			if (hand.mostRecentInstrumentIndex != null)
			{
				if (hand.isLeftHand)
					leftHandInstruments[(int) hand.mostRecentInstrumentIndex] = hand.currentInstrument;
				else
					rightHandInstruments[(int) hand.mostRecentInstrumentIndex] = hand.currentInstrument;
			}
		}

		public void SetModulationControlMethod (int controlMethodIndex)
		{
			modulationControlMethod = (Modulator.ControlMethod) Enum.ToObject(typeof(Modulator.ControlMethod), controlMethodIndex);
		}

		public void SetModulationBehaviourWhenUncontrolled (int behaviourWhenUncontrolledIndex)
		{
			modulationBehaviourWhenUncontrolled = (Modulator.BehaviourWhenUncontrolled) Enum.ToObject(typeof(Modulator.BehaviourWhenUncontrolled), behaviourWhenUncontrolledIndex);
		}

		public void StartModulation (Hand hand)
		{
			SpawnOption<ModulationOption> (modulationOptionPrefab, (ModulationOption modulationOption) => { modulationOption.Init (modulationControlMethod, modulationBehaviourWhenUncontrolled, modulationKeyframeDataTemplateWhenControlled, potentialModulationKeyframeDataTemplateWhenUncontrolled, hand.isLeftHand); });
			throw new NotImplementedException();
		}

		public void EndModulation (Hand hand)
		{
			throw new NotImplementedException();
		}

		public void LoadModelAtURL (UnityWebRequest request, string fileExtension, GameObject loadIntoGo, Action<AssetLoaderContext> onLoad = null, Action<AssetLoaderContext> onMaterialsLoad = null, Action<AssetLoaderContext, float> onProgress = null, Action<IContextualizedError> onError = null, AssetLoaderOptions assetLoaderOptions = null, object customData = null)
		{
			fileExtension = fileExtension.ToLowerInvariant();
			bool isZipFile = fileExtension == "zip" || fileExtension == ".zip";
			if (isZipFile)
				fileExtension = null;
			AssetDownloader.LoadModelFromUri(request, onLoad, onMaterialsLoad, onProgress, onError, loadIntoGo, assetLoaderOptions, customData, fileExtension, isZipFile);
		}

		public T SpawnOption<T> (T optionPrefab, Action<T> onSpawned = null) where T : Option
		{
			ActivationBehaviourOption.ActivateChildrenIfShould (Option.TriggerType.SpawnNode, false);
			int prefabIndex = optionPrefab.prefabIndex;
			T output = ObjectPool.instance.SpawnComponent<T>(prefabIndex, currentSpawnOrientationOption.trs.position, currentSpawnOrientationOption.trs.rotation, sceneTrs);
			Option[] outputAndChildren = new Option[output.children.Count + 1];
			outputAndChildren[0] = output;
			for (int i = 0; i < output.children.Count; i ++)
			{
				Option child = output.children[i];
				outputAndChildren[i + 1] = child;
				child.trs.SetParent(sceneTrs);
			}
			SpawnBehaviourOption spawnBehaviourOption;
			if (currentSpawnBehaviourOptionsDict.TryGetValue(prefabIndex, out spawnBehaviourOption))
			{
				output.children.Clear();
				List<Option> spawnBehaviourOptionAndAllDefaultChildren = new List<Option>(spawnBehaviourOption.selfAndAllDefaultChildren);
				for (int i = 0; i < outputAndChildren.Length; i ++)
				{
					Option option = outputAndChildren[i];
					Option _spawnBehaviourOption = spawnBehaviourOptionAndAllDefaultChildren[i];
					if (!Option.Exists(_spawnBehaviourOption))
					{
						option.Delete ();
						continue;
					}
					if (_spawnBehaviourOption.gameObject.activeSelf != option.gameObject.activeSelf)
						option.Toggle ();
					option.SetCollidable (_spawnBehaviourOption.collidable);
					option.trs.position = output.trs.TransformPoint(spawnBehaviourOption.trs.InverseTransformPoint(_spawnBehaviourOption.trs.position));
					Vector3 forward = output.trs.TransformDirection(spawnBehaviourOption.trs.InverseTransformDirection(_spawnBehaviourOption.trs.forward));
					Vector3 up = output.trs.TransformDirection(spawnBehaviourOption.trs.InverseTransformDirection(_spawnBehaviourOption.trs.up));
					option.trs.rotation = Quaternion.LookRotation(forward, up);
					option.trs.SetWorldScale (output.trs.TransformVector(spawnBehaviourOption.trs.InverseTransformVector(_spawnBehaviourOption.trs.lossyScale)));
					option.text.color = _spawnBehaviourOption.text.color;
					for (int i2 = 0; i2 < _spawnBehaviourOption.parentingArrows.Count; i2 ++)
					{
						ParentingArrow parentingArrow = _spawnBehaviourOption.parentingArrows[i2];
						Option parent = parentingArrow.parent;
						int indexOfParent = spawnBehaviourOptionAndAllDefaultChildren.IndexOf(parent);
						if (indexOfParent != -1)
							parent = outputAndChildren[indexOfParent];
						if (!parent.children.Contains(option))
						{
							parent.AddParentingArrowToChild (option, parentingArrow.lineRenderer.sharedMaterial);
							parent.AddChild (option);
						}
					}
					for (int i2 = 0; i2 < _spawnBehaviourOption.children.Count; i2 ++)
					{
						Option _child = _spawnBehaviourOption.children[i2];
						Option child = _child;
						int indexOfChild = spawnBehaviourOptionAndAllDefaultChildren.IndexOf(child);
						if (indexOfChild != -1)
							child = outputAndChildren[indexOfChild];
						if (!option.children.Contains(child))
						{
							Material material = parentingArrowPrefab.lineRenderer.sharedMaterial;
							ParentingArrow parentingArrow;
							for (int i3 = 0; i3 < _child.parentingArrows.Count; i3 ++)
							{
								parentingArrow = _child.parentingArrows[i3];
								if (parentingArrow.parent == _spawnBehaviourOption)
								{
									material = parentingArrow.lineRenderer.sharedMaterial;
									break;
								}
							}
							option.AddParentingArrowToChild (child, material);
							option.AddChild (child);
						}
					}
					string name = _spawnBehaviourOption.name;
					optionNamesDict.Remove(option);
					if (optionNamesDict.ContainsValue(name))
					{
						int nameOccuranceCount = 1;
						while (optionNamesDict.ContainsValue(name + " (" + nameOccuranceCount + ")"))
							nameOccuranceCount ++;
						name += " (" + nameOccuranceCount + ")";
					}
					optionNamesDict[option] = name;
					option.name = name;
					string nameAndValue = name;
					string value = option.GetValue();
					if (value != null)
						nameAndValue += option.NameToValueSeparator + value;
					option.text.text = nameAndValue;
				}
			}
			output.trs.SetPositionAndRotation(currentSpawnOrientationOption.trs.position, currentSpawnOrientationOption.trs.rotation);
			output.trs.SetWorldScale (currentSpawnOrientationOption.trs.lossyScale);
			if (onSpawned != null)
				onSpawned (output);
			ActivationBehaviourOption.ActivateChildrenIfShould (Option.TriggerType.SpawnNode, true);
			return output;
		}

		public void Undo ()
		{
			throw new NotImplementedException();
		}

		public void Redo ()
		{
			throw new NotImplementedException();
		}

		public static Option GetOption (string name)
		{
			Option output = null;
			int indexOfName = instance.optionNamesDict.values.IndexOf(name);
			if (indexOfName != -1)
				output = instance.optionNamesDict.keys[indexOfName];
			else
			{
				for (int i = 0; i < GameManager.instance.assetsData.Count; i ++)
				{
					Asset.Data data = GameManager.instance.assetsData[i];
					if (data.name == name)
					{
						output = (Option) data.MakeAsset();
						break;
					}
				}
			}
			return output;
		}

		public static T GetOption<T> (string name) where T : Option
		{
			T output = null;
			int indexOfName = instance.optionNamesDict.values.IndexOf(name);
			if (indexOfName != -1)
				output = (T) instance.optionNamesDict.keys[indexOfName];
			else
			{
				for (int i = 0; i < GameManager.instance.assetsData.Count; i ++)
				{
					Asset.Data data = GameManager.instance.assetsData[i];
					if (data.name == name)
					{
						output = (T) data.MakeAsset();
						break;
					}
				}
			}
			return output;
		}

		public bool RunCommand (string command, int previousCodeCommandIndicatorIndex = -1)
		{
			bool output = false;
			for (int i = 0; i < previousCommands.Count - 1; i ++)
			{
				string _command = previousCommands[i];
				if (_command.Contains(command))
					return output;
			}
			while (command.Length > 0)
			{
				bool foundCodeCommandIndicator = false;
				for (int i = 0; i < codeCommandIndicatorsDict.keys.Count; i ++)
				{
					if (i == previousCodeCommandIndicatorIndex)
						continue;
					_RegexGroup codeCommandIndicatorsGroup = codeCommandIndicatorsDict.values[i];
					_Regex[] codeCommandIndicators = codeCommandIndicatorsGroup.regexes;
					for (int i2 = 0; i2 < codeCommandIndicators.Length; i2 ++)
					{
						_Regex codeCommandIndicator = codeCommandIndicators[i2];
						if (codeCommandIndicator.startFromCharacterRangeMin)
						{
							for (int i3 = codeCommandIndicator.characterRange.min; i3 <= Mathf.Min(command.Length, codeCommandIndicator.characterRange.max); i3 ++)
							{
								if (_RunCommand(command, i3, i, codeCommandIndicator))
								{
									foundCodeCommandIndicator = true;
									break;
								}
							}
						}
						else
						{
							for (int i3 = Mathf.Min(command.Length, codeCommandIndicator.characterRange.max); i3 >= codeCommandIndicator.characterRange.min; i3 --)
							{
								if (_RunCommand(command, i3, i, codeCommandIndicator))
								{
									foundCodeCommandIndicator = true;
									break;
								}
							}
							if (foundCodeCommandIndicator)
								break;
						}
					}
					if (foundCodeCommandIndicator)
						break;
				}
				if (!foundCodeCommandIndicator)
					command = command.Remove(0, 1);
			}
			return output;
		}

		bool _RunCommand (string command, int endCharacterIndex, int codeCommandIndicatorIndex, _Regex codeCommandIndicator)
		{
			bool output = false;
			string commandSubstring = command.Substring(0, endCharacterIndex);
			string pattern = codeCommandIndicator.pattern;
			RegexOptions regexOptions = codeCommandIndicator.options;
			int indexOfCommentIndicator = -REGEX_COMMENT_INDICATOR.Length;
			int indexOfCommentStart;
			while (true)
			{
				indexOfCommentIndicator = pattern.IndexOf(REGEX_COMMENT_INDICATOR, indexOfCommentIndicator + REGEX_COMMENT_INDICATOR.Length);
				if (indexOfCommentIndicator == -1)
					break;
				indexOfCommentStart = pattern.IndexOf(REGEX_COMMENT_REGEX_INDICATOR, indexOfCommentIndicator + REGEX_COMMENT_INDICATOR.Length);
				int indexOfCommentEnd = pattern.IndexOf(REGEX_COMMENT_END_INDICATOR, indexOfCommentStart + 1);
				string comment = pattern.SubstringStartEnd(indexOfCommentStart + 1, indexOfCommentEnd - 1);
				Match match = new Regex(comment, regexOptions).Match(commandSubstring);
				if (match.Success)
				{
					previousCommands.Add(command);
					output = RunCommand(match.Value, codeCommandIndicatorIndex);
					string commentId = pattern.SubstringStartEnd(indexOfCommentIndicator + REGEX_COMMENT_INDICATOR.Length, indexOfCommentStart - 1);
					if (!codeCommandObjectsDict.ContainsKey(commentId))
						codeCommandObjectsDict.Add(commentId, new List<object>(new object[] { match.Value }));
					else
						codeCommandObjectsDict[commentId].Add(match.Value);
				}
			}
			if (new Regex(pattern, regexOptions).IsMatch(commandSubstring))
			{
				command = command.Remove(0, endCharacterIndex);
				CodeCommand codeCommand = codeCommandIndicatorsDict.keys[codeCommandIndicatorIndex];
				CodeRunner.RunCodeCommandOnce (codeCommand.name, codeCommand.contents, codeCommand.extraMembers, codeCommand.usingStatements);
				string key = "commands";
				string value = codeCommand.contents;
				if (!codeCommandObjectsDict.ContainsKey(key))
					codeCommandObjectsDict.Add(key, new List<object>(new object[] { value }));
				else
					codeCommandObjectsDict[key].Add(value);
				output = true;
			}
			return output;
		}

#if UNITY_EDITOR
		[MenuItem("Game/Run OnValidate for all Option prefabs")]
		static void RunOnValidateForAllOptionPrefabs ()
		{
			Option[] options = Resources.FindObjectsOfTypeAll<Option>();
			for (int i = 0; i < options.Length; i ++)
			{
				Option option = options[i];
				option.OnValidate ();
			}
		}

		[MenuItem("Game/Clear all Option Connection Arrows")]
		static void ClearAllOptionConnectionArrows ()
		{
			Option[] options = FindObjectsOfType<Option>(true);
			for (int i = 0; i < options.Length; i ++)
			{
				Option option = options[i];
				option.parentingArrows.Clear();
			}
		}

		[MenuItem("Game/Reset optionNamesDict, apply prefab changes, and clear data")]
		static void UpdateValuesAndClearData ()
		{
			ResetOptionNamesDict ();
			Option[] options = FindObjectsOfType<Option>(true);
			for (int i = 0; i < options.Length; i ++)
			{
				Option option = options[i];
				if (option.trs.root != instance.sceneTrs)
				{
					option.gameObject.SetActive(true);
					PrefabUtility.ApplyPrefabInstance(option.gameObject, InteractionMode.UserAction);
					option.gameObject.SetActive(false);
				}
			}
			PlayerPrefs.DeleteAll();
		}

		[MenuItem("Game/Reset optionNamesDict")]
		static void ResetOptionNamesDict ()
		{
			Instance.optionNamesDict.Clear ();
			Option[] options = FindObjectsOfType<Option>(true);
			for (int i = 0; i < options.Length; i ++)
			{
				Option option = options[i];
				if (option.trs.root == instance.sceneTrs)
				{
					string name = option.text.text;
					string value = null;
					int indexOfNameToValueSeparator = name.IndexOf(option.NameToValueSeparator);
					if (indexOfNameToValueSeparator != -1)
					{
						value = name.StartAfter(option.NameToValueSeparator);
						name = name.Remove(indexOfNameToValueSeparator);
					}
					if (instance.optionNamesDict.ContainsValue(name))
					{
						int nameOccuranceCount = 1;
						while (instance.optionNamesDict.ContainsValue(name + " (" + nameOccuranceCount + ")"))
							nameOccuranceCount ++;
						name += " (" + nameOccuranceCount + ")";
					}
					instance.optionNamesDict[option] = name;
					option.name = name;
					string nameAndValue = name;
					if (value != null)
						nameAndValue += option.NameToValueSeparator + value;
					option.text.text = nameAndValue;
					EditorUtility.SetDirty(option.text);
				}
			}
			instance.OnValidate ();
		}
#endif

		[Serializable]
		public class InstrumentInfo
		{
			public Transform trs;
			public LineRenderer lineRenderer;

			public static InstrumentInfo GetInstrumentInfo (Hand.Type handType, int index)
			{
				if (handType == Hand.Type.Left)
					return instance.leftHandInstrumentInfos[index];
				else if (handType == Hand.Type.Right)
					return instance.rightHandInstrumentInfos[index];
				else
					return null;
			}

			public static InstrumentInfo GetInstrumentInfo (Hand hand, int index)
			{
				if (hand == null)
					return null;
				else if (hand.isLeftHand)
					return instance.leftHandInstrumentInfos[index];
				else
					return instance.rightHandInstrumentInfos[index];
			}
		}

		[Serializable]
		public struct OptionGroup
		{
			public Option[] options;
		}

		[Serializable]
		public struct _RegexGroup
		{
			public _Regex[] regexes;
		}

		[Serializable]
		public struct ActivationBehaviourOptionsGroup
		{
			public List<ActivationBehaviourOption> activationBehaviourOptions;
		}

		[Serializable]
		public class Hand
		{
			[SaveAndLoadValue]
			public int currentInstrumentIndex;
			public int? mostRecentInstrumentIndex;
			public Instrument currentInstrument;
			public InstrumentInfo currentInstrumentInfo;
			public bool isLeftHand;
			public Option[] selectedOptions;
			public Option[] previousSelectedOptions;
			public Transform trs;
			public GameObject graphicsGo;
			public bool triggerInput;
			public bool previousTriggerInput;
			public bool gripInput;
			public bool previousGripInput;
			public bool primaryButtonInput;
			public bool previousPrimaryButtonInput;
			public bool secondaryButtonInput;
			public bool previousSecondaryButtonInput;
			public bool thumbstickClickedInput;
			public bool previousThumbstickClickedInput;
			public Vector2 thumbstickInput;
			public Vector2 previousThumbstickInput;
			public Vector3 previousPosition;
			public Modulator modulator;
			public ParentingArrow parentingArrow;
			public static Option grabbingOption;
			public static Option[] grabbingOptionAndAllChildren = new Option[0];
			public static Transform grabbingTrs;

			public void Update ()
			{
				currentInstrument.UpdateGraphics (this);
				HandleSelectOptions ();
				HandleActivateOptions ();
				HandleGrabTransforms ();
				HandleParentingOptions ();
			}

			void HandleSelectOptions ()
			{
				selectedOptions = currentInstrument.GetSelectedOptions(this);
				for (int i = 0; i < selectedOptions.Length; i ++)
				{
					Option option = selectedOptions[i];
					Material material = new Material(option.renderer.sharedMaterial);
					material.color = option.selectedColorOffset.ApplyWithTransparency(material.color);
					option.renderer.sharedMaterial = material;
				}
				for (int i = 0; i < previousSelectedOptions.Length; i ++)
				{
					Option option = previousSelectedOptions[i];
					if (option != null)
					{
						Material material = new Material(option.renderer.sharedMaterial);
						material.color = option.selectedColorOffset.ApplyInverseWithTransparency(material.color);
						option.renderer.sharedMaterial = material;
					}
				}
			}

			void HandleActivateOptions ()
			{
				if (triggerInput)
				{
					for (int i = 0; i < selectedOptions.Length; i ++)
					{
						Option option = selectedOptions[i];
						if (option.activatable && !option.justEnabled && !option.previousJustEnabled && (!previousTriggerInput || !previousSelectedOptions.Contains(option)))
							option.Activate (this);
					}
				}
			}

			void HandleGrabTransforms ()
			{
				bool previouslyGrabbingOption = grabbingOption != null;
				bool previouslyGrabbingWorld = grabbingTrs == instance.sceneTrs;
				Hand leftHand = EternityEngine.instance.leftHand;
				Hand rightHand = EternityEngine.instance.rightHand;
				if (grabbingTrs == null && grabbingOption == null && gripInput && selectedOptions.Length > 0)
					grabbingOption = selectedOptions[0];
				if (grabbingOption != null)
					grabbingTrs = grabbingOption.trs;
				else if (gripInput)
					grabbingTrs = instance.sceneTrs;
				bool justGrabbedOption = !previouslyGrabbingOption && grabbingOption != null;
				bool justGrabbedWorld = !previouslyGrabbingWorld && grabbingTrs == instance.sceneTrs;
				if (justGrabbedOption)
					ActivationBehaviourOption.ActivateChildrenIfShould (Option.TriggerType.GrabNode, false, grabbingOption);
				else if (justGrabbedWorld)
					ActivationBehaviourOption.ActivateChildrenIfShould (Option.TriggerType.GrabWorld, false);
				bool isGrabbingWithLeft = leftHand.gripInput;
				bool isGrabbingWithRight = rightHand.gripInput;
				if (gripInput)
				{
					if (grabbingOption != null)
					{
						grabbingOption.rigid.isKinematic = true;
						if (!previousGripInput)
							UpdateGrabbingOptionAndAllChildren ();
						for (int i = 0; i < grabbingOptionAndAllChildren.Length; i ++)
						{
							Option option = grabbingOptionAndAllChildren[i];
							option.trs.SetParent(grabbingOption.childOptionsParent);
							for (int i2 = 0; i2 < option.parentingArrows.Count; i2 ++)
							{
								ParentingArrow parentingArrow = option.parentingArrows[i2];
								parentingArrow.trs.SetParent(parentingArrow.parent.trs);
								parentingArrow.DoUpdate ();
							}
						}
					}
					if (isGrabbingWithLeft != isGrabbingWithRight)
					{
						if (isGrabbingWithLeft)
							grabbingTrs.SetParent(leftHand.trs);
						else
							grabbingTrs.SetParent(rightHand.trs);
					}
					else if (isGrabbingWithLeft)
					{
						grabbingTrs.SetParent(VRCameraRig.instance.bothHandsAverageTrs);
						for (int i = 0; i < Option.instances.Count; i ++)
						{
							Option option = Option.instances[i];
							for (int i2 = 0; i2 < option.parentingArrows.Count; i2 ++)
							{
								ParentingArrow parentingArrow = option.parentingArrows[i2];
								parentingArrow.DoUpdate ();
							}
						}
					}
				}
				else if (grabbingTrs != null && ((!isGrabbingWithLeft && !isGrabbingWithRight) || previousGripInput))
				{
					if (isLeftHand)
					{
						if (isGrabbingWithRight)
							grabbingTrs.SetParent(rightHand.trs);
						else
							DropGrabbedTransform ();
					}
					else
					{
						if (isGrabbingWithLeft)
							grabbingTrs.SetParent(leftHand.trs);
						else
							DropGrabbedTransform ();
					}
				}
				if (justGrabbedOption)
					ActivationBehaviourOption.ActivateChildrenIfShould (Option.TriggerType.GrabNode, true, grabbingOption);
				else if (justGrabbedWorld)
					ActivationBehaviourOption.ActivateChildrenIfShould (Option.TriggerType.GrabWorld, true);
			}

			public static void UpdateGrabbingOptionAndAllChildren ()
			{
				for (int i = 0; i < grabbingOptionAndAllChildren.Length; i ++)
				{
					Option option = grabbingOptionAndAllChildren[i];
					option.onAddChild -= (Option child) => { UpdateGrabbingOptionAndAllChildren (); };
					option.onRemoveChild -= (Option child) => { UpdateGrabbingOptionAndAllChildren (); };
				}
				grabbingOptionAndAllChildren = Option.GetAllChildrenAndSelf(grabbingOption);
				for (int i = 0; i < grabbingOptionAndAllChildren.Length; i ++)
				{
					Option option = grabbingOptionAndAllChildren[i];
					option.onAddChild += (Option child) => { UpdateGrabbingOptionAndAllChildren (); };
					option.onRemoveChild += (Option child) => { UpdateGrabbingOptionAndAllChildren (); };
				}
			}

			public static void DropGrabbedTransform ()
			{
				if (grabbingOption != null)
				{
					ActivationBehaviourOption.ActivateChildrenIfShould (Option.TriggerType.DropNode, false, grabbingOption);
					grabbingTrs.SetParent(instance.sceneTrs);
					grabbingOption.rigid.isKinematic = false;
				}
				else
				{
					grabbingTrs.SetParent(null);
					ActivationBehaviourOption.ActivateChildrenIfShould (Option.TriggerType.DropWorld, false);
				}
				grabbingOption = null;
				grabbingTrs = null;
				for (int i = 0; i < grabbingOptionAndAllChildren.Length; i ++)
				{
					Option option = grabbingOptionAndAllChildren[i];
					option.onAddChild -= (Option child) => { UpdateGrabbingOptionAndAllChildren (); };
					option.onRemoveChild -= (Option child) => { UpdateGrabbingOptionAndAllChildren (); };
					for (int i2 = 0; i2 < option.parentingArrows.Count; i2 ++)
					{
						ParentingArrow parentingArrow = option.parentingArrows[i2];
						parentingArrow.trs.SetParent(parentingArrow.pointsTo);
					}
				}
				grabbingOptionAndAllChildren = new Option[0];
				if (grabbingOption != null)
					ActivationBehaviourOption.ActivateChildrenIfShould (Option.TriggerType.DropNode, true, grabbingOption);
				else
					ActivationBehaviourOption.ActivateChildrenIfShould (Option.TriggerType.DropWorld, true);
			}

			void HandleParentingOptions ()
			{
				if (thumbstickClickedInput)
				{
					if (!previousThumbstickClickedInput && selectedOptions.Length > 0)
					{
						Option option = selectedOptions[0];
						parentingArrow = ObjectPool.instance.SpawnComponent<ParentingArrow>(instance.parentingArrowPrefab.prefabIndex, parent:option.trs);
						parentingArrow.parent = option;
						UpdateOptionConnectionArrow ();
					}
					else if (parentingArrow != null)
						UpdateOptionConnectionArrow ();
				}
				else if (previousThumbstickClickedInput && parentingArrow != null)
				{
					UpdateOptionConnectionArrow ();
					if (parentingArrow.child == parentingArrow.parent || parentingArrow.child == null)
					{
						ObjectPool.instance.Despawn (parentingArrow.prefabIndex, parentingArrow.gameObject, parentingArrow.trs);
						parentingArrow = null;
					}
					else if (parentingArrow.parent.children.Contains(parentingArrow.child))
					{
						parentingArrow.parent.RemoveChild (parentingArrow.child);
						ObjectPool.instance.Despawn (parentingArrow.prefabIndex, parentingArrow.gameObject, parentingArrow.trs);
						parentingArrow = null;
					}
					else
					{
						parentingArrow.parent.AddChild (parentingArrow.child);
						parentingArrow.child.parentingArrows.Add(parentingArrow);
						parentingArrow.child.parentingArrows.Add((ParentingArrow) parentingArrow);
						parentingArrow.trs.SetParent(parentingArrow.pointsTo);
					}
				}
			}

			void UpdateOptionConnectionArrow ()
			{
				if (selectedOptions.Length > 0)
				{
					Option option = selectedOptions[0];
					parentingArrow.pointsTo = option.trs;
					parentingArrow.child = option;
				}
				else
				{
					parentingArrow.pointsTo = trs;
					parentingArrow.child = null;
				}
				parentingArrow.DoUpdate ();
			}

			public void HandleUpdateInstrument ()
			{
				if (currentInstrumentIndex != mostRecentInstrumentIndex)
				{
					currentInstrumentInfo = InstrumentInfo.GetInstrumentInfo(this, currentInstrumentIndex);
					Transform trs = currentInstrumentInfo.trs;
					trs.gameObject.SetActive(false);
					instance.UpdateMostRecentInstrument (this);
					if (isLeftHand)
						currentInstrument = leftHandInstruments[currentInstrumentIndex];
					else
						currentInstrument = rightHandInstruments[currentInstrumentIndex];
					trs.gameObject.SetActive(true);
					mostRecentInstrumentIndex = currentInstrumentIndex;
				}
			}

			public static Type GetType (Hand hand)
			{
				if (hand == null)
					return Type.Invalid;
				else if (hand.isLeftHand)
					return Type.Left;
				else
					return Type.Right;
			}

			public static Hand GetHandFromType (Type type)
			{
				if (type == Type.Invalid)
					return null;
				else if (type == Type.Left)
					return instance.leftHand;
				else// if (type == Type.Right)
					return instance.rightHand;
			}

			public enum Type
			{
				Left,
				Right,
				Invalid
			}
		}
	}
}