using TMPro;
using System;
using Extensions;
using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;
#endif

namespace _EternityEngine
{
	public class Option : Asset
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public virtual string NameToValueSeparator
		{
			get
			{
				return ":\n";
			}
		}
		public bool collidable;
		public bool activatable;
		public ColorOffset notActivatableColorOffset;
		public ColorOffset selectedColorOffset;
		// public Action<EternityEngine.Hand> onActivated;
		[HideInInspector]
		public Rigidbody rigid;
		[HideInInspector]
		public Collider collider;
		[HideInInspector]
		public Renderer renderer;
		[HideInInspector]
		public Transform uiTrs;
		[HideInInspector]
		public Transform textTrs;
		[HideInInspector]
		public Transform childOptionsParent;
		[HideInInspector]
		public TMP_Text text;
		[HideInInspector]
		public GameObject hasHiddenChildIndicator;
		[HideInInspector]
		public GameObject hasHiddenParentIndicator;
		[HideInInspector]
		public bool justEnabled;
		[HideInInspector]
		public bool previousJustEnabled;
		// [HideInInspector]
		public List<Option> children = new List<Option>();
		// [HideInInspector]
		public List<ParentingArrow> parentingArrows = new List<ParentingArrow>();
		public Action<Option> onAddChild;
		public Action<Option> onRemoveChild;
		public delegate bool OnAboutToAddChild(Option child);
		public event OnAboutToAddChild onAboutToAddChild;
		public delegate bool OnAboutToRemoveChild(Option child);
		public event OnAboutToRemoveChild onAboutToRemoveChild;
		[HideInInspector]
		public bool recordingActivateInAutomationSequence;
		public MeshFilter meshFilter;
#if UNITY_EDITOR
		List<Option> previousChildren = new List<Option>();
#endif
		public static List<Option> instances = new List<Option>();
		public AnimationEntry activateAnimationEntry;
		public ValueType valueType;
		bool deleted;
		uint hiddenChildCount;
		uint hiddenParentCount;

		public override void Awake ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
			// Selection.selectionChanged += OnSelectionChanged;
#endif
			if (string.IsNullOrEmpty(SaveAndLoadManager.MostRecentSaveFileName))
				Init ();
		}

		void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			// {
			// 	children.Clear();
			// 	for (int i = 0; i < childOptionsParent.childCount; i ++)
			// 	{
			// 		Transform child = childOptionsParent.GetChild(i);
			// 		Option option = child.GetComponent<Option>();
			// 		children.Add(option);
			// 	}
				return;
			// }
#endif
			ActivationBehaviourOption.ActivateChildrenIfShould (TriggerType.Visible, false, this);
			justEnabled = true;
			if (collidable)
				StartCoroutine(UpdateOptionConnectionArrowsRoutine ());
			instances.Add(this);
			for (int i = 0; i < parentingArrows.Count; i ++)
			{
				ParentingArrow parentingArrow = parentingArrows[i];
				parentingArrow.gameObject.SetActive(true);
			}
			for (int i = 0; i < children.Count; i ++)
			{
				Option child = children[i];
				if (child != null && child.gameObject.activeInHierarchy)
				{
					for (int i2 = 0; i2 < child.parentingArrows.Count; i2 ++)
					{
						ParentingArrow parentingArrow = child.parentingArrows[i2];
						if (parentingArrow.parent == this)
						{
							parentingArrow.gameObject.SetActive(true);
							break;
						}
					}
				}
			}
			ActivationBehaviourOption.ActivateChildrenIfShould (TriggerType.Visible, true, this);
		}

		public virtual void Init ()
		{
			if (!activatable)
			{
				Material material = new Material(renderer.sharedMaterial);
				material.color = notActivatableColorOffset.ApplyWithTransparency(material.color);
				// material.SetColor("_EmissionColor", notActivatableEmissionOffset.ApplyWithTransparency(material.GetColor("_EmissionColor")));
				renderer.sharedMaterial = material;
			}
			for (int i = 0; i < children.Count; i ++)
			{
				Option child = children[i];
				AddParentingArrowToChild (child);
				// child.optionConnectionArrows.Add(parentingArrow);
			}
		}

		IEnumerator UpdateOptionConnectionArrowsRoutine ()
		{
			Vector3 previousPosition;
			do
			{
				for (int i = 0; i < parentingArrows.Count; i ++)
				{
					ParentingArrow parentingArrow = parentingArrows[i];
					parentingArrow.DoUpdate ();
				}
				previousPosition = trs.position;
				yield return new WaitForFixedUpdate();
			} while (trs.position != previousPosition); // WARN: Potential crashes would happen if you are not carefel when adding a new Activation Behaviour like Change Node Position
		}

#if UNITY_EDITOR
		public virtual void OnValidate ()
		{
			if (!Application.isPlaying)
			{
				if (trs == null)
					trs = GetComponent<Transform>();
				if (rigid == null)
					rigid = GetComponent<Rigidbody>();
				if (collider == null)
					collider = GetComponent<Collider>();
				if (renderer == null)
					renderer = GetComponent<Renderer>();
				if (uiTrs == null)
					uiTrs = trs.Find("Canvas (World)");
				if (textTrs == null)
					textTrs = uiTrs.Find("Text");
				if (childOptionsParent == null)
					childOptionsParent = trs.Find("Children");
				if (text == null)
					text = textTrs.GetComponent<TMP_Text>();
				if (activateAnimationEntry.animator == null)
					activateAnimationEntry.animator = GetComponent<Animator>();
				if (string.IsNullOrEmpty(activateAnimationEntry.animatorStateName))
					activateAnimationEntry.animatorStateName = "Activate";
				if (meshFilter == null)
					meshFilter = GetComponent<MeshFilter>();
				if (hasHiddenChildIndicator == null)
				{
					Transform hasHiddenChildIndicatorTrs = uiTrs.Find("Has Hidden Child Indicator");
					if (hasHiddenChildIndicatorTrs == null)
						hasHiddenChildIndicatorTrs = Instantiate(EternityEngine.Instance.hasHiddenChildIndicatorRectTrsPrefab, uiTrs);
					hasHiddenChildIndicator = hasHiddenChildIndicatorTrs.gameObject;
				}
				if (hasHiddenParentIndicator == null)
				{
					Transform hasHiddenParentIndicatorTrs = uiTrs.Find("Has Hidden Parent Indicator");
					if (hasHiddenParentIndicatorTrs == null)
						hasHiddenParentIndicatorTrs = Instantiate(EternityEngine.Instance.hasHiddenParentIndicatorRectTrsPrefab, uiTrs);
					hasHiddenParentIndicator = hasHiddenParentIndicatorTrs.gameObject;
				}
				EditorUtility.SetDirty(this);
				Option prefab = PrefabUtility.GetCorrespondingObjectFromSource(this);
				if (prefab != null && (prefab.hasHiddenChildIndicator == null || prefab.hasHiddenParentIndicator == null))
				{
					EditorUtility.SetDirty(prefab);
					SerializedObject serializedObject = new SerializedObject(this);
					string prefabPath = PrefabUtility.GetPrefabAssetPathOfNearestInstanceRoot(this);
					PrefabUtility.ApplyAddedGameObjects(new GameObject[] { hasHiddenChildIndicator, hasHiddenParentIndicator }, prefabPath, InteractionMode.AutomatedAction);
					SerializedProperty serializedProperty = serializedObject.FindProperty("hasHiddenParentIndicator");
					PrefabUtility.ApplyPropertyOverride(serializedProperty, prefabPath, InteractionMode.AutomatedAction);
					serializedProperty = serializedObject.FindProperty("hasHiddenChildIndicator");
					PrefabUtility.ApplyPropertyOverride(serializedProperty, prefabPath, InteractionMode.AutomatedAction);
					PrefabUtility.SavePrefabAsset(prefab.gameObject);
				}
				return;
			}
			// if (EternityEngine.Instance == null)
			// 	return;
			// EternityEngine.instance.Awake ();
			// SetActivatable (activatable);
			// SetCollidable (collidable);
			// if (Application.isPlaying)
			// {
			// 	if (children != previousChildren)
			// 	{
			// 		if (GameManager.framesSinceLevelLoaded > 1)
			// 		{
			// 			for (int i = 0; i < previousChildren.Count; i ++)
			// 			{
			// 				Option previousChild = previousChildren[i];
			// 				if (previousChild != null && !children.Contains(previousChild))
			// 				{
			// 					children.Add(previousChild);
			// 					RemoveChild (previousChild);
			// 				}
			// 			}
			// 			for (int i = 0; i < children.Count; i ++)
			// 			{
			// 				Option child = children[i];
			// 				if (child != null && !previousChildren.Contains(child))
			// 				{
			// 					children.RemoveAt(i);
			// 					if (AddChild(child))
			// 					{
			// 						ParentingArrow parentingArrow = ObjectPool.Instance.SpawnComponent<ParentingArrow>(EternityEngine.instance.parentingArrowPrefab.prefabIndex, parent:child.trs);
			// 						parentingArrow.pointsTo = child.trs;
			// 						parentingArrow.child = child;
			// 						parentingArrow.parent = this;
			// 						parentingArrow.DoUpdate ();
			// 						parentingArrow.gameObject.SetActive(child.gameObject.activeInHierarchy);
			// 						child.parentingArrows.Add(parentingArrow);
			// 						// child.optionConnectionArrows.Add(parentingArrow);
			// 					}
			// 				}
			// 			}
			// 		}
			// 		previousChildren = new List<Option>(children);
			// 		return;
			// 	}
			// }
			// enabled = !enabled;
			// enabled = !enabled;
			// // EditorUtility.SetDirty(this);
			// // PrefabUtility.RecordPrefabInstancePropertyModifications(this);
		}

		void OnSelectionChanged ()
		{
			// if (Selection.gameObjects.Contains(gameObject))
			// {
			// 	EternityEngine.Hand.grabbingTrs = trs;
			// 	EternityEngine.Hand.grabbingOption = this;
			// 	rigid.isKinematic = true;
			// 	EternityEngine.Hand.UpdateGrabbingOptionAndAllChildren ();
			// 	for (int i = 0; i < EternityEngine.Hand.grabbingOptionAndAllChildren.Length; i ++)
			// 	{
			// 		Option option = EternityEngine.Hand.grabbingOptionAndAllChildren[i];
			// 		option.trs.SetParent(EternityEngine.Hand.grabbingOption.childOptionsParent);
			// 	}
			// 	for (int i = 0; i < optionConnectionArrows.Count; i ++)
			// 	{
			// 		OptionConnectionArrow optionConnectionArrow = optionConnectionArrows[i];
			// 		optionConnectionArrow.trs.SetParent(optionConnectionArrow.parent.trs);
			// 	}
			// 	StopCoroutine(_UpdateOptionConnectionArrowsRoutine ());
			// 	StartCoroutine(_UpdateOptionConnectionArrowsRoutine ());
			// }
			// else
			// {
			// 	StopCoroutine(_UpdateOptionConnectionArrowsRoutine ());
			// 	for (int i2 = 0; i2 < optionConnectionArrows.Count; i2 ++)
			// 	{
			// 		OptionConnectionArrow optionConnectionArrow = optionConnectionArrows[i2];
			// 		optionConnectionArrow.trs.SetParent(optionConnectionArrow.pointsTo);
			// 	}
			// }
		}

		// IEnumerator _UpdateOptionConnectionArrowsRoutine ()
		// {
		// 	while (true)
		// 	{
		// 		for (int i = 0; i < optionConnectionArrows.Count; i ++)
		// 		{
		// 			OptionConnectionArrow optionConnectionArrow = optionConnectionArrows[i];
		// 			optionConnectionArrow.DoUpdate ();
		// 		}
		// 		yield return new WaitForEndOfFrame();
		// 	}
		// }
#endif

		public virtual void Activate (EternityEngine.Hand hand)
		{
			ActivationBehaviourOption.ActivateChildrenIfShould (TriggerType.Activate, false, this);
			OnActivate (hand);
			// for (int i = 0; i < controlFlowArrows.Count; i ++)
			// {
			// 	ControlFlowArrow controlFlowArrow = controlFlowArrows[i];
			// 	controlFlowArrow.child.Activate (null);
			// }
			activateAnimationEntry.Play(0);
			ActivationBehaviourOption.ActivateChildrenIfShould (TriggerType.Activate, true, this);
		}

		public virtual void OnActivate (EternityEngine.Hand hand)
		{
			// if (onActivated != null)
			// 	onActivated (hand);
		}

		// public virtual void OnActivated (EternityEngine.Hand hand)
		// {
		// 	List<AutomationOption> automationOptions = AutomationOption.recordingActivateOptionsDict[this];
		// 	for (int i = 0; i < automationOptions.Count; i ++)
		// 	{
		// 		AutomationOption automationOption = automationOptions[i];
		// 		if (!automationOption.pauseRecording && ((automationOption.recordLeftHand && hand.isLeftHand) || (automationOption.recordRightHand && !hand.isLeftHand)))
		// 		{
		// 			AutomationSequence.SequenceEntry sequenceEntry = new AutomationSequence.SequenceEntry();
		// 			// onActivated = null;
		// 			SetData ();
		// 			// onActivated += (EternityEngine.Hand hand) => { OnActivated (hand); };
		// 			bool spawnIfInvalid = automationOption.invalidOptionFixBehaviour == AutomationSequence.InvalidOptionFixBehaviour.Spawn;
		// 			sequenceEntry.activateOptionStart = new AutomationSequence.SequenceEntry.OptionEntry(_Data, spawnIfInvalid);
		// 			sequenceEntry.time = Time.time - automationOption.recordingStartTime;
		// 			automationOption.automationSequence.sequenceEntries.Add(sequenceEntry);
		// 		}
		// 	}
		// }

		public void SetActivatable (bool activatable)
		{
			if (activatable != this.activatable)
			{
				if (activatable)
					ActivationBehaviourOption.ActivateChildrenIfShould (TriggerType.Activatable, false, this);
				else
					ActivationBehaviourOption.ActivateChildrenIfShould (TriggerType.Unactivatable, false, this);
				Material material = new Material(renderer.sharedMaterial);
				if (activatable)
				// {
					material.color = notActivatableColorOffset.ApplyInverseWithTransparency(material.color);
				// 	material.SetColor("_EmissionColor", notActivatableEmissionOffset.ApplyInverseWithTransparency(material.GetColor("_EmissionColor")));
				// }
				else
				// {
					material.color = notActivatableColorOffset.ApplyWithTransparency(material.color);
				// 	material.SetColor("_EmissionColor", notActivatableEmissionOffset.ApplyWithTransparency(material.GetColor("_EmissionColor")));
				// }
				renderer.sharedMaterial = material;
				this.activatable = activatable;
				if (activatable)
					ActivationBehaviourOption.ActivateChildrenIfShould (TriggerType.Activatable, true, this);
				else
					ActivationBehaviourOption.ActivateChildrenIfShould (TriggerType.Unactivatable, true, this);
			}
		}

		public void SetCollidable (bool collidable)
		{
			if (collidable != this.collidable)
			{
				if (collidable)
					ActivationBehaviourOption.ActivateChildrenIfShould (TriggerType.Collidable, false, this);
				else
					ActivationBehaviourOption.ActivateChildrenIfShould (TriggerType.Uncollidable, false, this);
				collider.isTrigger = !collidable;
				Material material = new Material(renderer.sharedMaterial);
				if (collidable)
					StartCoroutine(UpdateOptionConnectionArrowsRoutine ());
				renderer.sharedMaterial = material;
				this.collidable = collidable;
				if (collidable)
					ActivationBehaviourOption.ActivateChildrenIfShould (TriggerType.Collidable, true, this);
				else
					ActivationBehaviourOption.ActivateChildrenIfShould (TriggerType.Uncollidable, true, this);
			}
		}

		public void HandleNaming ()
		{
			string newName = text.text;
			int indexOfNameToValueSeparator = newName.IndexOf(NameToValueSeparator);
			if (indexOfNameToValueSeparator != -1)
				newName = newName.Remove(indexOfNameToValueSeparator);
			SetName (newName);
		}

		public void SetName (string name)
		{
			if (this.name.Equals(name))
				return;
#if UNITY_EDITOR
			if (!EditorSceneManager.IsPreviewScene(gameObject.scene) && EternityEngine.Instance != null)
			{
#endif
				EternityEngine.Instance.optionNamesDict.Remove(this);
				if (EternityEngine.instance.optionNamesDict.ContainsValue(name))
				{
					int nameOccuranceCount = 1;
					while (EternityEngine.instance.optionNamesDict.ContainsValue(name + " (" + nameOccuranceCount + ")"))
						nameOccuranceCount ++;
					name += " (" + nameOccuranceCount + ")";
				}
				EternityEngine.instance.optionNamesDict[this] = name;
#if UNITY_EDITOR
			}
#endif
			this.name = name;
			string nameAndValue = name;
			string value = GetValue();
			if (value != null)
				nameAndValue += NameToValueSeparator + value;
			text.text = nameAndValue;
		}

		public virtual string GetValue ()
		{
			string output = null;
			int indexOfNameToValueSeparator = text.text.IndexOf(NameToValueSeparator);
			if (indexOfNameToValueSeparator != -1)
				output = text.text.Substring(indexOfNameToValueSeparator + NameToValueSeparator.Length);
			return output;
		}

		public virtual bool SetValue (string value)
		{
			if (value.Equals(GetValue()))
			{
				ActivationBehaviourOption.ActivateChildrenIfShould (TriggerType.ValueSetToSame, false, this);
				ActivationBehaviourOption.ActivateChildrenIfShould (TriggerType.ValueSetToSame, true, this);
				return false;
			}
			ActivationBehaviourOption.ActivateChildrenIfShould (TriggerType.ValueChange, false, this);
			text.text = name + NameToValueSeparator + value;
			ActivationBehaviourOption.ActivateChildrenIfShould (TriggerType.ValueChange, true, this);
			return true;
		}

		public bool AddChild (Option childOption)
		{
			if (onAboutToAddChild != null && !onAboutToAddChild(childOption))
			{
				for (int i = 0; i < childOption.parentingArrows.Count; i ++)
				{
					ParentingArrow parentingArrow = childOption.parentingArrows[i];
					if (parentingArrow.parent == this)
					{
						ObjectPool.instance.Despawn (parentingArrow.prefabIndex, parentingArrow.gameObject, parentingArrow.trs);
						childOption.parentingArrows.RemoveAt(i);
						break;
					}
				}
				return false;
			}
			ActivationBehaviourOption.ActivateChildrenIfShould (TriggerType.AddChild, false, this);
			children.Add(childOption);
			if (onAddChild != null)
				onAddChild (childOption);
			ActivationBehaviourOption.ActivateChildrenIfShould (TriggerType.AddChild, true, this);
			return true;
		}

		public bool RemoveChild (Option childOption)
		{
			if (onAboutToRemoveChild != null && !onAboutToRemoveChild(childOption))
				return false;
			ActivationBehaviourOption.ActivateChildrenIfShould (TriggerType.RemoveChild, false, this);
			children.Remove(childOption);
			for (int i = 0; i < childOption.parentingArrows.Count; i ++)
			{
				ParentingArrow parentingArrow = childOption.parentingArrows[i];
				if (parentingArrow.parent == this)
				{
					ObjectPool.instance.Despawn (parentingArrow.prefabIndex, parentingArrow.gameObject, parentingArrow.trs);
					childOption.parentingArrows.RemoveAt(i);
					break;
				}
			}
			childOption.trs.SetParent(EternityEngine.instance.sceneTrs);
			if (onRemoveChild != null)
				onRemoveChild (childOption);
			ActivationBehaviourOption.ActivateChildrenIfShould (TriggerType.RemoveChild, true, this);
			return true;
		}

		public virtual void OnAddedOrRemovedChild (Option childOption)
		{
			EternityEngine.Hand hand = EternityEngine.instance.leftHand;
			if (hand.parentingArrow.parent != this || hand.parentingArrow.child != childOption)
				hand = EternityEngine.instance.rightHand;
			List<AutomationOption> automationOptions = AutomationOption.recordingParentingOptionsDict[this];
			for (int i = 0; i < automationOptions.Count; i ++)
			{
				AutomationOption automationOption = automationOptions[i];
				if (!automationOption.pauseRecording && ((automationOption.recordLeftHand && hand.isLeftHand) || (automationOption.recordRightHand && !hand.isLeftHand)))
				{
					AutomationSequence.SequenceEntry sequenceEntry = new AutomationSequence.SequenceEntry();
					// if (recordingActivateInAutomationSequence)
					// 	onActivated = null;
					SetData ();
					// if (recordingActivateInAutomationSequence)
					// 	onActivated += (EternityEngine.Hand hand) => { OnActivated (hand); };
					bool spawnIfInvalid = automationOption.invalidOptionFixBehaviour == AutomationSequence.InvalidOptionFixBehaviour.Spawn;
					sequenceEntry.parentOption = new AutomationSequence.SequenceEntry.OptionEntry(_Data, spawnIfInvalid);
					sequenceEntry.childOption = new AutomationSequence.SequenceEntry.OptionEntry(childOption._Data, spawnIfInvalid);
					sequenceEntry.time = Time.time - automationOption.recordingStartTime;
					automationOption.automationSequence.sequenceEntries.Add(sequenceEntry);
				}
			}
		}

		public virtual void OnDisable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			instances.Remove(this);
			if (!GameManager.isQuitting)
			{
				ActivationBehaviourOption.ActivateChildrenIfShould (TriggerType.Hidden, false, this);
				for (int i = 0; i < parentingArrows.Count; i ++)
				{
					ParentingArrow parentingArrow = parentingArrows[i];
					parentingArrow.gameObject.SetActive(false);
				}
				for (int i = 0; i < children.Count; i ++)
				{
					Option child = children[i];
					for (int i2 = 0; i2 < child.parentingArrows.Count; i2 ++)
					{
						ParentingArrow parentingArrow = child.parentingArrows[i2];
						if (parentingArrow != null && parentingArrow.parent == this)
						{
							parentingArrow.gameObject.SetActive(false);
							break;
						}
					}
				}
				ActivationBehaviourOption.ActivateChildrenIfShould (TriggerType.Hidden, true, this);
			}
		}

		void OnDestroy ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
			// Selection.selectionChanged -= OnSelectionChanged;
#endif
			if (!deleted && !GameManager.isQuitting)
				OnDeleted ();
		}

		public virtual void OnDeleted ()
		{
			deleted = true;
			ActivationBehaviourOption.ActivateChildrenIfShould (TriggerType.Delete, false, this);
			for (int i = 0; i < parentingArrows.Count; i ++)
			{
				ParentingArrow parentingArrow = parentingArrows[i];
				parentingArrow.parent.RemoveChild (this);
				i --;
			}
			if (EternityEngine.Hand.grabbingOption == this)
				EternityEngine.Hand.DropGrabbedTransform ();
			// AutomationOption.recordingActivateOptionsDict.Remove(this);
			// AutomationOption.recordingParentingOptionsDict.Remove(this);
			ActivationBehaviourOption.ActivateChildrenIfShould (TriggerType.Delete, true, this);
		}

		public void Hide ()
		{
			if (!gameObject.activeSelf)
				return;
			for (int i = 0; i < children.Count; i ++)
			{
				Option child = children[i];
				child.trs.SetParent(EternityEngine.instance.sceneTrs);
				child.hiddenParentCount ++;
				child.hasHiddenParentIndicator.SetActive(true);
			}
			for (int i = 0; i < parentingArrows.Count; i ++)
			{
				ParentingArrow parentingArrow = parentingArrows[i];
				Option parent = parentingArrow.parent;
				parent.hiddenChildCount ++;
				parent.hasHiddenChildIndicator.SetActive(true);
			}
			gameObject.SetActive(false);
		}

		public void HideChildren ()
		{
			for (int i = 0; i < children.Count; i ++)
			{
				Option childOption = children[i];
				childOption.Hide ();
			}
		}

		public void Show ()
		{
			if (gameObject.activeSelf)
				return;
			for (int i = 0; i < children.Count; i ++)
			{
				Option child = children[i];
				child.hiddenParentCount --;
				child.hasHiddenParentIndicator.SetActive(child.hiddenParentCount > 0);
			}
			for (int i = 0; i < parentingArrows.Count; i ++)
			{
				ParentingArrow parentingArrow = parentingArrows[i];
				Option parent = parentingArrow.parent;
				parent.hiddenChildCount --;
				parent.hasHiddenChildIndicator.SetActive(parent.hiddenChildCount > 0);
			}
			gameObject.SetActive(true);
		}

		public void ShowChildren ()
		{
			for (int i = 0; i < children.Count; i ++)
			{
				Option childOption = children[i];
				childOption.Show ();
			}
		}

		public virtual void Delete ()
		{
			if (!gameObject.activeSelf)
			{
				for (int i = 0; i < children.Count; i ++)
				{
					Option child = children[i];
					child.trs.SetParent(EternityEngine.instance.sceneTrs);
					child.hiddenParentCount --;
					child.hasHiddenParentIndicator.SetActive(child.hiddenParentCount > 0);
				}
				for (int i = 0; i < parentingArrows.Count; i ++)
				{
					ParentingArrow parentingArrow = parentingArrows[i];
					Option parent = parentingArrow.parent;
					parent.hiddenChildCount --;
					parent.hasHiddenChildIndicator.SetActive(parent.hiddenChildCount > 0);
				}
			}
			DestroyImmediate(gameObject);
		}

		public void DeleteChildren ()
		{
			for (int i = 0; i < children.Count; i ++)
			{
				Option child = children[0];
				child.Delete ();
				i --;
			}
		}

		public void ToggleChildrenCollidable ()
		{
			for (int i = 0; i < children.Count; i ++)
			{
				Option child = children[i];
				child.SetCollidable (!child.collidable);
			}
		}

		public void SetChildrenCollidable ()
		{
			for (int i = 0; i < children.Count; i ++)
			{
				Option child = children[i];
				child.SetCollidable (true);
			}
		}

		public void SetChildrenUncollidable ()
		{
			for (int i = 0; i < children.Count; i ++)
			{
				Option child = children[i];
				child.SetCollidable (false);
			}
		}

		public void Toggle ()
		{
			if (gameObject.activeSelf)
				Hide ();
			else
				Show ();
		}

		public void ToggleChildren ()
		{
			for (int i = 0; i < children.Count; i ++)
			{
				Option childOption = children[i];
				childOption.Toggle ();
			}
		}

		public ParentingArrow AddParentingArrowToChild (Option child)
		{
			ParentingArrow parentingArrow = ObjectPool.Instance.SpawnComponent<ParentingArrow>(EternityEngine.instance.parentingArrowPrefab.prefabIndex, parent:child.trs);
			parentingArrow.pointsTo = child.trs;
			parentingArrow.child = child;
			parentingArrow.parent = this;
			parentingArrow.DoUpdate ();
			parentingArrow.gameObject.SetActive(child.gameObject.activeSelf && gameObject.activeSelf);
			child.parentingArrows.Add(parentingArrow);
			return parentingArrow;
		}

		public ParentingArrow AddParentingArrowToChild (Option child, Material material)
		{
			ParentingArrow parentingArrow = AddParentingArrowToChild(child);
			parentingArrow.lineRenderer.sharedMaterial = material;
			return parentingArrow;
		}

		public static Option[] GetAllChildrenAndSelf (Option option)
		{
			List<Option> output = new List<Option>();
			List<Option> checkedOptions = new List<Option>() { option };
			List<Option> remainingOptions = new List<Option>() { option };
			while (remainingOptions.Count > 0)
			{
				Option option2 = remainingOptions[0];
				if (Exists(option2))
				{
					output.Add(option2);
					for (int i = 0; i < option2.children.Count; i ++)
					{
						Option connectedOption = option2.children[i];
						if (!checkedOptions.Contains(connectedOption))
						{
							checkedOptions.Add(connectedOption);
							remainingOptions.Add(connectedOption);
						}
					}
				}
				remainingOptions.RemoveAt(0);
			}
			return output.ToArray();
		}

		public static bool Exists (Option option)
		{
			return option != null;
		}

		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			SetTextOfData ();
			SetNameOfData ();
			SetActiveOfData ();
			SetActivatableOfData ();
			SetPositionOfData ();
			SetEulerAnglesOfData ();
			SetSizeOfData ();
			SetChildNamesOfData ();
			SetCollidableOfData ();
			SetTextColorOfData ();
			SetTextSizeOfData ();
		}

		public void SetTextOfData ()
		{
			_Data.text = text.text;
		}

		public void SetTextFromData ()
		{
			text.text = _Data.text;
		}

		public void SetNameOfData ()
		{
			_Data.name = name;
		}

		public void SetNameFromData ()
		{
			HandleNaming ();
		}

		public void SetActiveOfData ()
		{
			_Data.active = gameObject.activeSelf;
		}

		public void SetActiveFromData ()
		{
			gameObject.SetActive(_Data.active);
		}

		public void SetActivatableOfData ()
		{
			_Data.activatable = activatable;
		}

		public void SetActivatableFromData ()
		{
			activatable = _Data.activatable;
		}

		public void SetPositionOfData ()
		{
			_Data.position = _Vector3.FromVec3(trs.position);
		}

		public void SetPositionFromData ()
		{
			trs.position = _Data.position.ToVec3();
		}

		public void SetEulerAnglesOfData ()
		{
			_Data.eulerAngles = _Vector3.FromVec3(trs.eulerAngles);
		}

		public void SetEulerAnglesFromData ()
		{
			trs.eulerAngles = _Data.eulerAngles.ToVec3();
		}

		public void SetSizeOfData ()
		{
			_Data.size = trs.lossyScale.x;
		}

		public void SetSizeFromData ()
		{
			trs.SetWorldScale (Vector3.one * _Data.size);
		}

		public void SetChildNamesOfData ()
		{
			_Data.childNames = new string[children.Count];
			for (int i = 0; i < children.Count; i ++)
			{
				Option child = children[i];
				_Data.childNames[i] = child.name;
			}
		}

		public void SetChildNamesFromData ()
		{
			children.Clear();
			for (int i = 0; i < _Data.childNames.Length; i ++)
			{
				string name = _Data.childNames[i];
				children.Add(EternityEngine.GetOption(name));
			}
		}

		public void SetCollidableOfData ()
		{
			_Data.collidable = collidable;
		}

		public void SetCollidableFromData ()
		{
			SetCollidable (_Data.collidable);
		}

		public void SetTextColorOfData ()
		{
			_Data.textColor = _Color.FromColor(text.color);
		}

		public void SetTextColorFromData ()
		{
			text.color = _Data.textColor.ToColor();
		}

		public void SetTextSizeOfData ()
		{
			_Data.textSize = textTrs.localScale.y;
		}

		public void SetTextSizeFromData ()
		{
			textTrs.localScale = Vector3.one * _Data.textSize;
		}

		public enum ValueType
		{
			None,
			Bool,
			Int,
			Float,
			Text,
			Enum,
			Vector3,
			Color,
			Transform
		}

		[Flags]
		public enum TriggerType
		{
			NewFrame = 1,
			Startup = 2,
			Load = 4,
			Quit = 8,
			Save = 16,
			GrabWorld = 32,
			DropWorld = 64,
			Delete = 128,
			AddChild = 256,
			RemoveChild = 512,
			Hidden = 1024,
			Visible = 2048,
			Collidable = 4096,
			Uncollidable = 8192,
			NameChange = 16384,
			InvalidNameChangeAttempt = 32768,
			ValueChange = 65536,
			ValueSetToSame = 131072,
			TypeCharacter = 262144,
			TypeBackspace = 524288,
			ChangeTypingTarget = 1048576,
			Activatable = 2097152,
			Unactivatable = 4194304,
			Duplicate = 8388608,
			GrabNode = 16777216,
			DropNode = 33554432,
			Activate = 67108864,
			Vision = 134217728,
			SpawnNode = 268435456
		}

		[Serializable]
		public class Data : Asset.Data
		{
			public string text;
			public bool active;
			public bool activatable;
			public _Vector3 position;
			public _Vector3 eulerAngles;
			public float size;
			public string[] childNames = new string[0];
			public bool collidable;
			public _Color textColor;
			public float textSize;
			
			public override object MakeAsset ()
			{
				Option option = ObjectPool.instance.SpawnComponent<Option>(EternityEngine.instance.optionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (option);
				option.Init ();
				return option;
			}

			public override void Apply (Asset asset)
			{
				Option option = (Option) asset;
				option._Data = this;
				option.SetTextFromData ();
				option.SetNameFromData ();
				option.SetActiveFromData ();
				option.SetActivatableFromData ();
				option.SetPositionFromData ();
				option.SetEulerAnglesFromData ();
				option.SetSizeFromData ();
				option.SetChildNamesFromData ();
				option.SetCollidableFromData ();
				option.SetTextColorFromData ();
				option.SetTextSizeFromData ();
			}
		}
	}
}