using System;

namespace _EternityEngine
{
	public class SoundMaxDistanceOption : TypingTargetOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public SoundFileInstanceOption soundFileInstanceOption;

		public override bool SetValue (string value)
		{
			if (base.SetValue(value))
			{
				if (Exists(soundFileInstanceOption))
				{
					float? maxDistance = GetNumberValue();
					if (maxDistance != null)
						soundFileInstanceOption.SetMaxDistance ((float) maxDistance);
				}
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetSoundFileInstanceOptionNameOfData ();
		}

		void SetSoundFileInstanceOptionNameOfData ()
		{
			if (Exists(soundFileInstanceOption))
				_Data.soundFileInstanceOptionName = soundFileInstanceOption.name;
		}

		void SetSoundFileInstanceOptionNameFromData ()
		{
			if (_Data.soundFileInstanceOptionName != null)
				soundFileInstanceOption = EternityEngine.GetOption<SoundFileInstanceOption>(_Data.soundFileInstanceOptionName);
		}

		[Serializable]
		public class Data : TypingTargetOption.Data
		{
			public string soundFileInstanceOptionName = null;

			public override object MakeAsset ()
			{
				SoundMaxDistanceOption soundMaxDistanceOption = ObjectPool.instance.SpawnComponent<SoundMaxDistanceOption>(EternityEngine.instance.soundMaxDistanceOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (soundMaxDistanceOption);
				soundMaxDistanceOption.Init ();
				return soundMaxDistanceOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				SoundMaxDistanceOption soundMaxDistanceOption = (SoundMaxDistanceOption) asset;
				soundMaxDistanceOption._Data = this;
				soundMaxDistanceOption.SetSoundFileInstanceOptionNameFromData ();
			}
		}
	}
}
