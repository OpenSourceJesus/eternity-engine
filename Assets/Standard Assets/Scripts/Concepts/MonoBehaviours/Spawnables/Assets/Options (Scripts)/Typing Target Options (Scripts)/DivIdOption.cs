using System;

namespace _EternityEngine
{
	public class DivIdOption : TypingTargetOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public DivOption divOption;

		public override bool SetValue (string value)
		{
			if (base.SetValue(value))
			{
				if (Exists(divOption))
					divOption.SetId (value);
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetDivOptionNameOfData ();
		}

		void SetDivOptionNameOfData ()
		{
			if (Exists(divOption))
				_Data.divOptionName = divOption.name;
		}

		void SetDivOptionNameFromData ()
		{
			if (_Data.divOptionName != null)
				divOption = EternityEngine.GetOption<DivOption>(_Data.divOptionName);
		}

		[Serializable]
		public class Data : TypingTargetOption.Data
		{
			public string divOptionName = null;

			public override object MakeAsset ()
			{
				DivIdOption divIdOption = ObjectPool.instance.SpawnComponent<DivIdOption>(EternityEngine.instance.divIdOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (divIdOption);
				divIdOption.Init ();
				return divIdOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				DivIdOption divIdOption = (DivIdOption) asset;
				divIdOption._Data = this;
				divIdOption.SetDivOptionNameFromData ();
			}
		}
	}
}
