using System;

namespace _EternityEngine
{
	public class LightIntensityOption : TypingTargetOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public LightOption lightOption;

		public override bool SetValue (string value)
		{
			if (base.SetValue(value))
			{
				if (Exists(lightOption))
				{
					float? intensity = GetNumberValue();
					if (intensity != null)
						lightOption.SetIntensity ((float) intensity);
				}
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetLightOptionNameOfData ();
		}

		void SetLightOptionNameOfData ()
		{
			if (Exists(lightOption))
				_Data.lightOptionName = lightOption.name;
		}

		void SetLightOptionNameFromData ()
		{
			if (_Data.lightOptionName != null)
				lightOption = EternityEngine.GetOption<LightOption>(_Data.lightOptionName);
		}

		[Serializable]
		public class Data : TypingTargetOption.Data
		{
			public string lightOptionName = null;

			public override object MakeAsset ()
			{
				LightIntensityOption lightIntensityOption = ObjectPool.instance.SpawnComponent<LightIntensityOption>(EternityEngine.instance.lightIntensityOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (lightIntensityOption);
				lightIntensityOption.Init ();
				return lightIntensityOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				LightIntensityOption lightIntensityOption = (LightIntensityOption) asset;
				lightIntensityOption._Data = this;
				lightIntensityOption.SetLightOptionNameFromData ();
			}
		}
	}
}
