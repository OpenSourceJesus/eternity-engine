using System;

namespace _EternityEngine
{
	public class DuplicateChildrenKeepParentingArrowsBehaviourOption : EnumOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public DuplicateChildrenOption duplicateChildrenOption;

		public override bool SetValue (int value)
		{
			if (base.SetValue(value))
			{
				if (Exists(duplicateChildrenOption))
					duplicateChildrenOption.keepParentingArrowsBehaviour = (DuplicateChildrenOption.KeepParentingArrowsBehaviour) Enum.ToObject(enumType, value);
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetDuplicateChildrenOptionNameOfData ();
		}

		void SetDuplicateChildrenOptionNameOfData ()
		{
			if (Exists(duplicateChildrenOption))
				_Data.duplicateChildrenOptionName = duplicateChildrenOption.name;
		}

		void SetDuplicateChildrenOptionNameFromData ()
		{
			if (_Data.duplicateChildrenOptionName != null)
				duplicateChildrenOption = EternityEngine.GetOption<DuplicateChildrenOption>(_Data.duplicateChildrenOptionName);
		}

		[Serializable]
		public class Data : EnumOption.Data
		{
			public string duplicateChildrenOptionName = null;

			public override object MakeAsset ()
			{
				DuplicateChildrenKeepParentingArrowsBehaviourOption duplicateChildrenKeepParentingArrowsBehaviourOption = ObjectPool.instance.SpawnComponent<DuplicateChildrenKeepParentingArrowsBehaviourOption>(EternityEngine.instance.duplicateChildrenKeepParentingArrowsBehaviourOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (duplicateChildrenKeepParentingArrowsBehaviourOption);
				duplicateChildrenKeepParentingArrowsBehaviourOption.Init ();
				return duplicateChildrenKeepParentingArrowsBehaviourOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				DuplicateChildrenKeepParentingArrowsBehaviourOption duplicateChildrenKeepParentingArrowsBehaviourOption = (DuplicateChildrenKeepParentingArrowsBehaviourOption) asset;
				duplicateChildrenKeepParentingArrowsBehaviourOption._Data = this;
				duplicateChildrenKeepParentingArrowsBehaviourOption.SetDuplicateChildrenOptionNameFromData ();
			}
		}
	}
}