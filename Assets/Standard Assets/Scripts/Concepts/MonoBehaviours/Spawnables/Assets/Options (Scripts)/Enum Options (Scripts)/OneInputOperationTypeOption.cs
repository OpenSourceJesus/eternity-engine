using System;

namespace _EternityEngine
{
	public class OneInputOperationTypeOption : EnumOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public OneInputOperationsOption oneInputOperationsOption;

		public override bool SetValue (int value)
		{
			if (base.SetValue(value))
			{
				if (Exists(oneInputOperationsOption))
					oneInputOperationsOption.SetOperationType ((OneInputOperationsOption.OperationType) Enum.ToObject(enumType, value));
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetOneInputOperationsOptionNameOfData ();
		}

		void SetOneInputOperationsOptionNameOfData ()
		{
			if (Exists(oneInputOperationsOption))
				_Data.oneInputOperationsOptionName = oneInputOperationsOption.name;
		}

		void SetOneInputOperationsOptionNameFromData ()
		{
			if (_Data.oneInputOperationsOptionName != null)
				oneInputOperationsOption = EternityEngine.GetOption<OneInputOperationsOption>(_Data.oneInputOperationsOptionName);
		}

		[Serializable]
		public class Data : EnumOption.Data
		{
			public string oneInputOperationsOptionName = null;

			public override object MakeAsset ()
			{
				OneInputOperationTypeOption arithmeticOperationTypeOption = ObjectPool.instance.SpawnComponent<OneInputOperationTypeOption>(EternityEngine.instance.oneInputOperationTypeOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (arithmeticOperationTypeOption);
				arithmeticOperationTypeOption.Init ();
				return arithmeticOperationTypeOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				OneInputOperationTypeOption arithmeticOperationTypeOption = (OneInputOperationTypeOption) asset;
				arithmeticOperationTypeOption._Data = this;
				arithmeticOperationTypeOption.SetOneInputOperationsOptionNameFromData ();
			}
		}
	}
}