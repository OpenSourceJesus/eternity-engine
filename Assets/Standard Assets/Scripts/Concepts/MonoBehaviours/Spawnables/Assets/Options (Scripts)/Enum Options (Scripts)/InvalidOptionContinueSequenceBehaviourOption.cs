using System;

namespace _EternityEngine
{
	public class InvalidOptionContinueSequenceBehaviourOption : EnumOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public AutomationOption automationOption;

		public override bool SetValue (int value)
		{
			if (base.SetValue(value))
			{
				if (Exists(automationOption))
					automationOption.invalidOptionContinueSequenceBehaviour = (AutomationSequence.InvalidOptionContinueSequenceBehaviour) Enum.ToObject(enumType, value);
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetAutomationOptionNameOfData ();
		}

		void SetAutomationOptionNameOfData ()
		{
			if (Exists(automationOption))
				_Data.automationOptionName = automationOption.name;
		}

		void SetAutomationOptionNameFromData ()
		{
			if (_Data.automationOptionName != null)
				automationOption = EternityEngine.GetOption<AutomationOption>(_Data.automationOptionName);
		}

		[Serializable]
		public class Data : EnumOption.Data
		{
			public string automationOptionName = null;

			public override object MakeAsset ()
			{
				InvalidOptionContinueSequenceBehaviourOption invalidOptionContinueSequenceBehaviourOption = ObjectPool.instance.SpawnComponent<InvalidOptionContinueSequenceBehaviourOption>(EternityEngine.instance.invalidOptionContinueSequenceBehaviourOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (invalidOptionContinueSequenceBehaviourOption);
				// if (!invalidOptionContinueSequenceBehaviourOption.isInitialized)
				// 	invalidOptionContinueSequenceBehaviourOption.Init ();
				return invalidOptionContinueSequenceBehaviourOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				InvalidOptionContinueSequenceBehaviourOption invalidOptionContinueSequenceBehaviourOption = (InvalidOptionContinueSequenceBehaviourOption) asset;
				invalidOptionContinueSequenceBehaviourOption._Data = this;
				invalidOptionContinueSequenceBehaviourOption.SetAutomationOptionNameFromData ();
			}
		}
	}
}