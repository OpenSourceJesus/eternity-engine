using System;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class CodeCommandRunOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public CodeCommandOption codeCommandOption;
		
		public override void OnActivate (EternityEngine.Hand hand)
		{
			CodeRunner.RunCodeCommandOnce ("CodeCommand" + codeCommandOption.uniqueId, codeCommandOption.contents, usingStatements:"using Extensions;\nusing System.Collections.Generic;");
			// base.OnActivate (hand);
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetCodeCommandOptionNameOfData ();
		}

		void SetCodeCommandOptionNameOfData ()
		{
			if (Exists(codeCommandOption))
				_Data.codeCommandOptionName = codeCommandOption.name;
		}

		void SetCodeCommandOptionNameFromData ()
		{
			if (_Data.codeCommandOptionName != null)
				codeCommandOption = EternityEngine.GetOption<CodeCommandOption>(_Data.codeCommandOptionName);
		}

		[Serializable]
		public class Data : Option.Data
		{
			public string codeCommandOptionName = null;

			public override object MakeAsset ()
			{
				CodeCommandRunOption codeCommandRunOption = ObjectPool.instance.SpawnComponent<CodeCommandRunOption>(EternityEngine.instance.codeCommandRunOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (codeCommandRunOption);
				codeCommandRunOption.Init ();
				return codeCommandRunOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				CodeCommandRunOption codeCommandRunOption = (CodeCommandRunOption) asset;
				codeCommandRunOption._Data = this;
				codeCommandRunOption.SetCodeCommandOptionNameFromData ();
			}
		}
	}
}