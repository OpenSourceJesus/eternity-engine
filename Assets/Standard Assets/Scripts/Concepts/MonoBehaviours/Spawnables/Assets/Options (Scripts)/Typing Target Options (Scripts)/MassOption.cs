using System;

namespace _EternityEngine
{
	public class MassOption : TypingTargetOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public ModelFileInstanceOption modelFileInstanceOption;

		public override bool SetValue (string value)
		{
			if (base.SetValue(value))
			{
				if (Exists(modelFileInstanceOption))
				{
					float? mass = GetNumberValue();
					if (mass != null)
						modelFileInstanceOption.SetMass ((float) mass);
				}
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetModelFileInstanceOptionNameOfData ();
		}

		void SetModelFileInstanceOptionNameOfData ()
		{
			if (Exists(modelFileInstanceOption))
				_Data.modelFileInstanceOptionName = modelFileInstanceOption.name;
		}

		void SetModelFileInstanceOptionNameFromData ()
		{
			if (_Data.modelFileInstanceOptionName != null)
				modelFileInstanceOption = EternityEngine.GetOption<ModelFileInstanceOption>(_Data.modelFileInstanceOptionName);
		}

		[Serializable]
		public class Data : TypingTargetOption.Data
		{
			public string modelFileInstanceOptionName = null;

			public override object MakeAsset ()
			{
				MassOption massOption = ObjectPool.instance.SpawnComponent<MassOption>(EternityEngine.instance.massOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (massOption);
				massOption.Init ();
				return massOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				MassOption massOption = (MassOption) asset;
				massOption._Data = this;
				massOption.SetModelFileInstanceOptionNameFromData ();
			}
		}
	}
}