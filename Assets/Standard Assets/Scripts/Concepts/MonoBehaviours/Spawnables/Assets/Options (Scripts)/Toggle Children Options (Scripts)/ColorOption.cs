using System;
using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class ColorOption : ToggleChildrenOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public Color value;

		public virtual bool SetR (float r)
		{
			if (value.r == r)
				return false;
			value.r = r;
			return true;
		}

		public virtual bool SetG (float g)
		{
			if (value.g == g)
				return false;
			value.g = g;
			return true;
		}

		public virtual bool SetB (float b)
		{
			if (value.b == b)
				return false;
			value.b = b;
			return true;
		}

		public virtual bool SetA (float a)
		{
			if (value.a == a)
				return false;
			value.a = a;
			return true;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetValueOfData ();
		}

		void SetValueOfData ()
		{
			_Data.value = _Color.FromColor(value);
		}

		void SetValueFromData ()
		{
			value = _Data.value.ToColor();
		}

		[Serializable]
		public class Data : ToggleChildrenOption.Data
		{
			public _Color value;

			public override object MakeAsset ()
			{
				ColorOption colorOption = ObjectPool.instance.SpawnComponent<ColorOption>(EternityEngine.instance.colorOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (colorOption);
				colorOption.Init ();
				return colorOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				ColorOption colorOption = (ColorOption) asset;
				colorOption._Data = this;
				colorOption.SetValueFromData ();
			}
		}
	}
}