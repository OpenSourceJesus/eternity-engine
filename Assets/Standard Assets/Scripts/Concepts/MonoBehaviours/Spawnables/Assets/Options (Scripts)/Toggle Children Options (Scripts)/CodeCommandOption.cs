using System;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class CodeCommandOption : ToggleChildrenOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public string contents;
		public bool runRepeatedly;
		[HideInInspector]
		public uint uniqueId;
		static uint lastUniqueId;

		public override void Init ()
		{
			base.Init ();
			uniqueId = lastUniqueId;
			lastUniqueId ++;
		}

		public void SetRunRepeatedly (bool runRepeatedly)
		{
			this.runRepeatedly = runRepeatedly;
			if (runRepeatedly)
				CodeRunner.StartRunningCodeCommand ("CodeCommand" + uniqueId, contents, usingStatements:"using Extensions;\nusing System.Collections.Generic;");
			else
				CodeRunner.StopRunningCodeCommand ("CodeCommand" + uniqueId);
		}

		public void SetContents (string contents)
		{
			this.contents = contents;
			if (runRepeatedly)
			{
				CodeRunner.StopRunningCodeCommand ("CodeCommand" + uniqueId);
				CodeRunner.StartRunningCodeCommand ("CodeCommand" + uniqueId, contents, usingStatements:"using Extensions;\nusing System.Collections.Generic;");
			}
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
		}

		[Serializable]
		public class Data : ToggleChildrenOption.Data
		{
			public override object MakeAsset ()
			{
				CodeCommandOption codeCommandOption = ObjectPool.instance.SpawnComponent<CodeCommandOption>(EternityEngine.instance.codeCommandOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (codeCommandOption);
				codeCommandOption.Init ();
				return codeCommandOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				CodeCommandOption codeCommandOption = (CodeCommandOption) asset;
				codeCommandOption._Data = this;
			}
		}
	}
}