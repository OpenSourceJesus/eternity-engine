using System;

namespace _EternityEngine
{
	public class SetVector3XOption : TypingTargetOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public Vector3Option vector3Option;

		public override bool SetValue (string value)
		{
			if (base.SetValue(value))
			{
				if (Exists(vector3Option))
				{
					float? x = GetNumberValue();
					if (x != null)
						vector3Option.SetX ((float) x);
				}
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetVector3OptionNameOfData ();
		}

		void SetVector3OptionNameOfData ()
		{
			if (Exists(vector3Option))
				_Data.vector3OptionName = vector3Option.name;
		}

		void SetVector3OptionNameFromData ()
		{
			if (_Data.vector3OptionName != null)
				vector3Option = EternityEngine.GetOption<Vector3Option>(_Data.vector3OptionName);
		}

		[Serializable]
		public class Data : TypingTargetOption.Data
		{
			public string vector3OptionName = null;

			public override object MakeAsset ()
			{
				SetVector3XOption setVector3XOption = ObjectPool.instance.SpawnComponent<SetVector3XOption>(EternityEngine.instance.setVector3XOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (setVector3XOption);
				setVector3XOption.Init ();
				return setVector3XOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				SetVector3XOption setVector3XOption = (SetVector3XOption) asset;
				setVector3XOption._Data = this;
				setVector3XOption.SetVector3OptionNameFromData ();
			}
		}
	}
}