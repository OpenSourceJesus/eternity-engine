using System;

namespace _EternityEngine
{
	public class AutomationTimeMultiplierOption : TypingTargetOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public AutomationOption automationOption;

		public override bool SetValue (string value)
		{
			if (base.SetValue(value))
			{
				if (Exists(automationOption))
				{
					float? timeMultiplier = GetNumberValue();
					if (timeMultiplier != null)
						automationOption.automationSequence.timeMultiplier = (float) timeMultiplier;
				}
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetAutomationOptionNameOfData ();
		}

		void SetAutomationOptionNameOfData ()
		{
			if (Exists(automationOption))
				_Data.automationOptionName = automationOption.name;
		}

		void SetAutomationOptionNameFromData ()
		{
			if (_Data.automationOptionName != null)
				automationOption = EternityEngine.GetOption<AutomationOption>(_Data.automationOptionName);
		}

		[Serializable]
		public class Data : TypingTargetOption.Data
		{
			public string automationOptionName = null;

			public override object MakeAsset ()
			{
				AutomationTimeMultiplierOption automationTimeMultiplierOption = ObjectPool.instance.SpawnComponent<AutomationTimeMultiplierOption>(EternityEngine.instance.automationTimeMultiplierOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (automationTimeMultiplierOption);
				automationTimeMultiplierOption.Init ();
				return automationTimeMultiplierOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				AutomationTimeMultiplierOption automationTimeMultiplierOption = (AutomationTimeMultiplierOption) asset;
				automationTimeMultiplierOption._Data = this;
				automationTimeMultiplierOption.SetAutomationOptionNameFromData ();
			}
		}
	}
}
