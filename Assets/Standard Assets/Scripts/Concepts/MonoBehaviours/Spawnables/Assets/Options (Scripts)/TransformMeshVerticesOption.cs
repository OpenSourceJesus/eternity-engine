using System;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class TransformMeshVerticesOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public Option modelFileInstanceOptionsParent;
		public List<ModelFileInstanceOption> modelFileInstanceOptions = new List<ModelFileInstanceOption>();
		public TransformOption fromTransformOption;
		public TransformOption toTransformOption;

		public override void Init ()
		{
			base.Init ();
			fromTransformOption.Init (EternityEngine.instance.actOnMeshVerticesFromTrs);
			toTransformOption.Init (EternityEngine.instance.actOnMeshVerticesToTrs);
			modelFileInstanceOptionsParent.onAddChild += OnAddChild;
			modelFileInstanceOptionsParent.onAddChild += OnRemoveChild;
		}

		public override void OnDeleted ()
		{
			modelFileInstanceOptionsParent.onAddChild -= OnAddChild;
			modelFileInstanceOptionsParent.onRemoveChild -= OnRemoveChild;
			base.OnDeleted ();
		}


		void OnAddChild (Option child)
		{
			ModelFileInstanceOption modelFileInstanceOption = child as ModelFileInstanceOption;
			if (modelFileInstanceOption != null)
				modelFileInstanceOptions.Add(modelFileInstanceOption);
		}
		
		void OnRemoveChild (Option child)
		{
			ModelFileInstanceOption modelFileInstanceOption = child as ModelFileInstanceOption;
			if (modelFileInstanceOption != null)
				modelFileInstanceOptions.Remove(modelFileInstanceOption);
		}

		public override void OnActivate (EternityEngine.Hand hand)
		{
			for (int i = 0; i < modelFileInstanceOptions.Count; i ++)
			{
				ModelFileInstanceOption modelFileInstanceOption = modelFileInstanceOptions[i];
				PhysicsObject3D physicsObject = modelFileInstanceOption.physicsObject;
				Mesh mesh = physicsObject.meshFilter.mesh;
				List<Vector3> vertices = new List<Vector3>();
				mesh.GetVertices(vertices);
				Transform physicsObjectTrs = physicsObject.trs;
				for (int i2 = 0; i2 < mesh.vertexCount; i2 ++)
				{
					Vector3 vertex = vertices[i2];
					Transform fromTrs = fromTransformOption.referenceTrs;
					float fromTrsSize = fromTrs.lossyScale.x;
					vertex = physicsObjectTrs.TransformPoint(vertex);
					if ((vertex - fromTrs.position).sqrMagnitude < fromTrsSize * fromTrsSize)
					{
						Transform toTrs = toTransformOption.referenceTrs;
						Vector3 positionOffset = toTrs.position - fromTrs.position;
						Quaternion rotation = Quaternion.Euler(QuaternionExtensions.GetDeltaAngles(fromTrs.eulerAngles, toTrs.eulerAngles));
						vertices[i2] = vertex + (rotation * positionOffset * (toTrs.lossyScale.x / fromTrsSize));
					}
				}
				mesh.SetVertices(vertices);
			}
			// base.OnActivate (hand);
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetModelFileInstanceOptionsParentNameOfData ();
			SetModelFileInstanceOptionsNamesOfData ();
			SetFromTransformOptionNameOfData ();
			SetToTransformOptionNameOfData ();
		}
		
		void SetModelFileInstanceOptionsParentNameOfData ()
		{
			if (Exists(modelFileInstanceOptionsParent))
				_Data.modelFileInstanceOptionsParentName = modelFileInstanceOptionsParent.name;
		}

		void SetModelFileInstanceOptionsParentNameFromData ()
		{
			if (_Data.modelFileInstanceOptionsParentName != null)
				modelFileInstanceOptionsParent = EternityEngine.GetOption(_Data.modelFileInstanceOptionsParentName);
		}

		void SetModelFileInstanceOptionsNamesOfData ()
		{
			_Data.modelFileInstanceOptionsNames = new string[modelFileInstanceOptions.Count];
			for (int i = 0; i < modelFileInstanceOptions.Count; i ++)
			{
				ModelFileInstanceOption modelFileInstanceOption = modelFileInstanceOptions[i];
				if (Exists(modelFileInstanceOption))
					_Data.modelFileInstanceOptionsNames[i] = modelFileInstanceOption.name;
			}
		}

		void SetModelFileInstanceOptionsNamesFromData ()
		{
			for (int i = 0; i < _Data.modelFileInstanceOptionsNames.Length; i ++)
			{
				string parentName = _Data.modelFileInstanceOptionsNames[i];
				modelFileInstanceOptions.Add(EternityEngine.GetOption<ModelFileInstanceOption>(parentName));
			}
		}
		
		void SetFromTransformOptionNameOfData ()
		{
			if (Exists(fromTransformOption))
				_Data.fromTransformOptionName = fromTransformOption.name;
		}

		void SetFromTransformOptionNameFromData ()
		{
			if (_Data.fromTransformOptionName != null)
				fromTransformOption = EternityEngine.GetOption<TransformOption>(_Data.fromTransformOptionName);
		}
		
		void SetToTransformOptionNameOfData ()
		{
			if (Exists(fromTransformOption))
				_Data.toTransformOptionName = toTransformOption.name;
		}

		void SetToTransformOptionNameFromData ()
		{
			if (_Data.toTransformOptionName != null)
				toTransformOption = EternityEngine.GetOption<TransformOption>(_Data.toTransformOptionName);
		}

		[Serializable]
		public class Data : Option.Data
		{
			public string[] modelFileInstanceOptionsNames = new string[0];
			public string modelFileInstanceOptionsParentName = null;
			public string fromTransformOptionName = null;
			public string toTransformOptionName = null;

			public override object MakeAsset ()
			{
				TransformMeshVerticesOption transformMeshVerticesOption = ObjectPool.instance.SpawnComponent<TransformMeshVerticesOption>(EternityEngine.instance.transformMeshVerticesOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (transformMeshVerticesOption);
				transformMeshVerticesOption.Init ();
				return transformMeshVerticesOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				TransformMeshVerticesOption transformMeshVerticesOption = (TransformMeshVerticesOption) asset;
				transformMeshVerticesOption._Data = this;
				transformMeshVerticesOption.SetModelFileInstanceOptionsParentNameFromData ();
				transformMeshVerticesOption.SetModelFileInstanceOptionsNamesFromData ();
				transformMeshVerticesOption.SetFromTransformOptionNameFromData ();
				transformMeshVerticesOption.SetToTransformOptionNameFromData ();
			}
		}
	}
}