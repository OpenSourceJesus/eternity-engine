using System;

namespace _EternityEngine
{
	public class StopEmittingSoundOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public SoundFileInstanceOption soundFileInstanceOption;

		public override void OnActivate (EternityEngine.Hand hand)
		{
			if (Exists(soundFileInstanceOption))
				soundFileInstanceOption.EndEmit ();
			// base.OnActivate (hand);
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetSoundFileInstanceOptionNameOfData ();
		}

		void SetSoundFileInstanceOptionNameOfData ()
		{
			if (Exists(soundFileInstanceOption))
				_Data.soundFileInstanceOptionName = soundFileInstanceOption.name;
		}

		void SetSoundFileInstanceOptionNameFromData ()
		{
			if (_Data.soundFileInstanceOptionName != null)
				soundFileInstanceOption = EternityEngine.GetOption<SoundFileInstanceOption>(_Data.soundFileInstanceOptionName);
		}

		[Serializable]
		public class Data : Option.Data
		{
			public string soundFileInstanceOptionName = null;

			public override object MakeAsset ()
			{
				StopEmittingSoundOption stopEmittingSoundOption = ObjectPool.instance.SpawnComponent<StopEmittingSoundOption>(EternityEngine.instance.stopEmittingSoundOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (stopEmittingSoundOption);
				stopEmittingSoundOption.Init ();
				return stopEmittingSoundOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				StopEmittingSoundOption stopEmittingSoundOption = (StopEmittingSoundOption) asset;
				stopEmittingSoundOption._Data = this;
				stopEmittingSoundOption.SetSoundFileInstanceOptionNameFromData ();
			}
		}
	}
}