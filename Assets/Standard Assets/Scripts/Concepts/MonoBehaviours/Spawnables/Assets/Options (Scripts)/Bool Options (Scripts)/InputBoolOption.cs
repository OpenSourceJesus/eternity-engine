using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace _EternityEngine
{
	public class InputBoolOption : BoolOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public bool isForLeftHand;
		public Type type;

		public override void OnActivate (EternityEngine.Hand hand)
		{
			EternityEngine.Hand _hand = EternityEngine.instance.leftHand;
			if (!isForLeftHand)
				_hand = EternityEngine.instance.rightHand;
			if (type == Type.TriggerPressed)
				SetValue (_hand.triggerInput);
			else if (type == Type.GripPressed)
				SetValue (_hand.gripInput);
			else if (type == Type.PrimaryButtonPressed)
				SetValue (_hand.primaryButtonInput);
			else if (type == Type.SecondaryButtonPressed)
				SetValue (_hand.secondaryButtonInput);
			else if (type == Type.AKey)
				SetValue (Keyboard.current != null && Keyboard.current.aKey.isPressed);
			else if (type == Type.BKey)
				SetValue (Keyboard.current != null && Keyboard.current.bKey.isPressed);
			else if (type == Type.CKey)
				SetValue (Keyboard.current != null && Keyboard.current.cKey.isPressed);
			else if (type == Type.DKey)
				SetValue (Keyboard.current != null && Keyboard.current.dKey.isPressed);
			else if (type == Type.EKey)
				SetValue (Keyboard.current != null && Keyboard.current.eKey.isPressed);
			else if (type == Type.FKey)
				SetValue (Keyboard.current != null && Keyboard.current.fKey.isPressed);
			else if (type == Type.GKey)
				SetValue (Keyboard.current != null && Keyboard.current.gKey.isPressed);
			else if (type == Type.HKey)
				SetValue (Keyboard.current != null && Keyboard.current.hKey.isPressed);
			else if (type == Type.IKey)
				SetValue (Keyboard.current != null && Keyboard.current.iKey.isPressed);
			else if (type == Type.JKey)
				SetValue (Keyboard.current != null && Keyboard.current.jKey.isPressed);
			else if (type == Type.KKey)
				SetValue (Keyboard.current != null && Keyboard.current.kKey.isPressed);
			else if (type == Type.LKey)
				SetValue (Keyboard.current != null && Keyboard.current.lKey.isPressed);
			else if (type == Type.MKey)
				SetValue (Keyboard.current != null && Keyboard.current.mKey.isPressed);
			else if (type == Type.NKey)
				SetValue (Keyboard.current != null && Keyboard.current.nKey.isPressed);
			else if (type == Type.OKey)
				SetValue (Keyboard.current != null && Keyboard.current.oKey.isPressed);
			else if (type == Type.PKey)
				SetValue (Keyboard.current != null && Keyboard.current.pKey.isPressed);
			else if (type == Type.QKey)
				SetValue (Keyboard.current != null && Keyboard.current.qKey.isPressed);
			else if (type == Type.RKey)
				SetValue (Keyboard.current != null && Keyboard.current.rKey.isPressed);
			else if (type == Type.SKey)
				SetValue (Keyboard.current != null && Keyboard.current.sKey.isPressed);
			else if (type == Type.TKey)
				SetValue (Keyboard.current != null && Keyboard.current.tKey.isPressed);
			else if (type == Type.UKey)
				SetValue (Keyboard.current != null && Keyboard.current.uKey.isPressed);
			else if (type == Type.VKey)
				SetValue (Keyboard.current != null && Keyboard.current.vKey.isPressed);
			else if (type == Type.WKey)
				SetValue (Keyboard.current != null && Keyboard.current.wKey.isPressed);
			else if (type == Type.XKey)
				SetValue (Keyboard.current != null && Keyboard.current.xKey.isPressed);
			else if (type == Type.YKey)
				SetValue (Keyboard.current != null && Keyboard.current.yKey.isPressed);
			else if (type == Type.ZKey)
				SetValue (Keyboard.current != null && Keyboard.current.zKey.isPressed);
			else// if (type == Type.LeftShiftKey)
				SetValue (Keyboard.current != null && Keyboard.current.leftShiftKey.isPressed);
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
		}

		public enum Type
		{
			ThumbstickTouched,
			ThumbstickPressed,
			PrimaryButtonTouched,
			PrimaryButtonPressed,
			SecondaryButtonTouched,
			SecondaryButtonPressed,
			TriggerTouched,
			TriggerPressed,
			GripTouched,
			GripPressed,
			MenuButtonPressed,
			AKey,
			BKey,
			CKey,
			DKey,
			EKey,
			FKey,
			GKey,
			HKey,
			IKey,
			JKey,
			KKey,
			LKey,
			MKey,
			NKey,
			OKey,
			PKey,
			QKey,
			RKey,
			SKey,
			TKey,
			UKey,
			VKey,
			WKey,
			XKey,
			YKey,
			ZKey,
			LeftShiftKey
		}

		[Serializable]
		public class Data : BoolOption.Data
		{
			public override object MakeAsset ()
			{
				InputBoolOption inputBoolOption = ObjectPool.instance.SpawnComponent<InputBoolOption>(EternityEngine.instance.inputBoolOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (inputBoolOption);
				inputBoolOption.Init ();
				return inputBoolOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				InputBoolOption inputBoolOption = (InputBoolOption) asset;
				inputBoolOption._Data = this;
			}
		}
	}
}