using System;
using Extensions;

namespace _EternityEngine
{
	public class LoadProjectOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public string projectPath;

		public override void Activate (EternityEngine.Hand hand)
		{
			ActivationBehaviourOption.ActivateChildrenIfShould (TriggerType.Activate, false, this);
			OnActivate (hand);
			ActivationBehaviourOption.ActivateChildrenIfShould (TriggerType.Activate, true, this);
			SaveAndLoadManager.instance.Load (projectPath);
		}

		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetProjectPathOfData ();
		}

		void SetProjectPathOfData ()
		{
			_Data.projectPath = projectPath;
		}

		void SetProjectPathFromData ()
		{
			projectPath = _Data.projectPath;
		}

		[Serializable]
		public class Data : Option.Data
		{
			public string projectPath;

			public override object MakeAsset ()
			{
				LoadProjectOption loadProjectOption = ObjectPool.instance.SpawnComponent<LoadProjectOption>(EternityEngine.instance.loadProjectOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (loadProjectOption);
				loadProjectOption.Init ();
				return loadProjectOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				LoadProjectOption loadProjectOption = (LoadProjectOption) asset;
				loadProjectOption._Data = this;
				loadProjectOption.SetProjectPathFromData ();
			}
		}
	}
}