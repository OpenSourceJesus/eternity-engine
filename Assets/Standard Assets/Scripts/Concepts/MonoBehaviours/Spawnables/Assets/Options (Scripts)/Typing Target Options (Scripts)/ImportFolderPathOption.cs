using System;
using Extensions;

namespace _EternityEngine
{
	public class ImportFolderPathOption : TypingTargetOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public ImportFolderOption importFolderOption;

		public override bool SetValue (string value)
		{
			bool output = base.SetValue(value);
			if (output && Exists(importFolderOption))
				importFolderOption.folderPath = value;
			return output;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetImportFolderPathOptionNameOfData ();
		}

		void SetImportFolderPathOptionNameOfData ()
		{
			if (Exists(importFolderOption))
				_Data.importFolderOptionName = importFolderOption.name;
		}

		void SetImportFolderPathOptionNameFromData ()
		{
			if (_Data.importFolderOptionName != null)
				importFolderOption = EternityEngine.GetOption<ImportFolderOption>(_Data.importFolderOptionName);
		}

		[Serializable]
		public class Data : TypingTargetOption.Data
		{
			public string importFolderOptionName = null;

			public override object MakeAsset ()
			{
				ImportFolderPathOption importFolderPathOption = ObjectPool.instance.SpawnComponent<ImportFolderPathOption>(EternityEngine.instance.importFolderPathOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (importFolderPathOption);
				importFolderPathOption.Init ();
				return importFolderPathOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				ImportFolderPathOption importFolderPathOption = (ImportFolderPathOption) asset;
				importFolderPathOption._Data = this;
				importFolderPathOption.SetImportFolderPathOptionNameFromData ();
			}
		}
	}
}
