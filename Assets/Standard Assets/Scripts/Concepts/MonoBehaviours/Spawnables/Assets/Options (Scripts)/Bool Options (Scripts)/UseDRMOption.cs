using System;
using UnityEngine;

namespace _EternityEngine
{
	public class UseDRMOption : BoolOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}

		public override bool SetValue (bool value)
		{
			if (base.SetValue(value))
			{
				DRMManager.useSaveFileDRM = value;
				return true;
			}
			else
				return false;
		}

		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
		}

		[Serializable]
		public class Data : BoolOption.Data
		{
			public override object MakeAsset ()
			{
				UseDRMOption useDRMOption = ObjectPool.instance.SpawnComponent<UseDRMOption>(EternityEngine.instance.useDRMOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (useDRMOption);
				useDRMOption.Init ();
				return useDRMOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				UseDRMOption useDRMOption = (UseDRMOption) asset;
				useDRMOption._Data = this;
			}
		}
	}
}