using System;

namespace _EternityEngine
{
	public class TwoInputsOperationTypeOption : EnumOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public TwoInputsOperationsOption twoInputsOperationsOption;

		public override bool SetValue (int value)
		{
			if (base.SetValue(value))
			{
				if (Exists(twoInputsOperationsOption))
					twoInputsOperationsOption.SetOperationType ((TwoInputsOperationsOption.OperationType) Enum.ToObject(enumType, value));
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetTwoInputsOperationsOptionNameOfData ();
		}

		void SetTwoInputsOperationsOptionNameOfData ()
		{
			if (Exists(twoInputsOperationsOption))
				_Data.twoInputsOperationsOptionName = twoInputsOperationsOption.name;
		}

		void SetTwoInputsOperationsOptionNameFromData ()
		{
			if (_Data.twoInputsOperationsOptionName != null)
				twoInputsOperationsOption = EternityEngine.GetOption<TwoInputsOperationsOption>(_Data.twoInputsOperationsOptionName);
		}

		[Serializable]
		public class Data : EnumOption.Data
		{
			public string twoInputsOperationsOptionName = null;

			public override object MakeAsset ()
			{
				TwoInputsOperationTypeOption twoInputsOperationTypeOption = ObjectPool.instance.SpawnComponent<TwoInputsOperationTypeOption>(EternityEngine.instance.twoInputsOperationTypeOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (twoInputsOperationTypeOption);
				twoInputsOperationTypeOption.Init ();
				return twoInputsOperationTypeOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				TwoInputsOperationTypeOption twoInputsOperationTypeOption = (TwoInputsOperationTypeOption) asset;
				twoInputsOperationTypeOption._Data = this;
				twoInputsOperationTypeOption.SetTwoInputsOperationsOptionNameFromData ();
			}
		}
	}
}