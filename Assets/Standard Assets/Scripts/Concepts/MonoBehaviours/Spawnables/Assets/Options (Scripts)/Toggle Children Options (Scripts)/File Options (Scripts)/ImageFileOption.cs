using System;
using Extensions;
using UnityEngine;
using Unity.Collections;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class ImageFileOption : FileOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public Texture2D texture;

		public void Init (string filePath, Texture2D texture)
		{
			Init (filePath);
			this.texture = texture;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetSizeOfData ();
			SetColorsOfData ();
		}

		void SetSizeOfData ()
		{
			_Data.size = new _Vector2Int(texture.width, texture.height);
		}

		void SetSizeFromData ()
		{
			texture = new Texture2D(_Data.size.x, _Data.size.y);
		}

		void SetColorsOfData ()
		{
			Color[] colors = texture.GetRawTextureData<Color>().ToArray();
			_Data.colors = new _Color[colors.Length];
			for (int i = 0; i < colors.Length; i ++)
			{
				Color color = colors[i];
				_Data.colors[i] = _Color.FromColor(color);
			}
		}

		void SetColorsFromData ()
		{
			Color[] colors = new Color[_Data.colors.Length];
			for (int i = 0; i < _Data.colors.Length; i ++)
				colors[i] = _Data.colors[i].ToColor();
			NativeArray<Color> _colors = new NativeArray<Color>(colors, Allocator.Temp);
			texture.LoadRawTextureData(_colors);
		}

		[Serializable]
		public class Data : FileOption.Data
		{
			public _Vector2Int size;
			public _Color[] colors;

			public override object MakeAsset ()
			{
				ImageFileOption imageFileOption = ObjectPool.instance.SpawnComponent<ImageFileOption>(EternityEngine.instance.imageFileOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (imageFileOption);
				imageFileOption.Init ();
				return imageFileOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				ImageFileOption imageFileOption = (ImageFileOption) asset;
				imageFileOption._Data = this;
				imageFileOption.SetSizeFromData ();
				imageFileOption.SetColorsFromData ();
			}
		}
	}
}