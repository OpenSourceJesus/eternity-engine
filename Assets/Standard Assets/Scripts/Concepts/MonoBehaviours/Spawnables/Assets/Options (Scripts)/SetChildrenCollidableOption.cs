using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class SetChildrenCollidableOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}

		public override void OnActivate (EternityEngine.Hand hand)
		{
			SetChildrenCollidable ();
			// base.OnActivate (hand);
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
		}

		[Serializable]
		public class Data : Option.Data
		{
			public override object MakeAsset ()
			{
				SetChildrenCollidableOption setChildrenCollidableOption = ObjectPool.instance.SpawnComponent<SetChildrenCollidableOption>(EternityEngine.instance.setChildrenCollidableOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (setChildrenCollidableOption);
				setChildrenCollidableOption.Init ();
				return setChildrenCollidableOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				SetChildrenCollidableOption setChildrenCollidableOption = (SetChildrenCollidableOption) asset;
				setChildrenCollidableOption._Data = this;
			}
		}
	}
}