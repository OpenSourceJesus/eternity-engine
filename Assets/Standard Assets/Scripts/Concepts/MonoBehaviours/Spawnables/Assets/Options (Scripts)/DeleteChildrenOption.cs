using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class DeleteChildrenOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}

		public override void OnActivate (EternityEngine.Hand hand)
		{
			DeleteChildren ();
			// base.OnActivate (hand);
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
		}

		[Serializable]
		public class Data : Option.Data
		{
			public override object MakeAsset ()
			{
				DeleteChildrenOption deleteChildrenOption = ObjectPool.instance.SpawnComponent<DeleteChildrenOption>(EternityEngine.instance.deleteChildrenOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (deleteChildrenOption);
				deleteChildrenOption.Init ();
				return deleteChildrenOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				DeleteChildrenOption deleteChildrenOption = (DeleteChildrenOption) asset;
				deleteChildrenOption._Data = this;
			}
		}
	}
}