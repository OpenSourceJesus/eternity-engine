using TMPro;
using System;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class InstrumentOption : ToggleChildrenOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public int instrumentIndex;
		public bool isForLeftHand;

		public void SwitchToInstrument ()
		{
			if (isForLeftHand)
				EternityEngine.instance.SetInstrument (EternityEngine.instance.leftHand, instrumentIndex);
			else
				EternityEngine.instance.SetInstrument (EternityEngine.instance.rightHand, instrumentIndex);
		}

		public void SetRadius (float radius)
		{
			if (isForLeftHand)
			{
				if (EternityEngine.instance.leftHand.currentInstrumentIndex == instrumentIndex)
					EternityEngine.instance.leftHand.currentInstrument.radius = radius;
				else
					EternityEngine.leftHandInstruments[instrumentIndex].radius = radius;
			}
			else
			{
				if (EternityEngine.instance.rightHand.currentInstrumentIndex == instrumentIndex)
					EternityEngine.instance.rightHand.currentInstrument.radius = radius;
				else
					EternityEngine.rightHandInstruments[instrumentIndex].radius = radius;
			}
		}

		public void SetMinLength (float minLength)
		{
			if (isForLeftHand)
			{
				 if (EternityEngine.instance.leftHand.currentInstrumentIndex == instrumentIndex)
					EternityEngine.instance.leftHand.currentInstrument.length.min = minLength;
				else
					EternityEngine.leftHandInstruments[instrumentIndex].length.min = minLength;
			}
			else
			{
				if (EternityEngine.instance.rightHand.currentInstrumentIndex == instrumentIndex)
					EternityEngine.instance.rightHand.currentInstrument.length.min = minLength;
				else
					EternityEngine.rightHandInstruments[instrumentIndex].length.min = minLength;
			}
		}

		public void SetMaxLength (float maxLength)
		{
			if (isForLeftHand)
			{
				if (EternityEngine.instance.leftHand.currentInstrumentIndex == instrumentIndex)
					EternityEngine.instance.leftHand.currentInstrument.length.max = maxLength;
				else
					EternityEngine.leftHandInstruments[instrumentIndex].length.max = maxLength;
			}
			else
			{
				if (EternityEngine.instance.rightHand.currentInstrumentIndex == instrumentIndex)
					EternityEngine.instance.rightHand.currentInstrument.length.max = maxLength;
				else
					EternityEngine.rightHandInstruments[instrumentIndex].length.max = maxLength;
			}
		}

		public void SetSampleCount (int sampleCount)
		{
			if (isForLeftHand)
			{
				if (EternityEngine.instance.leftHand.currentInstrumentIndex == instrumentIndex)
					EternityEngine.instance.leftHand.currentInstrument.sampleCount = sampleCount;
				else
					EternityEngine.leftHandInstruments[instrumentIndex].sampleCount = sampleCount;
			}
			else
			{
				if (EternityEngine.instance.rightHand.currentInstrumentIndex == instrumentIndex)
					EternityEngine.instance.rightHand.currentInstrument.sampleCount = sampleCount;
				else
					EternityEngine.rightHandInstruments[instrumentIndex].sampleCount = sampleCount;
			}
		}

		public void SetGraphicsVisible (bool visible)
		{
			if (isForLeftHand)
				EternityEngine.instance.leftHand.graphicsGo.SetActive(visible);
			else
				EternityEngine.instance.rightHand.graphicsGo.SetActive(visible);
		}

		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetInstrumentIndexOfData ();
			SetIsForLeftHandOfData ();
			SetGraphicsAreVisibleOfData ();
		}
		
		void SetInstrumentIndexOfData ()
		{
			_Data.instrumentIndex = (byte) instrumentIndex;
		}

		void SetInstrumentIndexFromData ()
		{
			instrumentIndex = (int) _Data.instrumentIndex;
		}
		
		void SetIsForLeftHandOfData ()
		{
			_Data.isForLeftHand = isForLeftHand;
		}

		void SetIsForLeftHandFromData ()
		{
			isForLeftHand = _Data.isForLeftHand;
		}

		void SetGraphicsAreVisibleOfData ()
		{
			_Data.graphicsAreVisible = EternityEngine.instance.leftHand.graphicsGo.activeSelf;
		}

		void SetGraphicsAreVisibleFromData ()
		{
			EternityEngine.instance.leftHand.graphicsGo.SetActive(_Data.graphicsAreVisible);
		}

		[Serializable]
		public class Data : ToggleChildrenOption.Data
		{
			public byte instrumentIndex;
			public bool isForLeftHand;
			public bool graphicsAreVisible;
			
			public override object MakeAsset ()
			{
				InstrumentOption instrumentOption = ObjectPool.instance.SpawnComponent<InstrumentOption>(EternityEngine.instance.instrumentOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (instrumentOption);
				instrumentOption.Init ();
				return instrumentOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				InstrumentOption instrumentOption = (InstrumentOption) asset;
				instrumentOption._Data = this;
				instrumentOption.SetInstrumentIndexFromData ();
				instrumentOption.SetIsForLeftHandFromData ();
				instrumentOption.SetGraphicsAreVisibleFromData ();
			}
		}
	}
}