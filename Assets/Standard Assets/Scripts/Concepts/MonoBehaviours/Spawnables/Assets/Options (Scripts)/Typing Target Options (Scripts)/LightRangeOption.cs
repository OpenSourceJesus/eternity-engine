using System;

namespace _EternityEngine
{
	public class LightRangeOption : TypingTargetOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public LightOption lightOption;

		public override bool SetValue (string value)
		{
			if (base.SetValue(value))
			{
				if (Exists(lightOption))
				{
					float? range = GetNumberValue();
					if (range != null)
						lightOption.SetRange ((float) range);
				}
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetLightOptionNameOfData ();
		}

		void SetLightOptionNameOfData ()
		{
			if (Exists(lightOption))
				_Data.lightOptionName = lightOption.name;
		}

		void SetLightOptionNameFromData ()
		{
			if (_Data.lightOptionName != null)
				lightOption = EternityEngine.GetOption<LightOption>(_Data.lightOptionName);
		}

		[Serializable]
		public class Data : TypingTargetOption.Data
		{
			public string lightOptionName = null;

			public override object MakeAsset ()
			{
				LightRangeOption lightRangeOption = ObjectPool.instance.SpawnComponent<LightRangeOption>(EternityEngine.instance.lightRangeOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (lightRangeOption);
				lightRangeOption.Init ();
				return lightRangeOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				LightRangeOption lightRangeOption = (LightRangeOption) asset;
				lightRangeOption._Data = this;
				lightRangeOption.SetLightOptionNameFromData ();
			}
		}
	}
}
