using System;

namespace _EternityEngine
{
	public class SoundPitchOption : TypingTargetOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public SoundFileInstanceOption soundFileInstanceOption;

		public override bool SetValue (string value)
		{
			if (base.SetValue(value))
			{
				if (Exists(soundFileInstanceOption))
				{
					float? pitch = GetNumberValue();
					if (pitch != null)
						soundFileInstanceOption.SetPitch ((float) pitch);
				}
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetSoundFileInstanceOptionNameOfData ();
		}

		void SetSoundFileInstanceOptionNameOfData ()
		{
			if (Exists(soundFileInstanceOption))
				_Data.soundFileInstanceOptionName = soundFileInstanceOption.name;
		}

		void SetSoundFileInstanceOptionNameFromData ()
		{
			if (_Data.soundFileInstanceOptionName != null)
				soundFileInstanceOption = EternityEngine.GetOption<SoundFileInstanceOption>(_Data.soundFileInstanceOptionName);
		}

		[Serializable]
		public class Data : TypingTargetOption.Data
		{
			public string soundFileInstanceOptionName = null;

			public override object MakeAsset ()
			{
				SoundPitchOption soundPitchOption = ObjectPool.instance.SpawnComponent<SoundPitchOption>(EternityEngine.instance.soundPitchOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (soundPitchOption);
				soundPitchOption.Init ();
				return soundPitchOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				SoundPitchOption soundPitchOption = (SoundPitchOption) asset;
				soundPitchOption._Data = this;
				soundPitchOption.SetSoundFileInstanceOptionNameFromData ();
			}
		}
	}
}
