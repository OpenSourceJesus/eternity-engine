using System;

namespace _EternityEngine
{
	public class SpawnBehaviourDeleteCurrentBehaviourOption : EnumOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public SpawnBehaviourOption spawnBehaviourOption;

		public override bool SetValue (int value)
		{
			bool output = base.SetValue(value);
			if (output && Exists(spawnBehaviourOption))
				spawnBehaviourOption.deleteCurrentBehaviour = (SpawnBehaviourOption.DeleteCurrentBehaviour) Enum.ToObject(enumType, this.value);
			return output;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetSpawnBehaviourOptionNameOfData ();
		}

		void SetSpawnBehaviourOptionNameOfData ()
		{
			if (Exists(spawnBehaviourOption))
				_Data.spawnBehaviourOptionName = spawnBehaviourOption.name;
		}

		void SetSpawnBehaviourOptionNameFromData ()
		{
			if (_Data.spawnBehaviourOptionName != null)
				spawnBehaviourOption = EternityEngine.GetOption<SpawnBehaviourOption>(_Data.spawnBehaviourOptionName);
		}

		[Serializable]
		public class Data : EnumOption.Data
		{
			public string spawnBehaviourOptionName = null;

			public override object MakeAsset ()
			{
				SpawnBehaviourDeleteCurrentBehaviourOption spawnBehaviourDeleteCurrentBehaviourOption = ObjectPool.instance.SpawnComponent<SpawnBehaviourDeleteCurrentBehaviourOption>(EternityEngine.instance.spawnBehaviourDeleteCurrentBehaviourOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (spawnBehaviourDeleteCurrentBehaviourOption);
				spawnBehaviourDeleteCurrentBehaviourOption.Init ();
				return spawnBehaviourDeleteCurrentBehaviourOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				SpawnBehaviourDeleteCurrentBehaviourOption spawnBehaviourDeleteCurrentBehaviourOption = (SpawnBehaviourDeleteCurrentBehaviourOption) asset;
				spawnBehaviourDeleteCurrentBehaviourOption._Data = this;
				spawnBehaviourDeleteCurrentBehaviourOption.SetSpawnBehaviourOptionNameFromData ();
			}
		}
	}
}