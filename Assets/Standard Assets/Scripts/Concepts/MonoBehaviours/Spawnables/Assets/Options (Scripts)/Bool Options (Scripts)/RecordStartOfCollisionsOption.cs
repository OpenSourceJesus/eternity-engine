using System;

namespace _EternityEngine
{
	public class RecordStartOfCollisionsOption : BoolOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public ModelFileInstanceOption modelFileInstanceOption;

		public override bool SetValue (bool value)
		{
			if (base.SetValue(value))
			{
				if (Exists(modelFileInstanceOption))
					modelFileInstanceOption.RecordStartOfCollisions (value);
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetModelFileInstanceOptionNameOfData ();
		}

		void SetModelFileInstanceOptionNameOfData ()
		{
			if (Exists(modelFileInstanceOption))
				_Data.modelFileInstanceOptionName = modelFileInstanceOption.name;
		}

		void SetModelFileInstanceOptionNameFromData ()
		{
			if (_Data.modelFileInstanceOptionName != null)
				modelFileInstanceOption = EternityEngine.GetOption<ModelFileInstanceOption>(_Data.modelFileInstanceOptionName);
		}

		[Serializable]
		public class Data : BoolOption.Data
		{
			public string modelFileInstanceOptionName = null;

			public override object MakeAsset ()
			{
				RecordStartOfCollisionsOption recordStartOfCollisionsOption = ObjectPool.instance.SpawnComponent<RecordStartOfCollisionsOption>(EternityEngine.instance.recordStartOfCollisionsOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (recordStartOfCollisionsOption);
				recordStartOfCollisionsOption.Init ();
				return recordStartOfCollisionsOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				RecordStartOfCollisionsOption recordStartOfCollisionsOption = (RecordStartOfCollisionsOption) asset;
				recordStartOfCollisionsOption._Data = this;
				recordStartOfCollisionsOption.SetModelFileInstanceOptionNameFromData ();
			}
		}
	}
}