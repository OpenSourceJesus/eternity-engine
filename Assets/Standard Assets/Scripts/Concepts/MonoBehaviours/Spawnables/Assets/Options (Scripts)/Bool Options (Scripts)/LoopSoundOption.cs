using System;

namespace _EternityEngine
{
	public class LoopSoundOption : BoolOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public SoundFileInstanceOption soundFileInstanceOption;

		public override bool SetValue (bool value)
		{
			bool output =base.SetValue(value);
			if (output && Exists(soundFileInstanceOption))
				soundFileInstanceOption.loop = value;
			return output;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetModelFileInstanceOptionNameOfData ();
		}

		void SetModelFileInstanceOptionNameOfData ()
		{
			if (Exists(soundFileInstanceOption))
				_Data.soundFileInstanceOptionName = soundFileInstanceOption.name;
		}

		void SetModelFileInstanceOptionNameFromData ()
		{
			if (_Data.soundFileInstanceOptionName != null)
				soundFileInstanceOption = EternityEngine.GetOption<SoundFileInstanceOption>(_Data.soundFileInstanceOptionName);
		}

		[Serializable]
		public class Data : BoolOption.Data
		{
			public string soundFileInstanceOptionName = null;

			public override object MakeAsset ()
			{
				LoopSoundOption recordStartOfCollisionsOption = ObjectPool.instance.SpawnComponent<LoopSoundOption>(EternityEngine.instance.recordStartOfCollisionsOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (recordStartOfCollisionsOption);
				recordStartOfCollisionsOption.Init ();
				return recordStartOfCollisionsOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				LoopSoundOption recordStartOfCollisionsOption = (LoopSoundOption) asset;
				recordStartOfCollisionsOption._Data = this;
				recordStartOfCollisionsOption.SetModelFileInstanceOptionNameFromData ();
			}
		}
	}
}