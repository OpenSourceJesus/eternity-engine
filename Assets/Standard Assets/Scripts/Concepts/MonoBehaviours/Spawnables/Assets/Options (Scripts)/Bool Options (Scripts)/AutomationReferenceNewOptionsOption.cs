using System;

namespace _EternityEngine
{
	public class AutomationReferenceNewOptionsOption : BoolOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public AutomationOption automationOption;

		public override bool SetValue (bool value)
		{
			if (base.SetValue(value))
			{
				if (Exists(automationOption))
					automationOption.referenceNewOptions = value;
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetAutomationOptionNameOfData ();
		}

		void SetAutomationOptionNameOfData ()
		{
			if (Exists(automationOption))
				_Data.automationOptionName = automationOption.name;
		}

		void SetAutomationOptionNameFromData ()
		{
			if (_Data.automationOptionName != null)
				automationOption = EternityEngine.GetOption<AutomationOption>(_Data.automationOptionName);
		}

		[Serializable]
		public class Data : BoolOption.Data
		{
			public string automationOptionName = null;

			public override object MakeAsset ()
			{
				AutomationReferenceNewOptionsOption automationReferenceNewOptionsOption = ObjectPool.instance.SpawnComponent<AutomationReferenceNewOptionsOption>(EternityEngine.instance.automationReferenceNewOptionsOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (automationReferenceNewOptionsOption);
				automationReferenceNewOptionsOption.Init ();
				return automationReferenceNewOptionsOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				AutomationReferenceNewOptionsOption automationReferenceNewOptionsOption = (AutomationReferenceNewOptionsOption) asset;
				automationReferenceNewOptionsOption._Data = this;
				automationReferenceNewOptionsOption.SetAutomationOptionNameFromData ();
			}
		}
	}
}