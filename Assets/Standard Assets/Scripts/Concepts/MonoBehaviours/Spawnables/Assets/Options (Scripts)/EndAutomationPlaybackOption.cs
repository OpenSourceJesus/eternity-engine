using System;

namespace _EternityEngine
{
	public class EndAutomationPlaybackOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public AutomationOption automationOption;

		public override void OnActivate (EternityEngine.Hand hand)
		{
			if (Exists(automationOption))
				automationOption.EndPlayback ();
			// base.OnActivate (hand);
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetAutomationOptionNameOfData ();
		}

		void SetAutomationOptionNameOfData ()
		{
			if (Exists(automationOption))
				_Data.automationOptionName = automationOption.name;
		}

		void SetAutomationOptionNameFromData ()
		{
			if (_Data.automationOptionName != null)
				automationOption = EternityEngine.GetOption<AutomationOption>(_Data.automationOptionName);
		}

		[Serializable]
		public class Data : Option.Data
		{
			public string automationOptionName = null;

			public override object MakeAsset ()
			{
				EndAutomationPlaybackOption endAutomationPlaybackOption = ObjectPool.instance.SpawnComponent<EndAutomationPlaybackOption>(EternityEngine.instance.endAutomationPlaybackOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (endAutomationPlaybackOption);
				endAutomationPlaybackOption.Init ();
				return endAutomationPlaybackOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				EndAutomationPlaybackOption endAutomationPlaybackOption = (EndAutomationPlaybackOption) asset;
				endAutomationPlaybackOption._Data = this;
				endAutomationPlaybackOption.SetAutomationOptionNameFromData ();
			}
		}
	}
}