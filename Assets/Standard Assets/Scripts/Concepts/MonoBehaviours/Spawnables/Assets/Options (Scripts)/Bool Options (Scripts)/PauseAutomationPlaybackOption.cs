using System;

namespace _EternityEngine
{
	public class PauseAutomationPlaybackOption : BoolOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public AutomationOption automationOption;

		public override bool SetValue (bool value)
		{
			if (base.SetValue(value))
			{
				if (Exists(automationOption))
					automationOption.PausePlaying (value);
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetAutomationOptionNameOfData ();
		}

		void SetAutomationOptionNameOfData ()
		{
			if (Exists(automationOption))
				_Data.automationOptionName = automationOption.name;
		}

		void SetAutomationOptionNameFromData ()
		{
			if (_Data.automationOptionName != null)
				automationOption = EternityEngine.GetOption<AutomationOption>(_Data.automationOptionName);
		}

		[Serializable]
		public class Data : BoolOption.Data
		{
			public string automationOptionName = null;

			public override object MakeAsset ()
			{
				PauseAutomationPlaybackOption pauseAutomationPlaybackOption = ObjectPool.instance.SpawnComponent<PauseAutomationPlaybackOption>(EternityEngine.instance.pauseAutomationPlaybackOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (pauseAutomationPlaybackOption);
				pauseAutomationPlaybackOption.Init ();
				return pauseAutomationPlaybackOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				PauseAutomationPlaybackOption pauseAutomationPlaybackOption = (PauseAutomationPlaybackOption) asset;
				pauseAutomationPlaybackOption._Data = this;
				pauseAutomationPlaybackOption.SetAutomationOptionNameFromData ();
			}
		}
	}
}