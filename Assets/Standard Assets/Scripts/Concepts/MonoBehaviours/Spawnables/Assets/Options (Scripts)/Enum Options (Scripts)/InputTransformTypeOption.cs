using System;

namespace _EternityEngine
{
	public class InputTransformTypeOption : EnumOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public InputTransformOption inputTransformOption;

		public override bool SetValue (int value)
		{
			if (base.SetValue(value))
			{
				if (Exists(inputTransformOption))
				{
					inputTransformOption.type = (InputTransformOption.Type) Enum.ToObject(enumType, value);
					inputTransformOption.ApplyType ();
				}
				return true;
			}
			else
				return false;
		}

		public override void Init ()
		{
			base.Init ();
			if (Exists(inputTransformOption))
				inputTransformOption.ApplyType ();
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetInputTransformOptionNameOfData ();
		}

		void SetInputTransformOptionNameOfData ()
		{
			if (Exists(inputTransformOption))
				_Data.inputTransformOptionName = inputTransformOption.name;
		}

		void SetInputTransformOptionNameFromData ()
		{
			if (_Data.inputTransformOptionName != null)
				inputTransformOption = EternityEngine.GetOption<InputTransformOption>(_Data.inputTransformOptionName);
		}

		[Serializable]
		public class Data : EnumOption.Data
		{
			public string inputTransformOptionName = null;

			public override object MakeAsset ()
			{
				InputTransformTypeOption inputTransformTypeOption = ObjectPool.instance.SpawnComponent<InputTransformTypeOption>(EternityEngine.instance.inputTransformTypeOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (inputTransformTypeOption);
				inputTransformTypeOption.Init ();
				return inputTransformTypeOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				InputTransformTypeOption inputTransformTypeOption = (InputTransformTypeOption) asset;
				inputTransformTypeOption._Data = this;
				inputTransformTypeOption.SetInputTransformOptionNameFromData ();
			}
		}
	}
}