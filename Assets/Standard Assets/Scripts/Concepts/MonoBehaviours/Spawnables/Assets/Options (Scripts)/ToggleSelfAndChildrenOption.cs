using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class ToggleSelfAndChildrenOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}

		public override void OnActivate (EternityEngine.Hand hand)
		{
			Toggle ();
			ToggleChildren ();
			// base.OnActivate (hand);
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
		}

		[Serializable]
		public class Data : Option.Data
		{
			public override object MakeAsset ()
			{
				ToggleSelfAndChildrenOption toggleSelfAndChildrenOption = ObjectPool.instance.SpawnComponent<ToggleSelfAndChildrenOption>(EternityEngine.instance.toggleSelfAndChildrenOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (toggleSelfAndChildrenOption);
				// if (!toggleSelfAndChildrenOption.isInitialized)
				// 	toggleSelfAndChildrenOption.Init ();
				return toggleSelfAndChildrenOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				ToggleSelfAndChildrenOption toggleSelfAndChildrenOption = (ToggleSelfAndChildrenOption) asset;
				toggleSelfAndChildrenOption._Data = this;
			}
		}
	}
}