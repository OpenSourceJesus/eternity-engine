using System;

namespace _EternityEngine
{
	public class ActivationBehaviourTriggerTypeOption : EnumOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public ActivationBehaviourOption activationBehaviourOption;

		public override bool SetFlagValue (int flagIndex, bool value)
		{
			if (base.SetFlagValue(flagIndex, value))
			{
				if (Exists(activationBehaviourOption))
				{
					activationBehaviourOption.triggerType = (TriggerType) Enum.ToObject(enumType, this.value);
					activationBehaviourOption.ToggleRegister (flagIndex);
				}
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetActivationBehaviourOptionNameOfData ();
		}

		void SetActivationBehaviourOptionNameOfData ()
		{
			if (Exists(activationBehaviourOption))
				_Data.activationBehaviourOptionName = activationBehaviourOption.name;
		}

		void SetActivationBehaviourOptionNameFromData ()
		{
			if (_Data.activationBehaviourOptionName != null)
				activationBehaviourOption = EternityEngine.GetOption<ActivationBehaviourOption>(_Data.activationBehaviourOptionName);
		}

		[Serializable]
		public class Data : EnumOption.Data
		{
			public string activationBehaviourOptionName = null;

			public override object MakeAsset ()
			{
				ActivationBehaviourTriggerTypeOption activationBehaviourTriggerTypeOption = ObjectPool.instance.SpawnComponent<ActivationBehaviourTriggerTypeOption>(EternityEngine.instance.activationBehaviourTriggerTypeOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (activationBehaviourTriggerTypeOption);
				// if (!activationBehaviourTriggerTypeOption.isInitialized)
				// 	activationBehaviourTriggerTypeOption.Init ();
				return activationBehaviourTriggerTypeOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				ActivationBehaviourTriggerTypeOption activationBehaviourTriggerTypeOption = (ActivationBehaviourTriggerTypeOption) asset;
				activationBehaviourTriggerTypeOption._Data = this;
				activationBehaviourTriggerTypeOption.SetActivationBehaviourOptionNameFromData ();
			}
		}
	}
}