using System;
using System.IO;
using TriLibCore;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class ImportFolderOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public string folderPath;

		public override void OnActivate (EternityEngine.Hand hand)
		{
			string[] filePaths = Directory.GetFiles(folderPath);
			for (int i = 0; i < filePaths.Length; i ++)
			{
				string filePath = filePaths[i];
				if (filePath.EndsWith(".wav"))
					ImportAudioWithFileNameOption.Do (filePath);
				else if (filePath.EndsWith(".jpg") || filePath.EndsWith(".png") || filePath.EndsWith(".bmp") || filePath.EndsWith(".tif") || filePath.EndsWith(".tga") || filePath.EndsWith(".psd"))
					ImportImageWithFileNameOption.Do (filePath);
				else
				{
					string[] folderExtensions = ((List<string>) Readers.Extensions).ToArray();
					for (int i2 = 0; i2 < folderExtensions.Length; i2 ++)
					{
						string folderExtension = folderExtensions[i2];
						if (filePath.EndsWith(folderExtension))
						{
							ImportModelWithFileNameOption.Do (filePath);
							break;
						}
					}
				}
			}
			// base.OnActivate (hand);
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetFolderPathOfData ();
		}

		void SetFolderPathOfData ()
		{
			_Data.folderPath = folderPath;
		}

		void SetFolderPathFromData ()
		{
			folderPath = _Data.folderPath;
		}

		[Serializable]
		public class Data : Option.Data
		{
			public string folderPath;

			public override object MakeAsset ()
			{
				ImportFolderOption importFolderOption = ObjectPool.instance.SpawnComponent<ImportFolderOption>(EternityEngine.instance.importFolderOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (importFolderOption);
				importFolderOption.Init ();
				return importFolderOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				ImportFolderOption importFolderOption = (ImportFolderOption) asset;
				importFolderOption._Data = this;
				importFolderOption.SetFolderPathFromData ();
			}
		}
	}
}