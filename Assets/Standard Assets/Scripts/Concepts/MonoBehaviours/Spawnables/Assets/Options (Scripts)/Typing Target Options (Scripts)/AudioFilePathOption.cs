using System;
using Extensions;

namespace _EternityEngine
{
	public class AudioFilePathOption : TypingTargetOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public ImportAudioWithFileNameOption importAudioWithFileNameOption;

		public override bool SetValue (string value)
		{
			if (base.SetValue(value))
			{
				if (Exists(importAudioWithFileNameOption))
					importAudioWithFileNameOption.audioFilePath = value;
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetImportAudioWithFileNameOptionNameOfData ();
		}

		void SetImportAudioWithFileNameOptionNameOfData ()
		{
			if (Exists(importAudioWithFileNameOption))
				_Data.importAudioWithFileNameOptionName = importAudioWithFileNameOption.name;
		}

		void SetImportAudioWithFileNameOptionNameFromData ()
		{
			if (_Data.importAudioWithFileNameOptionName != null)
				importAudioWithFileNameOption = EternityEngine.GetOption<ImportAudioWithFileNameOption>(_Data.importAudioWithFileNameOptionName);
		}

		[Serializable]
		public class Data : TypingTargetOption.Data
		{
			public string importAudioWithFileNameOptionName = null;

			public override object MakeAsset ()
			{
				AudioFilePathOption audioFilePathOption = ObjectPool.instance.SpawnComponent<AudioFilePathOption>(EternityEngine.instance.audioFilePathOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (audioFilePathOption);
				audioFilePathOption.Init ();
				return audioFilePathOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				AudioFilePathOption audioFilePathOption = (AudioFilePathOption) asset;
				audioFilePathOption._Data = this;
				audioFilePathOption.SetImportAudioWithFileNameOptionNameFromData ();
			}
		}
	}
}
