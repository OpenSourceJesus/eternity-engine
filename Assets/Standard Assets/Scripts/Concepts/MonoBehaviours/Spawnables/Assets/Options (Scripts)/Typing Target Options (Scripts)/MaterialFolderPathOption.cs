using System;

namespace _EternityEngine
{
	public class MaterialFolderPathOption : TypingTargetOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public SkyboxOption skyboxOption;

		public override bool SetValue (string value)
		{
			if (base.SetValue(value))
			{
				if (Exists(skyboxOption))
					skyboxOption.materialFolderPath = value;
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetSkyboxOptionNameOfData ();
		}

		void SetSkyboxOptionNameOfData ()
		{
			if (Exists(skyboxOption))
				_Data.skyboxOptionName = skyboxOption.name;
		}

		void SetSkyboxOptionNameFromData ()
		{
			if (_Data.skyboxOptionName != null)
				skyboxOption = EternityEngine.GetOption<SkyboxOption>(_Data.skyboxOptionName);
		}

		[Serializable]
		public class Data : TypingTargetOption.Data
		{
			public string skyboxOptionName = null;

			public override object MakeAsset ()
			{
				MaterialFolderPathOption materialFolderPathOption = ObjectPool.instance.SpawnComponent<MaterialFolderPathOption>(EternityEngine.instance.materialFolderPathOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (materialFolderPathOption);
				materialFolderPathOption.Init ();
				return materialFolderPathOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				MaterialFolderPathOption materialFolderPathOption = (MaterialFolderPathOption) asset;
				materialFolderPathOption._Data = this;
				materialFolderPathOption.SetSkyboxOptionNameFromData ();
			}
		}
	}
}