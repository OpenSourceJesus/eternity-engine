using System;

namespace _EternityEngine
{
	public class ActivateAfterEventOption : BoolOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public ActivationBehaviourOption activationBehaviourOption;

		public override bool SetValue (bool value)
		{
			if (base.SetValue(value))
			{
				if (Exists(activationBehaviourOption))
					activationBehaviourOption.activateAfterEvent = value;
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetActivationBehaviourOptionNameOfData ();
		}

		void SetActivationBehaviourOptionNameOfData ()
		{
			if (Exists(activationBehaviourOption))
				_Data.activationBehaviourOptionName = activationBehaviourOption.name;
		}

		void SetActivationBehaviourOptionNameFromData ()
		{
			if (_Data.activationBehaviourOptionName != null)
				activationBehaviourOption = EternityEngine.GetOption<ActivationBehaviourOption>(_Data.activationBehaviourOptionName);
		}

		[Serializable]
		public class Data : BoolOption.Data
		{
			public string activationBehaviourOptionName = null;

			public override object MakeAsset ()
			{
				ActivateAfterEventOption activateAfterEventOption = ObjectPool.instance.SpawnComponent<ActivateAfterEventOption>(EternityEngine.instance.activateAfterEventOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (activateAfterEventOption);
				activateAfterEventOption.Init ();
				return activateAfterEventOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				ActivateAfterEventOption activateAfterEventOption = (ActivateAfterEventOption) asset;
				activateAfterEventOption._Data = this;
				activateAfterEventOption.SetActivationBehaviourOptionNameFromData ();
			}
		}
	}
}