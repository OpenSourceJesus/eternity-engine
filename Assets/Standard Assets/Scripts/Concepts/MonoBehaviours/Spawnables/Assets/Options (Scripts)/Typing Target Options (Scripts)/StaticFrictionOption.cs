using System;

namespace _EternityEngine
{
	public class StaticFrictionOption : TypingTargetOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public ModelFileInstanceOption modelFileInstanceOption;

		public override bool SetValue (string value)
		{
			if (base.SetValue(value))
			{
				if (Exists(modelFileInstanceOption))
				{
					float? staticFriction = GetNumberValue();
					if (staticFriction != null)
						modelFileInstanceOption.SetStaticFriction ((float) staticFriction);
				}
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetModelFileInstanceOptionNameOfData ();
		}

		void SetModelFileInstanceOptionNameOfData ()
		{
			if (Exists(modelFileInstanceOption))
				_Data.modelFileInstanceOptionName = modelFileInstanceOption.name;
		}

		void SetModelFileInstanceOptionNameFromData ()
		{
			if (_Data.modelFileInstanceOptionName != null)
				modelFileInstanceOption = EternityEngine.GetOption<ModelFileInstanceOption>(_Data.modelFileInstanceOptionName);
		}

		[Serializable]
		public class Data : TypingTargetOption.Data
		{
			public string modelFileInstanceOptionName = null;

			public override object MakeAsset ()
			{
				StaticFrictionOption staticFrictionOption = ObjectPool.instance.SpawnComponent<StaticFrictionOption>(EternityEngine.instance.staticFrictionOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (staticFrictionOption);
				staticFrictionOption.Init ();
				return staticFrictionOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				StaticFrictionOption staticFrictionOption = (StaticFrictionOption) asset;
				staticFrictionOption._Data = this;
				staticFrictionOption.SetModelFileInstanceOptionNameFromData ();
			}
		}
	}
}