using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class HideSelfAndChildrenOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}

		public override void OnActivate (EternityEngine.Hand hand)
		{
			Hide ();
			HideChildren ();
			// base.OnActivate (hand);
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
		}

		[Serializable]
		public class Data : Option.Data
		{
			public override object MakeAsset ()
			{
				HideSelfAndChildrenOption hideSelfAndChildrenOption = ObjectPool.instance.SpawnComponent<HideSelfAndChildrenOption>(EternityEngine.instance.hideSelfAndChildrenOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (hideSelfAndChildrenOption);
				hideSelfAndChildrenOption.Init ();
				return hideSelfAndChildrenOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				HideSelfAndChildrenOption hideSelfAndChildrenOption = (HideSelfAndChildrenOption) asset;
				hideSelfAndChildrenOption._Data = this;
			}
		}
	}
}