using System;

namespace _EternityEngine
{
	public class ConditionalTooManyChildrenBehaviourOption : EnumOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public ConditionalOption conditionalOption;

		public override bool SetValue (int value)
		{
			if (base.SetValue(value))
			{
				if (Exists(conditionalOption))
				{
					conditionalOption.ApplyTooManyChildrenBehaviour ((ConditionalOption.TooManyChildrenBehaviour) Enum.ToObject(enumType, value));
				}
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetConditionalOptionNameOfData ();
		}

		void SetConditionalOptionNameOfData ()
		{
			if (Exists(conditionalOption))
				_Data.conditionalOptionName = conditionalOption.name;
		}

		void SetConditionalOptionNameFromData ()
		{
			if (_Data.conditionalOptionName != null)
				conditionalOption = EternityEngine.GetOption<ConditionalOption>(_Data.conditionalOptionName);
		}

		[Serializable]
		public class Data : EnumOption.Data
		{
			public string conditionalOptionName = null;

			public override object MakeAsset ()
			{
				ConditionalTooManyChildrenBehaviourOption conditionalTooManyChildrenBehaviourOption = ObjectPool.instance.SpawnComponent<ConditionalTooManyChildrenBehaviourOption>(EternityEngine.instance.conditionalTooManyChildrenBehaviourOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (conditionalTooManyChildrenBehaviourOption);
				conditionalTooManyChildrenBehaviourOption.Init ();
				return conditionalTooManyChildrenBehaviourOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				ConditionalTooManyChildrenBehaviourOption conditionalTooManyChildrenBehaviourOption = (ConditionalTooManyChildrenBehaviourOption) asset;
				conditionalTooManyChildrenBehaviourOption._Data = this;
				conditionalTooManyChildrenBehaviourOption.SetConditionalOptionNameFromData ();
			}
		}
	}
}