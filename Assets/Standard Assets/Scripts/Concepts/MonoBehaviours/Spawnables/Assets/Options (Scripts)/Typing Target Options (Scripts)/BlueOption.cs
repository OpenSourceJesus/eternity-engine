using System;

namespace _EternityEngine
{
	public class BlueOption : TypingTargetOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public ColorOption colorOption;

		public override bool SetValue (string value)
		{
			if (base.SetValue(value))
			{
				if (Exists(colorOption))
				{
					float? b = GetNumberValue();
					if (b != null)
						colorOption.SetB ((float) b);
				}
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetColorOptionNameOfData ();
		}

		void SetColorOptionNameOfData ()
		{
			if (Exists(colorOption))
				_Data.colorOptionName = colorOption.name;
		}

		void SetColorOptionNameFromData ()
		{
			if (_Data.colorOptionName != null)
				colorOption = EternityEngine.GetOption<ColorOption>(_Data.colorOptionName);
		}

		[Serializable]
		public class Data : TypingTargetOption.Data
		{
			public string colorOptionName = null;

			public override object MakeAsset ()
			{
				BlueOption blueOption = ObjectPool.instance.SpawnComponent<BlueOption>(EternityEngine.instance.blueOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (blueOption);
				blueOption.Init ();
				return blueOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				BlueOption blueOption = (BlueOption) asset;
				blueOption._Data = this;
				blueOption.SetColorOptionNameFromData ();
			}
		}
	}
}
