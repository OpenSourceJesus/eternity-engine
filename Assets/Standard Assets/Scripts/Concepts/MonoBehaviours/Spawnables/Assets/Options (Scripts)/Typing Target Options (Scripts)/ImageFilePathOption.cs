using System;
using Extensions;

namespace _EternityEngine
{
	public class ImageFilePathOption : TypingTargetOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public ImportImageWithFileNameOption importImageWithFileNameOption;

		public override bool SetValue (string value)
		{
			if (base.SetValue(value))
			{
				if (Exists(importImageWithFileNameOption))
					importImageWithFileNameOption.imageFilePath = value;
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetImportImageWithFileNameOptionNameOfData ();
		}

		void SetImportImageWithFileNameOptionNameOfData ()
		{
			if (Exists(importImageWithFileNameOption))
				_Data.importImageWithFileNameOptionName = importImageWithFileNameOption.name;
		}

		void SetImportImageWithFileNameOptionNameFromData ()
		{
			if (_Data.importImageWithFileNameOptionName != null)
				importImageWithFileNameOption = EternityEngine.GetOption<ImportImageWithFileNameOption>(_Data.importImageWithFileNameOptionName);
		}

		[Serializable]
		public class Data : TypingTargetOption.Data
		{
			public string importImageWithFileNameOptionName = null;

			public override object MakeAsset ()
			{
				ImageFilePathOption imageFilePathOption = ObjectPool.instance.SpawnComponent<ImageFilePathOption>(EternityEngine.instance.imageFilePathOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (imageFilePathOption);
				imageFilePathOption.Init ();
				return imageFilePathOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				ImageFilePathOption imageFilePathOption = (ImageFilePathOption) asset;
				imageFilePathOption._Data = this;
				imageFilePathOption.SetImportImageWithFileNameOptionNameFromData ();
			}
		}
	}
}
