using System;

namespace _EternityEngine
{
	public class NewNameOption : TypingTargetOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public RenameOption renameOption;

		public override bool SetValue (string value)
		{
			if (base.SetValue(value))
			{
				if (Exists(renameOption))
					renameOption.renameTo = value;
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetRenameOptionNameOfData ();
		}

		void SetRenameOptionNameOfData ()
		{
			if (Exists(renameOption))
				_Data.renameOptionName = renameOption.name;
		}

		void SetRenameOptionNameFromData ()
		{
			if (_Data.renameOptionName != null)
				renameOption = EternityEngine.GetOption<RenameOption>(_Data.renameOptionName);
		}

		[Serializable]
		public class Data : TypingTargetOption.Data
		{
			public string renameOptionName = null;

			public override object MakeAsset ()
			{
				NewNameOption newNameOption = ObjectPool.instance.SpawnComponent<NewNameOption>(EternityEngine.instance.newNameOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (newNameOption);
				newNameOption.Init ();
				return newNameOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				NewNameOption newNameOption = (NewNameOption) asset;
				newNameOption._Data = this;
				newNameOption.SetRenameOptionNameFromData ();
			}
		}
	}
}
