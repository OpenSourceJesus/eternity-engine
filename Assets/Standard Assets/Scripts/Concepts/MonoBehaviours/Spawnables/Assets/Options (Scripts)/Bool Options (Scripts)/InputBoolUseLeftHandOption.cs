using System;

namespace _EternityEngine
{
	public class InputBoolUseLeftHandOption : BoolOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public InputBoolOption inputBoolOption;

		public override bool SetValue (bool value)
		{
			if (base.SetValue(value))
			{
				if (Exists(inputBoolOption))
					inputBoolOption.isForLeftHand = value;
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetInputBoolOptionNameOfData ();
		}

		void SetInputBoolOptionNameOfData ()
		{
			if (Exists(inputBoolOption))
				_Data.inputBoolOptionName = inputBoolOption.name;
		}

		void SetInputBoolOptionNameFromData ()
		{
			if (_Data.inputBoolOptionName != null)
				inputBoolOption = EternityEngine.GetOption<InputBoolOption>(_Data.inputBoolOptionName);
		}

		[Serializable]
		public class Data : BoolOption.Data
		{
			public string inputBoolOptionName = null;

			public override object MakeAsset ()
			{
				InputBoolUseLeftHandOption inputBoolUseLeftHandOption = ObjectPool.instance.SpawnComponent<InputBoolUseLeftHandOption>(EternityEngine.instance.inputBoolUseLeftHandOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (inputBoolUseLeftHandOption);
				inputBoolUseLeftHandOption.Init ();
				return inputBoolUseLeftHandOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				InputBoolUseLeftHandOption inputBoolUseLeftHandOption = (InputBoolUseLeftHandOption) asset;
				inputBoolUseLeftHandOption._Data = this;
				inputBoolUseLeftHandOption.SetInputBoolOptionNameFromData ();
			}
		}
	}
}