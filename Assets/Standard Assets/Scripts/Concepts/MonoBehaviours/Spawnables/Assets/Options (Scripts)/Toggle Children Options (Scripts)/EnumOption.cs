using System;
using Extensions;
using UnityEngine;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;
#endif

namespace _EternityEngine
{
	public class EnumOption : ToggleChildrenOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public string enumTypeName;
		public Type enumType;
		public int value;
		public Dictionary<int, EnumValueOption[]> valueOptionsDict = new Dictionary<int, EnumValueOption[]>();
		public Dictionary<KeyValuePair<int, bool>, EnumFlagOption[]> flagOptionsDict = new Dictionary<KeyValuePair<int, bool>, EnumFlagOption[]>();
#if UNITY_EDITOR
		public bool updateChildren;
#endif

		public override void Awake ()
		{
			base.Awake ();
			enumType = Type.GetType(enumTypeName);
		}

#if UNITY_EDITOR
		public override void OnValidate ()
		{
			if (updateChildren)
			{
				enumType = Type.GetType(enumTypeName);
				int newChildCount = Enum.GetValues(enumType).Length;
				int changeInChildCount = newChildCount - children.Count;
				for (int i = 0; i < -changeInChildCount; i ++)
				{
					Option child = children[0];
					if (child != null)
#if UNITY_EDITOR
						GameManager.DestroyOnNextEditorUpdate (child.gameObject);
#else
						Destroy(child.gameObject);
#endif
					children.RemoveAt(0);
				}
				if (enumType.GetCustomAttributes(typeof(FlagsAttribute), false).Length > 0)
				{
					for (int i = 0; i < changeInChildCount; i ++)
						children.Add(Instantiate(EternityEngine.Instance.enumFlagOptionPrefab, childOptionsParent));
					Array enumNames = Enum.GetNames(enumType);
					Array enumValues = Enum.GetValues(enumType);
					Enum enumValue = GetValue();
					for (int i = 0; i < children.Count; i ++)
					{
						EnumFlagOption child = (EnumFlagOption) children[i];
						string enumName = (string) enumNames.GetValue(i);
						string childName = "";
						for (int i2 = 0; i2 < enumName.Length; i2 ++)
						{
							char c = enumName[i2];
							if (Char.IsUpper(c) && i2 > 0)
								childName += " ";
							childName += c;
						}
						child.SetName (childName);
						child.trs.localScale = Vector3.one;
						child.trs.localPosition = Vector3.left * (i + 1);
						Enum flagValue = (Enum) enumValues.GetValue(i);
						child.value = enumValue._HasFlag(flagValue);
						child.flagIndex = i;
						child.enumOption = this;
						child.gameObject.SetActive(false);
					}
				}
				else
				{
					for (int i = 0; i < changeInChildCount; i ++)
						children.Add(Instantiate(EternityEngine.Instance.enumValueOptionPrefab, childOptionsParent));
					children[value].activatable = false;
					for (int i = 0; i < children.Count; i ++)
					{
						EnumValueOption child = (EnumValueOption) children[i];
						string enumName = Enum.GetName(enumType, i);
						string childName = "";
						for (int i2 = 0; i2 < enumName.Length; i2 ++)
						{
							char c = enumName[i2];
							if (Char.IsUpper(c) && i2 > 0)
								childName += " ";
							childName += c;
						}
						child.SetName (childName);
						child.trs.localScale = Vector3.one;
						child.trs.localPosition = Vector3.left * (i + 1);
						child.value = i;
						child.enumOption = this;
						child.gameObject.SetActive(false);
					}
				}
				updateChildren = false;
			}
			base.OnValidate ();
		}
#endif

		public virtual bool SetValue (int value)
		{
			if (this.value == value)
				return false;
			EnumValueOption[] valueOptions = valueOptionsDict[value];
			for (int i = 0; i < valueOptions.Length; i ++)
			{
				EnumValueOption valueOption = valueOptions[i];
				valueOption.SetActivatable (false);
			}
			valueOptions = valueOptionsDict[this.value];
			for (int i = 0; i < valueOptions.Length; i ++)
			{
				EnumValueOption valueOption = valueOptions[i];
				valueOption.SetActivatable (true);
			}
			this.value = value;
			return true;
		}

		public virtual bool SetFlagValue (int flagIndex, bool value)
		{
			Array enumValues = Enum.GetValues(enumType);
			Enum flagValue = (Enum) enumValues.GetValue(flagIndex);
			if (GetValue()._HasFlag(flagValue) != value)
			{
				if (!value)
					this.value &= ~flagValue.GetHashCode();
				else
					this.value |= flagValue.GetHashCode();
				KeyValuePair<int, bool> keyValuePair = new KeyValuePair<int, bool>(flagIndex, !value);
				EnumFlagOption[] flagOptions;
				if (flagOptionsDict.TryGetValue(keyValuePair, out flagOptions))
				{
					for (int i = 0; i < flagOptions.Length; i ++)
					{
						EnumFlagOption enumFlagOption = flagOptions[i];
						enumFlagOption.SetActivatable (false);
					}
				}
				keyValuePair = new KeyValuePair<int, bool>(flagIndex, value);
				if (flagOptionsDict.TryGetValue(keyValuePair, out flagOptions))
				{
					for (int i = 0; i < flagOptions.Length; i ++)
					{
						EnumFlagOption enumFlagOption = flagOptions[i];
						enumFlagOption.SetActivatable (true);
					}
				}
				return true;
			}
			else
				return false;
		}

		public Enum GetValue ()
		{
			return (Enum) Enum.ToObject(enumType, value);
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetValueOfData ();
		}

		void SetValueOfData ()
		{
			_Data.value = value;
		}

		void SetValueFromData ()
		{
			value = _Data.value;
		}

		[Serializable]
		public class Data : ToggleChildrenOption.Data
		{
			public int value;
			
			public override object MakeAsset ()
			{
				EnumOption enumOption = ObjectPool.instance.SpawnComponent<EnumOption>(EternityEngine.instance.enumOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (enumOption);
				enumOption.Init ();
				return enumOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				EnumOption enumOption = (EnumOption) asset;
				enumOption._Data = this;
				enumOption.SetValueFromData ();
			}
		}
	}
}