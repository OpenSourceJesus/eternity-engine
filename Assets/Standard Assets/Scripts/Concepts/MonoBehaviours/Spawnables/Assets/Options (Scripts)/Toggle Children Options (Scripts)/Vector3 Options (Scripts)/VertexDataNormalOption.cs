
using System;
using UnityEngine;

namespace _EternityEngine
{
	public class VertexDataNormalOption : Vector3Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
public VertexDataOption vertexDataOption;
		MeshFilter referenceMeshFilter;
		uint vertexIndex;
		Transform referenceTrs;

		public override void Init ()
		{
			base.Init ();
			PhysicsObject3D physicsObject = vertexDataOption.modelFileInstanceOption.physicsObject;
			referenceMeshFilter = physicsObject.meshFilter;
			referenceTrs = physicsObject.trs;
			vertexIndex = vertexDataOption.vertexIndex;
		}

		public override void UpdateValue ()
		{
			value = referenceTrs.TransformPoint(referenceMeshFilter.mesh.normals[vertexIndex]);
		}

		public override bool SetX (float x)
		{
			if (base.SetX(x))
			{
				Mesh mesh = referenceMeshFilter.mesh;
				Vector3[] normals = mesh.normals;
				normals[vertexIndex].x = referenceTrs.InverseTransformPoint(value).x;
				mesh.SetNormals(normals);
				return true;
			}
			else
				return false;
		}

		public override bool SetY (float y)
		{
			if (base.SetY(y))
			{
				Mesh mesh = referenceMeshFilter.mesh;
				Vector3[] normals = mesh.normals;
				normals[vertexIndex].y = referenceTrs.InverseTransformPoint(value).y;
				mesh.SetNormals(normals);
				return true;
			}
			else
				return false;
		}

		public override bool SetZ (float z)
		{
			if (base.SetZ(z))
			{
				Mesh mesh = referenceMeshFilter.mesh;
				Vector3[] normals = mesh.normals;
				normals[vertexIndex].z = referenceTrs.InverseTransformPoint(value).z;
				mesh.SetNormals(normals);
				return true;
			}
			else
				return false;
		}

		public override bool SetValue (Vector3 value)
		{
			if (base.SetValue(value))
			{
				Vector3[] normals = meshFilter.mesh.normals;
				normals[vertexIndex] = value;
				meshFilter.mesh.SetNormals(normals);
				return true;
			}
			else
				return false;
		}

		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
SetVertexDataOptionNameOfData ();
		}

		
		public void SetVertexDataOptionNameOfData ()
		{
			if (Exists(vertexDataOption))
				_Data.vertexDataOptionName = vertexDataOption.name;
		}

		public void SetVertexDataOptionNameFromData ()
		{
			if (_Data.vertexDataOptionName != null)
				vertexDataOption = EternityEngine.GetOption<VertexDataOption>(_Data.vertexDataOptionName);
		}

		[Serializable]
		public class Data : Vector3Option.Data
		{
public string vertexDataOptionName = null;

			public override object MakeAsset ()
			{
				VertexDataNormalOption vertexDataNormalOption = ObjectPool.instance.SpawnComponent<VertexDataNormalOption>(EternityEngine.instance.vertexDataNormalOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (vertexDataNormalOption);
				vertexDataNormalOption.Init ();
				return vertexDataNormalOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				VertexDataNormalOption vertexDataNormalOption = (VertexDataNormalOption) asset;
				vertexDataNormalOption._Data = this;
 vertexDataNormalOption.SetVertexDataOptionNameFromData ();
			}
		}
	}
}