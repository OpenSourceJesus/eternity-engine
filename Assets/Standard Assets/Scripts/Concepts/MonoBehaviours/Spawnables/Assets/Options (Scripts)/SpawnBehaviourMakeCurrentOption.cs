using System;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class SpawnBehaviourMakeCurrentOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public SpawnBehaviourOption spawnBehaviourOption;
		static Dictionary<int, List<SpawnBehaviourMakeCurrentOption>> makeCurrentOptionsDict = new Dictionary<int, List<SpawnBehaviourMakeCurrentOption>>();

		public override void Init ()
		{
			base.Init ();
			bool spawnBehaviourOptionExists = Exists(spawnBehaviourOption);
			SetActivatable (spawnBehaviourOptionExists && spawnBehaviourOption.spawnedSpawnBehaviourOptionsDict.Count > 1);
			if (spawnBehaviourOptionExists)
			{
				List<SpawnBehaviourMakeCurrentOption> makeCurrentOptions = new List<SpawnBehaviourMakeCurrentOption>();
				if (makeCurrentOptionsDict.TryGetValue(spawnBehaviourOption.optionPrefabIndex, out makeCurrentOptions))
				{
					makeCurrentOptions.Add(this);
					makeCurrentOptionsDict[spawnBehaviourOption.optionPrefabIndex] = makeCurrentOptions;
				}
				else
					makeCurrentOptionsDict.Add(spawnBehaviourOption.optionPrefabIndex, new List<SpawnBehaviourMakeCurrentOption>(new SpawnBehaviourMakeCurrentOption[] { this }));
			}
		}

		public override void OnActivate (EternityEngine.Hand hand)
		{
			if (Exists(spawnBehaviourOption))
			{
				List<SpawnBehaviourMakeCurrentOption> makeCurrentOptions = makeCurrentOptionsDict[spawnBehaviourOption.optionPrefabIndex];
				for (int i = 0; i < makeCurrentOptions.Count; i ++)
				{
					SpawnBehaviourMakeCurrentOption makeCurrentOption = makeCurrentOptions[i];
					makeCurrentOption.SetActivatable (false);
				}
				makeCurrentOptions = makeCurrentOptionsDict[spawnBehaviourOption.optionPrefabIndex];
				for (int i = 0; i < makeCurrentOptions.Count; i ++)
				{
					SpawnBehaviourMakeCurrentOption makeCurrentOption = makeCurrentOptions[i];
					makeCurrentOption.SetActivatable (true);
				}
				spawnBehaviourOption.MakeCurrent ();
			}
			// base.OnActivate (hand);
		}

		public override void OnDeleted ()
		{
			base.OnDeleted ();
			if (Exists(spawnBehaviourOption))
			{
				List<SpawnBehaviourMakeCurrentOption> makeCurrentOptions = new List<SpawnBehaviourMakeCurrentOption>();
				if (makeCurrentOptionsDict.TryGetValue(spawnBehaviourOption.optionPrefabIndex, out makeCurrentOptions))
				{
					makeCurrentOptions.Add(this);
					makeCurrentOptionsDict.Add(spawnBehaviourOption.optionPrefabIndex, makeCurrentOptions);
				}
				else
					makeCurrentOptionsDict.Add(spawnBehaviourOption.optionPrefabIndex, new List<SpawnBehaviourMakeCurrentOption>(new SpawnBehaviourMakeCurrentOption[] { this }));
			}
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetSpawnBehaviourOptionNameOfData ();
		}

		void SetSpawnBehaviourOptionNameOfData ()
		{
			if (Exists(spawnBehaviourOption))
				_Data.spawnBehaviourOptionName = spawnBehaviourOption.name;
		}

		void SetSpawnBehaviourOptionNameFromData ()
		{
			if (_Data.spawnBehaviourOptionName != null)
				spawnBehaviourOption = EternityEngine.GetOption<SpawnBehaviourOption>(_Data.spawnBehaviourOptionName);
		}

		[Serializable]
		public class Data : Option.Data
		{
			public string spawnBehaviourOptionName = null;

			public override object MakeAsset ()
			{
				SpawnBehaviourMakeCurrentOption spawnBehaviourMakeCurrentOption = ObjectPool.instance.SpawnComponent<SpawnBehaviourMakeCurrentOption>(EternityEngine.instance.spawnBehaviourMakeCurrentOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (spawnBehaviourMakeCurrentOption);
				spawnBehaviourMakeCurrentOption.Init ();
				return spawnBehaviourMakeCurrentOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				SpawnBehaviourMakeCurrentOption spawnBehaviourMakeCurrentOption = (SpawnBehaviourMakeCurrentOption) asset;
				spawnBehaviourMakeCurrentOption._Data = this;
				spawnBehaviourMakeCurrentOption.SetSpawnBehaviourOptionNameFromData ();
			}
		}
	}
}