using System;

namespace _EternityEngine
{
	public class DragOption : TypingTargetOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public ModelFileInstanceOption modelFileInstanceOption;

		public override bool SetValue (string value)
		{
			if (base.SetValue(value))
			{
				if (Exists(modelFileInstanceOption))
				{
					float? drag = GetNumberValue();
					if (drag != null)
						modelFileInstanceOption.SetDrag ((float) drag);
				}
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetModelFileInstanceOptionNameOfData ();
		}

		void SetModelFileInstanceOptionNameOfData ()
		{
			if (Exists(modelFileInstanceOption))
				_Data.modelFileInstanceOptionName = modelFileInstanceOption.name;
		}

		void SetModelFileInstanceOptionNameFromData ()
		{
			if (_Data.modelFileInstanceOptionName != null)
				modelFileInstanceOption = EternityEngine.GetOption<ModelFileInstanceOption>(_Data.modelFileInstanceOptionName);
		}

		[Serializable]
		public class Data : TypingTargetOption.Data
		{
			public string modelFileInstanceOptionName = null;

			public override object MakeAsset ()
			{
				DragOption dragOption = ObjectPool.instance.SpawnComponent<DragOption>(EternityEngine.instance.dragOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (dragOption);
				dragOption.Init ();
				return dragOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				DragOption dragOption = (DragOption) asset;
				dragOption._Data = this;
				dragOption.SetModelFileInstanceOptionNameFromData ();
			}
		}
	}
}