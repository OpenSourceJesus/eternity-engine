using System;

namespace _EternityEngine
{
	public class GreenOption : TypingTargetOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public ColorOption colorOption;

		public override bool SetValue (string value)
		{
			if (base.SetValue(value))
			{
				if (Exists(colorOption))
				{
					float? g = GetNumberValue();
					if (g != null)
						colorOption.SetG ((float) g);
				}
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetColorOptionNameOfData ();
		}

		void SetColorOptionNameOfData ()
		{
			if (Exists(colorOption))
				_Data.colorOptionName = colorOption.name;
		}

		void SetColorOptionNameFromData ()
		{
			if (_Data.colorOptionName != null)
				colorOption = EternityEngine.GetOption<ColorOption>(_Data.colorOptionName);
		}

		[Serializable]
		public class Data : TypingTargetOption.Data
		{
			public string colorOptionName = null;

			public override object MakeAsset ()
			{
				GreenOption greenOption = ObjectPool.instance.SpawnComponent<GreenOption>(EternityEngine.instance.greenOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (greenOption);
				greenOption.Init ();
				return greenOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				GreenOption greenOption = (GreenOption) asset;
				greenOption._Data = this;
				greenOption.SetColorOptionNameFromData ();
			}
		}
	}
}