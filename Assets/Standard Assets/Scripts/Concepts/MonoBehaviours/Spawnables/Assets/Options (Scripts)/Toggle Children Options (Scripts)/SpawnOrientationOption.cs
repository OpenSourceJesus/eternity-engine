using System;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class SpawnOrientationOption : ToggleChildrenOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public DeleteCurrentBehaviour deleteCurrentBehaviour;
		public SortedDictionary<int, SpawnOrientationOption> spawnedSpawnOrientationOptionsDict = new SortedDictionary<int, SpawnOrientationOption>();
		static int mostRecentSpawnedOrder = -1;
		int activatedOrder;
		int spawnedOrder;
		SortedDictionary<int, SpawnOrientationOption> activatedSpawnOrientationOptionsDict = new SortedDictionary<int, SpawnOrientationOption>();

		public override void Init ()
		{
			base.Init ();
			if (EternityEngine.instance.currentSpawnOrientationOption == null)
				EternityEngine.instance.currentSpawnOrientationOption = this;
			activatedOrder = activatedSpawnOrientationOptionsDict.Count;
			activatedSpawnOrientationOptionsDict.Add(activatedOrder, this);
			mostRecentSpawnedOrder ++;
			spawnedOrder = mostRecentSpawnedOrder;
			spawnedSpawnOrientationOptionsDict.Add(spawnedOrder, this);
		}

		public void MakeCurrent ()
		{
			EternityEngine.instance.currentSpawnOrientationOption = this;
			activatedSpawnOrientationOptionsDict.Remove(activatedOrder);
			activatedOrder = activatedSpawnOrientationOptionsDict.Count - 1;
			activatedSpawnOrientationOptionsDict.Add(activatedOrder, this);
		}

		public override void OnDeleted ()
		{
			base.OnDeleted ();
			activatedSpawnOrientationOptionsDict.Remove(activatedOrder);
			spawnedSpawnOrientationOptionsDict.Remove(spawnedOrder);
			if (deleteCurrentBehaviour == DeleteCurrentBehaviour.PreferUseMostRecentActiveThenMostRecentSpawned)
			{
				if (!TryMakeCurrentMostRecentInDictionary(activatedSpawnOrientationOptionsDict))
					TryMakeCurrentMostRecentInDictionary (spawnedSpawnOrientationOptionsDict);
			}
			else if (deleteCurrentBehaviour == DeleteCurrentBehaviour.PreferUseMostRecentActiveThenLeastRecentSpawned)
			{
				if (!TryMakeCurrentMostRecentInDictionary(activatedSpawnOrientationOptionsDict))
					TryMakeCurrentLeastRecentInDictionary (spawnedSpawnOrientationOptionsDict);
			}
			else if (deleteCurrentBehaviour == DeleteCurrentBehaviour.PreferUseLeastRecentActiveThenMostRecentSpawned)
			{
				if (!TryMakeCurrentLeastRecentInDictionary(activatedSpawnOrientationOptionsDict))
					TryMakeCurrentMostRecentInDictionary (spawnedSpawnOrientationOptionsDict);
			}
			else if (deleteCurrentBehaviour == DeleteCurrentBehaviour.PreferUseLeastRecentActiveThenLeastRecentSpawned)
			{
				if (!TryMakeCurrentLeastRecentInDictionary(activatedSpawnOrientationOptionsDict))
					TryMakeCurrentLeastRecentInDictionary (spawnedSpawnOrientationOptionsDict);
			}
			else if (deleteCurrentBehaviour == DeleteCurrentBehaviour.UseMostRecentSpawned)
			{
				TryMakeCurrentMostRecentInDictionary (spawnedSpawnOrientationOptionsDict);
			}
			else// if (deleteCurrentBehaviour == DeleteCurrentBehaviour.UseLeastRecentSpawned)
			{
				TryMakeCurrentLeastRecentInDictionary (spawnedSpawnOrientationOptionsDict);
			}
		}

		bool TryMakeCurrentLeastRecentInDictionary (SortedDictionary<int, SpawnOrientationOption> dict)
		{
			if (dict.Count == 0)
				return false;
			SpawnOrientationOption spawnOrientationOption;
			int spawnOrientationOptionIndex = 0;
			while (!dict.TryGetValue(spawnOrientationOptionIndex, out spawnOrientationOption))
			{
				spawnOrientationOptionIndex ++;
				if (spawnOrientationOptionIndex > mostRecentSpawnedOrder)
					return false;
			}
			spawnOrientationOption.MakeCurrent ();
			return true;
		}

		bool TryMakeCurrentMostRecentInDictionary (SortedDictionary<int, SpawnOrientationOption> dict)
		{
			if (dict.Count == 0)
				return false;
			SpawnOrientationOption spawnOrientationOption;
			int spawnOrientationOptionIndex = mostRecentSpawnedOrder;
			while (!dict.TryGetValue(spawnOrientationOptionIndex, out spawnOrientationOption))
			{
				spawnOrientationOptionIndex --;
				if (spawnOrientationOptionIndex == -1)
					return false;
			}
			spawnOrientationOption.MakeCurrent ();
			return true;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetActivatedOrderOfData ();
			SetSpawnedOrderOfData ();
		}

		void SetActivatedOrderOfData ()
		{
			_Data.activatedOrder = activatedOrder;
		}

		void SetActivatedOrderFromData ()
		{
			activatedOrder = _Data.activatedOrder;
		}

		void SetSpawnedOrderOfData ()
		{
			_Data.spawnedOrder = spawnedOrder;
		}

		void SetSpawnedOrderFromData ()
		{
			spawnedOrder = _Data.spawnedOrder;
			if (spawnedOrder > mostRecentSpawnedOrder)
				mostRecentSpawnedOrder = spawnedOrder;
		}

		public enum DeleteCurrentBehaviour
		{
			PreferUseMostRecentActiveThenMostRecentSpawned,
			PreferUseMostRecentActiveThenLeastRecentSpawned,
			PreferUseLeastRecentActiveThenMostRecentSpawned,
			PreferUseLeastRecentActiveThenLeastRecentSpawned,
			UseMostRecentSpawned,
			UseLeastRecentSpawned
		}

		[Serializable]
		public class Data : ToggleChildrenOption.Data
		{
			public int activatedOrder;
			public int spawnedOrder;

			public override object MakeAsset ()
			{
				SpawnOrientationOption spawnOrientationOption = ObjectPool.instance.SpawnComponent<SpawnOrientationOption>(EternityEngine.instance.spawnOrientationOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (spawnOrientationOption);
				spawnOrientationOption.Init ();
				return spawnOrientationOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				SpawnOrientationOption spawnOrientationOption = (SpawnOrientationOption) asset;
				spawnOrientationOption._Data = this;
				spawnOrientationOption.SetActivatedOrderFromData ();
				spawnOrientationOption.SetSpawnedOrderFromData ();
			}
		}
	}
}