using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class DeltaTimeOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public override string NameToValueSeparator
		{
			get
			{
				return ": ";
			}
		}
		
		public override void OnActivate (EternityEngine.Hand hand)
		{
			SetValue ("" + Time.deltaTime);
			// base.OnActivate (hand);
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
		}

		[Serializable]
		public class Data : Option.Data
		{
			public override object MakeAsset ()
			{
				DeltaTimeOption deltaTimeOption = ObjectPool.instance.SpawnComponent<DeltaTimeOption>(EternityEngine.instance.deltaTimeOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (deltaTimeOption);
				deltaTimeOption.Init ();
				return deltaTimeOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				DeltaTimeOption deltaTimeOption = (DeltaTimeOption) asset;
				deltaTimeOption._Data = this;
			}
		}
	}
}