using System;

namespace _EternityEngine
{
	public class OneInputOperationsTooManyChildrenBehaviourOption : EnumOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public OneInputOperationsOption oneInputOperationsOption;

		public override bool SetValue (int value)
		{
			bool output = base.SetValue(value);
			if (output && Exists(oneInputOperationsOption))
				oneInputOperationsOption.tooManyChildrenBehaviour = (OneInputOperationsOption.TooManyChildrenBehaviour) Enum.ToObject(enumType, value);
			return output;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetOneInputOperationsOptionNameOfData ();
		}

		void SetOneInputOperationsOptionNameOfData ()
		{
			if (Exists(oneInputOperationsOption))
				_Data.oneInputOperationsOptionName = oneInputOperationsOption.name;
		}

		void SetOneInputOperationsOptionNameFromData ()
		{
			if (_Data.oneInputOperationsOptionName != null)
				oneInputOperationsOption = EternityEngine.GetOption<OneInputOperationsOption>(_Data.oneInputOperationsOptionName);
		}

		[Serializable]
		public class Data : EnumOption.Data
		{
			public string oneInputOperationsOptionName = null;

			public override object MakeAsset ()
			{
				OneInputOperationsTooManyChildrenBehaviourOption oneInputOperationsOption = ObjectPool.instance.SpawnComponent<OneInputOperationsTooManyChildrenBehaviourOption>(EternityEngine.instance.oneInputOperationsTooManyChildrenBehaviourOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (oneInputOperationsOption);
				oneInputOperationsOption.Init ();
				return oneInputOperationsOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				OneInputOperationsTooManyChildrenBehaviourOption oneInputOperationsOption = (OneInputOperationsTooManyChildrenBehaviourOption) asset;
				oneInputOperationsOption._Data = this;
				oneInputOperationsOption.SetOneInputOperationsOptionNameFromData ();
			}
		}
	}
}