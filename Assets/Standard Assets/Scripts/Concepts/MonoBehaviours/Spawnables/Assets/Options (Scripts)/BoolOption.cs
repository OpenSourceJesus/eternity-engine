using System;

namespace _EternityEngine
{
	public class BoolOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public override string NameToValueSeparator
		{
			get
			{
				return ": ";
			}
		}
		public bool value;

#if UNITY_EDITOR
		public override void OnValidate ()
		{
			if (EternityEngine.Instance != null)
			{
				EternityEngine.instance.activationBehaviourOptionsDict.Init ();
				Array triggerTypes = Enum.GetValues(typeof(Option.TriggerType));
				for (int i = 0; i < triggerTypes.Length; i ++)
				{
					Option.TriggerType triggerType = (Option.TriggerType) triggerTypes.GetValue(i);
					EternityEngine.instance.activationBehaviourOptionsDict[triggerType].Init ();
				}
				if (value)
					SetValue ("True");
				else
					SetValue ("False");
			}
			base.OnValidate ();
		}
#endif

		public override void OnActivate (EternityEngine.Hand hand)
		{
			SetValue (!value);
			// base.OnActivate (hand);
		}

		public virtual bool SetValue (bool value)
		{
			if (this.value == value)
				return false;
			this.value = value;
			if (value)
				SetValue ("True");
			else
				SetValue ("False");
			return true;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetValueOfData ();
		}

		void SetValueOfData ()
		{
			value = _Data.value;
		}

		void SetValueFromData ()
		{
			SetValue (_Data.value);
			_Data.value = value;
		}

		[Serializable]
		public class Data : Option.Data
		{
			public bool value;
			
			public override object MakeAsset ()
			{
				BoolOption boolOption = ObjectPool.instance.SpawnComponent<BoolOption>(EternityEngine.instance.boolOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (boolOption);
				boolOption.Init ();
				return boolOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				BoolOption boolOption = (BoolOption) asset;
				boolOption._Data = this;
				boolOption.SetValueFromData ();
			}
		}
	}
}