using System;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class ModelFileInstanceOption : FileInstanceOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public TransformOption transformOption;
		public PhysicsObject3D physicsObject;
		public Option collisionLayersParentOption;
		public Option startedCollisionsParentOption;
		public Option continuedCollisionsParentOption;
		public Option endedCollisionsParentOption;
		public _Rigidbody modelRigid;
		public static Dictionary<MeshCollider, ModelFileInstanceOption> meshCollidersDict = new Dictionary<MeshCollider, ModelFileInstanceOption>();
		public static Dictionary<string, List<ModelFileInstanceOption>> collisionLayersDict = new Dictionary<string, List<ModelFileInstanceOption>>();
		Shader shader;
		_PhysicsMaterial physicsMaterial;
		_Material material;
		PhysicsMode physicsMode = PhysicsMode.RigidBody;
		GameObject modelGo;
		bool shouldRecordStartOfCollisions;
		bool shouldRecordContinuedCollisions;
		bool shouldRecordEndOfCollisions;
		RecordStartOfCollisions recordStartOfCollisions;
		RecordContinuedCollisions recordContinuedCollisions;
		RecordEndOfCollisions recordEndOfCollisions;
		bool visible;

		public override void Init (FileOption fileOption)
		{
			this.fileOption = fileOption;
			HandleNaming ();
			ModelFileOption modelFile = (ModelFileOption) fileOption;
			modelGo = Instantiate(modelFile.modelGo);
			physicsObject = modelGo.AddComponent<PhysicsObject3D>();
			physicsObject.renderer = modelGo.GetComponentInChildren<Renderer>();
			physicsObject.renderer.enabled = false;
			physicsObject.meshFilter = physicsObject.renderer.GetComponent<MeshFilter>();
			physicsObject.trs = modelGo.GetComponent<Transform>();
			Rigidbody rigid = modelGo.AddComponent<Rigidbody>();
			rigid.isKinematic = true;
			rigid.constraints = RigidbodyConstraints.FreezeAll;
			rigid.angularDamping = 0;
			physicsObject.rigid = rigid;
			modelGo.layer = LayerMask.NameToLayer("Models");
			modelGo.SetActive(true);
			transformOption.Init (physicsObject.trs);
			if (Exists(collisionLayersParentOption))
			{
				collisionLayersParentOption.onAboutToAddChild += OnAboutToAddCollisionLayerChild;
				collisionLayersParentOption.onAddChild += OnAddCollisionLayerChild;
				collisionLayersParentOption.onRemoveChild += OnRemoveCollisionLayerChild;
			}
			RecordStartOfCollisions (shouldRecordStartOfCollisions);
			RecordContinuedCollisions (shouldRecordContinuedCollisions);
			RecordEndOfCollisions (shouldRecordEndOfCollisions);
			physicsObject.modelFileInstanceOption = this;
		}

		bool OnAboutToAddCollisionLayerChild (Option child)
		{
			return true;
		}

		void OnAddCollisionLayerChild (Option child)
		{
			
		}

		void OnRemoveCollisionLayerChild (Option child)
		{
			
		}

		public void RecordStartOfCollisions (bool value)
		{
			shouldRecordStartOfCollisions = value;
			if (value)
			{
				recordStartOfCollisions = modelGo.AddComponent<RecordStartOfCollisions>();
				recordStartOfCollisions.parentOption = startedCollisionsParentOption;
			}
			else
				Destroy(recordStartOfCollisions);
		}

		public void RecordContinuedCollisions (bool value)
		{
			shouldRecordContinuedCollisions = value;
			if (value)
			{
				recordContinuedCollisions = modelGo.AddComponent<RecordContinuedCollisions>();
				recordContinuedCollisions.parentOption = continuedCollisionsParentOption;
			}
			else
				Destroy(recordContinuedCollisions);
		}

		public void RecordEndOfCollisions (bool value)
		{
			shouldRecordEndOfCollisions = value;
			if (value)
			{
				recordEndOfCollisions = modelGo.AddComponent<RecordEndOfCollisions>();
				recordEndOfCollisions.parentOption = endedCollisionsParentOption;
			}
			else
				Destroy(recordEndOfCollisions);
		}

		public void SetPhysicsMode (PhysicsMode physicsMode)
		{
			if (physicsMode == PhysicsMode.None)
			{
				if (physicsObject.rigid != null)
					Destroy(physicsObject.rigid);
				else
					Destroy(physicsObject.articulationBody);
			}
			else if (physicsMode == PhysicsMode.RigidBody)
			{
				if (physicsObject.articulationBody != null)
					Destroy(physicsObject.articulationBody);
				if (physicsObject.rigid == null)
					physicsObject.rigid = modelGo.AddComponent<Rigidbody>();
			}
			else// if (physicsMode == PhysicsMode.ArticulationBody)
			{
				if (physicsObject.rigid != null)
					Destroy(physicsObject.rigid);
				physicsObject.articulationBody = modelGo.AddComponent<ArticulationBody>();
			}
		}

		public void SetVisible (bool value)
		{
			visible = value;
			physicsObject.renderer.enabled = value;
		}

		public void SetShader (string shaderName)
		{
			material.shaderName = shaderName;
		}

		public void SetMaterial (string materialFolderPath)
		{
			physicsObject.renderer.sharedMaterial = _Material.ToMat(materialFolderPath, material.shaderName);
		}

		public void SetMeshCollider ()
		{
			if (physicsObject.collider != null)
			{
				DestroyImmediate(physicsObject.collider);
				meshCollidersDict.Remove((MeshCollider) physicsObject.collider);
			}
			MeshCollider meshCollider = physicsObject.renderer.gameObject.AddComponent<MeshCollider>();
			physicsObject.collider = meshCollider;
			meshCollidersDict.Add(meshCollider, this);
			physicsObject.collider.material = physicsMaterial.ToPhysicsMat();
		}

		public void SetMass (float mass)
		{
			modelRigid.mass = mass;
			if (physicsObject.rigid != null)
				physicsObject.rigid.mass = mass;			
		}

		public void SetConstrainType (_Rigidbody.ConstrainType constrainType)
		{
			modelRigid.constrainType = constrainType;
			if (physicsObject.rigid != null)
				modelRigid.ApplyConstrainType (physicsObject.rigid);
		}

		public void SetStaticFriction (float staticFriction)
		{
			physicsMaterial.staticFriction = staticFriction;
			if (physicsObject.collider != null)
				physicsObject.collider.material.staticFriction = physicsMaterial.staticFriction;
		}

		public void SetDynamicFriction (float dynamicFriction)
		{
			physicsMaterial.dynamicFriction = dynamicFriction;
			if (physicsObject.collider != null)
				physicsObject.collider.material.dynamicFriction = physicsMaterial.dynamicFriction;
		}

		public void SetFrictionCombineMode (_PhysicsMaterial.ValueCombineMode frictionCombineMode)
		{
			physicsMaterial.frictionCombineMode = frictionCombineMode;
			if (physicsObject.collider != null)
				physicsObject.collider.material.frictionCombine = (PhysicsMaterialCombine) Enum.ToObject(typeof(PhysicsMaterialCombine), frictionCombineMode.GetHashCode());
		}

		public void SetBounciness (float bounciness)
		{
			physicsMaterial.bounciness = bounciness;
			if (physicsObject.collider != null)
				physicsObject.collider.material.bounciness = physicsMaterial.bounciness;
		}

		public void SetBouncinessCombineMode (_PhysicsMaterial.ValueCombineMode bouncinessCombineMode)
		{
			physicsMaterial.bouncinessCombineMode = bouncinessCombineMode;
			if (physicsObject.collider != null)
				physicsObject.collider.material.bounceCombine = (PhysicsMaterialCombine) Enum.ToObject(typeof(PhysicsMaterialCombine), bouncinessCombineMode.GetHashCode());
		}

		public void SetDrag (float drag)
		{
			if (physicsObject.rigid != null)
				physicsObject.rigid.linearDamping = drag;
		}

		public void SetAngularDrag (float angularDrag)
		{
			if (physicsObject.rigid != null)
				physicsObject.rigid.angularDamping = angularDrag;
		}

		public override void OnDeleted ()
		{
			// collisionLayersParentOption.onAboutToAddChild -= OnAboutToAddCollisionLayerChild;
			// collisionLayersParentOption.onAddChild -= OnAddCollisionLayerChild;
			// collisionLayersParentOption.onRemoveChild -= OnRemoveCollisionLayerChild;
			base.OnDeleted ();
			Destroy(physicsObject.gameObject);
		}

		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetTransformOptionNameOfData ();
			SetShouldRecordStartOfCollisionsOfData ();
			SetShouldRecordContinuedCollisionsOfData ();
			SetShouldRecordEndOfCollisionsOfData ();
			SetStartedCollisionsParentOptionNameOfData ();
			SetContinuedCollisionsParentOptionNameOfData ();
			SetEndedCollisionsParentOptionNameOfData ();
			SetVisibleOfData ();
			SetMaterialOfData ();
			SetRigidbodyOfData ();
			SetPhysicsMaterialOfData ();
			SetPhysicsModeOfData ();
		}

		void SetTransformOptionNameOfData ()
		{
			if (Exists(transformOption))
				_Data.transformOptionName = transformOption.name;
		}

		void SetTransformOptionNameFromData ()
		{
			if (_Data.transformOptionName != null)
				transformOption = EternityEngine.GetOption<TransformOption>(_Data.transformOptionName);
		}

		void SetShouldRecordStartOfCollisionsOfData ()
		{
			_Data.shouldRecordStartOfCollisions = shouldRecordStartOfCollisions;
		}

		void SetShouldRecordStartOfCollisionsFromData ()
		{
			shouldRecordStartOfCollisions = _Data.shouldRecordStartOfCollisions;
		}

		void SetShouldRecordContinuedCollisionsOfData ()
		{
			_Data.shouldRecordContinuedCollisions = shouldRecordContinuedCollisions;
		}

		void SetShouldRecordContinuedCollisionsFromData ()
		{
			shouldRecordContinuedCollisions = _Data.shouldRecordContinuedCollisions;
		}

		void SetShouldRecordEndOfCollisionsOfData ()
		{
			_Data.shouldRecordEndOfCollisions = shouldRecordEndOfCollisions;
		}

		void SetShouldRecordEndOfCollisionsFromData ()
		{
			shouldRecordEndOfCollisions = _Data.shouldRecordEndOfCollisions;
		}

		void SetStartedCollisionsParentOptionNameOfData ()
		{
			if (Exists(startedCollisionsParentOption))
				_Data.startedCollisionsParentOptionName = startedCollisionsParentOption.name;
		}

		void SetStartedCollisionsParentOptionNameFromData ()
		{
			if (_Data.startedCollisionsParentOptionName != null)
				startedCollisionsParentOption = EternityEngine.GetOption(_Data.startedCollisionsParentOptionName);
		}

		void SetContinuedCollisionsParentOptionNameOfData ()
		{
			if (Exists(continuedCollisionsParentOption))
				_Data.continuedCollisionsParentOptionName = continuedCollisionsParentOption.name;
		}

		void SetContinuedCollisionsParentOptionNameFromData ()
		{
			if (_Data.continuedCollisionsParentOptionName != null)
				continuedCollisionsParentOption = EternityEngine.GetOption(_Data.continuedCollisionsParentOptionName);
		}

		void SetEndedCollisionsParentOptionNameOfData ()
		{
			if (Exists(endedCollisionsParentOption))
				_Data.endedCollisionsParentOptionName = endedCollisionsParentOption.name;
		}

		void SetEndedCollisionsParentOptionNameFromData ()
		{
			if (_Data.endedCollisionsParentOptionName != null)
				endedCollisionsParentOption = EternityEngine.GetOption(_Data.endedCollisionsParentOptionName);
		}

		void SetVisibleOfData ()
		{
			_Data.visible = visible;
		}

		void SetVisibleFromData ()
		{
			SetVisible (_Data.visible);
		}

		void SetMaterialOfData ()
		{
			_Data.material = material;
		}

		void SetMaterialFromData ()
		{
			material = _Data.material;
			Material _material = material.ToMat();
			if (_material != null)
				physicsObject.renderer.material = _material;
		}

		void SetPhysicsModeOfData ()
		{
			_Data.physicsMode = physicsMode;
		}

		void SetPhysicsModeFromData ()
		{
			physicsMode = _Data.physicsMode;
			SetPhysicsMode (physicsMode);
		}

		void SetRigidbodyOfData ()
		{
			if (physicsObject.rigid != null)
				modelRigid = _Rigidbody.FromRigid(physicsObject.rigid);
			else
			{
				modelRigid.position = _Vector3.FromVec3(physicsObject.trs.position);
				modelRigid.eulerAngles = _Vector3.FromVec3(physicsObject.trs.eulerAngles);
			}	
			_Data.rigid = modelRigid;
		}

		void SetRigidbodyFromData ()
		{
			modelRigid = _Data.rigid;
			Rigidbody rigid = physicsObject.rigid;
			if (rigid != null)
				modelRigid.ToRigid (rigid);
			else
			{
				physicsObject.trs.position = modelRigid.position.ToVec3();
				physicsObject.trs.eulerAngles = modelRigid.eulerAngles.ToVec3();
			}
		}

		void SetPhysicsMaterialOfData ()
		{
			_Data.physicsMaterial = physicsMaterial;
		}

		void SetPhysicsMaterialFromData ()
		{
			physicsMaterial = _Data.physicsMaterial;
		}

		public enum PhysicsMode
		{
			None,
			RigidBody,
			ArticulationBody
		}

		[Serializable]
		public class Data : FileInstanceOption.Data
		{
			public bool visible;
			public _Material material;
			public _Rigidbody rigid;
			public PhysicsMode physicsMode;
			public _Vector3 velocity;
			public _Vector3 angularVelocity;
			public _PhysicsMaterial physicsMaterial;
			public bool shouldRecordStartOfCollisions;
			public bool shouldRecordContinuedCollisions;
			public bool shouldRecordEndOfCollisions;
			public string transformOptionName = null;
			public string startedCollisionsParentOptionName = null;
			public string continuedCollisionsParentOptionName = null;
			public string endedCollisionsParentOptionName = null;

			public override object MakeAsset ()
			{
				ModelFileInstanceOption modelFileInstanceOption = ObjectPool.instance.SpawnComponent<ModelFileInstanceOption>(EternityEngine.instance.modelFileInstanceOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (modelFileInstanceOption);
				modelFileInstanceOption.Init ();
				return modelFileInstanceOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				ModelFileInstanceOption modelFileInstanceOption = (ModelFileInstanceOption) asset;
				modelFileInstanceOption._Data = this;
				modelFileInstanceOption.SetTransformOptionNameFromData ();
				modelFileInstanceOption.SetShouldRecordStartOfCollisionsFromData ();
				modelFileInstanceOption.SetShouldRecordContinuedCollisionsFromData ();
				modelFileInstanceOption.SetShouldRecordEndOfCollisionsFromData ();
				modelFileInstanceOption.SetStartedCollisionsParentOptionNameFromData ();
				modelFileInstanceOption.SetContinuedCollisionsParentOptionNameFromData ();
				modelFileInstanceOption.SetEndedCollisionsParentOptionNameFromData ();
				modelFileInstanceOption.Init (modelFileInstanceOption.fileOption);
				modelFileInstanceOption.SetVisibleFromData ();
				modelFileInstanceOption.SetMaterialFromData ();
				modelFileInstanceOption.SetRigidbodyFromData ();
				modelFileInstanceOption.SetPhysicsMaterialFromData ();
				modelFileInstanceOption.SetPhysicsModeFromData ();
			}
		}
	}
}