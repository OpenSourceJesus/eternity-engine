
using System;

namespace _EternityEngine
{
	public class VertexDataTangentOption : Vector3Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
public VertexDataOption vertexDataOption;

		public override void OnActivate (EternityEngine.Hand hand)
		{
			
			// base.OnActivate (hand);
		}

		public override bool SetValue (string value)
		{
			bool output = base.SetValue(value);

			return output;
		}

		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
SetVertexDataOptionNameOfData ();
		}

		
		public void SetVertexDataOptionNameOfData ()
		{
			if (Exists(vertexDataOption))
				_Data.vertexDataOptionName = vertexDataOption.name;
		}

		public void SetVertexDataOptionNameFromData ()
		{
			if (_Data.vertexDataOptionName != null)
				vertexDataOption = EternityEngine.GetOption<VertexDataOption>(_Data.vertexDataOptionName);
		}

		[Serializable]
		public class Data : Vector3Option.Data
		{
public string vertexDataOptionName = null;

			public override object MakeAsset ()
			{
				VertexDataTangentOption vertexDataTangentOption = ObjectPool.instance.SpawnComponent<VertexDataTangentOption>(EternityEngine.instance.vertexDataTangentOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (vertexDataTangentOption);
				vertexDataTangentOption.Init ();
				return vertexDataTangentOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				VertexDataTangentOption vertexDataTangentOption = (VertexDataTangentOption) asset;
				vertexDataTangentOption._Data = this;
 vertexDataTangentOption.SetVertexDataOptionNameFromData ();
			}
		}
	}
}