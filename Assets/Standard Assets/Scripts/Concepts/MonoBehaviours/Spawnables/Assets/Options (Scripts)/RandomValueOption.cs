using System;
using Random = UnityEngine.Random;

namespace _EternityEngine
{
	public class RandomValueOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public override string NameToValueSeparator
		{
			get
			{
				return ": ";
			}
		}
		
		public override void OnActivate (EternityEngine.Hand hand)
		{
			SetValue ("" + Random.value);
			// base.OnActivate (hand);
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
		}

		[Serializable]
		public class Data : Option.Data
		{
			public override object MakeAsset ()
			{
				RandomValueOption randomValueOption = ObjectPool.instance.SpawnComponent<RandomValueOption>(EternityEngine.instance.randomValueOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (randomValueOption);
				randomValueOption.Init ();
				return randomValueOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				RandomValueOption randomValueOption = (RandomValueOption) asset;
				randomValueOption._Data = this;
			}
		}
	}
}