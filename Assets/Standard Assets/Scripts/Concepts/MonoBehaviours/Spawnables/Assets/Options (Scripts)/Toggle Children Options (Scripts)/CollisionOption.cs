using System;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;

namespace _EternityEngine
{
	public class CollisionOption : ToggleChildrenOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public Vector3Option relativeVelocityOption;
		public Vector3Option impulseOption;
		public TypingTargetOption collidingObjectNameOption;

		public void Init (Collision coll)
		{
			relativeVelocityOption.SetValue (coll.relativeVelocity);
			relativeVelocityOption.UpdateTexts ();
			impulseOption.SetValue (coll.impulse);
			impulseOption.UpdateTexts ();
			collidingObjectNameOption.SetValue (ModelFileInstanceOption.meshCollidersDict[(MeshCollider) coll.collider].name);
			ContactPoint[] contactPoints = new ContactPoint[coll.contactCount];
			for (int i = 0; i < contactPoints.Length; i ++)
			{
				ContactPoint contactPoint = contactPoints[i];
				ContactPointOption contactPointOption = ObjectPool.instance.SpawnComponent<ContactPointOption>(EternityEngine.instance.contactPointOptionPrefab.prefabIndex, EternityEngine.instance.currentSpawnOrientationOption.trs.position, EternityEngine.instance.currentSpawnOrientationOption.trs.rotation, EternityEngine.instance.sceneTrs);
				contactPointOption.trs.SetWorldScale (EternityEngine.instance.currentSpawnOrientationOption.trs.lossyScale);
				contactPointOption.Init (contactPoint);
			}
		}

		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetCollidingObjectNameOptionNameOfData ();
		}

		void SetCollidingObjectNameOptionNameOfData ()
		{
			if (Exists(collidingObjectNameOption))
				_Data.collidingObjectNameOptionName = collidingObjectNameOption.name;
		}

		void SetCollidingObjectNameOptionNameFromData ()
		{
			if (_Data.collidingObjectNameOptionName != null)
				collidingObjectNameOption = EternityEngine.GetOption<TypingTargetOption>(_Data.collidingObjectNameOptionName);
		}

		[Serializable]
		public class Data : ToggleChildrenOption.Data
		{
			public string collidingObjectNameOptionName = null;
			
			public override object MakeAsset ()
			{
				CollisionOption collisionOption = ObjectPool.instance.SpawnComponent<CollisionOption>(EternityEngine.instance.collisionOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (collisionOption);
				collisionOption.Init ();
				return collisionOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				CollisionOption collisionOption = (CollisionOption) asset;
				collisionOption._Data = this;
				collisionOption.SetCollidingObjectNameOptionNameFromData ();
			}
		}
	}
}