using System;
using Extensions;
using UnityEngine;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class InputTransformOption : TransformOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public Type type;

		public override void Init ()
		{
			base.Init ();
			ApplyType ();
		}

		public void ApplyType ()
		{
			if (type == Type.LeftHand)
				Init (VRCameraRig.Instance.leftHandTrs);
			else if (type == Type.RightHand)
				Init (VRCameraRig.Instance.rightHandTrs);
			else// if (type == Type.Head)
				Init (VRCameraRig.Instance.eyesTrs);
		}

		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetTypeOfData ();
		}

		void SetTypeOfData ()
		{
			_Data.type = type;
		}

		void SetTypeFromData ()
		{
			type = _Data.type;
		}

		public enum Type
		{
			LeftHand,
			RightHand,
			Head
		}

		[Serializable]
		public class Data : TransformOption.Data
		{
			public Type type;

			public override object MakeAsset ()
			{
				InputTransformOption inputTransformOption = ObjectPool.instance.SpawnComponent<InputTransformOption>(EternityEngine.instance.inputTransformOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (inputTransformOption);
				inputTransformOption.Init ();
				return inputTransformOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				InputTransformOption inputTransformOption = (InputTransformOption) asset;
				inputTransformOption._Data = this;
				inputTransformOption.SetTypeFromData ();
			}
		}
	}
}