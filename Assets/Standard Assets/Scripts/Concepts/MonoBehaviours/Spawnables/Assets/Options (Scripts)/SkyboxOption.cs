using System;
using UnityEngine;

namespace _EternityEngine
{
	public class SkyboxOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public string materialFolderPath;
		public string shaderName;
		_Material material;

		public override void OnActivate (EternityEngine.Hand hand)
		{
			SaveAndLoadManager.saveData.skyboxMaterial = new _Material(materialFolderPath, shaderName);
			Material skybox = SaveAndLoadManager.saveData.skyboxMaterial.ToMat();
			if (skybox != null)
				RenderSettings.skybox = skybox;
			// base.OnActivate (hand);
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetMaterialFolderPathOfData ();
			SetShaderNameOfData ();
		}

		void SetMaterialFolderPathOfData ()
		{
			_Data.materialFolderPath = materialFolderPath;
		}

		void SetMaterialFolderPathFromData ()
		{
			materialFolderPath = _Data.materialFolderPath;
		}

		void SetShaderNameOfData ()
		{
			_Data.shaderName = shaderName;
		}

		void SetShaderNameFromData ()
		{
			shaderName = _Data.shaderName;
		}

		[Serializable]
		public class Data : Option.Data
		{
			public string materialFolderPath;
			public string shaderName = null;

			public override object MakeAsset ()
			{
				SkyboxOption skyboxOption = ObjectPool.instance.SpawnComponent<SkyboxOption>(EternityEngine.instance.skyboxOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (skyboxOption);
				skyboxOption.Init ();
				return skyboxOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				SkyboxOption skyboxOption = (SkyboxOption) asset;
				skyboxOption._Data = this;
				skyboxOption.SetMaterialFolderPathFromData ();
				skyboxOption.SetShaderNameFromData ();
			}
		}
	}
}