using System;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class ImageFileInstanceOption : FileInstanceOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public TransformOption transformOption;
		public SpritePivotOption spritePivotOption;
		public SpriteRenderer spriteRenderer;
		Vector2 pivot = new Vector2(.5f, .5f);

		public override void Init (FileOption fileOption)
		{
			base.Init (fileOption);
			spriteRenderer = new GameObject().AddComponent<SpriteRenderer>();
			if (Exists(spritePivotOption))
				pivot = spritePivotOption.value;
			if (Exists(fileOption))
			{
				ImageFileOption imageFileOption = (ImageFileOption) fileOption;
				spriteRenderer.sprite = Sprite.Create(imageFileOption.texture, new Rect(0, 0, imageFileOption.texture.width, imageFileOption.texture.height), pivot);
			}
			if (Exists(transformOption))
				transformOption.Init (spriteRenderer.GetComponent<Transform>());
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			// SetPivotOfData ();
			SetTransformOptionNameOfData ();
			SetSpritePivotOptionNameOfData ();
		}

		// void SetPivotOfData ()
		// {
		// 	_Data.pivot = _Vector2.FromVec2(pivot);
		// }

		// void SetPivotFromData ()
		// {
		// 	pivot = _Data.pivot.ToVec2();
		// }

		void SetTransformOptionNameOfData ()
		{
			if (Exists(transformOption))
				_Data.transformOptionName = transformOption.name;
		}

		void SetTransformOptionNameFromData ()
		{
			if (_Data.transformOptionName != null)
				transformOption = EternityEngine.GetOption<TransformOption>(_Data.transformOptionName);
		}

		void SetSpritePivotOptionNameOfData ()
		{
			if (Exists(spritePivotOption))
				_Data.spritePivotOptionName = spritePivotOption.name;
		}

		void SetSpritePivotOptionNameFromData ()
		{
			if (_Data.spritePivotOptionName != null)
				spritePivotOption = EternityEngine.GetOption<SpritePivotOption>(_Data.spritePivotOptionName);
		}

		[Serializable]
		public class Data : FileInstanceOption.Data
		{
			// public _Vector2 pivot;
			public string transformOptionName = null;
			public string spritePivotOptionName = null;

			public override object MakeAsset ()
			{
				ImageFileInstanceOption imageFileInstanceOption = ObjectPool.instance.SpawnComponent<ImageFileInstanceOption>(EternityEngine.instance.imageFileInstanceOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (imageFileInstanceOption);
				imageFileInstanceOption.Init ();
				return imageFileInstanceOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				ImageFileInstanceOption imageFileInstanceOption = (ImageFileInstanceOption) asset;
				imageFileInstanceOption._Data = this;
				// imageFileInstanceOption.SetPivotFromData ();
				imageFileInstanceOption.SetTransformOptionNameFromData ();
				imageFileInstanceOption.SetSpritePivotOptionNameFromData ();
				imageFileInstanceOption.Init (imageFileInstanceOption.fileOption);
			}
		}
	}
}