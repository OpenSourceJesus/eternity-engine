using System;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class SoundFileInstanceOption : FileInstanceOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public AudioClip audioClip;
		public bool loop;
		public Sound soundPrefab;
		public TransformOption transformOption;
		public Transform transformOptionReference;
		List<Sound> sounds = new List<Sound>();
		float pitch = 1;
		float volume = 1;
		float pan;
		float minDistance;
		float maxDistance = 99999;

		public override void Init (FileOption fileOption)
		{
			base.Init (fileOption);
			if (Exists(fileOption))
			{
				SoundFileOption soundFileOption = (SoundFileOption) fileOption;
				audioClip = soundFileOption.audioClip;
			}
			if (Exists(transformOption))
			{
				transformOption.Init (transformOptionReference);
				transformOptionReference.SetParent(null);
			}
		}

		public override void UpdateTexts (string filePath)
		{
			SetName ("\"" + filePath + "\" Emitter");
		}

		public void StartEmit ()
		{
			if (!Exists(transformOption))
				return;
			Sound sound = ObjectPool.instance.SpawnComponent<Sound>(soundPrefab.prefabIndex, transformOptionReference.position, transformOptionReference.rotation, transformOptionReference);
			sound.audioSource.clip = audioClip;
			sound.audioSource.loop = loop;
			sound.audioSource.pitch = pitch;
			sound.audioSource.volume = volume;
			sound.audioSource.panStereo = pan;
			sound.audioSource.Play();
			if (!loop)
				sound.delayedDespawn = ObjectPool.instance.DelayDespawn(sound.prefabIndex, sound.gameObject, sound.trs, audioClip.length);
			sounds.Add(sound);
		}

		public void EndEmit ()
		{
			for (int i = 0; i < sounds.Count; i ++)
			{
				Sound sound = sounds[i];
				sounds.RemoveAt(i);
				ObjectPool.DelayedDespawn delayedDespawn = sound.delayedDespawn;
				if (delayedDespawn != null)
					ObjectPool.instance.CancelDelayedDespawn (delayedDespawn);
				sound.audioSource.loop = false;
				ObjectPool.instance.Despawn (sound.prefabIndex, sound.gameObject, sound.trs);
				i --;
			}
		}

		public void SetPitch (float pitch)
		{
			this.pitch = pitch;
			for (int i = 0; i < sounds.Count; i ++)
			{
				Sound sound = sounds[i];
				sound.audioSource.pitch = pitch;
			}
		}

		public void SetVolume (float volume)
		{
			this.volume = volume;
			for (int i = 0; i < sounds.Count; i ++)
			{
				Sound sound = sounds[i];
				sound.audioSource.volume = volume;
			}
		}

		public void SetPan (float pan)
		{
			this.pan = pan;
			for (int i = 0; i < sounds.Count; i ++)
			{
				Sound sound = sounds[i];
				sound.audioSource.panStereo = pan;
			}
		}

		public void SetMinDistance (float minDistance)
		{
			this.minDistance = minDistance;
			for (int i = 0; i < sounds.Count; i ++)
			{
				Sound sound = sounds[i];
				sound.audioSource.minDistance = minDistance;
			}
		}

		public void SetMaxDistance (float maxDistance)
		{
			this.maxDistance = maxDistance;
			for (int i = 0; i < sounds.Count; i ++)
			{
				Sound sound = sounds[i];
				sound.audioSource.maxDistance = maxDistance;
			}
		}

		public override void OnDeleted ()
		{
			base.OnDeleted ();
			for (int i = 0; i < sounds.Count; i ++)
			{
				Sound sound = sounds[i];
				ObjectPool.DelayedDespawn delayedDespawn = sound.delayedDespawn;
				if (delayedDespawn != null)
					ObjectPool.instance.CancelDelayedDespawn (delayedDespawn);
				ObjectPool.instance.Despawn (sound.prefabIndex, sound.gameObject, sound.trs);
			}
			sounds.Clear();
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetTransformOptionNameOfData ();
			SetLoopOfData ();
			SetVolumeOfData ();
			SetPanOfData ();
			SetPitchOfData ();
			SetMinDistanceOfData ();
			SetMaxDistanceOfData ();
		}

		void SetTransformOptionNameOfData ()
		{
			if (Exists(transformOption))
				_Data.transformOptionName = transformOption.name;
		}

		void SetTransformOptionNameFromData ()
		{
			if (_Data.transformOptionName != null)
				transformOption = EternityEngine.GetOption<TransformOption>(_Data.transformOptionName);
		}

		void SetLoopOfData ()
		{
			_Data.loop = loop;
		}

		void SetLoopFromData ()
		{
			loop = _Data.loop;
		}

		void SetVolumeOfData ()
		{
			_Data.volume = volume;
		}

		void SetVolumeFromData ()
		{
			volume = _Data.volume;
		}

		void SetPanOfData ()
		{
			_Data.pan = pan;
		}

		void SetPanFromData ()
		{
			pan = _Data.pan;
		}

		void SetPitchOfData ()
		{
			_Data.pitch = pitch;
		}

		void SetPitchFromData ()
		{
			pitch = _Data.pitch;
		}

		void SetMinDistanceOfData ()
		{
			_Data.minDistance = minDistance;
		}

		void SetMinDistanceFromData ()
		{
			minDistance = _Data.minDistance;
		}

		void SetMaxDistanceOfData ()
		{
			_Data.maxDistance = maxDistance;
		}

		void SetMaxDistanceFromData ()
		{
			maxDistance = _Data.maxDistance;
		}

		[Serializable]
		public class Data : FileInstanceOption.Data
		{
			public string transformOptionName = null;
			public bool loop;
			public float volume;
			public float pan;
			public float pitch;
			public float minDistance;
			public float maxDistance;

			public override object MakeAsset ()
			{
				SoundFileInstanceOption soundFileInstanceOption = ObjectPool.instance.SpawnComponent<SoundFileInstanceOption>(EternityEngine.instance.soundFileInstanceOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (soundFileInstanceOption);
				soundFileInstanceOption.Init ();
				return soundFileInstanceOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				SoundFileInstanceOption soundFileInstanceOption = (SoundFileInstanceOption) asset;
				soundFileInstanceOption._Data = this;
				soundFileInstanceOption.SetTransformOptionNameFromData ();
				soundFileInstanceOption.Init (soundFileInstanceOption.fileOption);
				soundFileInstanceOption.SetLoopFromData ();
				soundFileInstanceOption.SetVolumeFromData ();
				soundFileInstanceOption.SetPanFromData ();
				soundFileInstanceOption.SetPitchFromData ();
				soundFileInstanceOption.SetMinDistanceFromData ();
				soundFileInstanceOption.SetMaxDistanceFromData ();
			}
		}
	}
}