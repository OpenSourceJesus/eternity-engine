using System;

namespace _EternityEngine
{
	public class RecordContinuedCollisionsOption : BoolOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public ModelFileInstanceOption modelFileInstanceOption;

		public override bool SetValue (bool value)
		{
			if (base.SetValue(value))
			{
				if (Exists(modelFileInstanceOption))
					modelFileInstanceOption.RecordContinuedCollisions (value);
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetModelFileInstanceOptionNameOfData ();
		}

		void SetModelFileInstanceOptionNameOfData ()
		{
			if (Exists(modelFileInstanceOption))
				_Data.modelFileInstanceOptionName = modelFileInstanceOption.name;
		}

		void SetModelFileInstanceOptionNameFromData ()
		{
			if (_Data.modelFileInstanceOptionName != null)
				modelFileInstanceOption = EternityEngine.GetOption<ModelFileInstanceOption>(_Data.modelFileInstanceOptionName);
		}

		[Serializable]
		public class Data : BoolOption.Data
		{
			public string modelFileInstanceOptionName = null;

			public override object MakeAsset ()
			{
				RecordContinuedCollisionsOption recordContinuedCollisionsOption = ObjectPool.instance.SpawnComponent<RecordContinuedCollisionsOption>(EternityEngine.instance.recordContinuedCollisionsOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (recordContinuedCollisionsOption);
				recordContinuedCollisionsOption.Init ();
				return recordContinuedCollisionsOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				RecordContinuedCollisionsOption recordContinuedCollisionsOption = (RecordContinuedCollisionsOption) asset;
				recordContinuedCollisionsOption._Data = this;
				recordContinuedCollisionsOption.SetModelFileInstanceOptionNameFromData ();
			}
		}
	}
}