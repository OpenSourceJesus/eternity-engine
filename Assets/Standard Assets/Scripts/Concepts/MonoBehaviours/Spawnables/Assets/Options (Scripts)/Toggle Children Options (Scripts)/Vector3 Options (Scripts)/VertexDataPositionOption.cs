
using System;
using UnityEngine;

namespace _EternityEngine
{
	public class VertexDataPositionOption : Vector3Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
public VertexDataOption vertexDataOption;
		MeshFilter referenceMeshFilter;
		uint vertexIndex;
		Transform referenceTrs;

		public void Init (VertexDataOption vertexDataOption)
		{
			this.vertexDataOption = vertexDataOption;
		}

		public override void Init ()
		{
			base.Init ();
			PhysicsObject3D physicsObject = vertexDataOption.modelFileInstanceOption.physicsObject;
			referenceMeshFilter = physicsObject.meshFilter;
			referenceTrs = physicsObject.trs;
			vertexIndex = vertexDataOption.vertexIndex;
		}

		public override void UpdateValue ()
		{
			value = referenceTrs.TransformPoint(referenceMeshFilter.mesh.vertices[vertexIndex]);
		}

		public override bool SetX (float x)
		{
			if (base.SetX(x))
			{
				Mesh mesh = referenceMeshFilter.mesh;
				Vector3[] vertices = mesh.vertices;
				vertices[vertexIndex].x = referenceTrs.InverseTransformPoint(value).x;
				mesh.SetVertices(vertices);
				return true;
			}
			else
				return false;
		}

		public override bool SetY (float y)
		{
			if (base.SetY(y))
			{
				Mesh mesh = referenceMeshFilter.mesh;
				Vector3[] vertices = mesh.vertices;
				vertices[vertexIndex].y = referenceTrs.InverseTransformPoint(value).y;
				mesh.SetVertices(vertices);
				return true;
			}
			else
				return false;
		}

		public override bool SetZ (float z)
		{
			if (base.SetZ(z))
			{
				Mesh mesh = referenceMeshFilter.mesh;
				Vector3[] vertices = mesh.vertices;
				vertices[vertexIndex].z = referenceTrs.InverseTransformPoint(value).z;
				mesh.SetVertices(vertices);
				return true;
			}
			else
				return false;
		}

		public override bool SetValue (Vector3 value)
		{
			if (base.SetValue(value))
			{
				Vector3[] vertices = meshFilter.mesh.vertices;
				vertices[vertexIndex] = value;
				meshFilter.mesh.SetVertices(vertices);
				return true;
			}
			else
				return false;
		}

		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
SetVertexDataOptionNameOfData ();
		}

		
		public void SetVertexDataOptionNameOfData ()
		{
			if (Exists(vertexDataOption))
				_Data.vertexDataOptionName = vertexDataOption.name;
		}

		public void SetVertexDataOptionNameFromData ()
		{
			if (_Data.vertexDataOptionName != null)
				vertexDataOption = EternityEngine.GetOption<VertexDataOption>(_Data.vertexDataOptionName);
		}

		[Serializable]
		public class Data : Vector3Option.Data
		{
public string vertexDataOptionName = null;

			public override object MakeAsset ()
			{
				VertexDataPositionOption vertexDataPositionOption = ObjectPool.instance.SpawnComponent<VertexDataPositionOption>(EternityEngine.instance.vertexDataPositionOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (vertexDataPositionOption);
				vertexDataPositionOption.Init ();
				return vertexDataPositionOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				VertexDataPositionOption vertexDataPositionOption = (VertexDataPositionOption) asset;
				vertexDataPositionOption._Data = this;
 vertexDataPositionOption.SetVertexDataOptionNameFromData ();
			}
		}
	}
}