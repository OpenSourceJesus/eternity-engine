using System;
using UnityEngine;

namespace _EternityEngine
{
	public class MaxDepenetrationSpeedOption : TypingTargetOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}

		public override bool SetValue (string value)
		{
			if (base.SetValue(value))
			{
				float? speed = GetNumberValue();
				if (speed != null)
					Physics.defaultMaxDepenetrationVelocity = (float) speed;
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
		}

		[Serializable]
		public class Data : TypingTargetOption.Data
		{
			public override object MakeAsset ()
			{
				MaxDepenetrationSpeedOption maxDepenetrationSpeedOption = ObjectPool.instance.SpawnComponent<MaxDepenetrationSpeedOption>(EternityEngine.instance.maxDepenetrationSpeedOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (maxDepenetrationSpeedOption);
				maxDepenetrationSpeedOption.Init ();
				return maxDepenetrationSpeedOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				MaxDepenetrationSpeedOption maxDepenetrationSpeedOption = (MaxDepenetrationSpeedOption) asset;
				maxDepenetrationSpeedOption._Data = this;
			}
		}
	}
}