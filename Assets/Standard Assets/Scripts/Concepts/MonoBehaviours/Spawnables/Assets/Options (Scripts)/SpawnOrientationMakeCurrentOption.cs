using System;
using UnityEngine;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class SpawnOrientationMakeCurrentOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public SpawnOrientationOption spawnOrientationOption;
		static Dictionary<SpawnOrientationOption, List<SpawnOrientationMakeCurrentOption>> makeCurrentOptionsDict = new Dictionary<SpawnOrientationOption, List<SpawnOrientationMakeCurrentOption>>();

		public override void Init ()
		{
			base.Init ();
			bool spawnOrientationOptionExists = Exists(spawnOrientationOption);
			SetActivatable (spawnOrientationOptionExists && spawnOrientationOption.spawnedSpawnOrientationOptionsDict.Count > 1);
			if (spawnOrientationOptionExists)
			{
				List<SpawnOrientationMakeCurrentOption> makeCurrentOptions = new List<SpawnOrientationMakeCurrentOption>();
				if (makeCurrentOptionsDict.TryGetValue(spawnOrientationOption, out makeCurrentOptions))
				{
					makeCurrentOptions.Add(this);
					makeCurrentOptionsDict[spawnOrientationOption] = makeCurrentOptions;
				}
				else
					makeCurrentOptionsDict.Add(spawnOrientationOption, new List<SpawnOrientationMakeCurrentOption>(new SpawnOrientationMakeCurrentOption[] { this }));
			}
		}

		public override void OnActivate (EternityEngine.Hand hand)
		{
			if (Exists(spawnOrientationOption))
			{
				List<SpawnOrientationMakeCurrentOption> makeCurrentOptions = makeCurrentOptionsDict[spawnOrientationOption];
				for (int i = 0; i < makeCurrentOptions.Count; i ++)
				{
					SpawnOrientationMakeCurrentOption makeCurrentOption = makeCurrentOptions[i];
					makeCurrentOption.SetActivatable (false);
				}
				makeCurrentOptions = makeCurrentOptionsDict[EternityEngine.instance.currentSpawnOrientationOption];
				for (int i = 0; i < makeCurrentOptions.Count; i ++)
				{
					SpawnOrientationMakeCurrentOption makeCurrentOption = makeCurrentOptions[i];
					makeCurrentOption.SetActivatable (true);
				}
				spawnOrientationOption.MakeCurrent ();
			}
			// base.OnActivate (hand);
		}

		public override void OnDeleted ()
		{
			base.OnDeleted ();
			if (Exists(spawnOrientationOption))
			{
				List<SpawnOrientationMakeCurrentOption> makeCurrentOptions = new List<SpawnOrientationMakeCurrentOption>();
				if (makeCurrentOptionsDict.TryGetValue(spawnOrientationOption, out makeCurrentOptions))
				{
					makeCurrentOptions.Add(this);
					makeCurrentOptionsDict.Add(spawnOrientationOption, makeCurrentOptions);
				}
				else
					makeCurrentOptionsDict.Add(spawnOrientationOption, new List<SpawnOrientationMakeCurrentOption>(new SpawnOrientationMakeCurrentOption[] { this }));
			}
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetSpawnOrientationOptionNameOfData ();
		}

		void SetSpawnOrientationOptionNameOfData ()
		{
			if (Exists(spawnOrientationOption))
				_Data.spawnOrientationOptionName = spawnOrientationOption.name;
		}

		void SetSpawnOrientationOptionNameFromData ()
		{
			if (_Data.spawnOrientationOptionName != null)
				spawnOrientationOption = EternityEngine.GetOption<SpawnOrientationOption>(_Data.spawnOrientationOptionName);
		}

		[Serializable]
		public class Data : Option.Data
		{
			public string spawnOrientationOptionName = null;

			public override object MakeAsset ()
			{
				SpawnOrientationMakeCurrentOption spawnOrientationMakeCurrentOption = ObjectPool.instance.SpawnComponent<SpawnOrientationMakeCurrentOption>(EternityEngine.instance.spawnOrientationMakeCurrentOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (spawnOrientationMakeCurrentOption);
				spawnOrientationMakeCurrentOption.Init ();
				return spawnOrientationMakeCurrentOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				SpawnOrientationMakeCurrentOption spawnOrientationMakeCurrentOption = (SpawnOrientationMakeCurrentOption) asset;
				spawnOrientationMakeCurrentOption._Data = this;
				spawnOrientationMakeCurrentOption.SetSpawnOrientationOptionNameFromData ();
			}
		}
	}
}