using System;

namespace _EternityEngine
{
	public class LightOuterSpotAngleOption : TypingTargetOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public LightOption lightOption;

		public override bool SetValue (string value)
		{
			if (base.SetValue(value))
			{
				if (Exists(lightOption))
				{
					float? angle = GetNumberValue();
					if (angle != null)
						lightOption.SetOuterSpotAngle ((float) angle);
				}
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetLightOptionNameOfData ();
		}

		void SetLightOptionNameOfData ()
		{
			if (Exists(lightOption))
				_Data.lightOptionName = lightOption.name;
		}

		void SetLightOptionNameFromData ()
		{
			if (_Data.lightOptionName != null)
				lightOption = EternityEngine.GetOption<LightOption>(_Data.lightOptionName);
		}

		[Serializable]
		public class Data : TypingTargetOption.Data
		{
			public string lightOptionName = null;

			public override object MakeAsset ()
			{
				LightOuterSpotAngleOption lightOuterSpotAngleOption = ObjectPool.instance.SpawnComponent<LightOuterSpotAngleOption>(EternityEngine.instance.lightOuterSpotAngleOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (lightOuterSpotAngleOption);
				lightOuterSpotAngleOption.Init ();
				return lightOuterSpotAngleOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				LightOuterSpotAngleOption lightOuterSpotAngleOption = (LightOuterSpotAngleOption) asset;
				lightOuterSpotAngleOption._Data = this;
				lightOuterSpotAngleOption.SetLightOptionNameFromData ();
			}
		}
	}
}
