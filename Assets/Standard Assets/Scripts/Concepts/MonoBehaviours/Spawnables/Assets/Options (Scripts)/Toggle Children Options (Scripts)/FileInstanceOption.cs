using System;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class FileInstanceOption : ToggleChildrenOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public FileOption fileOption;

		public virtual void Init (FileOption fileOption)
		{
			this.fileOption = fileOption;
			if (Exists(fileOption))
				fileOption.fileInstanceOptions.Add(this);
			UpdateTexts (fileOption.path);
		}

		public virtual void UpdateTexts (string filePath)
		{
			SetName ("\"" + filePath + "\" Instance");
		}

		public override void OnDeleted ()
		{
			base.OnDeleted ();
			if (Exists(fileOption))
			{
				fileOption.fileInstanceOptions.Remove(this);
				if (fileOption.fileInstanceOptions.Count == 0 && Exists(fileOption.deleteFileInstancesOption))
					fileOption.deleteFileInstancesOption.SetActivatable (false);
			}
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetFileOptionNameOfData ();
		}

		void SetFileOptionNameOfData ()
		{
			if (Exists(fileOption))
				_Data.fileOptionName = fileOption.name;
		}

		void SetFileOptionNameFromData ()
		{
			if (_Data.fileOptionName != null)
				fileOption = EternityEngine.GetOption<FileOption>(_Data.fileOptionName);
		}

		[Serializable]
		public class Data : ToggleChildrenOption.Data
		{
			public string fileOptionName = null;
			
			public override object MakeAsset ()
			{
				FileInstanceOption fileInstanceOption = ObjectPool.instance.SpawnComponent<FileInstanceOption>(EternityEngine.instance.fileInstanceOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (fileInstanceOption);
				fileInstanceOption.Init ();
				return fileInstanceOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				FileInstanceOption fileInstanceOption = (FileInstanceOption) asset;
				fileInstanceOption._Data = this;
				fileInstanceOption.SetFileOptionNameFromData ();
			}
		}
	}
}