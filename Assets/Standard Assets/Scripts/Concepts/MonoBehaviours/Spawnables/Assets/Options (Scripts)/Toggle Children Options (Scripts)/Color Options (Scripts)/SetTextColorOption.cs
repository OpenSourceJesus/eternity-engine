using System;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class SetTextColorOption : ColorOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public Option childrenToSetParentOption;

		public override bool SetR (float r)
		{
			bool output = base.SetR(r);
			if (output && Exists(childrenToSetParentOption))
				Apply ();
			return output;
		}

		public override bool SetG (float g)
		{
			bool output = base.SetG(g);
			if (output && Exists(childrenToSetParentOption))
				Apply ();
			return output;
		}

		public override bool SetB (float b)
		{
			bool output = base.SetB(b);
			if (output && Exists(childrenToSetParentOption))
				Apply ();
			return output;
		}

		public override bool SetA (float a)
		{
			bool output = base.SetA(a);
			if (output && Exists(childrenToSetParentOption))
				Apply ();
			return output;
		}

		void Apply ()
		{
			for (int i = 0; i < childrenToSetParentOption.children.Count; i ++)
			{
				Option child = childrenToSetParentOption.children[i];
				child.text.color = value;
			}
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetChildrenToSetParentOptionNameOfData ();
		}

		void SetChildrenToSetParentOptionNameOfData ()
		{
			if (Exists(childrenToSetParentOption))
				_Data.childrenToSetParentOptionName = childrenToSetParentOption.name;
		}

		void SetChildrenToSetParentOptionNameFromData ()
		{
			if (_Data.childrenToSetParentOptionName != null)
				childrenToSetParentOption = EternityEngine.GetOption(_Data.childrenToSetParentOptionName);
		}

		[Serializable]
		public class Data : ColorOption.Data
		{
			public string childrenToSetParentOptionName = null;

			public override object MakeAsset ()
			{
				SetTextColorOption setTextColorOption = ObjectPool.instance.SpawnComponent<SetTextColorOption>(EternityEngine.instance.setTextColorOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (setTextColorOption);
				setTextColorOption.Init ();
				return setTextColorOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				SetTextColorOption setTextColorOption = (SetTextColorOption) asset;
				setTextColorOption._Data = this;
				setTextColorOption.SetChildrenToSetParentOptionNameFromData ();
			}
		}
	}
}