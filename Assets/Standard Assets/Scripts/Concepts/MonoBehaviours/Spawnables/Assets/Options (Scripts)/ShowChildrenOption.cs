using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class ShowChildrenOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}

		public override void OnActivate (EternityEngine.Hand hand)
		{
			ShowChildren ();
			// base.OnActivate (hand);
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
		}

		[Serializable]
		public class Data : Option.Data
		{
			public override object MakeAsset ()
			{
				ShowChildrenOption showChildrenOption = ObjectPool.instance.SpawnComponent<ShowChildrenOption>(EternityEngine.instance.showChildrenOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (showChildrenOption);
				showChildrenOption.Init ();
				return showChildrenOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				ShowChildrenOption showChildrenOption = (ShowChildrenOption) asset;
				showChildrenOption._Data = this;
			}
		}
	}
}