using System;
using Extensions;

namespace _EternityEngine
{
	public class RenameSystemComponentOption : TypingTargetOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public SystemComponentOption systemComponentOption;

		// public override void Init ()
		// {
		// 	base.Init ();
		// 	if (Exists(systemComponentOption))
		// 		systemComponentOption.defaultChildren = systemComponentOption.defaultChildren.Add(this);
		// }

		public override bool SetValue (string value)
		{
			bool output = base.SetValue(value);
			if (output && Exists(systemComponentOption))
				systemComponentOption.Rename (value);
			return output;
		}

		// public override void OnDeleted ()
		// {
		// 	base.OnDeleted ();
		// 	systemComponentOption.defaultChildren = systemComponentOption.defaultChildren.Remove(this);
		// }
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetSystemComponentOptionNameOfData ();
		}

		void SetSystemComponentOptionNameOfData ()
		{
			if (Exists(systemComponentOption))
				_Data.systemComponentOptionName = systemComponentOption.name;
		}

		void SetSystemComponentOptionNameFromData ()
		{
			if (_Data.systemComponentOptionName != null)
				systemComponentOption = EternityEngine.GetOption<SystemComponentOption>(_Data.systemComponentOptionName);
		}

		[Serializable]
		public class Data : TypingTargetOption.Data
		{
			public string systemComponentOptionName = null;

			public override object MakeAsset ()
			{
				RenameSystemComponentOption renameSystemComponentOption = ObjectPool.instance.SpawnComponent<RenameSystemComponentOption>(EternityEngine.instance.renameSystemComponentOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (renameSystemComponentOption);
				renameSystemComponentOption.Init ();
				return renameSystemComponentOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				RenameSystemComponentOption renameSystemComponentOption = (RenameSystemComponentOption) asset;
				renameSystemComponentOption._Data = this;
				renameSystemComponentOption.SetSystemComponentOptionNameFromData ();
			}
		}
	}
}