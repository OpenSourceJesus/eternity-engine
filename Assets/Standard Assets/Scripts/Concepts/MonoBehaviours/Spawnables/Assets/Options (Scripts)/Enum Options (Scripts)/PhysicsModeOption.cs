
using System;

namespace _EternityEngine
{
	public class PhysicsModeOption : EnumOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public ModelFileInstanceOption modelFileInstanceOption;

		public override bool SetValue (string value)
		{
			if (base.SetValue(value))
			{
				if (Exists(modelFileInstanceOption))
					modelFileInstanceOption.SetPhysicsMode ((ModelFileInstanceOption.PhysicsMode) Enum.ToObject(enumType, value));
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetModelFileInstanceOptionNameOfData ();
		}

		void SetModelFileInstanceOptionNameOfData ()
		{
			if (Exists(modelFileInstanceOption))
				_Data.modelFileInstanceOptionName = modelFileInstanceOption.name;
		}

		void SetModelFileInstanceOptionNameFromData ()
		{
			if (_Data.modelFileInstanceOptionName != null)
				modelFileInstanceOption = EternityEngine.GetOption<ModelFileInstanceOption>(_Data.modelFileInstanceOptionName);
		}

		[Serializable]
		public class Data : EnumOption.Data
		{
			public string modelFileInstanceOptionName = null;

			public override object MakeAsset ()
			{
				PhysicsModeOption physicsModeOption = ObjectPool.instance.SpawnComponent<PhysicsModeOption>(EternityEngine.instance.physicsModeOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (physicsModeOption);
				physicsModeOption.Init ();
				return physicsModeOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				PhysicsModeOption physicsModeOption = (PhysicsModeOption) asset;
				physicsModeOption._Data = this;
				physicsModeOption.SetModelFileInstanceOptionNameFromData ();
			}
		}
	}
}
