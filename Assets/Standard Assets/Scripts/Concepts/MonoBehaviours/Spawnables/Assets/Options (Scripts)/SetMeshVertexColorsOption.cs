using System;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class SetMeshVertexColorsOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public Option modelFileInstanceOptionsParent;
		public List<ModelFileInstanceOption> modelFileInstanceOptions = new List<ModelFileInstanceOption>();
		public TransformOption transformOption;
		public ColorOption colorOption;

		public override void Init ()
		{
			base.Init ();
			transformOption.Init (EternityEngine.instance.actOnMeshVerticesFromTrs);
			modelFileInstanceOptionsParent.onAddChild += OnAddChild;
			modelFileInstanceOptionsParent.onAddChild += OnRemoveChild;
		}

		public override void OnDeleted ()
		{
			modelFileInstanceOptionsParent.onAddChild -= OnAddChild;
			modelFileInstanceOptionsParent.onRemoveChild -= OnRemoveChild;
			base.OnDeleted ();
		}

		void OnAddChild (Option child)
		{
			ModelFileInstanceOption modelFileInstanceOption = child as ModelFileInstanceOption;
			if (modelFileInstanceOption != null)
				modelFileInstanceOptions.Add(modelFileInstanceOption);
		}
		
		void OnRemoveChild (Option child)
		{
			ModelFileInstanceOption modelFileInstanceOption = child as ModelFileInstanceOption;
			if (modelFileInstanceOption != null)
				modelFileInstanceOptions.Remove(modelFileInstanceOption);
		}

		public override void OnActivate (EternityEngine.Hand hand)
		{
			for (int i = 0; i < modelFileInstanceOptions.Count; i ++)
			{
				ModelFileInstanceOption modelFileInstanceOption = modelFileInstanceOptions[i];
				PhysicsObject3D physicsObject = modelFileInstanceOption.physicsObject;
				Mesh mesh = physicsObject.meshFilter.mesh;
				List<Vector3> vertices = new List<Vector3>();
				mesh.GetVertices(vertices);
				List<Color> colors = new List<Color>();
				mesh.GetColors(colors);
				Transform physicsObjectTrs = physicsObject.trs;
				for (int i2 = 0; i2 < mesh.vertexCount; i2 ++)
				{
					Vector3 vertex = vertices[i2];
					Transform trs = transformOption.referenceTrs;
					float trsSize = trs.lossyScale.x;
					vertex = physicsObjectTrs.TransformPoint(vertex);
					if ((vertex - trs.position).sqrMagnitude < trsSize * trsSize)
						colors[i2] = colorOption.value;
				}
				mesh.SetColors(colors);
			}
			// base.OnActivate (hand);
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetModelFileInstanceOptionsParentNameOfData ();
			SetModelFileInstanceOptionsNamesOfData ();
			SetTransformOptionNameOfData ();
			SetColorOptionNameOfData ();
		}
		
		void SetModelFileInstanceOptionsParentNameOfData ()
		{
			if (Exists(modelFileInstanceOptionsParent))
				_Data.modelFileInstanceOptionsParentName = modelFileInstanceOptionsParent.name;
		}

		void SetModelFileInstanceOptionsParentNameFromData ()
		{
			if (_Data.modelFileInstanceOptionsParentName != null)
				modelFileInstanceOptionsParent = EternityEngine.GetOption(_Data.modelFileInstanceOptionsParentName);
		}

		void SetModelFileInstanceOptionsNamesOfData ()
		{
			_Data.modelFileInstanceOptionsNames = new string[modelFileInstanceOptions.Count];
			for (int i = 0; i < modelFileInstanceOptions.Count; i ++)
			{
				ModelFileInstanceOption modelFileInstanceOption = modelFileInstanceOptions[i];
				if (Exists(modelFileInstanceOption))
					_Data.modelFileInstanceOptionsNames[i] = modelFileInstanceOption.name;
			}
		}

		void SetModelFileInstanceOptionsNamesFromData ()
		{
			for (int i = 0; i < _Data.modelFileInstanceOptionsNames.Length; i ++)
			{
				string parentName = _Data.modelFileInstanceOptionsNames[i];
				modelFileInstanceOptions.Add(EternityEngine.GetOption<ModelFileInstanceOption>(parentName));
			}
		}
		
		void SetTransformOptionNameOfData ()
		{
			if (Exists(transformOption))
				_Data.transformOptionName = transformOption.name;
		}

		void SetTransformOptionNameFromData ()
		{
			if (_Data.transformOptionName != null)
				transformOption = EternityEngine.GetOption<TransformOption>(_Data.transformOptionName);
		}
		
		void SetColorOptionNameOfData ()
		{
			if (Exists(colorOption))
				_Data.colorOptionName = colorOption.name;
		}

		void SetColorOptionNameFromData ()
		{
			if (_Data.colorOptionName != null)
				colorOption = EternityEngine.GetOption<ColorOption>(_Data.colorOptionName);
		}

		[Serializable]
		public class Data : Option.Data
		{
			public string[] modelFileInstanceOptionsNames = new string[0];
			public string modelFileInstanceOptionsParentName = null;
			public string transformOptionName = null;
			public string colorOptionName = null;

			public override object MakeAsset ()
			{
				SetMeshVertexColorsOption setMeshVertexColorsOption = ObjectPool.instance.SpawnComponent<SetMeshVertexColorsOption>(EternityEngine.instance.setMeshVertexColorsOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (setMeshVertexColorsOption);
				setMeshVertexColorsOption.Init ();
				return setMeshVertexColorsOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				SetMeshVertexColorsOption setMeshVertexColorsOption = (SetMeshVertexColorsOption) asset;
				setMeshVertexColorsOption._Data = this;
				setMeshVertexColorsOption.SetModelFileInstanceOptionsParentNameFromData ();
				setMeshVertexColorsOption.SetModelFileInstanceOptionsNamesFromData ();
				setMeshVertexColorsOption.SetTransformOptionNameFromData ();
				setMeshVertexColorsOption.SetColorOptionNameFromData ();
			}
		}
	}
}