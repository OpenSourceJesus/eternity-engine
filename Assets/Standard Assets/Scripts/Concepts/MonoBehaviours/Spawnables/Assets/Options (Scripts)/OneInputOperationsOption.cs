using System;
using Extensions;
using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class OneInputOperationsOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public OperationType operationType;
		public TooManyChildrenBehaviour tooManyChildrenBehaviour;
		public Option invalidAnswerOption;
		public Option value1OptionParent;
		Option value1Option;
		float? value1;

		public override void Init ()
		{
			base.Init ();
			value1OptionParent.onAddChild += (Option child) => { OnAddChild (child); };
			value1OptionParent.onRemoveChild += (Option child) => { OnRemoveChild (child); };
			value1OptionParent.onAboutToAddChild += OnAboutToAddChild_value1;
		}

		public override void OnActivate (EternityEngine.Hand hand)
		{
			float value1 = (float) this.value1;
			if (operationType == OperationType.Abs)
				value1Option.SetValue ("" + Mathf.Abs(value1));
			else if (operationType == OperationType.Sin)
				value1Option.SetValue ("" + Mathf.Sin(value1));
			else if (operationType == OperationType.Cos)
				value1Option.SetValue ("" + Mathf.Cos(value1));
			else if (operationType == OperationType.Cos)
				value1Option.SetValue ("" + Mathf.Tan(value1));
			else if (operationType == OperationType.Asin)
				value1Option.SetValue ("" + Mathf.Asin(value1));
			else if (operationType == OperationType.Acos)
				value1Option.SetValue ("" + Mathf.Acos(value1));
			else if (operationType == OperationType.Atan)
				value1Option.SetValue ("" + Mathf.Atan(value1));
			else if (operationType == OperationType.Round)
				value1Option.SetValue ("" + Mathf.Round(value1));
			else if (operationType == OperationType.Floor)
				value1Option.SetValue ("" + Mathf.Floor(value1));
			else// if (operationType == OperationType.Ceil)
				value1Option.SetValue ("" + Mathf.Ceil(value1));
			this.value1 = TypingTargetOption.GetNumberValue(value1Option.GetValue());
			// base.OnActivate (hand);
		}

		void OnAddChild (Option child)
		{
			if (value1OptionParent.children.Count > 1)
			{
				if (tooManyChildrenBehaviour == TooManyChildrenBehaviour.UnparentOldChildren)
				{
					while (value1OptionParent.children.Count > 1)
						value1OptionParent.RemoveChild (value1OptionParent.children[0]);
				}
			}
			SetValueOptionVariables (child);
			SetActivatable (ShouldBeActivatable());
		}

		void OnRemoveChild (Option child)
		{
			if (value1OptionParent.children.Count == 0)
			{
				if (value1OptionParent == value1OptionParent)
					SetValueOptionVariables (null);
				SetActivatable (false);
			}
			else
			{
				ApplyTooManyChildrenBehaviour ();
				SetActivatable (ShouldBeActivatable());
			}
		}

		bool OnAboutToAddChild_value1 (Option child)
		{
			if (tooManyChildrenBehaviour == TooManyChildrenBehaviour.Disallow)
				return value1OptionParent.children.Count == 0;
			else
				return true;
		}

		public void ApplyTooManyChildrenBehaviour ()
		{
			if (value1OptionParent.children.Count < 2)
				return;
			if (tooManyChildrenBehaviour == TooManyChildrenBehaviour.UseMostRecentChild)
				SetValueOptionVariables (value1OptionParent.children[value1OptionParent.children.Count - 1]);
			else if (tooManyChildrenBehaviour == TooManyChildrenBehaviour.UseMostRecentChild)
				SetValueOptionVariables (value1OptionParent.children[0]);
			else if (tooManyChildrenBehaviour == TooManyChildrenBehaviour.UnparentOldChildren)
			{
				while (value1OptionParent.children.Count > 1)
					value1OptionParent.RemoveChild (value1OptionParent.children[0]);
				SetValueOptionVariables (value1OptionParent.children[0]);
			}
		}

		public void SetOperationType (OperationType operationType)
		{
			this.operationType = operationType;
			SetActivatable (ShouldBeActivatable());
		}

		void SetValueOptionVariables (Option newValueOption)
		{
			value1Option = newValueOption;
			value1 = TypingTargetOption.GetNumberValue(value1Option.GetValue());
		}

		bool ShouldBeActivatable ()
		{
			return value1Option != null && value1 != null;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetOperationTypeOfData ();
			SetTooManyChildrenBehaviourOfData ();
			SetValue1OptionParentNameOfData ();
			SetValue1OptionNameOfData ();
		}

		void SetOperationTypeOfData ()
		{
			_Data.operationType = operationType;
		}

		void SetOperationTypeFromData ()
		{
			operationType = _Data.operationType;
		}

		void SetTooManyChildrenBehaviourOfData ()
		{
			_Data.tooManyChildrenBehaviour = tooManyChildrenBehaviour;
		}

		void SetTooManyChildrenBehaviourFromData ()
		{
			tooManyChildrenBehaviour = _Data.tooManyChildrenBehaviour;
		}

		void SetValue1OptionParentNameOfData ()
		{
			if (Exists(value1OptionParent))
				_Data.value1OptionParentName = value1OptionParent.name;
		}

		void SetValue1OptionParentNameFromData ()
		{
			if (_Data.value1OptionParentName != null)
				value1OptionParent = EternityEngine.GetOption(_Data.value1OptionParentName);
		}

		void SetValue1OptionNameOfData ()
		{
			if (Exists(value1Option))
				_Data.value1OptionName = value1Option.name;
		}

		void SetValue1OptionNameFromData ()
		{
			if (_Data.value1OptionName != null)
				value1Option = EternityEngine.GetOption(_Data.value1OptionName);
		}

		public enum TooManyChildrenBehaviour
		{
			Disallow,
			UseMostRecentChild,
			UseLeastRecentChild,
			UnparentOldChildren
		}

		public enum OperationType
		{
			Abs,
			Sin,
			Cos,
			Tan,
			Asin,
			Acos,
			Atan,
			Round,
			Floor,
			Ceil
		}

		[Serializable]
		public class Data : Option.Data
		{
			public OperationType operationType;
			public TooManyChildrenBehaviour tooManyChildrenBehaviour;
			public string value1OptionParentName = null;
			public string value1OptionName = null;

			public override object MakeAsset ()
			{
				OneInputOperationsOption oneInputOperationsOption = ObjectPool.instance.SpawnComponent<OneInputOperationsOption>(EternityEngine.instance.oneInputOperationsOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (oneInputOperationsOption);
				oneInputOperationsOption.Init ();
				return oneInputOperationsOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				OneInputOperationsOption oneInputOperationsOption = (OneInputOperationsOption) asset;
				oneInputOperationsOption._Data = this;
				oneInputOperationsOption.SetOperationTypeFromData ();
				oneInputOperationsOption.SetTooManyChildrenBehaviourFromData ();
				oneInputOperationsOption.SetValue1OptionParentNameFromData ();
				oneInputOperationsOption.SetValue1OptionNameFromData ();
			}
		}
	}
}