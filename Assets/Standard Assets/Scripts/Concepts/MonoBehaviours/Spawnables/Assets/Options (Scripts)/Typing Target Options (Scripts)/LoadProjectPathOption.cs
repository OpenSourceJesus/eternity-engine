using System;

namespace _EternityEngine
{
	public class LoadProjectPathOption : TypingTargetOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public LoadProjectOption loadProjectOption;

		public override bool SetValue (string value)
		{
			if (base.SetValue(value))
			{
				if (Exists(loadProjectOption))
					loadProjectOption.projectPath = value;
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetLoadProjectOptionNameOfData ();
		}

		void SetLoadProjectOptionNameOfData ()
		{
			if (Exists(loadProjectOption))
				_Data.loadProjectOptionName = loadProjectOption.name;
		}

		void SetLoadProjectOptionNameFromData ()
		{
			if (_Data.loadProjectOptionName != null)
				loadProjectOption = EternityEngine.GetOption<LoadProjectOption>(_Data.loadProjectOptionName);
		}

		[Serializable]
		public class Data : TypingTargetOption.Data
		{
			public string loadProjectOptionName = null;

			public override object MakeAsset ()
			{
				LoadProjectPathOption loadProjectPathOption = ObjectPool.instance.SpawnComponent<LoadProjectPathOption>(EternityEngine.instance.loadProjectPathOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (loadProjectPathOption);
				loadProjectPathOption.Init ();
				return loadProjectPathOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				LoadProjectPathOption loadProjectPathOption = (LoadProjectPathOption) asset;
				loadProjectPathOption._Data = this;
				loadProjectPathOption.SetLoadProjectOptionNameFromData ();
			}
		}
	}
}