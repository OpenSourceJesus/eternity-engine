using System;

namespace _EternityEngine
{
	public class AutomationRecordLeftHandOption : BoolOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public AutomationOption automationOption;

		public override bool SetValue (bool value)
		{
			if (base.SetValue(value))
			{
				if (Exists(automationOption))
					automationOption.recordLeftHand = value;
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetAutomationOptionNameOfData ();
		}

		void SetAutomationOptionNameOfData ()
		{
			if (Exists(automationOption))
				_Data.automationOptionName = automationOption.name;
		}

		void SetAutomationOptionNameFromData ()
		{
			if (_Data.automationOptionName != null)
				automationOption = EternityEngine.GetOption<AutomationOption>(_Data.automationOptionName);
		}

		[Serializable]
		public class Data : BoolOption.Data
		{
			public string automationOptionName = null;

			public override object MakeAsset ()
			{
				AutomationRecordLeftHandOption automationRecordLeftHandOption = ObjectPool.instance.SpawnComponent<AutomationRecordLeftHandOption>(EternityEngine.instance.automationRecordLeftHandOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (automationRecordLeftHandOption);
				automationRecordLeftHandOption.Init ();
				return automationRecordLeftHandOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				AutomationRecordLeftHandOption automationRecordLeftHandOption = (AutomationRecordLeftHandOption) asset;
				automationRecordLeftHandOption._Data = this;
				automationRecordLeftHandOption.SetAutomationOptionNameFromData ();
			}
		}
	}
}