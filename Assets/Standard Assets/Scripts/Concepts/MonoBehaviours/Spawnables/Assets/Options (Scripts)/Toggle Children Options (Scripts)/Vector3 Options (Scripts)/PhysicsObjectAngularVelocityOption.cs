using System;
using Extensions;
using UnityEngine;

namespace _EternityEngine
{
	public class PhysicsObjectAngularVelocityOption : Vector3Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public ModelFileInstanceOption modelFileInstanceOption;
		PhysicsObject3D physicsObject;

		public override void Init ()
		{
			base.Init ();
			if (Exists(modelFileInstanceOption))
				physicsObject = modelFileInstanceOption.physicsObject;
		}

		public override void UpdateValue ()
		{
			if (physicsObject != null)
			{
				if (physicsObject.rigid != null)
					value = physicsObject.rigid.linearVelocity;
			}
		}

		public override bool SetX (float x)
		{
			if (base.SetX(x))
			{
				if (physicsObject != null)
				{
					if (physicsObject.rigid != null)
						physicsObject.rigid.linearVelocity = physicsObject.rigid.linearVelocity.SetX(x);
				}
				return true;
			}
			else
				return false;
		}

		public override bool SetY (float y)
		{
			if (base.SetY(y))
			{
				if (physicsObject != null)
				{
					if (physicsObject.rigid != null)
						physicsObject.rigid.linearVelocity = physicsObject.rigid.linearVelocity.SetY(y);
				}
				return true;
			}
			else
				return false;
		}

		public override bool SetZ (float z)
		{
			if (base.SetZ(z))
			{
				if (physicsObject != null)
				{
					if (physicsObject.rigid != null)
						physicsObject.rigid.linearVelocity = physicsObject.rigid.linearVelocity.SetZ(z);
				}
				return true;
			}
			else
				return false;
		}

		public override bool SetValue (Vector3 value)
		{
			if (base.SetValue(value))
			{
				if (physicsObject != null)
				{
					if (physicsObject.rigid != null)
						physicsObject.rigid.linearVelocity = value;
				}
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetModelFileInstanceOptionNameOfData ();
		}

		void SetModelFileInstanceOptionNameOfData ()
		{
			if (Exists(modelFileInstanceOption))
				_Data.modelFileInstanceOptionName = modelFileInstanceOption.name;
		}

		void SetModelFileInstanceOptionNameFromData ()
		{
			if (_Data.modelFileInstanceOptionName != null)
				modelFileInstanceOption = EternityEngine.GetOption<ModelFileInstanceOption>(_Data.modelFileInstanceOptionName);
		}

		[Serializable]
		public class Data : Vector3Option.Data
		{
			public string modelFileInstanceOptionName = null;

			public override object MakeAsset ()
			{
				PhysicsObjectAngularVelocityOption physicsObjectAngularVelocityOption = ObjectPool.instance.SpawnComponent<PhysicsObjectAngularVelocityOption>(EternityEngine.instance.physicsObjectAngularVelocityOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (physicsObjectAngularVelocityOption);
				physicsObjectAngularVelocityOption.Init ();
				return physicsObjectAngularVelocityOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				PhysicsObjectAngularVelocityOption physicsObjectAngularVelocityOption = (PhysicsObjectAngularVelocityOption) asset;
				physicsObjectAngularVelocityOption._Data = this;
				physicsObjectAngularVelocityOption.SetModelFileInstanceOptionNameFromData ();
			}
		}
	}
}