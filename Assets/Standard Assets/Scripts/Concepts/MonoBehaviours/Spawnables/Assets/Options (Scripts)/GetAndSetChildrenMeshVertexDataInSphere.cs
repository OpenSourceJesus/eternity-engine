using System;
using UnityEngine;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class GetAndSetChildrenMeshVertexDataInSphereOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
public TransformOption fromTransformOption;
public ToggleChildrenOption modelFileInstanceOptionsParent;
		public List<ModelFileInstanceOption> modelFileInstanceOptions = new List<ModelFileInstanceOption>();

		public override void Init ()
		{
			base.Init ();
			fromTransformOption.Init (EternityEngine.instance.actOnMeshVerticesFromTrs);
			modelFileInstanceOptionsParent.onAddChild += OnAddChild;
			modelFileInstanceOptionsParent.onAddChild += OnRemoveChild;
		}

		public override void OnDeleted ()
		{
			modelFileInstanceOptionsParent.onAddChild -= OnAddChild;
			modelFileInstanceOptionsParent.onRemoveChild -= OnRemoveChild;
			base.OnDeleted ();
		}


		void OnAddChild (Option child)
		{
			ModelFileInstanceOption modelFileInstanceOption = child as ModelFileInstanceOption;
			if (modelFileInstanceOption != null)
				modelFileInstanceOptions.Add(modelFileInstanceOption);
		}
		
		void OnRemoveChild (Option child)
		{
			ModelFileInstanceOption modelFileInstanceOption = child as ModelFileInstanceOption;
			if (modelFileInstanceOption != null)
				modelFileInstanceOptions.Remove(modelFileInstanceOption);
		}

		public override void OnActivate (EternityEngine.Hand hand)
		{
			for (int i = 0; i < modelFileInstanceOptions.Count; i ++)
			{
				ModelFileInstanceOption modelFileInstanceOption = modelFileInstanceOptions[i];
				PhysicsObject3D physicsObject = modelFileInstanceOption.physicsObject;
				Mesh mesh = physicsObject.meshFilter.mesh;
				List<Vector3> vertices = new List<Vector3>();
				mesh.GetVertices(vertices);
				Transform physicsObjectTrs = physicsObject.trs;
				for (int i2 = 0; i2 < mesh.vertexCount; i2 ++)
				{
					Vector3 vertex = vertices[i2];
					Transform fromTrs = fromTransformOption.referenceTrs;
					float fromTrsSize = fromTrs.lossyScale.x;
					vertex = physicsObjectTrs.TransformPoint(vertex);
					if ((vertex - fromTrs.position).sqrMagnitude < fromTrsSize * fromTrsSize)
						EternityEngine.instance.SpawnOption<VertexDataOption> (EternityEngine.instance.vertexDataOptionWithChildrenPrefab, (VertexDataOption vertexDataOption) => { vertexDataOption.Init (modelFileInstanceOption, (uint) i2); });
				}
			}
			// base.OnActivate (hand);
		}

		public override bool SetValue (string value)
		{
			bool output = base.SetValue(value);

			return output;
		}

		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
SetTransformOptionNameOfData ();
SetToggleChildrenOptionNameOfData ();
SetModelFileInstanceOptionsNamesOfData ();
		}

		
		public void SetTransformOptionNameOfData ()
		{
			if (Exists(fromTransformOption))
				_Data.fromTransformOptionName = fromTransformOption.name;
		}

		public void SetTransformOptionNameFromData ()
		{
			if (_Data.fromTransformOptionName != null)
				fromTransformOption = EternityEngine.GetOption<TransformOption>(_Data.fromTransformOptionName);
		}

		public void SetToggleChildrenOptionNameOfData ()
		{
			if (Exists(modelFileInstanceOptionsParent))
				_Data.modelFileInstanceOptionsParentName = modelFileInstanceOptionsParent.name;
		}

		public void SetToggleChildrenOptionNameFromData ()
		{
			if (_Data.modelFileInstanceOptionsParentName != null)
				modelFileInstanceOptionsParent = EternityEngine.GetOption<ToggleChildrenOption>(_Data.modelFileInstanceOptionsParentName);
		}

		void SetModelFileInstanceOptionsNamesOfData ()
		{
			_Data.modelFileInstanceOptionsNames = new string[modelFileInstanceOptions.Count];
			for (int i = 0; i < modelFileInstanceOptions.Count; i ++)
			{
				ModelFileInstanceOption modelFileInstanceOption = modelFileInstanceOptions[i];
				if (Exists(modelFileInstanceOption))
					_Data.modelFileInstanceOptionsNames[i] = modelFileInstanceOption.name;
			}
		}

		void SetModelFileInstanceOptionsNamesFromData ()
		{
			for (int i = 0; i < _Data.modelFileInstanceOptionsNames.Length; i ++)
			{
				string parentName = _Data.modelFileInstanceOptionsNames[i];
				modelFileInstanceOptions.Add(EternityEngine.GetOption<ModelFileInstanceOption>(parentName));
			}
		}

		[Serializable]
		public class Data : Option.Data
		{
public string fromTransformOptionName = null;
public string modelFileInstanceOptionsParentName = null;
			public string[] modelFileInstanceOptionsNames = new string[0];

			public override object MakeAsset ()
			{
				GetAndSetChildrenMeshVertexDataInSphereOption getAndSetChildrenMeshVertexDataInSphereOption = ObjectPool.instance.SpawnComponent<GetAndSetChildrenMeshVertexDataInSphereOption>(EternityEngine.instance.getAndSetChildrenMeshVertexDataInSphereOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (getAndSetChildrenMeshVertexDataInSphereOption);
				getAndSetChildrenMeshVertexDataInSphereOption.Init ();
				return getAndSetChildrenMeshVertexDataInSphereOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				GetAndSetChildrenMeshVertexDataInSphereOption getAndSetChildrenMeshVertexDataInSphereOption = (GetAndSetChildrenMeshVertexDataInSphereOption) asset;
				getAndSetChildrenMeshVertexDataInSphereOption._Data = this;
getAndSetChildrenMeshVertexDataInSphereOption.SetTransformOptionNameFromData ();
getAndSetChildrenMeshVertexDataInSphereOption.SetToggleChildrenOptionNameFromData ();
				getAndSetChildrenMeshVertexDataInSphereOption.SetModelFileInstanceOptionsNamesFromData ();
			}
		}
	}
}