using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class LightOption : ToggleChildrenOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public TransformOption transformOption;
		public LightColorOption lightColorOption;
		public Light light;
		public LightType type;
		public LightRenderMode renderMode;
		public float range;
		public float intensity;
		public float innerSpotAngle;
		public float outerSpotAngle;

		public override void Init ()
		{
			base.Init ();
			light = new GameObject().AddComponent<Light>();
			if (Exists(transformOption))
			{
				Transform lightTrs = light.GetComponent<Transform>();
				transformOption.Init (lightTrs);
				if (Exists(transformOption.positionOption))
					lightTrs.position = transformOption.positionOption.value;
				if (Exists(transformOption.rotationOption))
					lightTrs.eulerAngles = transformOption.rotationOption.value;
			}
			if (Exists(lightColorOption))
				light.color = lightColorOption.value;
		}

		public override void OnDeleted ()
		{
			base.OnDeleted ();
			Destroy(light.gameObject);
		}

		public void SetType (Type type)
		{
			this.type = (LightType) Enum.ToObject(typeof(LightType), type.GetHashCode());
			light.type = this.type;
		}

		public void SetRenderMode (RenderMode renderMode)
		{
			this.renderMode = (LightRenderMode) Enum.ToObject(typeof(LightRenderMode), renderMode.GetHashCode());
			light.renderMode = this.renderMode;
		}

		public void SetColor (Color color)
		{
			light.color = color;
		}

		public void SetRange (float range)
		{
			this.range = range;
			light.range = range;
		}

		public void SetIntensity (float intensity)
		{
			this.intensity = intensity;
			light.intensity = intensity;
		}

		public void SetInnerSpotAngle (float angle)
		{
			innerSpotAngle = angle;
			light.innerSpotAngle = angle;
		}

		public void SetOuterSpotAngle (float angle)
		{
			outerSpotAngle = angle;
			light.spotAngle = angle;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetTransformOptionOfData ();
			SetLightColorOptionOfData ();
			SetTypeOfData ();
			SetRenderModeOfData ();
			SetRangeOfData ();
			SetIntensityOfData ();
			SetInnerSpotAngleOfData ();
			SetOuterSpotAngleOfData ();
		}

		void SetTransformOptionOfData ()
		{
			if (Exists(transformOption))
				_Data.transformOptionName = transformOption.name;
		}

		void SetTransformOptionFromData ()
		{
			if (_Data.transformOptionName != null)
				transformOption = EternityEngine.GetOption<TransformOption>(_Data.transformOptionName);
		}

		void SetLightColorOptionOfData ()
		{
			if (Exists(lightColorOption))
				_Data.lightColorOptionName = lightColorOption.name;
		}

		void SetLightColorOptionFromData ()
		{
			if (_Data.lightColorOptionName != null)
				lightColorOption = EternityEngine.GetOption<LightColorOption>(_Data.lightColorOptionName);
		}

		void SetTypeOfData ()
		{
			_Data.type = (Type) Enum.ToObject(typeof(Type), type.GetHashCode());
		}

		void SetTypeFromData ()
		{
			SetType (_Data.type);
		}

		void SetRenderModeOfData ()
		{
			_Data.renderMode = (RenderMode) Enum.ToObject(typeof(RenderMode), renderMode.GetHashCode());
		}

		void SetRenderModeFromData ()
		{
			SetRenderMode (_Data.renderMode);
		}

		void SetRangeOfData ()
		{
			_Data.range = range;
		}

		void SetRangeFromData ()
		{
			SetRange (_Data.range);
		}

		void SetIntensityOfData ()
		{
			_Data.intensity = intensity;
		}

		void SetIntensityFromData ()
		{
			SetIntensity (_Data.intensity);
		}

		void SetInnerSpotAngleOfData ()
		{
			_Data.innerSpotAngle = innerSpotAngle;
		}

		void SetInnerSpotAngleFromData ()
		{
			SetInnerSpotAngle (_Data.innerSpotAngle);
		}

		void SetOuterSpotAngleOfData ()
		{
			_Data.outerSpotAngle = outerSpotAngle;
		}

		void SetOuterSpotAngleFromData ()
		{
			SetOuterSpotAngle (_Data.outerSpotAngle);
		}

		public enum Type
		{
			Spot,
			Directional,
			Point
		}

		public enum RenderMode
		{
			Auto,
			Pixel,
			Vertex
		}

		[Serializable]
		public class Data : ToggleChildrenOption.Data
		{
			public string transformOptionName = null;
			public string lightColorOptionName = null;
			public Type type;
			public RenderMode renderMode;
			public float range;
			public float intensity;
			public float innerSpotAngle;
			public float outerSpotAngle;
			
			public override object MakeAsset ()
			{
				LightOption lightOption = ObjectPool.instance.SpawnComponent<LightOption>(EternityEngine.instance.lightOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (lightOption);
				// lightOption.Init ();
				return lightOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				LightOption lightOption = (LightOption) asset;
				lightOption._Data = this;
				lightOption.SetTransformOptionFromData ();
				lightOption.SetLightColorOptionFromData ();
				lightOption.Init ();
				lightOption.SetTypeFromData ();
				lightOption.SetRenderModeFromData ();
				lightOption.SetRangeFromData ();
				lightOption.SetIntensityFromData ();
				lightOption.SetInnerSpotAngleFromData ();
				lightOption.SetOuterSpotAngleFromData ();
			}
		}
	}
}