using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace _EternityEngine
{
	public class QuitOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}

		public override void Activate (EternityEngine.Hand hand)
		{
			GameManager.Quit ();
			// base.OnActivate (hand);
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
		}

		[Serializable]
		public class Data : Option.Data
		{
			public override object MakeAsset ()
			{
				QuitOption quitOption = ObjectPool.instance.SpawnComponent<QuitOption>(EternityEngine.instance.quitOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (quitOption);
				quitOption.Init ();
				return quitOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				QuitOption quitOption = (QuitOption) asset;
				quitOption._Data = this;
			}
		}
	}
}