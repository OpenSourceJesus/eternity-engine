using System;
using Extensions;
using UnityEngine;

namespace _EternityEngine
{
	public class TransformRotationOption : Vector3Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public Transform referenceTrs;

		public void Init (Transform referenceTrs)
		{
			this.referenceTrs = referenceTrs;
		}

		public override void UpdateValue ()
		{
			value = referenceTrs.eulerAngles;
		}

		public override bool SetX (float x)
		{
			if (base.SetX(x))
			{
				referenceTrs.eulerAngles = referenceTrs.eulerAngles.SetX(x);
				return true;
			}
			else
				return false;
		}

		public override bool SetY (float y)
		{
			if (base.SetY(y))
			{
				referenceTrs.eulerAngles = referenceTrs.eulerAngles.SetY(y);
				return true;
			}
			else
				return false;
		}

		public override bool SetZ (float z)
		{
			if (base.SetZ(z))
			{
				referenceTrs.eulerAngles = referenceTrs.eulerAngles.SetZ(z);
				return true;
			}
			else
				return false;
		}

		public override bool SetValue (Vector3 value)
		{
			if (base.SetValue(value))
			{
				referenceTrs.eulerAngles = value;
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
		}

		[Serializable]
		public class Data : Vector3Option.Data
		{
			public override object MakeAsset ()
			{
				TransformRotationOption transformRotationOption = ObjectPool.instance.SpawnComponent<TransformRotationOption>(EternityEngine.instance.transformRotationOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (transformRotationOption);
				transformRotationOption.Init ();
				return transformRotationOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				TransformRotationOption transformRotationOption = (TransformRotationOption) asset;
				transformRotationOption._Data = this;
			}
		}
	}
}