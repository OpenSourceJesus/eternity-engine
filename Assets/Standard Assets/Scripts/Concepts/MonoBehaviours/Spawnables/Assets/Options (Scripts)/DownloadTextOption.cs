using System;
using UnityEngine;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class DownloadTextOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public DivOption divOption;

		public override void OnActivate (EternityEngine.Hand hand)
		{
			if (Exists(divOption))
				divOption.DownloadText ();
			// base.OnActivate (hand);
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetDivOptionNameOfData ();
		}

		void SetDivOptionNameOfData ()
		{
			if (Exists(divOption))
				_Data.divOptionName = divOption.name;
		}

		void SetDivOptionNameFromData ()
		{
			if (_Data.divOptionName != null)
				divOption = EternityEngine.GetOption<DivOption>(_Data.divOptionName);
		}

		[Serializable]
		public class Data : Option.Data
		{
			public string divOptionName = null;

			public override object MakeAsset ()
			{
				DownloadTextOption downloadTextOption = ObjectPool.instance.SpawnComponent<DownloadTextOption>(EternityEngine.instance.downloadTextOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (downloadTextOption);
				downloadTextOption.Init ();
				return downloadTextOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				DownloadTextOption downloadTextOption = (DownloadTextOption) asset;
				downloadTextOption._Data = this;
				downloadTextOption.SetDivOptionNameFromData ();
			}
		}
	}
}