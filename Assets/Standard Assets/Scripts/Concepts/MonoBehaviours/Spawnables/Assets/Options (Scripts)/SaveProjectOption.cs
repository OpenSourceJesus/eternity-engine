using System;
using Extensions;

namespace _EternityEngine
{
	public class SaveProjectOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public string projectPath;

		public override void OnActivate (EternityEngine.Hand hand)
		{
			// base.OnActivate (hand);
			SaveAndLoadManager.instance.Save (projectPath);
		}

		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetProjectPathOfData ();
		}

		void SetProjectPathOfData ()
		{
			_Data.projectPath = projectPath;
		}

		void SetProjectPathFromData ()
		{
			projectPath = _Data.projectPath;
		}

		[Serializable]
		public class Data : Option.Data
		{
			public string projectPath;

			public override object MakeAsset ()
			{
				SaveProjectOption saveProjectOption = ObjectPool.instance.SpawnComponent<SaveProjectOption>(EternityEngine.instance.saveProjectOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (saveProjectOption);
				saveProjectOption.Init ();
				return saveProjectOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				SaveProjectOption saveProjectOption = (SaveProjectOption) asset;
				saveProjectOption._Data = this;
				saveProjectOption.SetProjectPathFromData ();
			}
		}
	}
}