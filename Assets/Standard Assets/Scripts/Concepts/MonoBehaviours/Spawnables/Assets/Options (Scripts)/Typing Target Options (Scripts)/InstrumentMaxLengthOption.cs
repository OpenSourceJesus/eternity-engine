using System;

namespace _EternityEngine
{
	public class InstrumentMaxLengthOption : TypingTargetOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public InstrumentOption instrumentOption;

		public override bool SetValue (string value)
		{
			if (base.SetValue(value))
			{
				if (Exists(instrumentOption))
				{
					float? maxLength = GetNumberValue();
					if (maxLength != null)
						instrumentOption.SetMaxLength ((float) maxLength);
				}
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetInstrumentOptionNameOfData ();
		}

		void SetInstrumentOptionNameOfData ()
		{
			if (Exists(instrumentOption))
				_Data.instrumentOptionName = instrumentOption.name;
		}

		void SetInstrumentOptionNameFromData ()
		{
			if (_Data.instrumentOptionName != null)
				instrumentOption = EternityEngine.GetOption<InstrumentOption>(_Data.instrumentOptionName);
		}

		[Serializable]
		public class Data : TypingTargetOption.Data
		{
			public string instrumentOptionName = null;

			public override object MakeAsset ()
			{
				InstrumentMaxLengthOption instrumentMaxLengthOption = ObjectPool.instance.SpawnComponent<InstrumentMaxLengthOption>(EternityEngine.instance.instrumentMaxLengthOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (instrumentMaxLengthOption);
				instrumentMaxLengthOption.Init ();
				return instrumentMaxLengthOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				InstrumentMaxLengthOption instrumentMaxLengthOption = (InstrumentMaxLengthOption) asset;
				instrumentMaxLengthOption._Data = this;
				instrumentMaxLengthOption.SetInstrumentOptionNameFromData ();
			}
		}
	}
}