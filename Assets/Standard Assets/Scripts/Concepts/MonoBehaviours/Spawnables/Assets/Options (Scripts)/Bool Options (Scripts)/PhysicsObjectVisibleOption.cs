using System;

namespace _EternityEngine
{
	public class PhysicsObjectVisibleOption : BoolOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public ModelFileInstanceOption modelFileInstanceOption;

		public override bool SetValue (bool value)
		{
			if (base.SetValue(value))
			{
				if (Exists(modelFileInstanceOption))
					modelFileInstanceOption.SetVisible (value);
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetModelFileInstanceOptionNameOfData ();
		}

		void SetModelFileInstanceOptionNameOfData ()
		{
			if (Exists(modelFileInstanceOption))
				_Data.modelFileInstanceOptionName = modelFileInstanceOption.name;
		}

		void SetModelFileInstanceOptionNameFromData ()
		{
			if (_Data.modelFileInstanceOptionName != null)
				modelFileInstanceOption = EternityEngine.GetOption<ModelFileInstanceOption>(_Data.modelFileInstanceOptionName);
		}

		[Serializable]
		public class Data : BoolOption.Data
		{
			public string modelFileInstanceOptionName = null;

			public override object MakeAsset ()
			{
				PhysicsObjectVisibleOption PhysicsObjectVisibleOption = ObjectPool.instance.SpawnComponent<PhysicsObjectVisibleOption>(EternityEngine.instance.physicsObjectVisibleOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (PhysicsObjectVisibleOption);
				PhysicsObjectVisibleOption.Init ();
				return PhysicsObjectVisibleOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				PhysicsObjectVisibleOption PhysicsObjectVisibleOption = (PhysicsObjectVisibleOption) asset;
				PhysicsObjectVisibleOption._Data = this;
				PhysicsObjectVisibleOption.SetModelFileInstanceOptionNameFromData ();
			}
		}
	}
}