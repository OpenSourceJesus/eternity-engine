using System;

namespace _EternityEngine
{
	public class CodeCommandContentsOption : TypingTargetOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public CodeCommandOption codeCommandOption;

		public override bool SetValue (string value)
		{
			if (base.SetValue(value))
			{
				codeCommandOption.SetContents (value);
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetCodeCommandOptionNameOfData ();
		}

		void SetCodeCommandOptionNameOfData ()
		{
			if (Exists(codeCommandOption))
				_Data.codeCommandOptionName = codeCommandOption.name;
		}

		void SetCodeCommandOptionNameFromData ()
		{
			if (_Data.codeCommandOptionName != null)
				codeCommandOption = EternityEngine.GetOption<CodeCommandOption>(_Data.codeCommandOptionName);
		}

		[Serializable]
		public class Data : TypingTargetOption.Data
		{
			public string codeCommandOptionName = null;

			public override object MakeAsset ()
			{
				CodeCommandContentsOption codeCommandContentsOption = ObjectPool.instance.SpawnComponent<CodeCommandContentsOption>(EternityEngine.instance.codeCommandContentsOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (codeCommandContentsOption);
				codeCommandContentsOption.Init ();
				return codeCommandContentsOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				CodeCommandContentsOption codeCommandContentsOption = (CodeCommandContentsOption) asset;
				codeCommandContentsOption._Data = this;
				codeCommandContentsOption.SetCodeCommandOptionNameFromData ();
			}
		}
	}
}
