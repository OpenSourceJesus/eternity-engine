using System;
using UnityEngine;

namespace _EternityEngine
{
	public class ImportAudioWithFileNameOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public string audioFilePath;

		public override void OnActivate (EternityEngine.Hand hand)
		{
			Do (audioFilePath);
			// base.OnActivate (hand);
		}

		public static void Do (string audioFilePath)
		{
			AudioUtilities.GetAudioClipFromWavFile_Callback (audioFilePath, (AudioClip audioClip) => { OnDone (audioFilePath, audioClip); });
		}

		static void OnDone (string audioFilePath, AudioClip audioClip)
		{
			EternityEngine.instance.SpawnOption<SoundFileOption> (EternityEngine.instance.soundFileOptionWithChildrenPrefab, (SoundFileOption soundFileOption) => { soundFileOption.Init (audioFilePath, audioClip); });
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetAudioFilePathOfData ();
		}

		void SetAudioFilePathOfData ()
		{
			_Data.audioFilePath = audioFilePath;
		}

		void SetAudioFilePathFromData ()
		{
			audioFilePath = _Data.audioFilePath;
		}

		[Serializable]
		public class Data : Option.Data
		{
			public string audioFilePath;

			public override object MakeAsset ()
			{
				ImportAudioWithFileNameOption importAudioWithFileNameOption = ObjectPool.instance.SpawnComponent<ImportAudioWithFileNameOption>(EternityEngine.instance.importAudioWithFileNameOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (importAudioWithFileNameOption);
				importAudioWithFileNameOption.Init ();
				return importAudioWithFileNameOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				ImportAudioWithFileNameOption importAudioWithFileNameOption = (ImportAudioWithFileNameOption) asset;
				importAudioWithFileNameOption._Data = this;
				importAudioWithFileNameOption.SetAudioFilePathFromData ();
			}
		}
	}
}