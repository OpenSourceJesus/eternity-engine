using System;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class TransformOption : ToggleChildrenOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public Transform referenceTrs;
		public TransformPositionOption positionOption;
		public TransformRotationOption rotationOption;
		public TransformSizeOption sizeOption;

		public void Init (Transform referenceTrs)
		{
			this.referenceTrs = referenceTrs;
			if (Exists(positionOption))
				positionOption.Init (referenceTrs);
			if (Exists(rotationOption))
				rotationOption.Init (referenceTrs);
			if (Exists(sizeOption))
				sizeOption.Init (referenceTrs);
		}

		public void UpdateTexts ()
		{
			positionOption.UpdateTexts ();
			rotationOption.UpdateTexts ();
			sizeOption.UpdateTexts ();
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetPositionOptionNameOfData ();
			SetRotationOptionNameOfData ();
			SetSizeOptionNameOfData ();
		}

		void SetPositionOptionNameOfData ()
		{
			if (Exists(positionOption))
				_Data.positionOptionName = positionOption.name;
		}

		void SetPositionOptionNameFromData ()
		{
			if (_Data.positionOptionName != null)
				positionOption = EternityEngine.GetOption<TransformPositionOption>(_Data.positionOptionName);
		}

		void SetRotationOptionNameOfData ()
		{
			if (Exists(rotationOption))
				_Data.rotationOptionName = rotationOption.name;
		}

		void SetRotationOptionNameFromData ()
		{
			if (_Data.rotationOptionName != null)
				rotationOption = EternityEngine.GetOption<TransformRotationOption>(_Data.rotationOptionName);
		}

		void SetSizeOptionNameOfData ()
		{
			if (Exists(sizeOption))
				_Data.sizeOptionName = sizeOption.name;
		}

		void SetSizeOptionNameFromData ()
		{
			if (_Data.sizeOptionName != null)
				sizeOption = EternityEngine.GetOption<TransformSizeOption>(_Data.sizeOptionName);
		}

		[Serializable]
		public class Data : ToggleChildrenOption.Data
		{
			public string positionOptionName = null;
			public string rotationOptionName = null;
			public string sizeOptionName = null;

			public override object MakeAsset ()
			{
				TransformOption transformOption = ObjectPool.instance.SpawnComponent<TransformOption>(EternityEngine.instance.transformOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (transformOption);
				transformOption.Init ();
				return transformOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				TransformOption transformOption = (TransformOption) asset;
				transformOption._Data = this;
				transformOption.SetPositionOptionNameFromData ();
				transformOption.SetRotationOptionNameFromData ();
				transformOption.SetSizeOptionNameFromData ();
			}
		}
	}
}