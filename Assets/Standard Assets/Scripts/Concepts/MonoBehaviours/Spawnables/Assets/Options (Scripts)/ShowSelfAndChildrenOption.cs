using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class ShowSelfAndChildrenOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}

		public override void OnActivate (EternityEngine.Hand hand)
		{
			Show ();
			ShowChildren ();
			// base.OnActivate (hand);
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
		}

		[Serializable]
		public class Data : Option.Data
		{
			public override object MakeAsset ()
			{
				ShowSelfAndChildrenOption showSelfAndChildrenOption = ObjectPool.instance.SpawnComponent<ShowSelfAndChildrenOption>(EternityEngine.instance.showSelfAndChildrenOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (showSelfAndChildrenOption);
				showSelfAndChildrenOption.Init ();
				return showSelfAndChildrenOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				ShowSelfAndChildrenOption showSelfAndChildrenOption = (ShowSelfAndChildrenOption) asset;
				showSelfAndChildrenOption._Data = this;
			}
		}
	}
}