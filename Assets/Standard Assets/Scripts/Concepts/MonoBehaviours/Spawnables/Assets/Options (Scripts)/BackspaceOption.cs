using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class BackspaceOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}

		public override void OnActivate (EternityEngine.Hand hand)
		{
			if (Exists(TypingTargetOption.currentActive))
			{
				ActivationBehaviourOption.ActivateChildrenIfShould (TriggerType.TypeBackspace, false, this);
				TypingTargetOption.currentActive.BackspaceText ();
				ActivationBehaviourOption.ActivateChildrenIfShould (TriggerType.TypeBackspace, true, this);
			}
			// base.OnActivate (hand);
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
		}

		[Serializable]
		public class Data : Option.Data
		{
			public override object MakeAsset ()
			{
				BackspaceOption backspaceOption = ObjectPool.instance.SpawnComponent<BackspaceOption>(EternityEngine.instance.backspaceOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (backspaceOption);
				backspaceOption.Init ();
				return backspaceOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				BackspaceOption backspaceOption = (BackspaceOption) asset;
				backspaceOption._Data = this;
			}
		}
	}
}