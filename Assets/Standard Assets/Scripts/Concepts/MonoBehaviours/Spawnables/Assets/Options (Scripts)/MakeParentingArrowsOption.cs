using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class MakeParentingArrowsOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public Option parentsParent;
		public Option childrenParent;
		List<Option> parents = new List<Option>();
		List<Option> _children = new List<Option>();

		public override void Init ()
		{
			base.Init ();
			parentsParent.onAddChild += OnAddChild_ParentsParent;
			parentsParent.onRemoveChild += OnRemoveChild_ParentsParent;
			childrenParent.onAddChild += OnAddChild_ChildrenParent;
			childrenParent.onRemoveChild += OnRemoveChild_ChildrenParent;
		}

		void OnAddChild_ParentsParent (Option child)
		{
			parents.Add(child);
		}
		
		void OnRemoveChild_ParentsParent (Option child)
		{
			parents.Remove(child);
		}

		void OnAddChild_ChildrenParent (Option child)
		{
			_children.Add(child);
		}

		void OnRemoveChild_ChildrenParent (Option child)
		{
			_children.Remove(child);
		}

		public override void OnActivate (EternityEngine.Hand hand)
		{
			for (int i = 0; i < parents.Count; i ++)
			{
				Option parent = parents[i];
				for (int i2 = 0; i2 < _children.Count; i2 ++)
				{
					Option child = _children[i2];
					if (!parent.children.Contains(child))
					{
						parent.AddParentingArrowToChild (child);
						parent.AddChild (child);
					}
				}
			}
			// base.OnActivate (hand);
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetParentsOptionNamesOfData ();
			SetChildrenOptionNamesOfData ();
			SetParentsParentOptionNameOfData ();
			SetChildrenParentOptionNameOfData ();
		}
		
		void SetParentsParentOptionNameOfData ()
		{
			if (Exists(parentsParent))
				_Data.parentsParentOptionName = parentsParent.name;
		}

		void SetParentsParentOptionNameFromData ()
		{
			if (_Data.parentsParentOptionName != null)
				parentsParent = EternityEngine.GetOption(_Data.parentsParentOptionName);
		}
		
		void SetChildrenParentOptionNameOfData ()
		{
			if (Exists(childrenParent))
				_Data.childrenParentOptionName = childrenParent.name;
		}

		void SetChildrenParentOptionNameFromData ()
		{
			if (_Data.childrenParentOptionName != null)
				childrenParent = EternityEngine.GetOption(_Data.childrenParentOptionName);
		}

		void SetParentsOptionNamesOfData ()
		{
			_Data.parentsOptionNames = new string[parents.Count];
			for (int i = 0; i < parents.Count; i ++)
			{
				Option parent = parents[i];
				if (Exists(parent))
					_Data.parentsOptionNames[i] = parent.name;
			}
		}

		void SetParentsOptionNamesFromData ()
		{
			for (int i = 0; i < _Data.parentsOptionNames.Length; i ++)
			{
				string parentName = _Data.parentsOptionNames[i];
				parents.Add(EternityEngine.GetOption(parentName));
			}
		}

		void SetChildrenOptionNamesOfData ()
		{
			_Data.childrenOptionNames = new string[_children.Count];
			for (int i = 0; i < _children.Count; i ++)
			{
				Option child = _children[i];
				if (Exists(child))
					_Data.childrenOptionNames[i] = child.name;
			}
		}

		void SetChildrenOptionNamesFromData ()
		{
			for (int i = 0; i < _Data.childrenOptionNames.Length; i ++)
			{
				string childeName = _Data.childrenOptionNames[i];
				_children.Add(EternityEngine.GetOption(childeName));
			}
		}

		[Serializable]
		public class Data : Option.Data
		{
			public string[] parentsOptionNames = new string[0];
			public string[] childrenOptionNames = new string[0];
			public string parentsParentOptionName = null;
			public string childrenParentOptionName = null;

			public override object MakeAsset ()
			{
				MakeParentingArrowsOption makeParentingArrowsOption = ObjectPool.instance.SpawnComponent<MakeParentingArrowsOption>(EternityEngine.instance.makeParentingArrowsOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (makeParentingArrowsOption);
				makeParentingArrowsOption.Init ();
				return makeParentingArrowsOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				MakeParentingArrowsOption makeParentingArrowsOption = (MakeParentingArrowsOption) asset;
				makeParentingArrowsOption._Data = this;
				makeParentingArrowsOption.SetParentsOptionNamesFromData ();
				makeParentingArrowsOption.SetChildrenOptionNamesFromData ();
				makeParentingArrowsOption.SetParentsParentOptionNameFromData ();
				makeParentingArrowsOption.SetChildrenParentOptionNameFromData ();
			}
		}
	}
}