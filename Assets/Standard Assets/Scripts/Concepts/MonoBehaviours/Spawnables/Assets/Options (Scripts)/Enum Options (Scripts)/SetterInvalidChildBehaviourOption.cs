using System;

namespace _EternityEngine
{
	public class SetterInvalidChildBehaviourOption : EnumOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public SetterOption setterOption;

		public override bool SetValue (int value)
		{
			if (base.SetValue(value))
			{
				if (Exists(setterOption))
					setterOption.ApplyInvalidChildBehaviour ((SetterOption.InvalidChildBehaviour) Enum.ToObject(enumType, value));
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetSetterOptionNameOfData ();
		}

		void SetSetterOptionNameOfData ()
		{
			if (Exists(setterOption))
				_Data.setterOptionName = setterOption.name;
		}

		void SetSetterOptionNameFromData ()
		{
			if (_Data.setterOptionName != null)
				setterOption = EternityEngine.GetOption<SetterOption>(_Data.setterOptionName);
		}

		[Serializable]
		public class Data : EnumOption.Data
		{
			public string setterOptionName = null;

			public override object MakeAsset ()
			{
				SetterInvalidChildBehaviourOption setterInvalidChildBehaviourOption = ObjectPool.instance.SpawnComponent<SetterInvalidChildBehaviourOption>(EternityEngine.instance.setterInvalidChildBehaviourOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (setterInvalidChildBehaviourOption);
				setterInvalidChildBehaviourOption.Init ();
				return setterInvalidChildBehaviourOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				SetterInvalidChildBehaviourOption setterInvalidChildBehaviourOption = (SetterInvalidChildBehaviourOption) asset;
				setterInvalidChildBehaviourOption._Data = this;
				setterInvalidChildBehaviourOption.SetSetterOptionNameFromData ();
			}
		}
	}
}