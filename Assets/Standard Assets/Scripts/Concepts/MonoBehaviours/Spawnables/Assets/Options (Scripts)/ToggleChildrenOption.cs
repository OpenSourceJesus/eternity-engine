using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class ToggleChildrenOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}

		public override void OnActivate (EternityEngine.Hand hand)
		{
			ToggleChildren ();
			// base.OnActivate (hand);
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
		}

		[Serializable]
		public class Data : Option.Data
		{
			public override object MakeAsset ()
			{
				ToggleChildrenOption toggleChildrenOption = ObjectPool.instance.SpawnComponent<ToggleChildrenOption>(EternityEngine.instance.toggleChildrenOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (toggleChildrenOption);
				toggleChildrenOption.Init ();
				return toggleChildrenOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				ToggleChildrenOption toggleChildrenOption = (ToggleChildrenOption) asset;
				toggleChildrenOption._Data = this;
			}
		}
	}
}