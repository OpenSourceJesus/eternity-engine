using System;
using Extensions;
using UnityEngine;

namespace _EternityEngine
{
	public class SpritePivotOption : Vector3Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public ImageFileInstanceOption imageFileInstanceOption;

		public override bool SetX (float x)
		{
			if (base.SetX(x))
			{
				Apply ();
				return true;
			}
			else
				return false;
		}

		public override bool SetY (float y)
		{
			if (base.SetY(y))
			{
				Apply ();
				return true;
			}
			else
				return false;
		}

		public override bool SetValue (Vector3 value)
		{
			if (base.SetValue(value))
			{
				Apply ();
				return true;
			}
			else
				return false;
		}

		void Apply ()
		{
			if (Exists(imageFileInstanceOption) && Exists(imageFileInstanceOption.fileOption))
			{
				ImageFileOption imageFileOption = (ImageFileOption) imageFileInstanceOption.fileOption;
				Texture2D texture = imageFileOption.texture;
				imageFileInstanceOption.spriteRenderer.sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), value);
			}
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetImageFileInstanceOptionNameOfData ();
		}

		void SetImageFileInstanceOptionNameOfData ()
		{
			if (Exists(imageFileInstanceOption))
				_Data.imageFileInstanceOptionName = imageFileInstanceOption.name;
		}

		void SetImageFileInstanceOptionNameFromData ()
		{
			if (_Data.imageFileInstanceOptionName != null)
				imageFileInstanceOption = EternityEngine.GetOption<ImageFileInstanceOption>(_Data.imageFileInstanceOptionName);
		}

		[Serializable]
		public class Data : Vector3Option.Data
		{
			public string imageFileInstanceOptionName = null;

			public override object MakeAsset ()
			{
				SpritePivotOption spritePivotOption = ObjectPool.instance.SpawnComponent<SpritePivotOption>(EternityEngine.instance.spritePivotOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (spritePivotOption);
				spritePivotOption.Init ();
				return spritePivotOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				SpritePivotOption spritePivotOption = (SpritePivotOption) asset;
				spritePivotOption._Data = this;
				spritePivotOption.SetImageFileInstanceOptionNameFromData ();
			}
		}
	}
}