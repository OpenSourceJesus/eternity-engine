using System;

namespace _EternityEngine
{
	public class SetVector3YOption : TypingTargetOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public Vector3Option vector3Option;

		public override bool SetValue (string value)
		{
			if (base.SetValue(value))
			{
				if (Exists(vector3Option))
				{
					float? y = GetNumberValue();
					if (y != null)
						vector3Option.SetY ((float) y);
				}
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetVector3OptionNameOfData ();
		}

		void SetVector3OptionNameOfData ()
		{
			if (Exists(vector3Option))
				_Data.vector3OptionName = vector3Option.name;
		}

		void SetVector3OptionNameFromData ()
		{
			if (_Data.vector3OptionName != null)
				vector3Option = EternityEngine.GetOption<Vector3Option>(_Data.vector3OptionName);
		}

		[Serializable]
		public class Data : TypingTargetOption.Data
		{
			public string vector3OptionName = null;

			public override object MakeAsset ()
			{
				SetVector3YOption setVector3YOption = ObjectPool.instance.SpawnComponent<SetVector3YOption>(EternityEngine.instance.setVector3YOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (setVector3YOption);
				setVector3YOption.Init ();
				return setVector3YOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				SetVector3YOption setVector3YOption = (SetVector3YOption) asset;
				setVector3YOption._Data = this;
				setVector3YOption.SetVector3OptionNameFromData ();
			}
		}
	}
}