using System;

namespace _EternityEngine
{
	public class AutomationSequenceEntryTimeOption : TypingTargetOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public AutomationSequenceEntryOption automationSequenceEntryOption;

		public override bool SetValue (string value)
		{
			if (base.SetValue(value))
			{
				if (Exists(automationSequenceEntryOption))
				{
					float? time = GetNumberValue();
					if (time != null)
						automationSequenceEntryOption.SetTime((float) time);
				}
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetAutomationSequenceEntryOptionNameOfData ();
		}

		void SetAutomationSequenceEntryOptionNameOfData ()
		{
			if (Exists(automationSequenceEntryOption))
				_Data.automationSequenceEntryOptionName = automationSequenceEntryOption.name;
		}

		void SetAutomationSequenceEntryOptionNameFromData ()
		{
			if (_Data.automationSequenceEntryOptionName != null)
				automationSequenceEntryOption = EternityEngine.GetOption<AutomationSequenceEntryOption>(_Data.automationSequenceEntryOptionName);
		}

		[Serializable]
		public class Data : TypingTargetOption.Data
		{
			public string automationSequenceEntryOptionName = null;

			public override object MakeAsset ()
			{
				AutomationSequenceEntryTimeOption automationSequenceEntryTimeOption = ObjectPool.instance.SpawnComponent<AutomationSequenceEntryTimeOption>(EternityEngine.instance.automationSequenceEntryTimeOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (automationSequenceEntryTimeOption);
				// if (!automationSequenceEntryTimeOption.isInitialized)
				// 	automationSequenceEntryTimeOption.Init ();
				return automationSequenceEntryTimeOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				AutomationSequenceEntryTimeOption automationSequenceEntryTimeOption = (AutomationSequenceEntryTimeOption) asset;
				automationSequenceEntryTimeOption._Data = this;
				automationSequenceEntryTimeOption.SetAutomationSequenceEntryOptionNameFromData ();
			}
		}
	}
}
