using System;
using Extensions;
using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class DuplicateChildrenOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public Option childrenToDuplicateParentOption;
		public KeepParentingArrowsBehaviour keepParentingArrowsBehaviour;

		public override void Activate (EternityEngine.Hand hand)
		{
			ActivationBehaviourOption.ActivateChildrenIfShould (TriggerType.Activate, false, this);
			OnActivate (hand);
			int childrenToDuplicateCount = childrenToDuplicateParentOption.children.Count;
			for (int i = 0; i < childrenToDuplicateCount; i ++)
			{
				Option childToDuplicateOption = childrenToDuplicateParentOption.children[i];
				Option[] childOptions = GetAllChildrenAndSelf(childToDuplicateOption);
				for (int i2 = 1; i2 < childOptions.Length; i2 ++)
				{
					Option childOption = childOptions[i2];
					childOption.trs.SetParent(childToDuplicateOption.childOptionsParent);
				}
				ActivationBehaviourOption.ActivateChildrenIfShould (TriggerType.Duplicate, false, childToDuplicateOption);
				Option duplicate = Instantiate(childToDuplicateOption, EternityEngine.instance.currentSpawnOrientationOption.trs.position, EternityEngine.instance.currentSpawnOrientationOption.trs.rotation, childrenToDuplicateParentOption.childOptionsParent);
				duplicate.trs.SetWorldScale (EternityEngine.instance.currentSpawnOrientationOption.trs.lossyScale);
				Option[] duplicateChildOptions = GetAllChildrenAndSelf(duplicate);
				for (int i2 = 0; i2 < duplicateChildOptions.Length; i2 ++)
				{
					Option duplicateChildOption = duplicateChildOptions[i2];
					duplicateChildOption.name = "\n";
					duplicateChildOption.HandleNaming ();
					duplicateChildOption.Init ();
				}
				ActivationBehaviourOption.ActivateChildrenIfShould (TriggerType.Duplicate, true, childToDuplicateOption);
			}
			activateAnimationEntry.Play(0);
			ActivationBehaviourOption.ActivateChildrenIfShould (TriggerType.Activate, true, this);
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetChildrenToDuplicateParentNameOfData ();
			SetKeepParentingArrowsBehaviourOfData ();
		}

		void SetChildrenToDuplicateParentNameOfData ()
		{
			if (Exists(childrenToDuplicateParentOption))
				_Data.childrenToDuplicateParentOptionName = childrenToDuplicateParentOption.name;
		}

		void SetChildrenToDuplicateParentNameFromData ()
		{
			if (_Data.childrenToDuplicateParentOptionName != null)
				childrenToDuplicateParentOption = EternityEngine.GetOption(_Data.childrenToDuplicateParentOptionName);
		}

		void SetKeepParentingArrowsBehaviourOfData ()
		{
			_Data.keepParentingArrowsBehaviour = keepParentingArrowsBehaviour;
		}

		void SetKeepParentingArrowsBehaviourFromData ()
		{
			keepParentingArrowsBehaviour = _Data.keepParentingArrowsBehaviour;
		}

		public enum KeepParentingArrowsBehaviour
		{
			KeepChildParentingArrows,
			KeepParentParentingArrows,
			KeepAllParentingArrows
		}

		[Serializable]
		public class Data : Option.Data
		{
			public string childrenToDuplicateParentOptionName = null;
			public KeepParentingArrowsBehaviour keepParentingArrowsBehaviour;

			public override object MakeAsset ()
			{
				DuplicateChildrenOption duplicateChildrenOption = ObjectPool.instance.SpawnComponent<DuplicateChildrenOption>(EternityEngine.instance.duplicateChildrenOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (duplicateChildrenOption);
				duplicateChildrenOption.Init ();
				return duplicateChildrenOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				DuplicateChildrenOption duplicateChildrenOption = (DuplicateChildrenOption) asset;
				duplicateChildrenOption._Data = this;
				duplicateChildrenOption.SetChildrenToDuplicateParentNameFromData ();
				duplicateChildrenOption.SetKeepParentingArrowsBehaviourFromData ();
			}
		}
	}
}