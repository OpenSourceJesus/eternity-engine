using System;

namespace _EternityEngine
{
	public class SaveProjectPathOption : TypingTargetOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public SaveProjectOption saveProjectOption;

		public override bool SetValue (string value)
		{
			if (base.SetValue(value))
			{
				if (Exists(saveProjectOption))
					saveProjectOption.projectPath = value;
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetSaveProjectOptionNameOfData ();
		}

		void SetSaveProjectOptionNameOfData ()
		{
			if (Exists(saveProjectOption))
				_Data.saveProjectOptionName = saveProjectOption.name;
		}

		void SetSaveProjectOptionNameFromData ()
		{
			if (_Data.saveProjectOptionName != null)
				saveProjectOption = EternityEngine.GetOption<SaveProjectOption>(_Data.saveProjectOptionName);
		}

		[Serializable]
		public class Data : TypingTargetOption.Data
		{
			public string saveProjectOptionName = null;

			public override object MakeAsset ()
			{
				SaveProjectPathOption saveProjectPathOption = ObjectPool.instance.SpawnComponent<SaveProjectPathOption>(EternityEngine.instance.saveProjectPathOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (saveProjectPathOption);
				saveProjectPathOption.Init ();
				return saveProjectPathOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				SaveProjectPathOption saveProjectPathOption = (SaveProjectPathOption) asset;
				saveProjectPathOption._Data = this;
				saveProjectPathOption.SetSaveProjectOptionNameFromData ();
			}
		}
	}
}