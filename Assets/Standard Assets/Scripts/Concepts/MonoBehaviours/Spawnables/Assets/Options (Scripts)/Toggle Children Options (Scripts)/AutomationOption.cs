using System;
using Extensions;
using UnityEngine;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class AutomationOption : ToggleChildrenOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public Option recordActivateOptionsParent;
		public Option recordParentingOptionsParent;
		public Option automationSequenceEntriesParent;
		public float timeMultiplier;
		public bool pauseRecording;
		public bool referenceNewOptions;
		public bool recordLeftHand;
		public bool recordRightHand;
		public bool recordInvalidHand;
		public bool pausePlayback;
		public AutomationSequence.InvalidOptionFixBehaviour invalidOptionFixBehaviour;
		public AutomationSequence.InvalidOptionContinueSequenceBehaviour invalidOptionContinueSequenceBehaviour;
		public DuplicateSequenceEntryIndexBehaviour duplicateSequenceEntryIndexBehaviour;
		public AutomationSequence automationSequence;
		public float recordingStartTime;
		public List<AutomationSequenceEntryOption> automationSequenceEntryOptions = new List<AutomationSequenceEntryOption>();
		// public static List<AutomationOption> currentRecording = new List<AutomationOption>();
		public static Dictionary<Option, List<AutomationOption>> recordingActivateOptionsDict = new Dictionary<Option, List<AutomationOption>>();
		public static Dictionary<Option, List<AutomationOption>> recordingParentingOptionsDict = new Dictionary<Option, List<AutomationOption>>();
		PlaybackHandler playbackHandler;

		public void Init ()
		{
			base.Init ();
			automationSequenceEntriesParent.onAddChild += OnAddChild;
			automationSequenceEntriesParent.onRemoveChild += OnRemoveChild;
		}

		public void StartRecording ()
		{
			recordingStartTime = Time.time;
			// currentRecording.Add(this);
			if (recordLeftHand)
				StartRecording (EternityEngine.instance.leftHand);
			else if (recordRightHand)
				StartRecording (EternityEngine.instance.rightHand);
			else// if (recordInvalidHand)
				StartRecording (null);
		}

		void StartRecording (EternityEngine.Hand hand)
		{
			for (int i = 0; i < recordActivateOptionsParent.children.Count; i ++)
			{
				Option option = recordActivateOptionsParent.children[i];
				RegisterActivateOption (option, hand);
			}
			recordActivateOptionsParent.onAddChild += (Option option) => { RegisterActivateOption (option, hand); };
			recordActivateOptionsParent.onRemoveChild += (Option option) => { UnregisterActivateOption (option, hand); };
			for (int i = 0; i < recordParentingOptionsParent.children.Count; i ++)
			{
				Option option = recordParentingOptionsParent.children[i];
				RegisterParentingOption (option, hand);
			}
			recordParentingOptionsParent.onAddChild += (Option option) => { RegisterParentingOption (option, hand); };
			recordParentingOptionsParent.onRemoveChild += (Option option) => { UnregisterParentingOption (option, hand); };
		}
		
		public void EndRecording ()
		{
			// currentRecording.Remove(this);
			EndRecording (EternityEngine.instance.leftHand);
			EndRecording (EternityEngine.instance.rightHand);
			EndRecording (null);
		}

		void EndRecording (EternityEngine.Hand hand)
		{
			foreach (KeyValuePair<Option, List<AutomationOption>> keyValuePair in recordingActivateOptionsDict)
				UnregisterActivateOption (keyValuePair.Key, hand);
			recordParentingOptionsParent.onAddChild += (Option option) => { RegisterParentingOption (option, hand); };
			recordParentingOptionsParent.onRemoveChild += (Option option) => { UnregisterParentingOption (option, hand); };
			foreach (KeyValuePair<Option, List<AutomationOption>> keyValuePair in recordingParentingOptionsDict)
				UnregisterParentingOption (keyValuePair.Key, hand);
			recordParentingOptionsParent.onAddChild -= (Option option) => { RegisterParentingOption (option, hand); };
			recordParentingOptionsParent.onRemoveChild -= (Option option) => { UnregisterParentingOption (option, hand); };
		}

		public void StartPlaying ()
		{
			playbackHandler = new PlaybackHandler();
			playbackHandler.Init (this);
		}

		public void PausePlaying (bool pause)
		{
			pausePlayback = pause;
			if (pause)
				GameManager.updatables = GameManager.updatables.Remove(playbackHandler);
			else
				GameManager.updatables = GameManager.updatables.Add(playbackHandler);
		}

		public void EndPlayback ()
		{
			playbackHandler.End ();
		}

		public void DeleteSequenceEntryOption (int index)
		{
			AutomationSequenceEntryOption automationSequenceEntryOption = automationSequenceEntryOptions[index];
			ObjectPool.instance.Despawn (automationSequenceEntryOption.prefabIndex, automationSequenceEntryOption.gameObject, automationSequenceEntryOption.trs);
			for (int i = 0; i < automationSequenceEntryOption.automationOptionsUsingMe.Count; i ++)
			{
				AutomationOption automationOption = automationSequenceEntryOption.automationOptionsUsingMe[i];
				automationOption.RemoveSequenceEntry (index);
			}
		}

		public void RemoveSequenceEntry (int index)
		{
			automationSequenceEntryOptions.RemoveAt(index);
			automationSequence.sequenceEntries.RemoveAt(index);
		}

		void RegisterActivateOption (Option option, EternityEngine.Hand hand)
		{
			if (!recordingActivateOptionsDict.ContainsKey(option))
			{
				recordingActivateOptionsDict.Add(option, new List<AutomationOption>() { this });
				// option.onActivated += (EternityEngine.Hand hand) => { option.OnActivated (hand); };
				option.recordingActivateInAutomationSequence = true;
			}
			else
				recordingActivateOptionsDict[option].Add(this);
		}

		void UnregisterActivateOption (Option option, EternityEngine.Hand hand)
		{
			recordingActivateOptionsDict[option].Remove(this);
			if (recordingActivateOptionsDict[option].Count == 0)
			{
				// option.onActivated = null;
				recordingActivateOptionsDict.Remove(option);
				option.recordingActivateInAutomationSequence = false;
			}
		}

		void RegisterParentingOption (Option option, EternityEngine.Hand hand)
		{
			if (!recordingParentingOptionsDict.ContainsKey(option))
			{
				recordingParentingOptionsDict.Add(option, new List<AutomationOption>() { this });
				option.onAddChild += option.OnAddedOrRemovedChild;
				option.onRemoveChild += option.OnAddedOrRemovedChild;
			}
			else
				recordingParentingOptionsDict[option].Add(this);
		}

		void UnregisterParentingOption (Option option, EternityEngine.Hand hand)
		{
			recordingParentingOptionsDict[option].Remove(this);
			if (recordingParentingOptionsDict[option].Count == 0)
			{
				option.onAddChild -= option.OnAddedOrRemovedChild;
				option.onRemoveChild -= option.OnAddedOrRemovedChild;
				recordingParentingOptionsDict.Remove(option);
			}
		}

		void OnAddChild (Option option)
		{
			AutomationSequenceEntryOption sequenceEntryOption = option as AutomationSequenceEntryOption;
			if (sequenceEntryOption != null)
			{
				int sequenceEntryIndex = sequenceEntryOption.sequenceEntryIndex;
				if (sequenceEntryIndex >= 0 && sequenceEntryIndex < automationSequenceEntryOptions.Count)
				{
					if (duplicateSequenceEntryIndexBehaviour == DuplicateSequenceEntryIndexBehaviour.PreferSwitch)
					{
						AutomationSequenceEntryOption mySequenceEntryOption = automationSequenceEntryOptions[sequenceEntryIndex];
						mySequenceEntryOption.sequenceEntry = sequenceEntryOption.sequenceEntry;
						automationSequence.sequenceEntries[sequenceEntryIndex] = sequenceEntryOption.sequenceEntry;
						mySequenceEntryOption.automationOptionsUsingMe.Remove(this);
						int indexOfMe = sequenceEntryOption.automationOptionsUsingMe.IndexOf(this);
						mySequenceEntryOption.automationOptionsUsingMe.Add(sequenceEntryOption.automationOptionsUsingMe[indexOfMe]);
					}
					else if (duplicateSequenceEntryIndexBehaviour == DuplicateSequenceEntryIndexBehaviour.PreferInsert)
					{
						automationSequence.sequenceEntries.Insert(sequenceEntryIndex, sequenceEntryOption.sequenceEntry);
						automationSequenceEntryOptions.Insert(sequenceEntryIndex, sequenceEntryOption);
						for (int i = sequenceEntryIndex; i < automationSequenceEntryOptions.Count; i ++)
						{
							AutomationSequenceEntryOption mySequenceEntryOption = automationSequenceEntryOptions[i];
							for (int i2 = 0; i2 < mySequenceEntryOption.automationSequenceEntryIndexOptions.Length; i2 ++)
							{
								AutomationSequenceEntryIndexOption automationSequenceEntryIndexOption = mySequenceEntryOption.automationSequenceEntryIndexOptions[i2];
								automationSequenceEntryIndexOption.SetValue ("" + (i + 1));
							}
						}
					}
					sequenceEntryOption.automationOptionsUsingMe.Add(this);
					automationSequenceEntryOptions.Add(sequenceEntryOption);
				}
			}
		}

		void OnRemoveChild (Option option)
		{
			AutomationSequenceEntryOption sequenceEntryOption = option as AutomationSequenceEntryOption;
			if (sequenceEntryOption != null)
			{
				int sequenceEntryIndex = sequenceEntryOption.sequenceEntryIndex;
				for (int i = 0; i < sequenceEntryOption.automationOptionsUsingMe.Count; i ++)
				{
					AutomationOption automationOption = sequenceEntryOption.automationOptionsUsingMe[i];
					sequenceEntryOption.automationOptionsUsingMe.RemoveAt(i);
					automationOption.automationSequenceEntryOptions.RemoveAt(i);
					automationSequence.sequenceEntries.RemoveAt(sequenceEntryIndex);
					for (int i2 = sequenceEntryIndex; i2 < automationSequenceEntryOptions.Count; i2 ++)
					{
						AutomationSequenceEntryOption sequenceEntryOption2 = automationSequenceEntryOptions[i2];
						for (int i3 = 0; i3 < sequenceEntryOption2.automationSequenceEntryIndexOptions.Length; i3 ++)
						{
							AutomationSequenceEntryIndexOption automationSequenceEntryIndexOption = sequenceEntryOption2.automationSequenceEntryIndexOptions[i3];
							automationSequenceEntryIndexOption.SetValue ("" + (i2 - 1));
						}
					}
					i --;
				}
			}
		}

		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetAutomationSequenceOfData ();
			SetRecordActivateOptionsParentNameOfData ();
			SetRecordParentingOptionsParentNameOfData ();
			SetAutomationSequenceEntriesParentNameOfData ();
		}

		void SetAutomationSequenceOfData ()
		{
			_Data.automationSequence = automationSequence;
		}

		void SetAutomationSequenceFromData ()
		{
			automationSequence = _Data.automationSequence;
		}

		void SetRecordActivateOptionsParentNameOfData ()
		{
			if (Exists(recordActivateOptionsParent))
				_Data.recordActivateOptionsParentName = recordActivateOptionsParent.name;
		}

		void SetRecordActivateOptionsParentNameFromData ()
		{
			if (_Data.recordActivateOptionsParentName != null)
				recordActivateOptionsParent = EternityEngine.GetOption(_Data.recordActivateOptionsParentName);
		}

		void SetRecordParentingOptionsParentNameOfData ()
		{
			if (Exists(recordParentingOptionsParent))
				_Data.recordActivateOptionsParentName = recordParentingOptionsParent.name;
		}

		void SetRecordParentingOptionsParentNameFromData ()
		{
			if (_Data.recordActivateOptionsParentName != null)
				recordActivateOptionsParent = EternityEngine.GetOption(_Data.recordActivateOptionsParentName);
		}

		void SetAutomationSequenceEntriesParentNameOfData ()
		{
			if (Exists(automationSequenceEntriesParent))
				_Data.automationSequenceEntriesParentName = automationSequenceEntriesParent.name;
		}

		void SetAutomationSequenceEntriesParentNameFromData ()
		{
			if (_Data.automationSequenceEntriesParentName != null)
				automationSequenceEntriesParent = EternityEngine.GetOption(_Data.automationSequenceEntriesParentName);
		}

		public class PlaybackHandler : IUpdatable
		{
			public AutomationOption automationOption;
			int sequenceEntryIndex;
			float playbackTime;

			public void Init (AutomationOption automationOption)
			{
				this.automationOption = automationOption;
			}

			public void Start ()
			{
				playbackTime = 0;
				GameManager.updatables = GameManager.updatables.Add(this);
			}

			public void DoUpdate ()
			{
				AutomationSequence.SequenceEntry sequenceEntry = automationOption.automationSequence.sequenceEntries[sequenceEntryIndex];
				while (playbackTime >= sequenceEntry.time * automationOption.automationSequence.timeMultiplier)
				{
					if (!sequenceEntry.activateOptionStart.Equals(null))
					{
						Option option = sequenceEntry.activateOptionStart.GetOption();
						if (option != null)
						{
							EternityEngine.Hand hand = EternityEngine.Hand.GetHandFromType(sequenceEntry.madeByHandType);
							option.Activate (hand);
						}
						else
						{
							if (automationOption.invalidOptionFixBehaviour == AutomationSequence.InvalidOptionFixBehaviour.DeleteSequenceEntry)
								automationOption.DeleteSequenceEntryOption (sequenceEntryIndex);
							if (automationOption.invalidOptionContinueSequenceBehaviour == AutomationSequence.InvalidOptionContinueSequenceBehaviour.Stop)
							{
								End ();
								return;
							}
							else if (automationOption.invalidOptionContinueSequenceBehaviour == AutomationSequence.InvalidOptionContinueSequenceBehaviour.Continue)
							{
								if (automationOption.invalidOptionFixBehaviour != AutomationSequence.InvalidOptionFixBehaviour.DeleteSequenceEntry)
								{
									sequenceEntryIndex ++;
									if (sequenceEntryIndex >= automationOption.automationSequence.sequenceEntries.Count)
										break;
									sequenceEntry = automationOption.automationSequence.sequenceEntries[sequenceEntryIndex];
								}
								continue;
							}
							else if (automationOption.invalidOptionContinueSequenceBehaviour == AutomationSequence.InvalidOptionContinueSequenceBehaviour.Restart)
							{
								End ();
								Start ();
								return;
							}
							else// if (automationOption.invalidOptionContinueSequenceBehaviour == AutomationSequence.InvalidOptionContinueSequenceBehaviour.Pause)
							{
								automationOption.pausePlayback = true;
								return;
							}
						}
					}
					else// if (!sequenceEntry.parentOption.Equals(null))
					{
						Option parent = sequenceEntry.parentOption.GetOption();
						if (parent != null)
						{
							Option child = sequenceEntry.childOption.GetOption();
							if (child != null)
							{
								if (parent.children.Contains(child))
									parent.RemoveChild (child);
								else
									parent.AddChild (child);
							}
							else
							{
								if (automationOption.invalidOptionFixBehaviour == AutomationSequence.InvalidOptionFixBehaviour.DeleteSequenceEntry)
									automationOption.DeleteSequenceEntryOption (sequenceEntryIndex);
								if (automationOption.invalidOptionContinueSequenceBehaviour == AutomationSequence.InvalidOptionContinueSequenceBehaviour.Stop)
								{
									End ();
									return;
								}
								else if (automationOption.invalidOptionContinueSequenceBehaviour == AutomationSequence.InvalidOptionContinueSequenceBehaviour.Continue)
								{
									if (automationOption.invalidOptionFixBehaviour != AutomationSequence.InvalidOptionFixBehaviour.DeleteSequenceEntry)
									{
										sequenceEntryIndex ++;
										if (sequenceEntryIndex >= automationOption.automationSequence.sequenceEntries.Count)
											break;
										sequenceEntry = automationOption.automationSequence.sequenceEntries[sequenceEntryIndex];
									}
									continue;
								}
								else if (automationOption.invalidOptionContinueSequenceBehaviour == AutomationSequence.InvalidOptionContinueSequenceBehaviour.Restart)
								{
									End ();
									Start ();
									return;
								}
								else// if (invalidOptionContinueSequenceBehaviour == AutomationSequence.InvalidOptionContinueSequenceBehaviour.Pause)
								{
									automationOption.pausePlayback = true;
									return;
								}
							}
						}
						else
						{
							if (automationOption.invalidOptionFixBehaviour == AutomationSequence.InvalidOptionFixBehaviour.DeleteSequenceEntry)
								automationOption.DeleteSequenceEntryOption (sequenceEntryIndex);
							if (automationOption.invalidOptionContinueSequenceBehaviour == AutomationSequence.InvalidOptionContinueSequenceBehaviour.Stop)
							{
								End ();
								return;
							}
							else if (automationOption.invalidOptionContinueSequenceBehaviour == AutomationSequence.InvalidOptionContinueSequenceBehaviour.Continue)
							{
								if (automationOption.invalidOptionFixBehaviour != AutomationSequence.InvalidOptionFixBehaviour.DeleteSequenceEntry)
								{
									sequenceEntryIndex ++;
									if (sequenceEntryIndex >= automationOption.automationSequence.sequenceEntries.Count)
										break;
									sequenceEntry = automationOption.automationSequence.sequenceEntries[sequenceEntryIndex];
								}
								continue;
							}
							else if (automationOption.invalidOptionContinueSequenceBehaviour == AutomationSequence.InvalidOptionContinueSequenceBehaviour.Restart)
							{
								End ();
								Start ();
								return;
							}
							else// if (automationOption.invalidOptionContinueSequenceBehaviour == AutomationSequence.InvalidOptionContinueSequenceBehaviour.Pause)
							{
								automationOption.pausePlayback = true;
								return;
							}
						}
					}
					sequenceEntryIndex ++;
					if (sequenceEntryIndex >= automationOption.automationSequence.sequenceEntries.Count)
						break;
					sequenceEntry = automationOption.automationSequence.sequenceEntries[sequenceEntryIndex];
				}
				playbackTime += Time.deltaTime;
			}

			public void End ()
			{
				GameManager.updatables = GameManager.updatables.Remove(this);
			}
		}

		public enum DuplicateSequenceEntryIndexBehaviour
		{
			Disallow,
			PreferSwitch,
			PreferInsert
		}

		[Serializable]
		public class Data : ToggleChildrenOption.Data
		{
			public AutomationSequence automationSequence;
			public string recordActivateOptionsParentName = null;
			public string recordParentingOptionsParentName = null;
			public string automationSequenceEntriesParentName = null;
			
			public override object MakeAsset ()
			{
				AutomationOption automationOption = ObjectPool.instance.SpawnComponent<AutomationOption>(EternityEngine.instance.automationOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (automationOption);
				automationOption.Init ();
				return automationOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				AutomationOption automationOption = (AutomationOption) asset;
				automationOption._Data = this;
				automationOption.SetAutomationSequenceFromData ();
				automationOption.SetRecordActivateOptionsParentNameFromData ();
				automationOption.SetRecordParentingOptionsParentNameFromData ();
				automationOption.SetAutomationSequenceEntriesParentNameFromData ();
			}
		}
	}
}