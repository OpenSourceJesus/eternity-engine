using System;

namespace _EternityEngine
{
	public class GetVector3ZOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public override string NameToValueSeparator
		{
			get
			{
				return ": ";
			}
		}
		public Vector3Option vector3Option;

		public override void OnActivate (EternityEngine.Hand hand)
		{
			if (Exists(vector3Option))
			{
				vector3Option.UpdateValue ();
				SetValue ("" + vector3Option.value.z);
			}
			// base.OnActivate (hand);
		}

		public override bool SetValue (string value)
		{
			if (base.SetValue(value))
			{
				if (Exists(vector3Option))
				{
					float? z = TypingTargetOption.GetNumberValue(GetValue());
					if (z != null)
						vector3Option.SetZ ((float) z);
				}
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetVector3OptionNameOfData ();
		}

		void SetVector3OptionNameOfData ()
		{
			if (Exists(vector3Option))
				_Data.vector3OptionName = vector3Option.name;
		}

		void SetVector3OptionNameFromData ()
		{
			if (_Data.vector3OptionName != null)
				vector3Option = EternityEngine.GetOption<Vector3Option>(_Data.vector3OptionName);
		}

		[Serializable]
		public class Data : Option.Data
		{
			public string vector3OptionName = null;

			public override object MakeAsset ()
			{
				GetVector3ZOption getVector3ZOption = ObjectPool.instance.SpawnComponent<GetVector3ZOption>(EternityEngine.instance.getVector3ZOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (getVector3ZOption);
				getVector3ZOption.Init ();
				return getVector3ZOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				GetVector3ZOption getVector3ZOption = (GetVector3ZOption) asset;
				getVector3ZOption._Data = this;
				getVector3ZOption.SetVector3OptionNameFromData ();
			}
		}
	}
}