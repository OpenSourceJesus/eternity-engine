using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class ContactPointOption : ToggleChildrenOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public Vector3Option pointVector3Option;
		public Vector3Option normalVector3Option;

		public virtual void Init (ContactPoint contactPoint)
		{
			pointVector3Option.SetValue (contactPoint.point);
			pointVector3Option.UpdateTexts ();
			normalVector3Option.SetValue (contactPoint.normal);
			normalVector3Option.UpdateTexts ();
		}

		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetPointVector3OptionNameOfData ();
			SetNormalVector3OptionNameOfData ();
		}

		void SetPointVector3OptionNameOfData ()
		{
			if (Exists(pointVector3Option))
				_Data.pointVector3OptionName = pointVector3Option.name;
		}

		void SetPointVector3OptionNameFromData ()
		{
			if (_Data.pointVector3OptionName != null)
				pointVector3Option = EternityEngine.GetOption<Vector3Option>(_Data.pointVector3OptionName);
		}

		void SetNormalVector3OptionNameOfData ()
		{
			if (Exists(normalVector3Option))
				_Data.normalVector3OptionName = normalVector3Option.name;
		}

		void SetNormalVector3OptionNameFromData ()
		{
			if (_Data.normalVector3OptionName != null)
				normalVector3Option = EternityEngine.GetOption<Vector3Option>(_Data.normalVector3OptionName);
		}

		[Serializable]
		public class Data : ToggleChildrenOption.Data
		{
			public string pointVector3OptionName = null;
			public string normalVector3OptionName = null;

			public override object MakeAsset ()
			{
				ContactPointOption contactPointOption = ObjectPool.instance.SpawnComponent<ContactPointOption>(EternityEngine.instance.contactPointOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (contactPointOption);
				contactPointOption.Init ();
				return contactPointOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				ContactPointOption contactPointOption = (ContactPointOption) asset;
				contactPointOption._Data = this;
				contactPointOption.SetPointVector3OptionNameFromData ();
				contactPointOption.SetNormalVector3OptionNameFromData ();
			}
		}
	}
}