using System;

namespace _EternityEngine
{
	public class LightRenderModeOption : EnumOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public LightOption lightOption;

		public override bool SetValue (int value)
		{
			if (base.SetValue(value))
			{
				if (Exists(lightOption))
					lightOption.SetRenderMode ((LightOption.RenderMode) Enum.ToObject(enumType, value));
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetLightOptionNameOfData ();
		}

		void SetLightOptionNameOfData ()
		{
			if (Exists(lightOption))
				_Data.lightOptionName = lightOption.name;
		}

		void SetLightOptionNameFromData ()
		{
			if (_Data.lightOptionName != null)
				lightOption = EternityEngine.GetOption<LightOption>(_Data.lightOptionName);
		}

		[Serializable]
		public class Data : EnumOption.Data
		{
			public string lightOptionName = null;

			public override object MakeAsset ()
			{
				LightRenderModeOption lightRenderModeOption = ObjectPool.instance.SpawnComponent<LightRenderModeOption>(EternityEngine.instance.lightRenderModeOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (lightRenderModeOption);
				lightRenderModeOption.Init ();
				return lightRenderModeOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				LightRenderModeOption lightRenderModeOption = (LightRenderModeOption) asset;
				lightRenderModeOption._Data = this;
				lightRenderModeOption.SetLightOptionNameFromData ();
			}
		}
	}
}