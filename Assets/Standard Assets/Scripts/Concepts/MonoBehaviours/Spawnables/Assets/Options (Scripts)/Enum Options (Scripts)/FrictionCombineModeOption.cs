
using System;

namespace _EternityEngine
{
	public class FrictionCombineModeOption : EnumOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public ModelFileInstanceOption modelFileInstanceOption;

		public override bool SetValue (string value)
		{
			if (base.SetValue(value))
			{
				if (Exists(modelFileInstanceOption))
					modelFileInstanceOption.SetFrictionCombineMode ((_PhysicsMaterial.ValueCombineMode) Enum.ToObject(enumType, value));
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetModelFileInstanceOptionNameOfData ();
		}

		void SetModelFileInstanceOptionNameOfData ()
		{
			if (Exists(modelFileInstanceOption))
				_Data.modelFileInstanceOptionName = modelFileInstanceOption.name;
		}

		void SetModelFileInstanceOptionNameFromData ()
		{
			if (_Data.modelFileInstanceOptionName != null)
				modelFileInstanceOption = EternityEngine.GetOption<ModelFileInstanceOption>(_Data.modelFileInstanceOptionName);
		}

		[Serializable]
		public class Data : EnumOption.Data
		{
			public string modelFileInstanceOptionName = null;

			public override object MakeAsset ()
			{
				FrictionCombineModeOption frictionCombineModeOption = ObjectPool.instance.SpawnComponent<FrictionCombineModeOption>(EternityEngine.instance.frictionCombineModeOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (frictionCombineModeOption);
				frictionCombineModeOption.Init ();
				return frictionCombineModeOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				FrictionCombineModeOption frictionCombineModeOption = (FrictionCombineModeOption) asset;
				frictionCombineModeOption._Data = this;
				frictionCombineModeOption.SetModelFileInstanceOptionNameFromData ();
			}
		}
	}
}
