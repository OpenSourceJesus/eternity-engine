using System;
using TriLibCore;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class ImportModelWithFileNameOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public string modelFilePath;

		public override void OnActivate (EternityEngine.Hand hand)
		{
			Do (modelFilePath);
			// base.OnActivate (hand);
		}

		public static void Do (string modelFilePath)
		{
			LoadModelFileAsyncFromFile (modelFilePath, new GameObject(), OnModelLoaded, OnModelMaterialsLoaded, OnModelLoadProgressed, OnModelLoadError, AssetLoader.CreateDefaultLoaderOptions());
		}

		static void LoadModelFileAsyncFromFile (string fileName, GameObject loadIntoGo, Action<AssetLoaderContext> onLoad = null, Action<AssetLoaderContext> onMaterialsLoad = null, Action<AssetLoaderContext, float> onProgress = null, Action<IContextualizedError> onError = null, AssetLoaderOptions assetLoaderOptions = null)
		{
			AssetLoader.LoadModelFromFile(fileName, onLoad, onMaterialsLoad, onProgress, onError, loadIntoGo, assetLoaderOptions);
		}

		static void OnModelLoadProgressed (AssetLoaderContext assetLoaderContext, float progress)
		{
		}

		static void OnModelMaterialsLoaded (AssetLoaderContext assetLoaderContext)
		{
		}

		static void OnModelLoaded (AssetLoaderContext assetLoaderContext)
		{
			GameObject modelRootGo = assetLoaderContext.RootGameObject;
			modelRootGo.SetActive(false);
			EternityEngine.instance.SpawnOption<ModelFileOption> (EternityEngine.instance.modelFileOptionWithChildrenPrefab, (ModelFileOption modelFileOption) => { modelFileOption.Init (assetLoaderContext.Filename, modelRootGo); });
		}

		static void OnModelLoadError (IContextualizedError error)
		{
			print(error.GetInnerException());
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetModelFilePathOfData ();
		}

		void SetModelFilePathOfData ()
		{
			_Data.modelFilePath = modelFilePath;
		}

		void SetModelFilePathFromData ()
		{
			modelFilePath = _Data.modelFilePath;
		}

		[Serializable]
		public class Data : Option.Data
		{
			public string modelFilePath;

			public override object MakeAsset ()
			{
				ImportModelWithFileNameOption importModelWithFileNameOption = ObjectPool.instance.SpawnComponent<ImportModelWithFileNameOption>(EternityEngine.instance.importModelWithFileNameOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (importModelWithFileNameOption);
				importModelWithFileNameOption.Init ();
				return importModelWithFileNameOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				ImportModelWithFileNameOption importModelWithFileNameOption = (ImportModelWithFileNameOption) asset;
				importModelWithFileNameOption._Data = this;
				importModelWithFileNameOption.SetModelFilePathFromData ();
			}
		}
	}
}