using System;
using Extensions;
using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class ArbitraryFloatOption : TypingTargetOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}

		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
		}

		[Serializable]
		public class Data : TypingTargetOption.Data
		{
			public override object MakeAsset ()
			{
				ArbitraryFloatOption arbitraryFloatOption = ObjectPool.instance.SpawnComponent<ArbitraryFloatOption>(EternityEngine.instance.arbitraryFloatOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (arbitraryFloatOption);
				arbitraryFloatOption.Init ();
				return arbitraryFloatOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				ArbitraryFloatOption arbitraryFloatOption = (ArbitraryFloatOption) asset;
				arbitraryFloatOption._Data = this;
			}
		}
	}
}