using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class MinBounceSpeedOption : TypingTargetOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}

		public override bool SetValue (string value)
		{
			if (base.SetValue(value))
			{
				float? minBounceSpeed = GetNumberValue();
				if (minBounceSpeed != null)
					Physics.bounceThreshold = (float) minBounceSpeed;
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
		}

		[Serializable]
		public class Data : TypingTargetOption.Data
		{
			public override object MakeAsset ()
			{
				MinBounceSpeedOption minBounceSpeedOption = ObjectPool.instance.SpawnComponent<MinBounceSpeedOption>(EternityEngine.instance.minBounceSpeedOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (minBounceSpeedOption);
				minBounceSpeedOption.Init ();
				return minBounceSpeedOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				MinBounceSpeedOption minBounceSpeedOption = (MinBounceSpeedOption) asset;
				minBounceSpeedOption._Data = this;
			}
		}
	}
}