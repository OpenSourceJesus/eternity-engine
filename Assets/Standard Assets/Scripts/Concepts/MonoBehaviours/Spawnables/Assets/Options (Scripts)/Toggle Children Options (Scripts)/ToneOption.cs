using System;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class ToneOption : ToggleChildrenOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public Tone tonePrefab;
		public Option frequencyOption;

		public void StartEmit (EternityEngine.Hand hand)
		{
			Tone tone = ObjectPool.instance.SpawnComponent<Tone>(tonePrefab.prefabIndex, EternityEngine.instance.currentSpawnOrientationOption.trs.position, EternityEngine.instance.currentSpawnOrientationOption.trs.rotation, EternityEngine.instance.sceneTrs);
			tone.frequency = (float) TypingTargetOption.GetNumberValue(frequencyOption.GetValue());
			tone.audioSource.Play();
		}

		public void EndEmit (EternityEngine.Hand hand)
		{
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
		}

		[Serializable]
		public class Data : ToggleChildrenOption.Data
		{
			public override object MakeAsset ()
			{
				ToneOption toneOption = ObjectPool.instance.SpawnComponent<ToneOption>(EternityEngine.instance.toneOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (toneOption);
				toneOption.Init ();
				return toneOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				ToneOption toneOption = (ToneOption) asset;
				toneOption._Data = this;
			}
		}
	}
}