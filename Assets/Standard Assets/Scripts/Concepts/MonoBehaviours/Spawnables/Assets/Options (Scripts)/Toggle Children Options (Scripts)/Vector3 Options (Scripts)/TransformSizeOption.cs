using System;
using Extensions;
using UnityEngine;

namespace _EternityEngine
{
	public class TransformSizeOption : Vector3Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public Transform referenceTrs;

		public void Init (Transform referenceTrs)
		{
			this.referenceTrs = referenceTrs;
			referenceTrs.SetWorldScale (value);
		}

		public override void UpdateValue ()
		{
			value = referenceTrs.lossyScale;
		}

		public override bool SetX (float x)
		{
			if (base.SetX(x))
			{
				referenceTrs.localScale = referenceTrs.localScale.SetX(1);
				referenceTrs.localScale = referenceTrs.localScale.SetX(x / referenceTrs.lossyScale.x);
				return true;
			}
			else
				return false;
		}

		public override bool SetY (float y)
		{
			if (base.SetY(y))
			{
				referenceTrs.localScale = referenceTrs.localScale.SetY(1);
				referenceTrs.localScale = referenceTrs.localScale.SetY(y / referenceTrs.lossyScale.y);
				return true;
			}
			else
				return false;
		}

		public override bool SetZ (float z)
		{
			if (base.SetZ(z))
			{
				referenceTrs.localScale = referenceTrs.localScale.SetZ(1);
				referenceTrs.localScale = referenceTrs.localScale.SetZ(z / referenceTrs.lossyScale.z);
				return true;
			}
			else
				return false;
		}

		public override bool SetValue (Vector3 value)
		{
			if (base.SetValue(value))
			{
				referenceTrs.SetWorldScale (value);
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
		}

		[Serializable]
		public class Data : Vector3Option.Data
		{
			public override object MakeAsset ()
			{
				TransformSizeOption transformSizeOption = ObjectPool.instance.SpawnComponent<TransformSizeOption>(EternityEngine.instance.transformSizeOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (transformSizeOption);
				transformSizeOption.Init ();
				return transformSizeOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				TransformSizeOption transformSizeOption = (TransformSizeOption) asset;
				transformSizeOption._Data = this;
			}
		}
	}
}