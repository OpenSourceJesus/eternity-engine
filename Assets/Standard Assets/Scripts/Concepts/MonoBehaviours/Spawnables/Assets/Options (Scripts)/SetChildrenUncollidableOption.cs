using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class SetChildrenUncollidableOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}

		public override void OnActivate (EternityEngine.Hand hand)
		{
			SetChildrenUncollidable ();
			// base.OnActivate (hand);
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
		}

		[Serializable]
		public class Data : Option.Data
		{
			public override object MakeAsset ()
			{
				SetChildrenUncollidableOption setChildrenCollidableOption = ObjectPool.instance.SpawnComponent<SetChildrenUncollidableOption>(EternityEngine.instance.setChildrenUncollidableOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (setChildrenCollidableOption);
				// if (!setChildrenCollidableOption.isInitialized)
				// 	setChildrenCollidableOption.Init ();
				return setChildrenCollidableOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				SetChildrenUncollidableOption setChildrenCollidableOption = (SetChildrenUncollidableOption) asset;
				setChildrenCollidableOption._Data = this;
			}
		}
	}
}