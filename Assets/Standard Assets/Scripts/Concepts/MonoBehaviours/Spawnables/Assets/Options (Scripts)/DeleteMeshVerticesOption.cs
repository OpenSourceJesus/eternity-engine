using System;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class DeleteMeshVerticesOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public Option modelFileInstanceOptionsParent;
		public List<ModelFileInstanceOption> modelFileInstanceOptions = new List<ModelFileInstanceOption>();
		public TransformOption transformOption;

		public override void Init ()
		{
			base.Init ();
			transformOption.Init (EternityEngine.instance.actOnMeshVerticesFromTrs);
			modelFileInstanceOptionsParent.onAddChild += OnAddChild;
			modelFileInstanceOptionsParent.onAddChild += OnRemoveChild;
		}

		public override void OnDeleted ()
		{
			modelFileInstanceOptionsParent.onAddChild -= OnAddChild;
			modelFileInstanceOptionsParent.onRemoveChild -= OnRemoveChild;
			base.OnDeleted ();
		}

		void OnAddChild (Option child)
		{
			ModelFileInstanceOption modelFileInstanceOption = child as ModelFileInstanceOption;
			if (modelFileInstanceOption != null)
				modelFileInstanceOptions.Add(modelFileInstanceOption);
		}
		
		void OnRemoveChild (Option child)
		{
			ModelFileInstanceOption modelFileInstanceOption = child as ModelFileInstanceOption;
			if (modelFileInstanceOption != null)
				modelFileInstanceOptions.Remove(modelFileInstanceOption);
		}

		public override void OnActivate (EternityEngine.Hand hand)
		{
			for (int i = 0; i < modelFileInstanceOptions.Count; i ++)
			{
				ModelFileInstanceOption modelFileInstanceOption = modelFileInstanceOptions[i];
				PhysicsObject3D physicsObject = modelFileInstanceOption.physicsObject;
				Mesh mesh = physicsObject.meshFilter.mesh;
				List<Vector3> vertices = new List<Vector3>();
				mesh.GetVertices(vertices);
				List<int> triangles = new List<int>(mesh.GetTriangles(0));
				Transform physicsObjectTrs = physicsObject.trs;
				for (int i2 = 0; i2 < mesh.vertexCount; i2 ++)
				{
					Vector3 vertex = vertices[i2];
					Transform fromTrs = transformOption.referenceTrs;
					float fromTrsSize = fromTrs.lossyScale.x;
					vertex = physicsObjectTrs.TransformPoint(vertex);
					if ((vertex - fromTrs.position).sqrMagnitude < fromTrsSize * fromTrsSize)
					{
						vertices.RemoveAt(i2);
						for (int i3 = 0; i3 < triangles.Count; i3 += 3)
						{
							int vertexIndex1 = triangles[i3];
							int vertexIndex2 = triangles[i3 + 1];
							int vertexIndex3 = triangles[i3 + 2];
							if (vertexIndex1 == i2 || vertexIndex2 == i2 || vertexIndex3 == i2)
							{
								triangles.RemoveRange(i, 3);
								i3 -= 3;
							}
						}
					}
				}
				mesh.SetVertices(vertices);
				mesh.SetTriangles(triangles, 0);
			}
			// base.OnActivate (hand);
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetModelFileInstanceOptionsParentNameOfData ();
			SetModelFileInstanceOptionsNamesOfData ();
			SetTransformOptionNameOfData ();
		}
		
		void SetModelFileInstanceOptionsParentNameOfData ()
		{
			if (Exists(modelFileInstanceOptionsParent))
				_Data.modelFileInstanceOptionsParentName = modelFileInstanceOptionsParent.name;
		}

		void SetModelFileInstanceOptionsParentNameFromData ()
		{
			if (_Data.modelFileInstanceOptionsParentName != null)
				modelFileInstanceOptionsParent = EternityEngine.GetOption(_Data.modelFileInstanceOptionsParentName);
		}

		void SetModelFileInstanceOptionsNamesOfData ()
		{
			_Data.modelFileInstanceOptionsNames = new string[modelFileInstanceOptions.Count];
			for (int i = 0; i < modelFileInstanceOptions.Count; i ++)
			{
				ModelFileInstanceOption modelFileInstanceOption = modelFileInstanceOptions[i];
				if (Exists(modelFileInstanceOption))
					_Data.modelFileInstanceOptionsNames[i] = modelFileInstanceOption.name;
			}
		}

		void SetModelFileInstanceOptionsNamesFromData ()
		{
			for (int i = 0; i < _Data.modelFileInstanceOptionsNames.Length; i ++)
			{
				string parentName = _Data.modelFileInstanceOptionsNames[i];
				modelFileInstanceOptions.Add(EternityEngine.GetOption<ModelFileInstanceOption>(parentName));
			}
		}
		
		void SetTransformOptionNameOfData ()
		{
			if (Exists(transformOption))
				_Data.transformOptionName = transformOption.name;
		}

		void SetTransformOptionNameFromData ()
		{
			if (_Data.transformOptionName != null)
				transformOption = EternityEngine.GetOption<TransformOption>(_Data.transformOptionName);
		}

		[Serializable]
		public class Data : Option.Data
		{
			public string[] modelFileInstanceOptionsNames = new string[0];
			public string modelFileInstanceOptionsParentName = null;
			public string transformOptionName = null;

			public override object MakeAsset ()
			{
				DeleteMeshVerticesOption deleteMeshVerticesOption = ObjectPool.instance.SpawnComponent<DeleteMeshVerticesOption>(EternityEngine.instance.deleteMeshVerticesOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (deleteMeshVerticesOption);
				deleteMeshVerticesOption.Init ();
				return deleteMeshVerticesOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				DeleteMeshVerticesOption deleteMeshVerticesOption = (DeleteMeshVerticesOption) asset;
				deleteMeshVerticesOption._Data = this;
				deleteMeshVerticesOption.SetModelFileInstanceOptionsParentNameFromData ();
				deleteMeshVerticesOption.SetModelFileInstanceOptionsNamesFromData ();
				deleteMeshVerticesOption.SetTransformOptionNameFromData ();
			}
		}
	}
}