using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class FileOption : SystemComponentOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public DeleteFileInstancesOption deleteFileInstancesOption;
		public List<FileInstanceOption> fileInstanceOptions = new List<FileInstanceOption>();
		public FileInstanceOption fileInstanceOptionPrefab;

		public void DeleteFileInstances ()
		{
			for (int i = 0; i < fileInstanceOptions.Count; i ++)
			{
				FileInstanceOption fileInstanceOption = fileInstanceOptions[i];
				fileInstanceOption.Delete ();
			}
		}

		// public override void Delete ()
		// {
		// 	EternityEngine.instance.DeleteFile (path);
		// 	base.Delete ();
		// }

		public override void Rename (string path)
		{
			File.Move(this.path, path);
			base.Rename (path);
			for (int i = 0; i < fileInstanceOptions.Count; i ++)
			{
				FileInstanceOption fileInstance = fileInstanceOptions[i];
				fileInstance.UpdateTexts (path);
			}
		}

		public void MakeFileInstance ()
		{
			FileInstanceOption fileInstanceOption = EternityEngine.instance.SpawnOption<FileInstanceOption>(fileInstanceOptionPrefab, (FileInstanceOption fileInstanceOption) => { fileInstanceOption.Init (this); });
			if (Exists(deleteFileInstancesOption))
				deleteFileInstancesOption.SetActivatable (true);
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetDeleteFileInstancesOptionNameOfData ();
		}

		void SetDeleteFileInstancesOptionNameOfData ()
		{
			if (Exists(deleteFileInstancesOption))
				_Data.deleteFileInstancesOptionName = deleteFileInstancesOption.name;
		}

		void SetDeleteFileInstancesOptionNameFromData ()
		{
			if (_Data.deleteFileInstancesOptionName != null)
				deleteFileInstancesOption = EternityEngine.GetOption<DeleteFileInstancesOption>(_Data.deleteFileInstancesOptionName);
		}

		[Serializable]
		public class Data : SystemComponentOption.Data
		{
			public string deleteFileInstancesOptionName = null;
			
			public override object MakeAsset ()
			{
				FileOption fileOption = ObjectPool.instance.SpawnComponent<FileOption>(EternityEngine.instance.fileOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (fileOption);
				fileOption.Init ();
				return fileOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				FileOption fileOption = (FileOption) asset;
				fileOption._Data = this;
				fileOption.SetDeleteFileInstancesOptionNameFromData ();
			}
		}
	}
}