using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class ToggleChildrenCollidableOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}

		public override void OnActivate (EternityEngine.Hand hand)
		{
			ToggleChildrenCollidable ();
			// base.OnActivate (hand);
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
		}

		[Serializable]
		public class Data : Option.Data
		{
			public override object MakeAsset ()
			{
				ToggleChildrenCollidableOption toggleChildrenCollidableOption = ObjectPool.instance.SpawnComponent<ToggleChildrenCollidableOption>(EternityEngine.instance.toggleChildrenCollidableOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (toggleChildrenCollidableOption);
				// if (!toggleChildrenCollidableOption.isInitialized)
				// 	toggleChildrenCollidableOption.Init ();
				return toggleChildrenCollidableOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				ToggleChildrenCollidableOption toggleChildrenCollidableOption = (ToggleChildrenCollidableOption) asset;
				toggleChildrenCollidableOption._Data = this;
			}
		}
	}
}