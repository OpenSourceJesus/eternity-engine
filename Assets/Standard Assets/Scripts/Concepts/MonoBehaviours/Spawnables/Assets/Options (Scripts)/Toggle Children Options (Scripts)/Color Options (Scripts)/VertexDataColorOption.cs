
using System;
using UnityEngine;

namespace _EternityEngine
{
	public class VertexDataColorOption : ColorOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
public VertexDataOption vertexDataOption;
		MeshFilter referenceMeshFilter;
		uint vertexIndex;

		public override void Init ()
		{
			base.Init ();
			PhysicsObject3D physicsObject = vertexDataOption.modelFileInstanceOption.physicsObject;
			referenceMeshFilter = physicsObject.meshFilter;
			vertexIndex = vertexDataOption.vertexIndex;
		}

		public override bool SetR (float r)
		{
			if (base.SetR(r))
			{
				Apply ();
				return true;
			}
			else
				return false;
		}

		public override bool SetG (float g)
		{
			if (base.SetG(g))
			{
				Apply ();
				return true;
			}
			else
				return false;
		}

		public override bool SetB (float b)
		{
			if (base.SetB(b))
			{
				Apply ();
				return true;
			}
			else
				return false;
		}

		public override bool SetA (float a)
		{
			if (base.SetA(a))
			{
				Apply ();
				return true;
			}
			else
				return false;
		}

		void Apply ()
		{
			Mesh mesh = referenceMeshFilter.mesh;
			Color[] colors = mesh.colors;
			colors[vertexIndex] = value;
			mesh.SetColors(colors);
		}

		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
SetVertexDataOptionNameOfData ();
		}

		
		public void SetVertexDataOptionNameOfData ()
		{
			if (Exists(vertexDataOption))
				_Data.vertexDataOptionName = vertexDataOption.name;
		}

		public void SetVertexDataOptionNameFromData ()
		{
			if (_Data.vertexDataOptionName != null)
				vertexDataOption = EternityEngine.GetOption<VertexDataOption>(_Data.vertexDataOptionName);
		}

		[Serializable]
		public class Data : ColorOption.Data
		{
public string vertexDataOptionName = null;

			public override object MakeAsset ()
			{
				VertexDataColorOption vertexDataColorOption = ObjectPool.instance.SpawnComponent<VertexDataColorOption>(EternityEngine.instance.vertexDataColorOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (vertexDataColorOption);
				vertexDataColorOption.Init ();
				return vertexDataColorOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				VertexDataColorOption vertexDataColorOption = (VertexDataColorOption) asset;
				vertexDataColorOption._Data = this;
 vertexDataColorOption.SetVertexDataOptionNameFromData ();
			}
		}
	}
}