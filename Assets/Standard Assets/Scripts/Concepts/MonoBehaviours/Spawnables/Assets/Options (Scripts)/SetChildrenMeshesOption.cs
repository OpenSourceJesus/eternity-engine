using System;
using Extensions;
using UnityEngine;

namespace _EternityEngine
{
	public class SetChildrenMeshesOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public ModelFileInstanceOption modelFileInstanceOption;
		SetPhysicsObjectMeshFilter setPhysicsObjectMeshFilter;
		MeshFilter physicsObjectMeshFilter;

		public override void Init ()
		{
			base.Init ();
			if (Exists(modelFileInstanceOption))
			{
				if (modelFileInstanceOption.physicsObject == null)
				{
					setPhysicsObjectMeshFilter = new SetPhysicsObjectMeshFilter();
					setPhysicsObjectMeshFilter.setChildrenMeshesOption = this;
					GameManager.updatables = GameManager.updatables.Add(setPhysicsObjectMeshFilter);
				}
				else
					physicsObjectMeshFilter = modelFileInstanceOption.physicsObject.meshFilter;
			}
		}

		public override void OnActivate (EternityEngine.Hand hand)
		{
			if (physicsObjectMeshFilter != null)
			{
				for (int i = 0; i < children.Count; i ++)
				{
					Option child = children[i];
					child.meshFilter.sharedMesh = physicsObjectMeshFilter.sharedMesh;
				}
			}
			// base.OnActivate (hand);
		}

		public override void OnDeleted ()
		{
			base.OnDeleted ();
			if (setPhysicsObjectMeshFilter != null)
				GameManager.updatables = GameManager.updatables.Remove(setPhysicsObjectMeshFilter);
		}

		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetModelFileInstanceOptionNameOfData ();
		}

		void SetModelFileInstanceOptionNameOfData ()
		{
			if (Exists(modelFileInstanceOption))
				_Data.modelFileInstanceOptionName = modelFileInstanceOption.name;
		}

		void SetModelFileInstanceOptionNameFromData ()
		{
			if (_Data.modelFileInstanceOptionName != null)
				modelFileInstanceOption = EternityEngine.GetOption<ModelFileInstanceOption>(_Data.modelFileInstanceOptionName);
		}

		public class SetPhysicsObjectMeshFilter : IUpdatable
		{
			public SetChildrenMeshesOption setChildrenMeshesOption;
			
			public void DoUpdate ()
			{
				ModelFileInstanceOption modelFileInstanceOption = setChildrenMeshesOption.modelFileInstanceOption;
				if (Exists(modelFileInstanceOption) && modelFileInstanceOption.physicsObject != null)
				{
					setChildrenMeshesOption.physicsObjectMeshFilter = modelFileInstanceOption.physicsObject.meshFilter;
					GameManager.updatables = GameManager.updatables.Remove(this);
				}
			}
		}

		[Serializable]
		public class Data : Option.Data
		{
			public string modelFileInstanceOptionName = null;

			public override object MakeAsset ()
			{
				SetChildrenMeshesOption setChildrenMeshesOption = ObjectPool.instance.SpawnComponent<SetChildrenMeshesOption>(EternityEngine.instance.setChildrenMeshesOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (setChildrenMeshesOption);
				setChildrenMeshesOption.Init ();
				return setChildrenMeshesOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				SetChildrenMeshesOption setChildrenMeshesOption = (SetChildrenMeshesOption) asset;
				setChildrenMeshesOption._Data = this;
				setChildrenMeshesOption.SetModelFileInstanceOptionNameFromData ();
			}
		}
	}
}