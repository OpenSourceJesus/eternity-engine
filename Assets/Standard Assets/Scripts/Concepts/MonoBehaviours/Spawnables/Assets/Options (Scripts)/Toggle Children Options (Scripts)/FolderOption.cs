using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class FolderOption : SystemComponentOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}

		public override void Delete ()
		{
			base.Delete ();
		}

		public override void Rename (string path)
		{
			base.Rename (path);
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
		}

		[Serializable]
		public class Data : SystemComponentOption.Data
		{
			public override object MakeAsset ()
			{
				FolderOption folderOption = ObjectPool.instance.SpawnComponent<FolderOption>(EternityEngine.instance.folderOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (folderOption);
				folderOption.Init ();
				return folderOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				FolderOption folderOption = (FolderOption) asset;
				folderOption._Data = this;
			}
		}
	}
}