using System;

namespace _EternityEngine
{
	public class ShaderNameOption : TypingTargetOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public SkyboxOption skyboxOption;

		public override bool SetValue (string value)
		{
			if (base.SetValue(value))
			{
				if (Exists(skyboxOption))
					skyboxOption.shaderName = value;
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetSkyboxOptionNameOfData ();
		}

		void SetSkyboxOptionNameOfData ()
		{
			if (Exists(skyboxOption))
				_Data.skyboxOptionName = skyboxOption.name;
		}

		void SetSkyboxOptionNameFromData ()
		{
			if (_Data.skyboxOptionName != null)
				skyboxOption = EternityEngine.GetOption<SkyboxOption>(_Data.skyboxOptionName);
		}

		[Serializable]
		public class Data : TypingTargetOption.Data
		{
			public string skyboxOptionName = null;

			public override object MakeAsset ()
			{
				ShaderNameOption shaderNameOption = ObjectPool.instance.SpawnComponent<ShaderNameOption>(EternityEngine.instance.shaderNameOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (shaderNameOption);
				shaderNameOption.Init ();
				return shaderNameOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				ShaderNameOption shaderNameOption = (ShaderNameOption) asset;
				shaderNameOption._Data = this;
				shaderNameOption.SetSkyboxOptionNameFromData ();
			}
		}
	}
}