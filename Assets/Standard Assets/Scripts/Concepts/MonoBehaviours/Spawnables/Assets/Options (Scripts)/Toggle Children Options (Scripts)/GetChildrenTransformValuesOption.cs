using TMPro;
using System;
using Extensions;
using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor.SceneManagement;
#endif

namespace _EternityEngine
{
	public class GetChildrenTransformValuesOption : ToggleChildrenOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public TransformOption transformOptionPrefab;
		public static Dictionary<Option, List<TransformOption>> childrenTransformOptionsDict = new Dictionary<Option, List<TransformOption>>();
		Dictionary<Option, TransformOption> optionsDict = new Dictionary<Option, TransformOption>();

		public override void Init ()
		{
			base.Init ();
			onAddChild += OnAddChild;
			onRemoveChild += OnRemoveChild;
		}

		public override void OnDeleted ()
		{
			onAddChild -= OnAddChild;
			onRemoveChild -= OnRemoveChild;
			base.OnDeleted ();
		}

		void OnAddChild (Option child)
		{
			EternityEngine.instance.SpawnOption (transformOptionPrefab, (TransformOption transformOption) => { OnSpawnTransformOption (transformOption, child); });
		}

		void OnSpawnTransformOption (TransformOption transformOption, Option child)
		{
			transformOption.Init (child.trs);
			transformOption.UpdateTexts ();
			optionsDict.Add(child, transformOption);
			List<TransformOption> transformOptions = new List<TransformOption>();
			if (childrenTransformOptionsDict.TryGetValue(child, out transformOptions))
			{
				transformOptions.Add(transformOption);
				childrenTransformOptionsDict[child].Add(transformOption);
			}
			else
				childrenTransformOptionsDict.Add(child, new List<TransformOption>() { transformOption });
			transformOption.SetName ("\"" + child.name + "\" Transform");
		}

		void OnRemoveChild (Option child)
		{
			List<TransformOption> transformOptions = childrenTransformOptionsDict[child];
			for (int i = 0; i < transformOptions.Count; i ++)
			{
				TransformOption transformOption = transformOptions[i];
				DestroyImmediate(transformOption.gameObject);
			}
			childrenTransformOptionsDict.Remove(child);
			optionsDict.Remove(child);
		}

		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetTransformOptionNamesOfData ();
		}

		void SetTransformOptionNamesOfData ()
		{
			List<TransformOption> transformOptions = new List<TransformOption>(optionsDict.Values);
			_Data.transformOptionNames = new string[transformOptions.Count];
			for (int i = 0; i < transformOptions.Count; i ++)
			{
				TransformOption transformOption = transformOptions[i];
				_Data.transformOptionNames[i] = transformOption.name;
			}
		}

		void SetTransformOptionNamesFromData ()
		{
			for (int i = 0; i < _Data.transformOptionNames.Length; i ++)
			{
				string transformOptionName = _Data.transformOptionNames[i];
				TransformOption transformOption = EternityEngine.GetOption<TransformOption>(transformOptionName);
				Option child = children[i];
				transformOption.Init (child.trs);
				optionsDict.Add(child, transformOption);
				List<TransformOption> transformOptions = new List<TransformOption>();
				if (childrenTransformOptionsDict.TryGetValue(child, out transformOptions))
				{
					transformOptions.Add(transformOption);
					childrenTransformOptionsDict[child].Add(transformOption);
				}
				else
					childrenTransformOptionsDict.Add(child, new List<TransformOption>() { transformOption });
			}
		}

		[Serializable]
		public class Data : ToggleChildrenOption.Data
		{
			public string[] transformOptionNames = new string[0];
			
			public override object MakeAsset ()
			{
				GetChildrenTransformValuesOption getChildrenTransformValuesOption = ObjectPool.instance.SpawnComponent<GetChildrenTransformValuesOption>(EternityEngine.instance.getChildrenTransformValuesOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (getChildrenTransformValuesOption);
				getChildrenTransformValuesOption.Init ();
				return getChildrenTransformValuesOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				GetChildrenTransformValuesOption getChildrenTransformValuesOption = (GetChildrenTransformValuesOption) asset;
				getChildrenTransformValuesOption._Data = this;
				getChildrenTransformValuesOption.SetTransformOptionNamesFromData ();
			}
		}
	}
}