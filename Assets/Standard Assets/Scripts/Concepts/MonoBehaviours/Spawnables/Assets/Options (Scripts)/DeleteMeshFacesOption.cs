using System;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class DeleteMeshFacesOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public Option modelFileInstanceOptionsParent;
		public List<ModelFileInstanceOption> modelFileInstanceOptions = new List<ModelFileInstanceOption>();
		public TransformOption transformOption;

		public override void Init ()
		{
			base.Init ();
			transformOption.Init (EternityEngine.instance.actOnMeshVerticesFromTrs);
			modelFileInstanceOptionsParent.onAddChild += OnAddChild;
			modelFileInstanceOptionsParent.onAddChild += OnRemoveChild;
		}

		public override void OnDeleted ()
		{
			modelFileInstanceOptionsParent.onAddChild -= OnAddChild;
			modelFileInstanceOptionsParent.onRemoveChild -= OnRemoveChild;
			base.OnDeleted ();
		}

		void OnAddChild (Option child)
		{
			ModelFileInstanceOption modelFileInstanceOption = child as ModelFileInstanceOption;
			if (modelFileInstanceOption != null)
				modelFileInstanceOptions.Add(modelFileInstanceOption);
		}
		
		void OnRemoveChild (Option child)
		{
			ModelFileInstanceOption modelFileInstanceOption = child as ModelFileInstanceOption;
			if (modelFileInstanceOption != null)
				modelFileInstanceOptions.Remove(modelFileInstanceOption);
		}

		public override void OnActivate (EternityEngine.Hand hand)
		{
			for (int i = 0; i < modelFileInstanceOptions.Count; i ++)
			{
				ModelFileInstanceOption modelFileInstanceOption = modelFileInstanceOptions[i];
				PhysicsObject3D physicsObject = modelFileInstanceOption.physicsObject;
				Mesh mesh = physicsObject.meshFilter.mesh;
				List<Vector3> vertices = new List<Vector3>();
				mesh.GetVertices(vertices);
				List<int> triangles = new List<int>(mesh.GetTriangles(0));
				List<int> removedTriangles = new List<int>();
				for (int i2 = 0; i2 < triangles.Count; i2 += 3)
				{
					int vertexIndex1 = triangles[i2];
					int vertexIndex2 = triangles[i2 + 1];
					int vertexIndex3 = triangles[i2 + 2];
					Transform physicsObjectTrs = physicsObject.trs;
					Vector3 vertex1 = physicsObjectTrs.TransformPoint(vertices[vertexIndex1]);
					Vector3 vertex2 = physicsObjectTrs.TransformPoint(vertices[vertexIndex2]);
					Vector3 vertex3 = physicsObjectTrs.TransformPoint(vertices[vertexIndex3]);
					Shape3D.Face face = new Shape3D.Face(new Vector3[] { vertex1, vertex2, vertex3 });
					Transform trs = transformOption.referenceTrs;
					float radius = trs.lossyScale.x * trs.lossyScale.x;
					if (face.GetDistanceSqr(trs.position) < radius * radius)
					{
						removedTriangles.AddRange(new int [] { vertexIndex1, vertexIndex2, vertexIndex3 });
						triangles.RemoveRange(i2, 3);
						i2 -= 3;
					}
				}
				for (int i2 = 0; i2 < removedTriangles.Count; i2 += 3)
				{
					int vertexIndex1 = removedTriangles[i2];
					int vertexIndex2 = removedTriangles[i2 + 1];
					int vertexIndex3 = removedTriangles[i2 + 2];
					bool shouldRemoveVertex1 = true;
					bool shouldRemoveVertex2 = true;
					bool shouldRemoveVertex3 = true;
					for (int i3 = i2 + 3; i3 < removedTriangles.Count; i3 += 3)
					{
						int _vertexIndex1 = removedTriangles[i3];
						int _vertexIndex2 = removedTriangles[i3 + 1];
						int _vertexIndex3 = removedTriangles[i3 + 2];
						if (vertexIndex1 == _vertexIndex1 || vertexIndex1 == _vertexIndex2 || vertexIndex1 == _vertexIndex3)
						{
							shouldRemoveVertex1 = false;
							if (!shouldRemoveVertex2 && !shouldRemoveVertex3)
								break;
						}
						if (vertexIndex2 == _vertexIndex1 || vertexIndex2 == _vertexIndex2 || vertexIndex2 == _vertexIndex3)
						{
							shouldRemoveVertex2 = false;
							if (!shouldRemoveVertex1 && !shouldRemoveVertex3)
								break;
						}
						if (vertexIndex3 == _vertexIndex1 || vertexIndex3 == _vertexIndex2 || vertexIndex3 == _vertexIndex3)
						{
							shouldRemoveVertex3 = false;
							if (!shouldRemoveVertex1 && !shouldRemoveVertex2)
								break;
						}
					}
				}
				mesh.SetVertices(vertices);
				mesh.SetTriangles(triangles, 0);
			}
			// base.OnActivate (hand);
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetModelFileInstanceOptionsParentNameOfData ();
			SetModelFileInstanceOptionsNamesOfData ();
			SetTransformOptionNameOfData ();
		}
		
		void SetModelFileInstanceOptionsParentNameOfData ()
		{
			if (Exists(modelFileInstanceOptionsParent))
				_Data.modelFileInstanceOptionsParentName = modelFileInstanceOptionsParent.name;
		}

		void SetModelFileInstanceOptionsParentNameFromData ()
		{
			if (_Data.modelFileInstanceOptionsParentName != null)
				modelFileInstanceOptionsParent = EternityEngine.GetOption(_Data.modelFileInstanceOptionsParentName);
		}

		void SetModelFileInstanceOptionsNamesOfData ()
		{
			_Data.modelFileInstanceOptionsNames = new string[modelFileInstanceOptions.Count];
			for (int i = 0; i < modelFileInstanceOptions.Count; i ++)
			{
				ModelFileInstanceOption modelFileInstanceOption = modelFileInstanceOptions[i];
				if (Exists(modelFileInstanceOption))
					_Data.modelFileInstanceOptionsNames[i] = modelFileInstanceOption.name;
			}
		}

		void SetModelFileInstanceOptionsNamesFromData ()
		{
			for (int i = 0; i < _Data.modelFileInstanceOptionsNames.Length; i ++)
			{
				string parentName = _Data.modelFileInstanceOptionsNames[i];
				modelFileInstanceOptions.Add(EternityEngine.GetOption<ModelFileInstanceOption>(parentName));
			}
		}
		
		void SetTransformOptionNameOfData ()
		{
			if (Exists(transformOption))
				_Data.transformOptionName = transformOption.name;
		}

		void SetTransformOptionNameFromData ()
		{
			if (_Data.transformOptionName != null)
				transformOption = EternityEngine.GetOption<TransformOption>(_Data.transformOptionName);
		}

		[Serializable]
		public class Data : Option.Data
		{
			public string[] modelFileInstanceOptionsNames = new string[0];
			public string modelFileInstanceOptionsParentName = null;
			public string transformOptionName = null;

			public override object MakeAsset ()
			{
				DeleteMeshFacesOption deleteMeshFacesOption = ObjectPool.instance.SpawnComponent<DeleteMeshFacesOption>(EternityEngine.instance.deleteMeshFacesOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (deleteMeshFacesOption);
				deleteMeshFacesOption.Init ();
				return deleteMeshFacesOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				DeleteMeshFacesOption deleteMeshFacesOption = (DeleteMeshFacesOption) asset;
				deleteMeshFacesOption._Data = this;
				deleteMeshFacesOption.SetModelFileInstanceOptionsParentNameFromData ();
				deleteMeshFacesOption.SetModelFileInstanceOptionsNamesFromData ();
				deleteMeshFacesOption.SetTransformOptionNameFromData ();
			}
		}
	}
}