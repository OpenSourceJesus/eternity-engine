using System;
using Extensions;
using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class ActivationBehaviourOption : ToggleChildrenOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public TriggerType triggerType;
		public Option activateChildrenOption;
		public Option triggerOptionsParent;
		public bool activateBeforeEvent;
		public bool activateAfterEvent;

		public override void Init ()
		{
			base.Init ();
			Array enumValues = Enum.GetValues(typeof(TriggerType));
			for (int i = 0; i < enumValues.Length; i ++)
			{
				TriggerType flagValue = (TriggerType) enumValues.GetValue(i);
				if (activateBeforeEvent)
					Register (false, flagValue);
				if (activateAfterEvent)
					Register (true, flagValue);
			}
		}

		public static void ActivateChildrenIfShould (TriggerType triggerType, bool isAfterEvent, Option optionContext = null)
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			List<ActivationBehaviourOption> activationBehaviourOptions = EternityEngine.instance.activationBehaviourOptionsDict[triggerType][isAfterEvent].activationBehaviourOptions;
			for (int i = 0; i < activationBehaviourOptions.Count; i ++)
			{
				ActivationBehaviourOption activationBehaviourOption = activationBehaviourOptions[i];
				if (optionContext == null || activationBehaviourOption.triggerOptionsParent.children.Contains(optionContext))
					activationBehaviourOption.ActivateChildren ();
			}
		}

		public void ActivateChildren ()
		{
			for (int i = 0; i < activateChildrenOption.children.Count; i ++)
			{
				Option child = activateChildrenOption.children[i];
				child.Activate (null);
			}
		}

		public void ToggleRegisterBeforeEvent (int flagIndex)
		{
			Array enumValues = Enum.GetValues(typeof(TriggerType));
			TriggerType flagValue = (TriggerType) enumValues.GetValue(flagIndex);
			if (activateBeforeEvent)
				Register (false, flagValue);
			else
				Unregister (false, flagValue);
		}

		public void ToggleRegisterAfterEvent (int flagIndex)
		{
			Array enumValues = Enum.GetValues(typeof(TriggerType));
			TriggerType flagValue = (TriggerType) enumValues.GetValue(flagIndex);
			if (activateAfterEvent)
				Register (true, flagValue);
			else
				Unregister (true, flagValue);
		}

		public void ToggleRegister (int flagIndex)
		{
			Array enumValues = Enum.GetValues(typeof(TriggerType));
			TriggerType flagValue = (TriggerType) enumValues.GetValue(flagIndex);
			if (triggerType.HasFlag(flagValue))
			{
				if (activateBeforeEvent)
					Register (false, flagValue);
				if (activateAfterEvent)
					Register (true, flagValue);
			}
			else
			{
				if (activateBeforeEvent)
					Unregister (false, flagValue);
				if (activateAfterEvent)
					Unregister (true, flagValue);
			}
		}

		void Register (bool activateAfterEvent, TriggerType triggerType)
		{
			EternityEngine.instance.activationBehaviourOptionsDict[triggerType][activateAfterEvent].activationBehaviourOptions.Add(this);
		}

		void Unregister (bool activateAfterEvent, TriggerType triggerType)
		{
			EternityEngine.instance.activationBehaviourOptionsDict[triggerType][activateAfterEvent].activationBehaviourOptions.Remove(this);
		}

		public override void OnDeleted ()
		{
			base.OnDeleted ();
			Array enumValues = Enum.GetValues(typeof(TriggerType));
			for (int i = 0; i < enumValues.Length; i ++)
			{
				TriggerType flagValue = (TriggerType) enumValues.GetValue(i);
				if (triggerType.HasFlag(flagValue))
				{
					if (activateBeforeEvent)
						Unregister (false, flagValue);
					if (activateAfterEvent)
						Unregister (true, flagValue);
				}
			}
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetTriggerTypeOfData ();
			SetActivateChildrenOptionNameOfData ();
			SetTriggerOptionsParentNameOfData ();
			SetActivateBeforeEventOfData ();
			SetActivateAfterEventOfData ();
		}

		void SetTriggerTypeOfData ()
		{
			_Data.triggerType = triggerType;
		}

		void SetTriggerTypeFromData ()
		{
			triggerType = _Data.triggerType;
		}

		void SetActivateChildrenOptionNameOfData ()
		{
			if (Exists(activateChildrenOption))
				_Data.activateChildrenOptionName = activateChildrenOption.name;
		}

		void SetActivateChildrenOptionNameFromData ()
		{
			if (_Data.activateChildrenOptionName != null)
				activateChildrenOption = EternityEngine.GetOption(_Data.activateChildrenOptionName);
		}

		void SetTriggerOptionsParentNameOfData ()
		{
			if (Exists(triggerOptionsParent))
				_Data.triggerOptionsParentName = triggerOptionsParent.name;
		}

		void SetTriggerOptionsParentNameFromData ()
		{
			if (_Data.activateChildrenOptionName != null)
				triggerOptionsParent = EternityEngine.GetOption(_Data.triggerOptionsParentName);
		}

		void SetActivateBeforeEventOfData ()
		{
			_Data.activateBeforeEvent = activateBeforeEvent;
		}

		void SetActivateBeforeEventFromData ()
		{
			activateBeforeEvent = _Data.activateBeforeEvent;
		}

		void SetActivateAfterEventOfData ()
		{
			_Data.activateAfterEvent = activateAfterEvent;
		}

		void SetActivateAfterEventFromData ()
		{
			activateAfterEvent = _Data.activateAfterEvent;
		}

		[Serializable]
		public class Data : ToggleChildrenOption.Data
		{
			public TriggerType triggerType;
			public string activateChildrenOptionName = null;
			public string triggerOptionsParentName = null;
			public bool activateBeforeEvent;
			public bool activateAfterEvent;

			public override object MakeAsset ()
			{
				ActivationBehaviourOption activationBehaviourOption = ObjectPool.instance.SpawnComponent<ActivationBehaviourOption>(EternityEngine.instance.activationBehaviourOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (activationBehaviourOption);
				activationBehaviourOption.Init ();
				return activationBehaviourOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				ActivationBehaviourOption activationBehaviourOption = (ActivationBehaviourOption) asset;
				activationBehaviourOption._Data = this;
				activationBehaviourOption.SetTriggerTypeFromData ();
				activationBehaviourOption.SetActivateChildrenOptionNameFromData ();
				activationBehaviourOption.SetTriggerOptionsParentNameFromData ();
				activationBehaviourOption.SetActivateBeforeEventFromData ();
				activationBehaviourOption.SetActivateAfterEventFromData ();
			}
		}
	}
}