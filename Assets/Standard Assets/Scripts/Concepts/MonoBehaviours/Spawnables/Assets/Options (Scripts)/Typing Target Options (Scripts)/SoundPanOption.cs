using System;

namespace _EternityEngine
{
	public class SoundPanOption : TypingTargetOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public SoundFileInstanceOption soundFileInstanceOption;

		public override bool SetValue (string value)
		{
			if (base.SetValue(value))
			{
				if (Exists(soundFileInstanceOption))
				{
					float? pan = GetNumberValue();
					if (pan != null)
						soundFileInstanceOption.SetPan ((float) pan);
				}
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetSoundFileInstanceOptionNameOfData ();
		}

		void SetSoundFileInstanceOptionNameOfData ()
		{
			if (Exists(soundFileInstanceOption))
				_Data.soundFileInstanceOptionName = soundFileInstanceOption.name;
		}

		void SetSoundFileInstanceOptionNameFromData ()
		{
			if (_Data.soundFileInstanceOptionName != null)
				soundFileInstanceOption = EternityEngine.GetOption<SoundFileInstanceOption>(_Data.soundFileInstanceOptionName);
		}

		[Serializable]
		public class Data : TypingTargetOption.Data
		{
			public string soundFileInstanceOptionName = null;

			public override object MakeAsset ()
			{
				SoundPanOption soundPanOption = ObjectPool.instance.SpawnComponent<SoundPanOption>(EternityEngine.instance.soundPanOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (soundPanOption);
				soundPanOption.Init ();
				return soundPanOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				SoundPanOption soundPanOption = (SoundPanOption) asset;
				soundPanOption._Data = this;
				soundPanOption.SetSoundFileInstanceOptionNameFromData ();
			}
		}
	}
}
