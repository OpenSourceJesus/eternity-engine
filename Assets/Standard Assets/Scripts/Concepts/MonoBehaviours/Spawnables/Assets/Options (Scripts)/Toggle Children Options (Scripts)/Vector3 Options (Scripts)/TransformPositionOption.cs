using System;
using Extensions;
using UnityEngine;

namespace _EternityEngine
{
	public class TransformPositionOption : Vector3Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public Transform referenceTrs;

		public void Init (Transform referenceTrs)
		{
			this.referenceTrs = referenceTrs;
		}

		public override void UpdateValue ()
		{
			value = referenceTrs.position;
		}

		public override bool SetX (float x)
		{
			if (base.SetX(x))
			{
				referenceTrs.position = referenceTrs.position.SetX(x);
				return true;
			}
			else
				return false;
		}

		public override bool SetY (float y)
		{
			if (base.SetY(y))
			{
				referenceTrs.position = referenceTrs.position.SetY(y);
				return true;
			}
			else
				return false;
		}

		public override bool SetZ (float z)
		{
			if (base.SetZ(z))
			{
				referenceTrs.position = referenceTrs.position.SetZ(z);
				return true;
			}
			else
				return false;
		}

		public override bool SetValue (Vector3 value)
		{
			if (base.SetValue(value))
			{
				referenceTrs.position = value;
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
		}

		[Serializable]
		public class Data : Vector3Option.Data
		{
			public override object MakeAsset ()
			{
				TransformPositionOption transformPositionOption = ObjectPool.instance.SpawnComponent<TransformPositionOption>(EternityEngine.instance.transformPositionOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (transformPositionOption);
				transformPositionOption.Init ();
				return transformPositionOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				TransformPositionOption transformPositionOption = (TransformPositionOption) asset;
				transformPositionOption._Data = this;
			}
		}
	}
}