using System;

namespace _EternityEngine
{
	public class SpawnOrientationDeleteCurrentBehaviourOption : EnumOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public SpawnOrientationOption spawnOrientationOption;

		public override bool SetValue (int value)
		{
			bool output = base.SetValue(value);
			if (output && Exists(spawnOrientationOption))
				spawnOrientationOption.deleteCurrentBehaviour = (SpawnOrientationOption.DeleteCurrentBehaviour) Enum.ToObject(enumType, this.value);
			return output;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetSpawnOrientationOptionNameOfData ();
		}

		void SetSpawnOrientationOptionNameOfData ()
		{
			if (Exists(spawnOrientationOption))
				_Data.spawnOrientationOptionName = spawnOrientationOption.name;
		}

		void SetSpawnOrientationOptionNameFromData ()
		{
			if (_Data.spawnOrientationOptionName != null)
				spawnOrientationOption = EternityEngine.GetOption<SpawnOrientationOption>(_Data.spawnOrientationOptionName);
		}

		[Serializable]
		public class Data : EnumOption.Data
		{
			public string spawnOrientationOptionName = null;

			public override object MakeAsset ()
			{
				SpawnOrientationDeleteCurrentBehaviourOption spawnOrientationDeleteCurrentBehaviourOption = ObjectPool.instance.SpawnComponent<SpawnOrientationDeleteCurrentBehaviourOption>(EternityEngine.instance.spawnOrientationDeleteCurrentBehaviourOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (spawnOrientationDeleteCurrentBehaviourOption);
				spawnOrientationDeleteCurrentBehaviourOption.Init ();
				return spawnOrientationDeleteCurrentBehaviourOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				SpawnOrientationDeleteCurrentBehaviourOption spawnOrientationDeleteCurrentBehaviourOption = (SpawnOrientationDeleteCurrentBehaviourOption) asset;
				spawnOrientationDeleteCurrentBehaviourOption._Data = this;
				spawnOrientationDeleteCurrentBehaviourOption.SetSpawnOrientationOptionNameFromData ();
			}
		}
	}
}