using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class SystemComponentOption : ToggleChildrenOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public string path;
		public RenameSystemComponentOption renameOption;
		// public Option[] defaultChildren = new Option[0];

		public virtual void Init (string path)
		{
			this.path = path;
			renameOption.text.text = renameOption.name + renameOption.NameToValueSeparator + path;
			SetName (path);
		}

		// public override void Delete ()
		// {
		// 	for (int i = 0; i < defaultChildren.Length; i ++)
		// 	{
		// 		Option child = defaultChildren[i];
		// 		child.Delete ();
		// 	}
		// 	DestroyImmediate(gameObject);
		// }

		public virtual void Rename (string path)
		{
			if (File.Exists(path))
			{
				int nameOccuranceCount = 1;
				while (File.Exists(path + " (" + nameOccuranceCount + ")"))
					nameOccuranceCount ++;
				path += " (" + nameOccuranceCount + ")";
			}
			this.path = path;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetPathOfData ();
		}

		void SetPathOfData ()
		{
			_Data.path = path;
		}

		void SetPathFromData ()
		{
			path = _Data.path;
		}

		[Serializable]
		public class Data : ToggleChildrenOption.Data
		{
			public string path;
			
			public override object MakeAsset ()
			{
				SystemComponentOption systemComponentOption = ObjectPool.instance.SpawnComponent<SystemComponentOption>(EternityEngine.instance.systemComponentOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (systemComponentOption);
				systemComponentOption.Init ();
				return systemComponentOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				SystemComponentOption systemComponentOption = (SystemComponentOption) asset;
				systemComponentOption._Data = this;
				systemComponentOption.SetPathFromData ();
			}
		}
	}
}