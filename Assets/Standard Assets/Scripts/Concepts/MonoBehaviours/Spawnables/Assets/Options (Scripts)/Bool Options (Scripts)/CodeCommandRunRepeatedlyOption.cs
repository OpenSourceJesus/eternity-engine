using System;

namespace _EternityEngine
{
	public class CodeCommandRunRepeatedlyOption : BoolOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public CodeCommandOption codeCommandOption;

		public override bool SetValue (bool value)
		{
			if (base.SetValue(value))
			{
				if (Exists(codeCommandOption))
					codeCommandOption.SetRunRepeatedly (value);
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetCodeCommandOptionNameOfData ();
		}

		void SetCodeCommandOptionNameOfData ()
		{
			if (Exists(codeCommandOption))
				_Data.codeCommandOptionName = codeCommandOption.name;
		}

		void SetCodeCommandOptionNameFromData ()
		{
			if (_Data.codeCommandOptionName != null)
				codeCommandOption = EternityEngine.GetOption<CodeCommandOption>(_Data.codeCommandOptionName);
		}

		[Serializable]
		public class Data : BoolOption.Data
		{
			public string codeCommandOptionName = null;

			public override object MakeAsset ()
			{
				CodeCommandRunRepeatedlyOption codeCommandRunRepeatedlyOption = ObjectPool.instance.SpawnComponent<CodeCommandRunRepeatedlyOption>(EternityEngine.instance.codeCommandRunRepeatedlyOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (codeCommandRunRepeatedlyOption);
				codeCommandRunRepeatedlyOption.Init ();
				return codeCommandRunRepeatedlyOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				CodeCommandRunRepeatedlyOption codeCommandRunRepeatedlyOption = (CodeCommandRunRepeatedlyOption) asset;
				codeCommandRunRepeatedlyOption._Data = this;
				codeCommandRunRepeatedlyOption.SetCodeCommandOptionNameFromData ();
			}
		}
	}
}