using System;
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class DivOption : ToggleChildrenOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public string url;
		public string id;
		public string className;

		public void SetURL (string url)
		{
			this.url = url;
		}

		public void SetId (string id)
		{
			this.id = id;
		}

		public void SetClassName (string className)
		{
			this.className = className;
		}

		public void DownloadText ()
		{
			StartCoroutine(DownloadTextRoutine ());
		}

		IEnumerator DownloadTextRoutine ()
		{
			UnityWebRequest unityWebRequest = UnityWebRequest.Get(url);
			DownloadHandler downloadHandler = new DownloadHandlerScript();
			unityWebRequest.downloadHandler = downloadHandler;
			yield return unityWebRequest.SendWebRequest();
			print(downloadHandler.data);
			unityWebRequest.Dispose();
		}

		public void DownloadModel ()
		{
			throw new NotImplementedException();
		}

		public void DownloadAudio ()
		{
			throw new NotImplementedException();
		}

		public void UploadText (string text)
		{
			throw new NotImplementedException();
		}

		public void UploadModel (ModelFileOption modelFileOption)
		{
			throw new NotImplementedException();
		}

		public void UploadAudio (SoundFileOption soundFileOptionPrefab)
		{
			throw new NotImplementedException();
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetURLOfData ();
			SetIdOfData ();
			SetClassNameOfData ();
		}

		void SetURLOfData ()
		{
			_Data.url = url;
		}

		void SetURLFromData ()
		{
			url = _Data.url;
		}

		void SetIdOfData ()
		{
			_Data.id = id;
		}

		void SetIdFromData ()
		{
			id = _Data.id;
		}

		void SetClassNameOfData ()
		{
			_Data.className = className;
		}

		void SetClassNameFromData ()
		{
			className = _Data.className;
		}

		[Serializable]
		public class Data : ToggleChildrenOption.Data
		{
			public string url;
			public string id;
			public string className;
			
			public override object MakeAsset ()
			{
				DivOption divOption = ObjectPool.instance.SpawnComponent<DivOption>(EternityEngine.instance.divOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (divOption);
				divOption.Init ();
				return divOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				DivOption divOption = (DivOption) asset;
				divOption._Data = this;
				divOption.SetURLFromData ();
				divOption.SetIdFromData ();
				divOption.SetClassNameFromData ();
			}
		}
	}
}