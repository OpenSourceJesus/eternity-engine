using System;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class LightColorOption : ColorOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public LightOption lightOption;

		public override bool SetR (float r)
		{
			bool output = base.SetR(r);
			if (output && Exists(lightOption))
				Apply ();
			return output;
		}

		public override bool SetG (float g)
		{
			bool output = base.SetG(g);
			if (output && Exists(lightOption))
				Apply ();
			return output;
		}

		public override bool SetB (float b)
		{
			bool output = base.SetB(b);
			if (output && Exists(lightOption))
				Apply ();
			return output;
		}

		public override bool SetA (float a)
		{
			bool output = base.SetA(a);
			if (output && Exists(lightOption))
				Apply ();
			return output;
		}

		void Apply ()
		{
			lightOption.SetColor (value);
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetLightOptionNameOfData ();
		}

		void SetLightOptionNameOfData ()
		{
			if (Exists(lightOption))
				_Data.lightOptionName = lightOption.name;
		}

		void SetLightOptionNameFromData ()
		{
			if (_Data.lightOptionName != null)
				lightOption = EternityEngine.GetOption<LightOption>(_Data.lightOptionName);
		}

		[Serializable]
		public class Data : ColorOption.Data
		{
			public string lightOptionName = null;

			public override object MakeAsset ()
			{
				LightColorOption lightColorOption = ObjectPool.instance.SpawnComponent<LightColorOption>(EternityEngine.instance.lightColorOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (lightColorOption);
				lightColorOption.Init ();
				return lightColorOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				LightColorOption lightColorOption = (LightColorOption) asset;
				lightColorOption._Data = this;
				lightColorOption.SetLightOptionNameFromData ();
			}
		}
	}
}