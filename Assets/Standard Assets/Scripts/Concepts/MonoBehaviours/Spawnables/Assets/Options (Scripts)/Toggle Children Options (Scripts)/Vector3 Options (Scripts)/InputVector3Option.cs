using System;
using Extensions;
using UnityEngine;
using UnityEngine.InputSystem;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class InputVector3Option : Vector3Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public Type type;

		public override void UpdateValue ()
		{
			if (Mouse.current != null)
				value = Mouse.current.position.ReadValue();
		}

		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetTypeOfData ();
		}

		void SetTypeOfData ()
		{
			_Data.type = type;
		}

		void SetTypeFromData ()
		{
			type = _Data.type;
		}

		public enum Type
		{
			Mouse
		}

		[Serializable]
		public class Data : Vector3Option.Data
		{
			public Type type;

			public override object MakeAsset ()
			{
				InputVector3Option inputVector3Option = ObjectPool.instance.SpawnComponent<InputVector3Option>(EternityEngine.instance.inputVector3OptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (inputVector3Option);
				inputVector3Option.Init ();
				return inputVector3Option;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				InputVector3Option inputVector3Option = (InputVector3Option) asset;
				inputVector3Option._Data = this;
				inputVector3Option.SetTypeFromData ();
			}
		}
	}
}