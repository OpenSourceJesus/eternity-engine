using System;

namespace _EternityEngine
{
	public class StartEmittingSoundOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public SoundFileInstanceOption soundFileInstanceOption;

		public override void OnActivate (EternityEngine.Hand hand)
		{
			if (Exists(soundFileInstanceOption))
				soundFileInstanceOption.StartEmit ();
			// base.OnActivate (hand);
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetSoundFileInstanceOptionNameOfData ();
		}

		void SetSoundFileInstanceOptionNameOfData ()
		{
			if (Exists(soundFileInstanceOption))
				_Data.soundFileInstanceOptionName = soundFileInstanceOption.name;
		}

		void SetSoundFileInstanceOptionNameFromData ()
		{
			if (_Data.soundFileInstanceOptionName != null)
				soundFileInstanceOption = EternityEngine.GetOption<SoundFileInstanceOption>(_Data.soundFileInstanceOptionName);
		}

		[Serializable]
		public class Data : Option.Data
		{
			public string soundFileInstanceOptionName = null;

			public override object MakeAsset ()
			{
				StartEmittingSoundOption startEmittingSoundOption = ObjectPool.instance.SpawnComponent<StartEmittingSoundOption>(EternityEngine.instance.startEmittingSoundOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (startEmittingSoundOption);
				startEmittingSoundOption.Init ();
				return startEmittingSoundOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				StartEmittingSoundOption startEmittingSoundOption = (StartEmittingSoundOption) asset;
				startEmittingSoundOption._Data = this;
				startEmittingSoundOption.SetSoundFileInstanceOptionNameFromData ();
			}
		}
	}
}