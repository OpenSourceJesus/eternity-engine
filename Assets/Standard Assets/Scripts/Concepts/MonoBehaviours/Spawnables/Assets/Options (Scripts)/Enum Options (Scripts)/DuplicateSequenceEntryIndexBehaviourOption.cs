using System;

namespace _EternityEngine
{
	public class DuplicateSequenceEntryIndexBehaviourOption : EnumOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public AutomationOption automationOption;

		public override bool SetValue (int value)
		{
			if (base.SetValue(value))
			{
				if (Exists(automationOption))
					automationOption.duplicateSequenceEntryIndexBehaviour = (AutomationOption.DuplicateSequenceEntryIndexBehaviour) Enum.ToObject(enumType, value);
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetAutomationOptionNameOfData ();
		}

		void SetAutomationOptionNameOfData ()
		{
			if (Exists(automationOption))
				_Data.automationOptionName = automationOption.name;
		}

		void SetAutomationOptionNameFromData ()
		{
			if (_Data.automationOptionName != null)
				automationOption = EternityEngine.GetOption<AutomationOption>(_Data.automationOptionName);
		}

		[Serializable]
		public class Data : EnumOption.Data
		{
			public string automationOptionName = null;

			public override object MakeAsset ()
			{
				DuplicateSequenceEntryIndexBehaviourOption duplicateSequenceEntryIndexBehaviourOption = ObjectPool.instance.SpawnComponent<DuplicateSequenceEntryIndexBehaviourOption>(EternityEngine.instance.duplicateSequenceEntryIndexBehaviourOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (duplicateSequenceEntryIndexBehaviourOption);
				// if (!duplicateSequenceEntryIndexBehaviourOption.isInitialized)
				// 	duplicateSequenceEntryIndexBehaviourOption.Init ();
				return duplicateSequenceEntryIndexBehaviourOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				DuplicateSequenceEntryIndexBehaviourOption duplicateSequenceEntryIndexBehaviourOption = (DuplicateSequenceEntryIndexBehaviourOption) asset;
				duplicateSequenceEntryIndexBehaviourOption._Data = this;
				duplicateSequenceEntryIndexBehaviourOption.SetAutomationOptionNameFromData ();
			}
		}
	}
}