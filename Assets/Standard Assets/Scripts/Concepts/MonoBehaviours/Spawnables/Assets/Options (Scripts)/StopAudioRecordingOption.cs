using System;
using System.IO;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class StopAudioRecordingOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		AudioSource audioSource;

		public override void Init ()
		{
			base.Init ();
			audioSource = EternityEngine.instance.audioSource;
		}

		public override void OnActivate (EternityEngine.Hand hand)
		{
			// base.OnActivate (hand);
			StopRecording ();
		}

		public void SaveRecording (Option fileNameOption)
		{
			string filePath = fileNameOption.GetValue();
			if (!filePath.ToLower().EndsWith(".wav"))
				filePath += ".wav";
			bool filePreviouslyExists = File.Exists(filePath);
			// audioSource.clip = AudioUtilities.TrimSilence(audioSource.clip, 0);
			// if (AudioUtilities.SaveWavFile(filePath, audioSource.clip))
			// {
			// 	if (filePreviouslyExists)
			// 	{
					
			// 		return;
			// 	}
			// 	EternityEngine.instance.SpawnOption<SoundFileOption> (EternityEngine.instance.soundFileOptionPrefab, (SoundFileOption soundFileOption) => { soundFileOption.Init (filePath, audioSource.clip); });
			// }
		}

		public void StopRecording ()
		{
			StopCoroutine(RecordingRoutine ());
			Microphone.End("");
		}

		IEnumerator RecordingRoutine ()
		{
			yield return Application.RequestUserAuthorization(UserAuthorization.Microphone);
			int recordingLength = EternityEngine.instance.recordingLength;
			int recordingFrequency = EternityEngine.instance.recordingFrequency;
			if (Application.HasUserAuthorization(UserAuthorization.Microphone))
			{
				audioSource.clip = Microphone.Start("", true, recordingLength, recordingFrequency);
				audioSource.loop = true;
				while (Microphone.GetPosition("") <= 0)
				{
				}
				// audioSource.Play();
			}
			else
				print("Microphone not authorized for user");
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
		}

		[Serializable]
		public class Data : Option.Data
		{
			public override object MakeAsset ()
			{
				StopAudioRecordingOption startAudioRecordingOption = ObjectPool.instance.SpawnComponent<StopAudioRecordingOption>(EternityEngine.instance.startAudioRecordingOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (startAudioRecordingOption);
				startAudioRecordingOption.Init ();
				return startAudioRecordingOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				StopAudioRecordingOption startAudioRecordingOption = (StopAudioRecordingOption) asset;
				startAudioRecordingOption._Data = this;
			}
		}
	}
}