using System;
using Extensions;
using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class TwoInputsOperationsOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public OperationType operationType;
		public TooManyChildrenBehaviour tooManyChildrenBehaviour;
		public Option invalidAnswerOption;
		public Option value1OptionParent;
		public Option value2OptionParent;
		Option value1Option;
		Option value2Option;

		public override void Init ()
		{
			base.Init ();
			value1OptionParent.onAddChild += (Option child) => { OnAddChild (value1OptionParent, child); };
			value1OptionParent.onRemoveChild += (Option child) => { OnRemoveChild (value1OptionParent, child); };
			value1OptionParent.onAboutToAddChild += OnAboutToAddChild_value1;
			value2OptionParent.onAddChild += (Option child) => { OnAddChild (value2OptionParent, child); };
			value2OptionParent.onRemoveChild += (Option child) => { OnRemoveChild (value2OptionParent, child); };
			value2OptionParent.onAboutToAddChild += OnAboutToAddChild_value2;
		}

		public override void OnActivate (EternityEngine.Hand hand)
		{
			if (!Exists(value1Option) || !Exists(value2Option))
				return;
			float value1 = (float) TypingTargetOption.GetNumberValue(value1Option.GetValue());
			float value2 = (float) TypingTargetOption.GetNumberValue(value2Option.GetValue());
			if (operationType == OperationType.Add)
				value1Option.SetValue ("" + (value1 + value2));
			else if (operationType == OperationType.Subtract)
				value1Option.SetValue ("" + (value1 - value2));
			else if (operationType == OperationType.Multiply)
				value1Option.SetValue ("" + (value1 * value2));
			else if (operationType == OperationType.Divide)
				value1Option.SetValue ("" + (value1 / value2));
			else// if (operationType == OperationType.Exponent)
				value1Option.SetValue ("" + Mathf.Pow(value1, value2));
			// base.OnActivate (hand);
		}

		void OnAddChild (Option parent, Option child)
		{
			print("OnAddChild: " + child);
			if (parent.children.Count > 1)
			{
				if (tooManyChildrenBehaviour == TooManyChildrenBehaviour.UnparentOldChildren)
				{
					while (parent.children.Count > 1)
						parent.RemoveChild (parent.children[0]);
				}
			}
			if (parent == value1OptionParent)
				value1Option = child;
			else
				value2Option = child;
			SetActivatable (ShouldBeActivatable());
		}

		void OnRemoveChild (Option parent, Option child)
		{
			print("OnRemoveChild: " + child);
			if (parent.children.Count == 0)
			{
				if (parent == value1OptionParent)
					value1Option = null;
				else
					value2Option = null;
				SetActivatable (false);
			}
			else
			{
				ApplyTooManyChildrenBehaviour (parent);
				SetActivatable (ShouldBeActivatable());
			}
		}

		bool OnAboutToAddChild_value1 (Option child)
		{
			print("OnAboutToAddChild_value1: " + child);
			if (tooManyChildrenBehaviour == TooManyChildrenBehaviour.Disallow)
				return value1OptionParent.children.Count == 0;
			else
				return true;
		}

		bool OnAboutToAddChild_value2 (Option child)
		{
			print("OnAboutToAddChild_value2: " + child);
			if (tooManyChildrenBehaviour == TooManyChildrenBehaviour.Disallow)
				return value2OptionParent.children.Count == 0;
			else
				return true;
		}

		public void ApplyTooManyChildrenBehaviour (Option parent)
		{
			if (parent.children.Count < 2)
				return;
			if (tooManyChildrenBehaviour == TooManyChildrenBehaviour.UseMostRecentChild)
			{
				if (parent == value1OptionParent)
					value1Option = value1OptionParent.children[value1OptionParent.children.Count - 1];
				else
					value1Option = value2OptionParent.children[value2OptionParent.children.Count - 1];
			}
			else if (tooManyChildrenBehaviour == TooManyChildrenBehaviour.UseMostRecentChild)
			{
				if (parent == value1OptionParent)
					value1Option = value1OptionParent.children[0];
				else
					value2Option = value2OptionParent.children[0];
			}
			else if (tooManyChildrenBehaviour == TooManyChildrenBehaviour.UnparentOldChildren)
			{
				while (parent.children.Count > 1)
					parent.RemoveChild (parent.children[0]);
				if (parent == value1OptionParent)
					value1Option = value1OptionParent.children[0];
				else
					value2Option = value2OptionParent.children[0];
			}
		}

		public void SetOperationType (OperationType operationType)
		{
			this.operationType = operationType;
			SetActivatable (ShouldBeActivatable());
		}

		void SetValueOptionVariables (ref Option valueOption, Option newValueOption, ref float? value)
		{
			valueOption = newValueOption;
			value = TypingTargetOption.GetNumberValue(valueOption.GetValue());
		}

		bool ShouldBeActivatable ()
		{
			return Exists(value1Option) && TypingTargetOption.GetNumberValue(value1Option.GetValue()) != null && Exists(value2Option) && TypingTargetOption.GetNumberValue(value2Option.GetValue()) != null;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetOperationTypeOfData ();
			SetTooManyChildrenBehaviourOfData ();
			SetValue1OptionParentNameOfData ();
			SetValue2OptionParentNameOfData ();
			SetValue1OptionNameOfData ();
			SetValue2OptionNameOfData ();
		}

		void SetOperationTypeOfData ()
		{
			_Data.operationType = operationType;
		}

		void SetOperationTypeFromData ()
		{
			operationType = _Data.operationType;
		}

		void SetTooManyChildrenBehaviourOfData ()
		{
			_Data.tooManyChildrenBehaviour = tooManyChildrenBehaviour;
		}

		void SetTooManyChildrenBehaviourFromData ()
		{
			tooManyChildrenBehaviour = _Data.tooManyChildrenBehaviour;
		}

		void SetValue1OptionParentNameOfData ()
		{
			if (Exists(value1OptionParent))
				_Data.value1OptionParentName = value1OptionParent.name;
		}

		void SetValue1OptionParentNameFromData ()
		{
			if (_Data.value1OptionParentName != null)
				value1OptionParent = EternityEngine.GetOption(_Data.value1OptionParentName);
		}

		void SetValue2OptionParentNameOfData ()
		{
			if (Exists(value2OptionParent))
				_Data.value2OptionParentName = value2OptionParent.name;
		}

		void SetValue2OptionParentNameFromData ()
		{
			if (_Data.value2OptionParentName != null)
				value2OptionParent = EternityEngine.GetOption(_Data.value2OptionParentName);
		}

		void SetValue1OptionNameOfData ()
		{
			if (Exists(value1Option))
				_Data.value1OptionName = value1Option.name;
		}

		void SetValue1OptionNameFromData ()
		{
			if (_Data.value1OptionName != null)
				value1Option = EternityEngine.GetOption(_Data.value1OptionName);
		}

		void SetValue2OptionNameOfData ()
		{
			if (Exists(value2Option))
				_Data.value2OptionName = value2Option.name;
		}

		void SetValue2OptionNameFromData ()
		{
			if (_Data.value2OptionName != null)
				value2Option = EternityEngine.GetOption(_Data.value2OptionName);
		}

		public enum TooManyChildrenBehaviour
		{
			Disallow,
			UseMostRecentChild,
			UseLeastRecentChild,
			UnparentOldChildren
		}

		public enum OperationType
		{
			Add,
			Subtract,
			Multiply,
			Divide,
			Exponent
		}

		[Serializable]
		public class Data : Option.Data
		{
			public OperationType operationType;
			public TooManyChildrenBehaviour tooManyChildrenBehaviour;
			public string value1OptionParentName = null;
			public string value2OptionParentName = null;
			public string value1OptionName = null;
			public string value2OptionName = null;

			public override object MakeAsset ()
			{
				TwoInputsOperationsOption twoInputsOperationsOption = ObjectPool.instance.SpawnComponent<TwoInputsOperationsOption>(EternityEngine.instance.twoInputsOperationsOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (twoInputsOperationsOption);
				twoInputsOperationsOption.Init ();
				return twoInputsOperationsOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				TwoInputsOperationsOption twoInputsOperationsOption = (TwoInputsOperationsOption) asset;
				twoInputsOperationsOption._Data = this;
				twoInputsOperationsOption.SetOperationTypeFromData ();
				twoInputsOperationsOption.SetTooManyChildrenBehaviourFromData ();
				twoInputsOperationsOption.SetValue1OptionParentNameFromData ();
				twoInputsOperationsOption.SetValue2OptionParentNameFromData ();
				twoInputsOperationsOption.SetValue1OptionNameFromData ();
				twoInputsOperationsOption.SetValue2OptionNameFromData ();
			}
		}
	}
}