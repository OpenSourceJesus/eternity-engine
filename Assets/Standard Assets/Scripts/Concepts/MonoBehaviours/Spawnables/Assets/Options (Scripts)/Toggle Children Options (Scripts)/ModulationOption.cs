using TMPro;
using System;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class ModulationOption : ToggleChildrenOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public Option modulateChildrenOption;
		public Modulator.ControlMethod controlMethod;
		public Modulator.BehaviourWhenUncontrolled behaviourWhenUncontrolled;
		public ModulationCurve modulationCurve;
		public ModulationCurve.KeyframeData keyframeDataTemplateWhenControlled;
		public ModulationCurve.KeyframeData potentialKeyframeDataTemplateWhenUncontrolled;
		public bool isForLeftHand;
		public List<ModulationTarget> modulateTargets = new List<ModulationTarget>();
		public Dictionary<TypingTargetOption, ModulationTarget> modulateTargetsDict = new Dictionary<TypingTargetOption, ModulationTarget>();
		public LineRenderer localDisplayerLineRenderer;
		public Transform tweakingDisplayerTrs;
		public LineRenderer tweakingDisplayerLineRenderer;
		public EnumOption tweakingTimeDisplayAxisEnumOption;
		public EnumOption tweakingValueDisplayAxisEnumOption;
		public static List<ModulationOption> displayingModulationOptionCurves = new List<ModulationOption>();
		public static int localDisplaySampleCount = 50;
		public static int tweakingDisplaySampleCount = 200;
		Keyframe lastKeyframe;
		ModulationCurve.KeyframeData lastKeyframeData;
		Modulator modulator;

		public void Init (Modulator.ControlMethod controlMethod, Modulator.BehaviourWhenUncontrolled behaviourWhenUncontrolled, ModulationCurve.KeyframeData keyframeDataTemplateWhenControlled, ModulationCurve.KeyframeData potentialKeyframeDataTemplateWhenUncontrolled, bool isForLeftHand)
		{
			this.controlMethod = controlMethod;
			this.behaviourWhenUncontrolled = behaviourWhenUncontrolled;
			this.keyframeDataTemplateWhenControlled = keyframeDataTemplateWhenControlled;
			this.potentialKeyframeDataTemplateWhenUncontrolled = potentialKeyframeDataTemplateWhenUncontrolled;
			this.isForLeftHand = isForLeftHand;
			modulateChildrenOption.onAddChild += OnAddChildToModulate;
			modulateChildrenOption.onRemoveChild += OnRemoveChildToModulate;
		}

		public void StartRecording ()
		{
			if (isForLeftHand)
				StartRecording (EternityEngine.instance.leftHand);
			else
				StartRecording (EternityEngine.instance.rightHand);
		}

		void StartRecording (EternityEngine.Hand hand)
		{
			modulationCurve.keyframeDatas = new List<ModulationCurve.KeyframeData>();
			EternityEngine.InstrumentInfo instrumentInfo = EternityEngine.InstrumentInfo.GetInstrumentInfo(hand, hand.currentInstrumentIndex);
			instrumentInfo.trs.gameObject.SetActive(false);
			modulator = hand.modulator;
			modulator.controlMethod = controlMethod;
			if (controlMethod == Modulator.ControlMethod.Distance)
			{
				modulator.minDistanceIndicatorTrs.SetWorldScale (Vector3.one * EternityEngine.instance.modulationDistanceRange.min);
				modulator.maxDistanceIndicatorTrs.SetWorldScale (Vector3.one * EternityEngine.instance.modulationDistanceRange.max);
				modulator.minDistanceIndicatorTrs.parent.gameObject.SetActive(true);
			}
			if (controlMethod == Modulator.ControlMethod.Slider)
			{
				modulator.sliderIndicatorTrs.SetWorldScale (modulator.sliderIndicatorTrs.lossyScale.SetZ(EternityEngine.instance.modulationSliderLength));
				modulator.sliderIndicatorTrs.gameObject.SetActive(true);
			}
			else if (controlMethod == Modulator.ControlMethod.PositionAngle)
			{
				float coneBaseAngle = (360f - EternityEngine.instance.modulationPositionAngleRange.min) / 2;
				Cone cone = new Cone(modulator.minPositionAngleIndicatorTrs.position, modulator.minPositionAngleIndicatorTrs.forward, modulator.minPositionAngleIndicatorTrs.lossyScale.x / 2, coneBaseAngle);
				MeshRenderer minPositionIndicator = cone.MakeMeshRenderer(EternityEngine.instance.minModulationPositionAngleIndicatorConeBasePointCount, Cone.FaceType.Inside);
				minPositionIndicator.sharedMaterial = EternityEngine.instance.minModulationPositionAngleIndicatorMaterial;
				coneBaseAngle = (360f - EternityEngine.instance.modulationPositionAngleRange.max) / 2;
				cone = new Cone(modulator.maxPositionAngleIndicatorTrs.position, modulator.maxPositionAngleIndicatorTrs.forward, modulator.maxPositionAngleIndicatorTrs.lossyScale.x / 2, coneBaseAngle);
				MeshRenderer maxPositionIndicator = cone.MakeMeshRenderer(EternityEngine.instance.maxModulationPositionAngleIndicatorConeBasePointCount, Cone.FaceType.Inside);
				maxPositionIndicator.sharedMaterial = EternityEngine.instance.maxModulationPositionAngleIndicatorMaterial;
				modulator.minPositionAngleIndicatorTrs.parent.gameObject.SetActive(true);
			}
			else// if (controlMethod == Modulator.ControlMethod.RotationAngle)
			{
				float coneBaseAngle = (360f - EternityEngine.instance.modulationRotationAngleRange.min) / 2;
				Cone cone = new Cone(modulator.minRotationAngleIndicatorTrs.position, modulator.minRotationAngleIndicatorTrs.forward, modulator.minRotationAngleIndicatorTrs.lossyScale.x / 2, coneBaseAngle);
				MeshRenderer minRotationIndicator = cone.MakeMeshRenderer(EternityEngine.instance.minModulationRotationAngleIndicatorConeBasePointCount, Cone.FaceType.Inside);
				minRotationIndicator.sharedMaterial = EternityEngine.instance.minModulationRotationAngleIndicatorMaterial;
				coneBaseAngle = (360f - EternityEngine.instance.modulationRotationAngleRange.max) / 2;
				cone = new Cone(modulator.maxRotationAngleIndicatorTrs.position, modulator.maxRotationAngleIndicatorTrs.forward, modulator.maxRotationAngleIndicatorTrs.lossyScale.x / 2, coneBaseAngle);
				MeshRenderer maxRotationIndicator = cone.MakeMeshRenderer(EternityEngine.instance.maxModulationRotationAngleIndicatorConeBasePointCount, Cone.FaceType.Inside);
				maxRotationIndicator.sharedMaterial = EternityEngine.instance.maxModulationRotationAngleIndicatorMaterial;
				modulator.minRotationAngleIndicatorTrs.parent.gameObject.SetActive(true);
			}
			modulator.onUncontrolled += OnModulatorUncontrolled;
			modulator.onValueChanged += OnModulatorValueChanged;
			modulator.trs.SetParent(null);
			modulator.modulationOption = this;
			modulator.gameObject.SetActive(true);
		}

		public void EndRecording ()
		{
			if (isForLeftHand)
				EndRecording (EternityEngine.instance.leftHand);
			else
				EndRecording (EternityEngine.instance.rightHand);
		}

		void EndRecording (EternityEngine.Hand hand)
		{
			if (controlMethod == Modulator.ControlMethod.Distance)
				modulator.minDistanceIndicatorTrs.parent.gameObject.SetActive(false);
			if (controlMethod == Modulator.ControlMethod.Slider)
				modulator.sliderIndicatorTrs.gameObject.SetActive(false);
			else if (controlMethod == Modulator.ControlMethod.PositionAngle)
				modulator.minPositionAngleIndicatorTrs.parent.gameObject.SetActive(false);
			else// if (controlMethod == Modulator.ControlMethod.RotationAngle)
				modulator.minRotationAngleIndicatorTrs.parent.gameObject.SetActive(false);
			modulator.gameObject.SetActive(false);
			modulator.onUncontrolled -= OnModulatorUncontrolled;
			modulator.onValueChanged -= OnModulatorValueChanged;
			EternityEngine.InstrumentInfo instrumentInfo = EternityEngine.InstrumentInfo.GetInstrumentInfo(hand, hand.currentInstrumentIndex);
			instrumentInfo.trs.gameObject.SetActive(true);
			modulator.trs.SetParent(hand.trs);
			modulator.trs.localPosition = Vector3.zero;
			Display (localDisplayerLineRenderer, 0, 1, false);
		}

		void OnModulatorUncontrolled ()
		{
			if (modulator.startTime == null)
				return;
			if (behaviourWhenUncontrolled == Modulator.BehaviourWhenUncontrolled.AddDefaultKeyframe)
			{
				lastKeyframeData = keyframeDataTemplateWhenControlled;
				lastKeyframeData.time = Time.time - (float) modulator.startTime;
				lastKeyframeData.value = potentialKeyframeDataTemplateWhenUncontrolled.value;
				lastKeyframe = lastKeyframeData.ToKeyframe();
				modulationCurve.normalizedValueOverTimeCurve.AddKey(lastKeyframe);
				modulationCurve.keyframeDatas.Add(lastKeyframeData);
			}
			else if (behaviourWhenUncontrolled == Modulator.BehaviourWhenUncontrolled.AddLastKeyframe)
			{
				lastKeyframeData.time = Time.time - (float) modulator.startTime;
				lastKeyframe.time = lastKeyframeData.time;
				modulationCurve.normalizedValueOverTimeCurve.AddKey(lastKeyframe);
				modulationCurve.keyframeDatas.Add(lastKeyframeData);
			}
		}

		void OnModulatorValueChanged (float value)
		{
			lastKeyframeData = keyframeDataTemplateWhenControlled;
			lastKeyframeData.time = Time.time - (float) modulator.startTime;
			lastKeyframeData.value = value;
			lastKeyframe = lastKeyframeData.ToKeyframe();
			modulationCurve.normalizedValueOverTimeCurve.AddKey(lastKeyframe);
			modulationCurve.keyframeDatas.Add(lastKeyframeData);
			ModulateTypingTargetOptions ();
		}

		void OnAddChildToModulate (Option option)
		{
			TypingTargetOption typingTargetOption = option as TypingTargetOption;
			if (typingTargetOption != null)
			{
				ModulationTarget modulationTarget = new ModulationTarget();
				modulationTarget.typingTargetOption = typingTargetOption;
				ModulationTargetValueRangeOption modulationTargetValueRangeOption = ObjectPool.instance.SpawnComponent<ModulationTargetValueRangeOption>(EternityEngine.instance.modulationTargetValueRangeOptionPrefab.prefabIndex, EternityEngine.instance.currentSpawnOrientationOption.trs.position, EternityEngine.instance.currentSpawnOrientationOption.trs.rotation, EternityEngine.instance.sceneTrs);
				modulationTargetValueRangeOption.trs.SetWorldScale (EternityEngine.instance.currentSpawnOrientationOption.trs.lossyScale);
				modulationTargetValueRangeOption.text.text = typingTargetOption.text.text.RemoveStartAt(NameToValueSeparator);
				modulationTarget.valueRangeOption = modulationTargetValueRangeOption;
				modulateTargets.Add(modulationTarget);
				modulateTargetsDict.Add(typingTargetOption, modulationTarget);
			}
		}

		void OnRemoveChildToModulate (Option option)
		{
			TypingTargetOption typingTargetOption = option as TypingTargetOption;
			if (typingTargetOption != null)
			{
				ModulationTarget modulationTarget = modulateTargetsDict[typingTargetOption];
				DestroyImmediate(modulationTarget.valueRangeOption.gameObject);
				modulateTargets.Remove(modulationTarget);
				modulateTargetsDict.Remove(typingTargetOption);
			}
		}

		void ModulateTypingTargetOptions ()
		{
			for (int i = 0; i < modulateTargets.Count; i ++)
			{
				ModulationTarget modulationTarget = modulateTargets[i];
				modulationTarget.typingTargetOption.SetValue ("" + modulationTarget.valueRangeOption.valueRange.GetNormalized(lastKeyframe.value));
			}
		}

		public void StartDisplayingTweakingDisplayer ()
		{
			if (!EternityEngine.instance.useSeparateTweakingDisplayersForModulationCurvesBoolOption.value)
			{
				DisplayAxis displayValueAxis = DisplayAxis.Y;
				if (displayingModulationOptionCurves.Count > 0)
					displayValueAxis = DisplayAxis.Z;
				Display (tweakingDisplayerLineRenderer, tweakingTimeDisplayAxisEnumOption.value, tweakingValueDisplayAxisEnumOption.value, false);
			}
			else
				Display (displayingModulationOptionCurves[0].tweakingDisplayerLineRenderer, tweakingTimeDisplayAxisEnumOption.value, tweakingValueDisplayAxisEnumOption.value, true);
			displayingModulationOptionCurves.Add(this);
		}

		public void StopDisplayingTweakingDisplayer ()
		{
			tweakingDisplayerLineRenderer.enabled = false;
			if (displayingModulationOptionCurves.Count > 1)
			{
				if (this == displayingModulationOptionCurves[0])
				{
					if (displayingModulationOptionCurves[1].tweakingDisplayerLineRenderer.enabled)
						displayingModulationOptionCurves[1].Display (displayingModulationOptionCurves[1].tweakingDisplayerLineRenderer, tweakingTimeDisplayAxisEnumOption.value, tweakingValueDisplayAxisEnumOption.value, false);
				}
				else if (displayingModulationOptionCurves[0].tweakingDisplayerLineRenderer.enabled)
					displayingModulationOptionCurves[0].Display (displayingModulationOptionCurves[0].tweakingDisplayerLineRenderer, tweakingTimeDisplayAxisEnumOption.value, tweakingValueDisplayAxisEnumOption.value, false);
			}
			displayingModulationOptionCurves.Remove(this);
		}

		void Display (LineRenderer lineRenderer, int timeDisplayAxisIndex, int valueDisplayAxisIndex, bool isAlreadyDisplayingAValue)
		{
			Keyframe lastKeyframe = modulationCurve.normalizedValueOverTimeCurve.keys[modulationCurve.normalizedValueOverTimeCurve.keys.Length - 1];
			float endTime = lastKeyframe.time;
			Vector3[] positions = new Vector3[tweakingDisplaySampleCount];
			if (isAlreadyDisplayingAValue)
				lineRenderer.GetPositions(positions);
			for (int i = 0; i < tweakingDisplaySampleCount; i ++)
			{
				float time = (float) i / (tweakingDisplaySampleCount - 1) * endTime;
				float value = modulationCurve.normalizedValueOverTimeCurve.Evaluate(time);
				Vector3 position = positions[i];
				float[] positionComponents = position.ToArray();
				positionComponents[timeDisplayAxisIndex] = time;
				positionComponents[valueDisplayAxisIndex] = value;
				positions[i] = positionComponents.ToVec3();
			}
			lineRenderer.positionCount = tweakingDisplaySampleCount;
			lineRenderer.SetPositions(positions);
			lineRenderer.enabled = true;
		}

		public override void OnDeleted ()
		{
			base.OnDeleted ();
			modulateChildrenOption.onAddChild -= OnAddChildToModulate;
			modulateChildrenOption.onRemoveChild -= OnRemoveChildToModulate;
			modulator.onUncontrolled -= OnModulatorUncontrolled;
			modulator.onValueChanged -= OnModulatorValueChanged;
			displayingModulationOptionCurves.Remove(this);
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetModulationCurveOfData ();
		}

		public void SetModulationCurveOfData ()
		{
			_Data.modulationCurve = modulationCurve;
		}

		public void SetModulationCurveFromData ()
		{
			modulationCurve = _Data.modulationCurve;
		}

		public enum DisplayAxis
		{
			X,
			Y,
			Z
		}

		[Serializable]
		public struct ModulationTarget
		{
			public TypingTargetOption typingTargetOption;
			public ModulationTargetValueRangeOption valueRangeOption;
		}

		[Serializable]
		public class Data : ToggleChildrenOption.Data
		{
			public ModulationCurve modulationCurve;
			
			public override object MakeAsset ()
			{
				ModulationOption modulationOption = ObjectPool.instance.SpawnComponent<ModulationOption>(EternityEngine.instance.modulationOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (modulationOption);
				modulationOption.Init ();
				return modulationOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				ModulationOption modulationOption = (ModulationOption) asset;
				modulationOption._Data = this;
				modulationOption.SetModulationCurveFromData ();
			}
		}
	}
}