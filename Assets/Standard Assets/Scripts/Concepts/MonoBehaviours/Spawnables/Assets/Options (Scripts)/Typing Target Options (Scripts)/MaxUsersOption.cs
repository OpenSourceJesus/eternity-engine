using System;

namespace _EternityEngine
{
	public class MaxUsersOption : TypingTargetOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}

		public override bool SetValue (string value)
		{
			if (base.SetValue(value))
			{
				float? maxUserCount = GetNumberValue();
				if (maxUserCount != null)
					DRMManager.usersPerLicense = (int) (float) maxUserCount;
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
		}

		[Serializable]
		public class Data : TypingTargetOption.Data
		{
			public override object MakeAsset ()
			{
				MaxUsersOption maxUsersOption = ObjectPool.instance.SpawnComponent<MaxUsersOption>(EternityEngine.instance.maxUsersOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (maxUsersOption);
				maxUsersOption.Init ();
				return maxUsersOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				MaxUsersOption maxUsersOption = (MaxUsersOption) asset;
				maxUsersOption._Data = this;
			}
		}
	}
}