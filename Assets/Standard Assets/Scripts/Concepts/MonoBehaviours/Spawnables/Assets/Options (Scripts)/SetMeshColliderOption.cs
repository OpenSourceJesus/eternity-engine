using System;
using UnityEngine;

namespace _EternityEngine
{
	public class SetMeshColliderOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public ModelFileInstanceOption modelFileInstanceOption;

		public override void OnActivate (EternityEngine.Hand hand)
		{
			if (Exists(modelFileInstanceOption))
				modelFileInstanceOption.SetMeshCollider ();
			// base.OnActivate (hand);
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetModelFileInstanceOptionNameOfData ();
		}

		void SetModelFileInstanceOptionNameOfData ()
		{
			if (Exists(modelFileInstanceOption))
				_Data.modelFileInstanceOptionName = modelFileInstanceOption.name;
		}

		void SetModelFileInstanceOptionNameFromData ()
		{
			if (_Data.modelFileInstanceOptionName != null)
				modelFileInstanceOption = EternityEngine.GetOption<ModelFileInstanceOption>(_Data.modelFileInstanceOptionName);
		}

		[Serializable]
		public class Data : Option.Data
		{
			public string modelFileInstanceOptionName = null;

			public override object MakeAsset ()
			{
				SetMeshColliderOption setMeshColliderOption = ObjectPool.instance.SpawnComponent<SetMeshColliderOption>(EternityEngine.instance.setMeshColliderOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (setMeshColliderOption);
				setMeshColliderOption.Init ();
				return setMeshColliderOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				SetMeshColliderOption setMeshColliderOption = (SetMeshColliderOption) asset;
				setMeshColliderOption._Data = this;
				setMeshColliderOption.SetModelFileInstanceOptionNameFromData ();
			}
		}
	}
}