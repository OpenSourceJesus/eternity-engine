using System;
using Extensions;
using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class ArbitraryIntOption : TypingTargetOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}

		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
		}

		[Serializable]
		public class Data : TypingTargetOption.Data
		{
			public override object MakeAsset ()
			{
				ArbitraryIntOption arbitraryIntOption = ObjectPool.instance.SpawnComponent<ArbitraryIntOption>(EternityEngine.instance.arbitraryIntOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (arbitraryIntOption);
				arbitraryIntOption.Init ();
				return arbitraryIntOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				ArbitraryIntOption arbitraryIntOption = (ArbitraryIntOption) asset;
				arbitraryIntOption._Data = this;
			}
		}
	}
}