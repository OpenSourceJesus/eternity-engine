using System;

namespace _EternityEngine
{
	public class SoundVolumeOption : TypingTargetOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public SoundFileInstanceOption soundFileInstanceOption;

		public override bool SetValue (string value)
		{
			if (base.SetValue(value))
			{
				if (Exists(soundFileInstanceOption))
				{
					float? volume = GetNumberValue();
					if (volume != null)
						soundFileInstanceOption.SetVolume ((float) volume);
				}
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetSoundFileInstanceOptionNameOfData ();
		}

		void SetSoundFileInstanceOptionNameOfData ()
		{
			if (Exists(soundFileInstanceOption))
				_Data.soundFileInstanceOptionName = soundFileInstanceOption.name;
		}

		void SetSoundFileInstanceOptionNameFromData ()
		{
			if (_Data.soundFileInstanceOptionName != null)
				soundFileInstanceOption = EternityEngine.GetOption<SoundFileInstanceOption>(_Data.soundFileInstanceOptionName);
		}

		[Serializable]
		public class Data : TypingTargetOption.Data
		{
			public string soundFileInstanceOptionName = null;

			public override object MakeAsset ()
			{
				SoundVolumeOption soundVolumeOption = ObjectPool.instance.SpawnComponent<SoundVolumeOption>(EternityEngine.instance.soundVolumeOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (soundVolumeOption);
				soundVolumeOption.Init ();
				return soundVolumeOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				SoundVolumeOption soundVolumeOption = (SoundVolumeOption) asset;
				soundVolumeOption._Data = this;
				soundVolumeOption.SetSoundFileInstanceOptionNameFromData ();
			}
		}
	}
}
