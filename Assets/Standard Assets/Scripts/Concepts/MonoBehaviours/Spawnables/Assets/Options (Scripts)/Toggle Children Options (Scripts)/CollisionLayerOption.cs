
using System;

namespace _EternityEngine
{
	public class CollisionLayerOption : ToggleChildrenOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}

		public override void OnActivate (EternityEngine.Hand hand)
		{
			
			// base.OnActivate (hand);
		}

		public override bool SetValue (string value)
		{
			bool output = base.SetValue(value);

			return output;
		}

		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
		}

		
		[Serializable]
		public class Data : ToggleChildrenOption.Data
		{

			public override object MakeAsset ()
			{
				CollisionLayerOption collisionLayerOption = ObjectPool.instance.SpawnComponent<CollisionLayerOption>(EternityEngine.instance.collisionLayerOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (collisionLayerOption);
				collisionLayerOption.Init ();
				return collisionLayerOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				CollisionLayerOption collisionLayerOption = (CollisionLayerOption) asset;
				collisionLayerOption._Data = this;
			}
		}
	}
}