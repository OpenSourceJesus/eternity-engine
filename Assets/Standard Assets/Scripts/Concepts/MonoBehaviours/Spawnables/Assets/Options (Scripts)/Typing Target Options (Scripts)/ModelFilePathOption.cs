using System;
using Extensions;

namespace _EternityEngine
{
	public class ModelFilePathOption : TypingTargetOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public ImportModelWithFileNameOption importModelWithFileNameOption;

		public override bool SetValue (string value)
		{
			if (base.SetValue(value))
			{
				if (Exists(importModelWithFileNameOption))
					importModelWithFileNameOption.modelFilePath = value;
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetImportModelWithFileNameOptionNameOfData ();
		}

		void SetImportModelWithFileNameOptionNameOfData ()
		{
			if (Exists(importModelWithFileNameOption))
				_Data.importModelWithFileNameOptionName = importModelWithFileNameOption.name;
		}

		void SetImportModelWithFileNameOptionNameFromData ()
		{
			if (_Data.importModelWithFileNameOptionName != null)
				importModelWithFileNameOption = EternityEngine.GetOption<ImportModelWithFileNameOption>(_Data.importModelWithFileNameOptionName);
		}

		[Serializable]
		public class Data : TypingTargetOption.Data
		{
			public string importModelWithFileNameOptionName = null;

			public override object MakeAsset ()
			{
				ModelFilePathOption modelFilePathOption = ObjectPool.instance.SpawnComponent<ModelFilePathOption>(EternityEngine.instance.modelFilePathOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (modelFilePathOption);
				modelFilePathOption.Init ();
				return modelFilePathOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				ModelFilePathOption modelFilePathOption = (ModelFilePathOption) asset;
				modelFilePathOption._Data = this;
				modelFilePathOption.SetImportModelWithFileNameOptionNameFromData ();
			}
		}
	}
}
