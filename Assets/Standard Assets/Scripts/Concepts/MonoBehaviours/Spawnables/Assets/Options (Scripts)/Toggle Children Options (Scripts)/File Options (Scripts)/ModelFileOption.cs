using System;
using System.IO;
using TriLibCore;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class ModelFileOption : FileOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public GameObject modelGo;
		string fileExtension;
		byte[] fileData;

		public void Init (string path, GameObject modelGo)
		{
			Init (path);
			this.modelGo = modelGo;
		}

		public override void Init (string path)
		{
			base.Init (path);
			int indexOfFileExtension = path.LastIndexOf(".");
			fileExtension = path.Substring(indexOfFileExtension + 1);
			fileData = File.ReadAllBytes(path);
		}

		void OnModelLoadError (IContextualizedError error)
		{
			print(error.GetInnerException());
		}

		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetFileExtensionOfData ();
			SetFileDataOfData ();
		}

		void SetFileExtensionOfData ()
		{
			_Data.fileExtension = fileExtension;
		}

		void SetFileExtensionFromData ()
		{
			fileExtension = _Data.fileExtension;
		}

		void SetFileDataOfData ()
		{
			_Data.fileData = fileData;
		}

		void SetFileDataFromData ()
		{
			int fileIndex = 0;
			do
			{
				path = Application.persistentDataPath + Path.DirectorySeparatorChar + fileIndex + "." + fileExtension;
				fileIndex ++;
			} while (File.Exists(path));
			fileData = _Data.fileData;
			File.WriteAllBytes(path, fileData);
			AssetLoaderContext assetLoaderContext = AssetLoader.LoadModelFromFileNoThread(path, OnModelLoadError, new GameObject(), AssetLoader.CreateDefaultLoaderOptions());
			modelGo = assetLoaderContext.RootGameObject;
			modelGo.SetActive(false);
			File.Delete(path);
		}

		[Serializable]
		public class Data : FileOption.Data
		{
			public string fileExtension;
			public byte[] fileData;

			public override object MakeAsset ()
			{
				ModelFileOption modelOption = ObjectPool.instance.SpawnComponent<ModelFileOption>(EternityEngine.instance.modelFileOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (modelOption);
				modelOption.Init ();
				return modelOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				ModelFileOption modelOption = (ModelFileOption) asset;
				modelOption._Data = this;
				modelOption.SetFileExtensionFromData ();
				modelOption.SetFileDataFromData ();
			}
		}
	}
}