using System;

namespace _EternityEngine
{
	public class SetterTooManyChildrenBehaviourOption : EnumOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public SetterOption setterOption;

		public override bool SetValue (int value)
		{
			if (base.SetValue(value))
			{
				if (Exists(setterOption))
					setterOption.ApplyTooManyChildrenBehaviour ((SetterOption.TooManyChildrenBehaviour) Enum.ToObject(enumType, value));
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetSetterOptionNameOfData ();
		}

		void SetSetterOptionNameOfData ()
		{
			if (Exists(setterOption))
				_Data.setterOptionName = setterOption.name;
		}

		void SetSetterOptionNameFromData ()
		{
			if (_Data.setterOptionName != null)
				setterOption = EternityEngine.GetOption<SetterOption>(_Data.setterOptionName);
		}

		[Serializable]
		public class Data : EnumOption.Data
		{
			public string setterOptionName = null;

			public override object MakeAsset ()
			{
				SetterTooManyChildrenBehaviourOption setterTooManyChildrenBehaviourOption = ObjectPool.instance.SpawnComponent<SetterTooManyChildrenBehaviourOption>(EternityEngine.instance.setterTooManyChildrenBehaviourOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (setterTooManyChildrenBehaviourOption);
				setterTooManyChildrenBehaviourOption.Init ();
				return setterTooManyChildrenBehaviourOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				SetterTooManyChildrenBehaviourOption setterTooManyChildrenBehaviourOption = (SetterTooManyChildrenBehaviourOption) asset;
				setterTooManyChildrenBehaviourOption._Data = this;
				setterTooManyChildrenBehaviourOption.SetSetterOptionNameFromData ();
			}
		}
	}
}