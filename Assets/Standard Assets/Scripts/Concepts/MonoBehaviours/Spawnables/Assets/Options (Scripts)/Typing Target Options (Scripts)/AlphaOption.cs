using System;

namespace _EternityEngine
{
	public class AlphaOption : TypingTargetOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public ColorOption colorOption;

		public override bool SetValue (string value)
		{
			if (base.SetValue(value))
			{
				if (Exists(colorOption))
				{
					float? a = GetNumberValue();
					if (a != null)
						colorOption.SetA ((float) a);
				}
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetColorOptionNameOfData ();
		}

		void SetColorOptionNameOfData ()
		{
			if (Exists(colorOption))
				_Data.colorOptionName = colorOption.name;
		}

		void SetColorOptionNameFromData ()
		{
			if (_Data.colorOptionName != null)
				colorOption = EternityEngine.GetOption<ColorOption>(_Data.colorOptionName);
		}

		[Serializable]
		public class Data : TypingTargetOption.Data
		{
			public string colorOptionName = null;

			public override object MakeAsset ()
			{
				AlphaOption alphaOption = ObjectPool.instance.SpawnComponent<AlphaOption>(EternityEngine.instance.alphaOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (alphaOption);
				alphaOption.Init ();
				return alphaOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				AlphaOption alphaOption = (AlphaOption) asset;
				alphaOption._Data = this;
				alphaOption.SetColorOptionNameFromData ();
			}
		}
	}
}
