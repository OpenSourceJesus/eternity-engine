using System;

namespace _EternityEngine
{
	public class GetVector3XOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public override string NameToValueSeparator
		{
			get
			{
				return ": ";
			}
		}
		public Vector3Option vector3Option;

		public override void OnActivate (EternityEngine.Hand hand)
		{
			if (Exists(vector3Option))
			{
				vector3Option.UpdateValue ();
				SetValue ("" + vector3Option.value.x);
			}
			// base.OnActivate (hand);
		}

		public override bool SetValue (string value)
		{
			if (base.SetValue(value))
			{
				if (Exists(vector3Option))
				{
					float? x = TypingTargetOption.GetNumberValue(GetValue());
					if (x != null)
						vector3Option.SetX ((float) x);
				}
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetVector3OptionNameOfData ();
		}

		void SetVector3OptionNameOfData ()
		{
			if (Exists(vector3Option))
				_Data.vector3OptionName = vector3Option.name;
		}

		void SetVector3OptionNameFromData ()
		{
			if (_Data.vector3OptionName != null)
				vector3Option = EternityEngine.GetOption<Vector3Option>(_Data.vector3OptionName);
		}

		[Serializable]
		public class Data : Option.Data
		{
			public string vector3OptionName = null;

			public override object MakeAsset ()
			{
				GetVector3XOption getVector3XOption = ObjectPool.instance.SpawnComponent<GetVector3XOption>(EternityEngine.instance.getVector3XOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (getVector3XOption);
				getVector3XOption.Init ();
				return getVector3XOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				GetVector3XOption getVector3XOption = (GetVector3XOption) asset;
				getVector3XOption._Data = this;
				getVector3XOption.SetVector3OptionNameFromData ();
			}
		}
	}
}