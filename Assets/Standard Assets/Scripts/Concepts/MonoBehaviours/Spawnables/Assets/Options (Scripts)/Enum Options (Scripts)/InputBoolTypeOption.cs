using System;

namespace _EternityEngine
{
	public class InputBoolTypeOption : EnumOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public InputBoolOption inputBoolOption;

		public override bool SetValue (int value)
		{
			if (base.SetValue(value))
			{
				if (Exists(inputBoolOption))
					inputBoolOption.type = (InputBoolOption.Type) Enum.ToObject(enumType, value);
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetInputBoolOptionNameOfData ();
		}

		void SetInputBoolOptionNameOfData ()
		{
			if (Exists(inputBoolOption))
				_Data.inputBoolOptionName = inputBoolOption.name;
		}

		void SetInputBoolOptionNameFromData ()
		{
			if (_Data.inputBoolOptionName != null)
				inputBoolOption = EternityEngine.GetOption<InputBoolOption>(_Data.inputBoolOptionName);
		}

		[Serializable]
		public class Data : EnumOption.Data
		{
			public string inputBoolOptionName = null;

			public override object MakeAsset ()
			{
				InputBoolTypeOption inputBoolTypeOption = ObjectPool.instance.SpawnComponent<InputBoolTypeOption>(EternityEngine.instance.inputBoolTypeOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (inputBoolTypeOption);
				inputBoolTypeOption.Init ();
				return inputBoolTypeOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				InputBoolTypeOption inputBoolTypeOption = (InputBoolTypeOption) asset;
				inputBoolTypeOption._Data = this;
				inputBoolTypeOption.SetInputBoolOptionNameFromData ();
			}
		}
	}
}