using TMPro;
using System;
using System.IO;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class SoundFileOption : FileOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public AudioClip audioClip;

		public void Init (string filePath, AudioClip audioClip)
		{
			Init (filePath);
			this.audioClip = audioClip;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetAudioClipDataOfData ();
		}

		void SetAudioClipDataOfData ()
		{
			if (audioClip != null)
				_Data.audioClipData = new AudioUtilities.AudioClipData(audioClip);
		}

		void SetAudioClipDataFromData ()
		{
			if (!_Data.audioClipData.Equals(default(AudioUtilities.AudioClipData)))
				audioClip = _Data.audioClipData.ToAudioClip();
		}

		[Serializable]
		public class Data : FileOption.Data
		{
			public AudioUtilities.AudioClipData audioClipData;
			
			public override object MakeAsset ()
			{
				SoundFileOption soundFileOption = ObjectPool.instance.SpawnComponent<SoundFileOption>(EternityEngine.instance.soundFileOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (soundFileOption);
				soundFileOption.Init ();
				return soundFileOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				SoundFileOption soundFileOption = (SoundFileOption) asset;
				soundFileOption._Data = this;
				soundFileOption.SetAudioClipDataFromData ();
			}
		}
	}
}