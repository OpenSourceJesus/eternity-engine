using System;
using Extensions;

namespace _EternityEngine
{
	public class AutomationSequenceEntryIndexOption : TypingTargetOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public AutomationSequenceEntryOption automationSequenceEntryOption;

		public override void Init ()
		{
			base.Init ();
			if (Exists(automationSequenceEntryOption))
				automationSequenceEntryOption.automationSequenceEntryIndexOptions = automationSequenceEntryOption.automationSequenceEntryIndexOptions.Add(this);
		}

		public override void OnDeleted ()
		{
			base.OnDeleted ();
			if (Exists(automationSequenceEntryOption))
				automationSequenceEntryOption.automationSequenceEntryIndexOptions = automationSequenceEntryOption.automationSequenceEntryIndexOptions.Remove(this);
		}

		public override bool SetValue (string value)
		{
			if (base.SetValue(value))
			{
				if (Exists(automationSequenceEntryOption))
				{
					float? index = GetNumberValue();
					if (index != null)
						automationSequenceEntryOption.SetSequenceEntryIndex ((int) (float) index);
				}
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetAutomationSequenceEntryIndexOptionNameOfData ();
		}

		void SetAutomationSequenceEntryIndexOptionNameOfData ()
		{
			if (Exists(automationSequenceEntryOption))
				_Data.automationSequenceEntryOptionName = automationSequenceEntryOption.name;
		}

		void SetAutomationSequenceEntryIndexOptionNameFromData ()
		{
			if (_Data.automationSequenceEntryOptionName != null)
				automationSequenceEntryOption = EternityEngine.GetOption<AutomationSequenceEntryOption>(_Data.automationSequenceEntryOptionName);
		}

		[Serializable]
		public class Data : TypingTargetOption.Data
		{
			public string automationSequenceEntryOptionName = null;

			public override object MakeAsset ()
			{
				AutomationSequenceEntryIndexOption automationSequenceEntryIndexOption = ObjectPool.instance.SpawnComponent<AutomationSequenceEntryIndexOption>(EternityEngine.instance.automationSequenceEntryIndexOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (automationSequenceEntryIndexOption);
				// if (!automationSequenceEntryIndexOption.isInitialized)
				// 	automationSequenceEntryIndexOption.Init ();
				return automationSequenceEntryIndexOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				AutomationSequenceEntryIndexOption automationSequenceEntryIndexOption = (AutomationSequenceEntryIndexOption) asset;
				automationSequenceEntryIndexOption._Data = this;
				automationSequenceEntryIndexOption.SetAutomationSequenceEntryIndexOptionNameFromData ();
			}
		}
	}
}
