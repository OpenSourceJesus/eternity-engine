using System;

namespace _EternityEngine
{
	public class AngularDragOption : TypingTargetOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public ModelFileInstanceOption modelFileInstanceOption;

		public override bool SetValue (string value)
		{
			if (base.SetValue(value))
			{
				if (Exists(modelFileInstanceOption))
				{
					float? angularDrag = GetNumberValue();
					if (angularDrag != null)
						modelFileInstanceOption.SetAngularDrag ((float) angularDrag);
				}
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetModelFileInstanceOptionNameOfData ();
		}

		void SetModelFileInstanceOptionNameOfData ()
		{
			if (Exists(modelFileInstanceOption))
				_Data.modelFileInstanceOptionName = modelFileInstanceOption.name;
		}

		void SetModelFileInstanceOptionNameFromData ()
		{
			if (_Data.modelFileInstanceOptionName != null)
				modelFileInstanceOption = EternityEngine.GetOption<ModelFileInstanceOption>(_Data.modelFileInstanceOptionName);
		}

		[Serializable]
		public class Data : TypingTargetOption.Data
		{
			public string modelFileInstanceOptionName = null;

			public override object MakeAsset ()
			{
				AngularDragOption angularDragOption = ObjectPool.instance.SpawnComponent<AngularDragOption>(EternityEngine.instance.angularDragOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (angularDragOption);
				angularDragOption.Init ();
				return angularDragOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				AngularDragOption angularDragOption = (AngularDragOption) asset;
				angularDragOption._Data = this;
				angularDragOption.SetModelFileInstanceOptionNameFromData ();
			}
		}
	}
}