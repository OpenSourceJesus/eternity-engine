using System;
using Extensions;
using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class Vector3Option : ToggleChildrenOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public Vector3 value;
		public GetVector3XOption getXOption;
		public GetVector3YOption getYOption;
		public GetVector3ZOption getZOption;

		public virtual void UpdateValue ()
		{
		}

		public void UpdateTexts ()
		{
			UpdateXText ();
			UpdateYText ();
			UpdateZText ();
		}

		public void UpdateXText ()
		{
			if (Exists(getXOption))
				getXOption.SetValue ("" + value.x);
		}

		public void UpdateYText ()
		{
			if (Exists(getYOption))
				getYOption.SetValue ("" + value.y);
		}

		public void UpdateZText ()
		{
			if (Exists(getZOption))
				getZOption.SetValue ("" + value.z);
		}

		public virtual bool SetX (float x)
		{
			if (value.x == x)
				return false;
			value.x = x;
			return true;
		}

		public virtual bool SetY (float y)
		{
			if (value.y == y)
				return false;
			value.y = y;
			return true;
		}

		public virtual bool SetZ (float z)
		{
			if (value.z == z)
				return false;
			value.z = z;
			return true;
		}

		public virtual bool SetValue (Vector3 value)
		{
			if (this.value == value)
				return false;
			this.value = value;
			return true;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetGetXOptionNameOfData ();
			SetGetYOptionNameOfData ();
			SetGetZOptionNameOfData ();
			SetValueOfData ();
		}

		void SetValueOfData ()
		{
			_Data.value = _Vector3.FromVec3(value);
		}

		void SetValueFromData ()
		{
			value = _Data.value.ToVec3();
		}

		void SetGetXOptionNameOfData ()
		{
			if (Exists(getXOption))
				_Data.getXOptionName = getXOption.name;
		}

		void SetGetXOptionNameFromData ()
		{
			if (_Data.getXOptionName != null)
				getXOption = EternityEngine.GetOption<GetVector3XOption>(_Data.getXOptionName);
		}

		void SetGetYOptionNameOfData ()
		{
			if (Exists(getYOption))
				_Data.getYOptionName = getYOption.name;
		}

		void SetGetYOptionNameFromData ()
		{
			if (_Data.getYOptionName != null)
				getYOption = EternityEngine.GetOption<GetVector3YOption>(_Data.getYOptionName);
		}

		void SetGetZOptionNameOfData ()
		{
			if (Exists(getZOption))
				_Data.getZOptionName = getZOption.name;
		}

		void SetGetZOptionNameFromData ()
		{
			if (_Data.getZOptionName != null)
				getZOption = EternityEngine.GetOption<GetVector3ZOption>(_Data.getZOptionName);
		}

		[Serializable]
		public class Data : ToggleChildrenOption.Data
		{
			public _Vector3 value;
			public string getXOptionName = null;
			public string getYOptionName = null;
			public string getZOptionName = null;

			public override object MakeAsset ()
			{
				Vector3Option vector3Option = ObjectPool.instance.SpawnComponent<Vector3Option>(EternityEngine.instance.vector3OptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (vector3Option);
				vector3Option.Init ();
				return vector3Option;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				Vector3Option vector3Option = (Vector3Option) asset;
				vector3Option._Data = this;
				vector3Option.SetGetXOptionNameFromData ();
				vector3Option.SetGetYOptionNameFromData ();
				vector3Option.SetGetZOptionNameFromData ();
				vector3Option.SetValueFromData ();
			}
		}
	}
}