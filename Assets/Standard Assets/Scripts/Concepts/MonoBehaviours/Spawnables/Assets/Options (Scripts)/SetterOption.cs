using System;
using Extensions;
using UnityEngine;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class SetterOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public InvalidChildBehaviour invalidChildBehaviour;
		public TooManyChildrenBehaviour tooManyChildrenBehaviour;
		public Option valuesToSetOptionsParent;
		public Option valueToSetToOptionParent;
		List<Option> valuesToSetOptions = new List<Option>();
		Option valueToSetToOption;

		public override void Init ()
		{
			base.Init ();
			valuesToSetOptionsParent.onAddChild += OnAddChild_ValuesToSet;
			valuesToSetOptionsParent.onRemoveChild += OnRemoveChild_ValuesToSet;
			valuesToSetOptionsParent.onAboutToAddChild += OnAboutToAddChild_ValuesToSet;
			valueToSetToOptionParent.onAddChild += OnAddChild_ValueToSetTo;
			valueToSetToOptionParent.onAboutToAddChild += OnAboutToAddChild_ValueToSetTo;
			valueToSetToOptionParent.onRemoveChild += OnRemoveChild_ValueToSetTo;
			valueToSetToOptionParent.onAboutToRemoveChild += OnAboutToRemoveChild_ValueToSetTo;
		}

		bool OnAboutToAddChild_ValuesToSet (Option child)
		{
			if (invalidChildBehaviour == InvalidChildBehaviour.Disallow && Exists(valueToSetToOption) && !IsValueToSetValid(child))
				return false;
			else
				return true;
		}

		void OnAddChild_ValuesToSet (Option child)
		{
			if (valuesToSetOptionsParent.children.Count == 1)
				valuesToSetOptions.Add(child);
			else
				ApplyInvalidChildBehaviour ();
			if (!activatable)
				SetActivatable (valuesToSetOptions.Count > 0 && Exists(valueToSetToOption));
		}
		
		void OnRemoveChild_ValuesToSet (Option child)
		{
			valuesToSetOptions.Remove(child);
			if (activatable)
				SetActivatable (valuesToSetOptions.Count > 0 && Exists(valueToSetToOption));
		}

		bool OnAboutToAddChild_ValueToSetTo (Option child)
		{
			if (tooManyChildrenBehaviour == TooManyChildrenBehaviour.Disallow && valueToSetToOptionParent.children.Count >= 1)
				return false;
			else
				return true;
		}

		void OnAddChild_ValueToSetTo (Option child)
		{
			if (valueToSetToOptionParent.children.Count == 1)
				valueToSetToOption = child;
			else
				ApplyTooManyChildrenBehaviour ();
			ApplyInvalidChildBehaviour ();
			SetActivatable (valuesToSetOptions.Count > 0);
		}

		bool OnAboutToRemoveChild_ValueToSetTo (Option child)
		{
			if (invalidChildBehaviour == InvalidChildBehaviour.Disallow)
			{
				if (tooManyChildrenBehaviour == TooManyChildrenBehaviour.UseMostRecentChild)
				{
					for (int i = 0; i < valuesToSetOptions.Count; i ++)
					{
						Option valueToSetOption = valuesToSetOptions[i];
						if (!CanSetValue(valueToSetOption, valuesToSetOptions[valuesToSetOptions.Count - 1]))
							return false;
					}
					return true;
				}
				else if (tooManyChildrenBehaviour == TooManyChildrenBehaviour.UseLeastRecentChild)
				{
					for (int i = 0; i < valuesToSetOptions.Count; i ++)
					{
						Option valueToSetOption = valuesToSetOptions[i];
						if (!CanSetValue(valueToSetOption, valuesToSetOptions[0]))
							return false;
					}
					return true;
				}
				else
					return true;
			}
			else
				return true;
		}

		void OnRemoveChild_ValueToSetTo (Option child)
		{
			if (valueToSetToOptionParent.children.Count == 0)
			{
				valueToSetToOption = null;
				SetActivatable (false);
				return;
			}
			if (tooManyChildrenBehaviour == TooManyChildrenBehaviour.UseMostRecentChild)
				valueToSetToOption = valueToSetToOptionParent.children[valueToSetToOptionParent.children.Count - 1];
			else if (tooManyChildrenBehaviour == TooManyChildrenBehaviour.UseMostRecentChild)
				valueToSetToOption = valueToSetToOptionParent.children[0];
			else if (tooManyChildrenBehaviour == TooManyChildrenBehaviour.UnparentMostRecentChildren)
				valueToSetToOption = valueToSetToOptionParent.children[valueToSetToOptionParent.children.Count - 1];
			else// if (tooManyChildrenBehaviour == TooManyChildrenBehaviour.UnparentLeastRecentChildren)
				valueToSetToOption = valueToSetToOptionParent.children[0];
			ApplyInvalidChildBehaviour ();
			SetActivatable (valuesToSetOptions.Count > 0);
		}

		public void ApplyInvalidChildBehaviour (InvalidChildBehaviour invalidChildBehaviour)
		{
			this.invalidChildBehaviour = invalidChildBehaviour;
			if (Exists(valuesToSetOptionsParent))
				ApplyInvalidChildBehaviour ();
		}

		void ApplyInvalidChildBehaviour ()
		{
			valuesToSetOptions = new List<Option>(valuesToSetOptionsParent.children);
			if (invalidChildBehaviour == InvalidChildBehaviour.Unparent)
			{
				for (int i = 0; i < valuesToSetOptionsParent.children.Count; i ++)
				{
					Option valueToSetOption = valuesToSetOptionsParent.children[i];
					if (!IsValueToSetValid(valueToSetOption))
					{
						valuesToSetOptionsParent.RemoveChild (valueToSetOption);
						i --;
					}
				}
			}
			else if (invalidChildBehaviour == InvalidChildBehaviour.Disallow)
			{
				int removedValuesToSet = 0;
				for (int i = 0; i < valuesToSetOptionsParent.children.Count; i ++)
				{
					Option valueToSetOption = valuesToSetOptionsParent.children[i];
					if (!IsValueToSetValid(valueToSetOption))
					{
						valuesToSetOptions.RemoveAt(i - removedValuesToSet);
						removedValuesToSet ++;
					}
				}
			}
		}

		public void ApplyTooManyChildrenBehaviour (TooManyChildrenBehaviour tooManyChildrenBehaviour)
		{
			this.tooManyChildrenBehaviour = tooManyChildrenBehaviour;
			if (Exists(valuesToSetOptionsParent) && Exists(valueToSetToOptionParent))
				ApplyTooManyChildrenBehaviour ();
		}

		void ApplyTooManyChildrenBehaviour ()
		{
			ApplyTooManyChildrenBehaviour (valuesToSetOptionsParent.children, valueToSetToOptionParent.children);
		}

		void ApplyTooManyChildrenBehaviour (List<Option> valuesToSetChildren, List<Option> valueToSetToChildren)
		{
			if (valueToSetToChildren.Count < 2)
				return;
			if (tooManyChildrenBehaviour == TooManyChildrenBehaviour.UseMostRecentChild)
				valueToSetToOption = valuesToSetOptionsParent.children[valuesToSetOptionsParent.children.Count - 1];
			else if (tooManyChildrenBehaviour == TooManyChildrenBehaviour.UseLeastRecentChild)
				valueToSetToOption = valuesToSetOptionsParent.children[0];
			else if (tooManyChildrenBehaviour == TooManyChildrenBehaviour.UnparentMostRecentChildren)
			{
				while (valueToSetToChildren.Count > 1)
					valueToSetToOptionParent.RemoveChild (valueToSetToChildren[valueToSetToChildren.Count - 1]);
				valueToSetToOption = valueToSetToChildren[0];
			}
			else if (tooManyChildrenBehaviour == TooManyChildrenBehaviour.UnparentLeastRecentChildren)
			{
				while (valueToSetToChildren.Count > 1)
					valueToSetToOptionParent.RemoveChild (valueToSetToChildren[0]);
				valueToSetToOption = valueToSetToChildren[0];
			}
		}

		bool IsValueToSetValid (Option valueToSetOption)
		{
			return CanSetValue(valueToSetOption, valueToSetToOption);
		}

		bool CanSetValue (Option valueToSetOption, Option valueToSetToOption)
		{
			ValueType valueToSetValueType = valueToSetOption.valueType;
			ValueType valueToSetToValueType = valueToSetToOption.valueType;
			if (valueToSetValueType == ValueType.None || valueToSetToValueType == ValueType.None || valueToSetValueType == ValueType.Enum || valueToSetToValueType == ValueType.Enum)
				return false;
			else if (valueToSetValueType == valueToSetToValueType || (valueToSetValueType == ValueType.Int && valueToSetToValueType == ValueType.Float) || (valueToSetValueType == ValueType.Float && valueToSetToValueType == ValueType.Int) || (valueToSetValueType == ValueType.Text && valueToSetToValueType == ValueType.Int) || (valueToSetValueType == ValueType.Text && valueToSetToValueType == ValueType.Float) || (valueToSetValueType == ValueType.Text && valueToSetToValueType == ValueType.Bool))
				return true;
			else
				return false;
		}

		public override void OnActivate (EternityEngine.Hand hand)
		{
			for (int i = 0; i < valuesToSetOptions.Count; i ++)
			{
				Option valueToSetOption = valuesToSetOptions[i];
				string value = valueToSetToOption.GetValue();
				if (valueToSetOption.valueType == ValueType.Int && valueToSetToOption.valueType == ValueType.Float)
					valueToSetOption.SetValue ("" + (int) (float) TypingTargetOption.GetNumberValue(value));
				else
					valueToSetOption.SetValue (value);
			}
			// base.OnActivate (hand);
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetInvalidChildBehaviourOfData ();
			SetTooManyChildrenBehaviourOfData ();
			SetValuesToSetOptionsParentNameOfData ();
			SetValueToSetToOptionParentNameOfData ();
			SetValuesToSetOptionsNamesOfData ();
			SetValueToSetToOptionNameOfData ();
		}

		void SetInvalidChildBehaviourOfData ()
		{
			_Data.invalidChildBehaviour = invalidChildBehaviour;
		}

		void SetInvalidChildBehaviourFromData ()
		{
			invalidChildBehaviour = _Data.invalidChildBehaviour;
		}

		void SetTooManyChildrenBehaviourOfData ()
		{
			_Data.tooManyChildrenBehaviour = tooManyChildrenBehaviour;
		}

		void SetTooManyChildrenBehaviourFromData ()
		{
			tooManyChildrenBehaviour = _Data.tooManyChildrenBehaviour;
		}

		void SetValuesToSetOptionsParentNameOfData ()
		{
			if (Exists(valuesToSetOptionsParent))
				_Data.valuesToSetOptionsParentName = valuesToSetOptionsParent.name;
		}

		void SetValuesToSetOptionsParentNameFromData ()
		{
			if (_Data.valuesToSetOptionsParentName != null)
				valuesToSetOptionsParent = EternityEngine.GetOption(_Data.valuesToSetOptionsParentName);
		}

		void SetValueToSetToOptionParentNameOfData ()
		{
			if (Exists(valueToSetToOptionParent))
				_Data.valueToSetToOptionParentName = valueToSetToOptionParent.name;
		}

		void SetValueToSetToOptionParentNameFromData ()
		{
			if (_Data.valueToSetToOptionParentName != null)
				valueToSetToOptionParent = EternityEngine.GetOption(_Data.valueToSetToOptionParentName);
		}

		void SetValuesToSetOptionsNamesOfData ()
		{
			_Data.valuesToSetOptionsNames = new string[valuesToSetOptions.Count];
			for (int i = 0; i < valuesToSetOptions.Count; i ++)
			{
				Option valueToSetOption = valuesToSetOptions[i];
				if (Exists(valueToSetOption))
					_Data.valuesToSetOptionsNames[i] = valueToSetOption.name;
			}
		}

		void SetValuesToSetOptionsNamesFromData ()
		{
			valuesToSetOptions = new List<Option>();
			for (int i = 0; i < _Data.valuesToSetOptionsNames.Length; i ++)
			{
				string valueToSetOptionName = _Data.valuesToSetOptionsNames[i];
				valuesToSetOptions.Add(EternityEngine.GetOption(valueToSetOptionName));
			}
		}

		void SetValueToSetToOptionNameOfData ()
		{
			if (Exists(valueToSetToOption))
				_Data.valueToSetToOptionName = valueToSetToOption.name;
		}

		void SetValueToSetToOptionNameFromData ()
		{
			if (_Data.valueToSetToOptionName != null)
				valueToSetToOption = EternityEngine.GetOption(_Data.valueToSetToOptionName);
		}

		public enum InvalidChildBehaviour
		{
			Disallow,
			Unparent,
			Ignore
		}

		public enum TooManyChildrenBehaviour
		{
			Disallow,
			UseMostRecentChild,
			UseLeastRecentChild,
			UnparentMostRecentChildren,
			UnparentLeastRecentChildren
		}

		[Serializable]
		public class Data : Option.Data
		{
			public InvalidChildBehaviour invalidChildBehaviour;
			public TooManyChildrenBehaviour tooManyChildrenBehaviour;
			public string valuesToSetOptionsParentName = null;
			public string valueToSetToOptionParentName = null;
			public string[] valuesToSetOptionsNames = new string[0];
			public string valueToSetToOptionName = null;

			public override object MakeAsset ()
			{
				SetterOption setterOption = ObjectPool.instance.SpawnComponent<SetterOption>(EternityEngine.instance.setterOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (setterOption);
				setterOption.Init ();
				return setterOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				SetterOption setterOption = (SetterOption) asset;
				setterOption._Data = this;
				setterOption.SetInvalidChildBehaviourFromData ();
				setterOption.SetTooManyChildrenBehaviourFromData ();
				setterOption.SetValuesToSetOptionsParentNameFromData ();
				setterOption.SetValueToSetToOptionParentNameFromData ();
				setterOption.SetValuesToSetOptionsNamesFromData ();
				setterOption.SetValueToSetToOptionNameFromData ();
			}
		}
	}
}