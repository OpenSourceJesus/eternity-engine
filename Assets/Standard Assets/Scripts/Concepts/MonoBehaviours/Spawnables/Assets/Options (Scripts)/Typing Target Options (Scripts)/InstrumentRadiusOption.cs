using System;

namespace _EternityEngine
{
	public class InstrumentRadiusOption : TypingTargetOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public InstrumentOption instrumentOption;

		public override bool SetValue (string value)
		{
			if (base.SetValue(value))
			{
				if (Exists(instrumentOption))
				{
					float? radius = GetNumberValue();
					if (radius != null)
						instrumentOption.SetRadius ((float) radius);
				}
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetInstrumentOptionNameOfData ();
		}

		void SetInstrumentOptionNameOfData ()
		{
			if (Exists(instrumentOption))
				_Data.instrumentOptionName = instrumentOption.name;
		}

		void SetInstrumentOptionNameFromData ()
		{
			if (_Data.instrumentOptionName != null)
				instrumentOption = EternityEngine.GetOption<InstrumentOption>(_Data.instrumentOptionName);
		}

		[Serializable]
		public class Data : TypingTargetOption.Data
		{
			public string instrumentOptionName = null;

			public override object MakeAsset ()
			{
				InstrumentRadiusOption instrumentRadiusOption = ObjectPool.instance.SpawnComponent<InstrumentRadiusOption>(EternityEngine.instance.instrumentRadiusOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (instrumentRadiusOption);
				instrumentRadiusOption.Init ();
				return instrumentRadiusOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				InstrumentRadiusOption instrumentRadiusOption = (InstrumentRadiusOption) asset;
				instrumentRadiusOption._Data = this;
				instrumentRadiusOption.SetInstrumentOptionNameFromData ();
			}
		}
	}
}