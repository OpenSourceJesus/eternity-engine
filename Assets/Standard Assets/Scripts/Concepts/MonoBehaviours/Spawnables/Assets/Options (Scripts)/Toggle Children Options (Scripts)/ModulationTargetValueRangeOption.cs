using TMPro;
using System;
using Extensions;
using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class ModulationTargetValueRangeOption : ToggleChildrenOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public FloatRange valueRange;

		public void SetValueRangeMin (TypingTargetOption valueRangeMinTypingTargetOption)
		{
			float? valueRangeMin = valueRangeMinTypingTargetOption.GetNumberValue();
			if (valueRangeMin != null)
			valueRange.max = (float) valueRangeMin;
		}

		public void SetValueRangeMax (TypingTargetOption valueRangeMaxTypingTargetOption)
		{
			float? valueRangeMax = valueRangeMaxTypingTargetOption.GetNumberValue();
			if (valueRangeMax != null)
			valueRange.max = (float) valueRangeMax;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetValueRangeOfData ();
		}

		public void SetValueRangeOfData ()
		{
			_Data.valueRange = valueRange;
		}

		public void SetValueRangeFromData ()
		{
			valueRange = _Data.valueRange;
		}

		[Serializable]
		public class Data : ToggleChildrenOption.Data
		{
			public FloatRange valueRange;

			public override object MakeAsset ()
			{
				ModulationTargetValueRangeOption modulationTargetValueRangeOption = ObjectPool.instance.SpawnComponent<ModulationTargetValueRangeOption>(EternityEngine.instance.modulationTargetValueRangeOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (modulationTargetValueRangeOption);
				modulationTargetValueRangeOption.Init ();
				return modulationTargetValueRangeOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				ModulationTargetValueRangeOption modulationTargetValueRangeOption = (ModulationTargetValueRangeOption) asset;
				modulationTargetValueRangeOption._Data = this;
				modulationTargetValueRangeOption.SetValueRangeFromData ();
			}
		}
	}
}