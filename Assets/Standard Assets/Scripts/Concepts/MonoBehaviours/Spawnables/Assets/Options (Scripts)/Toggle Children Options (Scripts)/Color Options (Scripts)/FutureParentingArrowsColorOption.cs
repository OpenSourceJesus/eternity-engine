using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class FutureParentingArrowsColorOption : ColorOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}

		public override bool SetR (float r)
		{
			bool output = base.SetR(r);
			if (output)
				Apply ();
			return output;
		}

		public override bool SetG (float g)
		{
			bool output = base.SetG(g);
			if (output)
				Apply ();
			return output;
		}

		public override bool SetB (float b)
		{
			bool output = base.SetB(b);
			if (output)
				Apply ();
			return output;
		}

		public override bool SetA (float a)
		{
			bool output = base.SetA(a);
			if (output)
				Apply ();
			return output;
		}

		void Apply ()
		{
			SaveAndLoadManager.saveData.futureParentingArrowsColor = _Color.FromColor(value);
			SaveAndLoadManager.saveData.futureParentingArrowsColorHasBeenSet = true;
			EternityEngine.instance.parentingArrowPrefab.lineRenderer.sharedMaterial.color = value;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
		}

		[Serializable]
		public class Data : ColorOption.Data
		{
			public override object MakeAsset ()
			{
				FutureParentingArrowsColorOption futureParentingArrowsColorOption = ObjectPool.instance.SpawnComponent<FutureParentingArrowsColorOption>(EternityEngine.instance.futureParentingArrowsColorOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (futureParentingArrowsColorOption);
				futureParentingArrowsColorOption.Init ();
				return futureParentingArrowsColorOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				FutureParentingArrowsColorOption futureParentingArrowsColorOption = (FutureParentingArrowsColorOption) asset;
				futureParentingArrowsColorOption._Data = this;
			}
		}
	}
}