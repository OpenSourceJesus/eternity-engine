using TMPro;
using System;
using Extensions;
using UnityEngine;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class AutomationSequenceEntryOption : ToggleChildrenOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public AutomationSequence.SequenceEntry sequenceEntry;
		public List<AutomationOption> automationOptionsUsingMe = new List<AutomationOption>();
		public EternityEngine.Hand.Type madeByHandType;
		public AutomationSequence.InvalidOptionFixBehaviour invalidOptionFixBehaviour;
		public AutomationSequence.InvalidOptionContinueSequenceBehaviour invalidOptionContinueSequenceBehaviour;
		public AutomationSequenceEntryIndexOption[] automationSequenceEntryIndexOptions = new AutomationSequenceEntryIndexOption[0];
		public int sequenceEntryIndex;

		public void SetTime (float time)
		{
			sequenceEntry.time = time;
			for (int i = 0; i < automationOptionsUsingMe.Count; i ++)
			{
				AutomationOption automationOption = automationOptionsUsingMe[i];
				if (Exists(automationOption))
				{
					AutomationSequence.SequenceEntry sequenceEntry = automationOption.automationSequence.sequenceEntries[sequenceEntryIndex];
					automationOption.automationSequence.sequenceEntries[sequenceEntryIndex] = sequenceEntry;
				}
			}
		}

		public void SetSequenceEntryIndex (int newSequenceEntryIndex)
		{
			for (int i = 0; i < automationOptionsUsingMe.Count; i ++)
			{
				AutomationOption automationOption = automationOptionsUsingMe[i];
				if (Exists(automationOption))
				{
					if (automationOption.duplicateSequenceEntryIndexBehaviour == AutomationOption.DuplicateSequenceEntryIndexBehaviour.PreferSwitch)
					{
						automationOption.automationSequence.sequenceEntries[newSequenceEntryIndex] = automationOption.automationSequence.sequenceEntries[sequenceEntryIndex];
						automationOption.automationSequenceEntryOptions[newSequenceEntryIndex] = automationOption.automationSequenceEntryOptions[sequenceEntryIndex];
						automationOption.automationSequence.sequenceEntries[sequenceEntryIndex] = automationOption.automationSequence.sequenceEntries[newSequenceEntryIndex];
						automationOption.automationSequenceEntryOptions[sequenceEntryIndex] = automationOption.automationSequenceEntryOptions[newSequenceEntryIndex];
					}
					else if (automationOption.duplicateSequenceEntryIndexBehaviour == AutomationOption.DuplicateSequenceEntryIndexBehaviour.PreferInsert)
					{
						automationOption.automationSequence.sequenceEntries.Insert(newSequenceEntryIndex, sequenceEntry);
						automationOption.automationSequenceEntryOptions.Insert(newSequenceEntryIndex, this);
						for (int i2 = newSequenceEntryIndex + 1; i2 < automationOption.automationSequenceEntryOptions.Count; i2 ++)
							automationOption.automationSequenceEntryOptions[i2].sequenceEntryIndex ++;
					}
				}
			}
			sequenceEntryIndex = newSequenceEntryIndex;
		}

		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetSequenceEntryOfData ();
			SetNamesOfAutomationOptionsUsingMeOfData ();
			SetSequenceEntryIndexOfData ();
		}

		void SetSequenceEntryOfData ()
		{
			_Data.sequenceEntry = sequenceEntry;
		}

		void SetSequenceEntryFromData ()
		{
			sequenceEntry = _Data.sequenceEntry;
		}

		void SetNamesOfAutomationOptionsUsingMeOfData ()
		{
			_Data.namesOfAutomationOptionsUsingMe = new string[automationOptionsUsingMe.Count];
			for (int i = 0; i < automationOptionsUsingMe.Count; i ++)
			{
				AutomationOption automationOption = automationOptionsUsingMe[i];
				_Data.namesOfAutomationOptionsUsingMe[i] = automationOption.name;
			}
		}

		void SetNamesOfAutomationOptionsUsingMeFromData ()
		{
			for (int i = 0; i < _Data.namesOfAutomationOptionsUsingMe.Length; i ++)
			{
				string name = _Data.namesOfAutomationOptionsUsingMe[i];
				automationOptionsUsingMe.Add(EternityEngine.GetOption<AutomationOption>(name));
			}
		}

		void SetSequenceEntryIndexOfData ()
		{
			_Data.sequenceEntryIndex = sequenceEntryIndex;
		}

		void SetSequenceEntryIndexFromData ()
		{
			sequenceEntryIndex = _Data.sequenceEntryIndex;
		}

		[Serializable]
		public class Data : ToggleChildrenOption.Data
		{
			public AutomationSequence.SequenceEntry sequenceEntry;
			public string[] namesOfAutomationOptionsUsingMe = new string[0];
			public int sequenceEntryIndex;

			public override object MakeAsset ()
			{
				AutomationSequenceEntryOption automationSequenceEntryOption = ObjectPool.instance.SpawnComponent<AutomationSequenceEntryOption>(EternityEngine.instance.automationSequenceEntryOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (automationSequenceEntryOption);
				// if (!automationSequenceEntryOption.isInitialized)
				// 	automationSequenceEntryOption.Init ();
				return automationSequenceEntryOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				AutomationSequenceEntryOption automationSequenceEntryOption = (AutomationSequenceEntryOption) asset;
				automationSequenceEntryOption._Data = this;
				automationSequenceEntryOption.SetSequenceEntryFromData ();
				automationSequenceEntryOption.SetNamesOfAutomationOptionsUsingMeFromData ();
				automationSequenceEntryOption.SetSequenceEntryIndexFromData ();
			}
		}
	}
}