using System;
using System.IO;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class ImportImageWithFileNameOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public string imageFilePath;

		public override void OnActivate (EternityEngine.Hand hand)
		{
			Do (imageFilePath);
			// base.OnActivate (hand);
		}

		public static void Do (string imageFilePath)
		{
			Texture2D texture = new Texture2D(1, 1);
			ImageConversion.LoadImage(texture, File.ReadAllBytes(imageFilePath), false);
			EternityEngine.instance.SpawnOption<ImageFileOption> (EternityEngine.instance.imageFileOptionWithChildrenPrefab, (ImageFileOption imageFileOption) => { imageFileOption.Init (imageFilePath, texture); });
		}

		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetImageFilePathOfData ();
		}

		void SetImageFilePathOfData ()
		{
			_Data.imageFilePath = imageFilePath;
		}

		void SetImageFilePathFromData ()
		{
			imageFilePath = _Data.imageFilePath;
		}

		[Serializable]
		public class Data : Option.Data
		{
			public string imageFilePath;

			public override object MakeAsset ()
			{
				ImportImageWithFileNameOption importImageWithFileNameOption = ObjectPool.instance.SpawnComponent<ImportImageWithFileNameOption>(EternityEngine.instance.importImageWithFileNameOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (importImageWithFileNameOption);
				importImageWithFileNameOption.Init ();
				return importImageWithFileNameOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				ImportImageWithFileNameOption importImageWithFileNameOption = (ImportImageWithFileNameOption) asset;
				importImageWithFileNameOption._Data = this;
				importImageWithFileNameOption.SetImageFilePathFromData ();
			}
		}
	}
}