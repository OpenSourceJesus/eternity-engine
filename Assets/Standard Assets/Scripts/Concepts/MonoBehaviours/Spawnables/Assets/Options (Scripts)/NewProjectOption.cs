using System;

namespace _EternityEngine
{
	public class NewProjectOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		
		public override void OnActivate (EternityEngine.Hand hand)
		{
            Option.instances.Clear();
			SaveAndLoadManager.MostRecentSaveFileName = null;
			DRMManager.licenseId = "";
			_SceneManager.instance.RestartSceneWithoutTransition ();
			// base.OnActivate (hand);
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
		}

		[Serializable]
		public class Data : Option.Data
		{
			public override object MakeAsset ()
			{
				NewProjectOption newProjectOption = ObjectPool.instance.SpawnComponent<NewProjectOption>(EternityEngine.instance.newProjectOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (newProjectOption);
				newProjectOption.Init ();
				return newProjectOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				NewProjectOption newProjectOption = (NewProjectOption) asset;
				newProjectOption._Data = this;
			}
		}
	}
}