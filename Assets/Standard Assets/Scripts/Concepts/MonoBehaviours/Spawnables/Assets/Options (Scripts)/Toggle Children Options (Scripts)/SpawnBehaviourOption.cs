using System;
using Extensions;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class SpawnBehaviourOption : ToggleChildrenOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
        public int optionPrefabIndex;
		public DeleteCurrentBehaviour deleteCurrentBehaviour;
		public SortedDictionary<int, SpawnBehaviourOption> spawnedSpawnBehaviourOptionsDict = new SortedDictionary<int, SpawnBehaviourOption>();
		static int mostRecentCreatedOrder = -1;
		int activatedOrder;
		int createdOrder;
		SortedDictionary<int, SpawnBehaviourOption> activatedSpawnBehaviourOptionsDict = new SortedDictionary<int, SpawnBehaviourOption>();
		public Option[] selfAndAllDefaultChildren = new Option[0];
#if UNITY_EDITOR
		public bool updateSelfAndAllDefaultChildren;
		public Option[] extraOptions = new Option[0];
#endif

		public override void Init ()
		{
			base.Init ();
			if (EternityEngine.instance.currentSpawnBehaviourOptionsDict[optionPrefabIndex] == null)
				EternityEngine.instance.currentSpawnBehaviourOptionsDict[optionPrefabIndex] = this;
			activatedOrder = activatedSpawnBehaviourOptionsDict.Count;
			activatedSpawnBehaviourOptionsDict.Add(activatedOrder, this);
			mostRecentCreatedOrder ++;
			createdOrder = mostRecentCreatedOrder;
			spawnedSpawnBehaviourOptionsDict.Add(createdOrder, this);
		}

#if UNITY_EDITOR
		public override void OnValidate ()
		{
			base.OnValidate ();
			if (updateSelfAndAllDefaultChildren)
			{
				updateSelfAndAllDefaultChildren = false;
				selfAndAllDefaultChildren = GetAllChildrenAndSelf(this).RemoveEach(extraOptions);
			}
		}
#endif

		public void MakeCurrent ()
		{
			EternityEngine.instance.currentSpawnBehaviourOptionsDict[optionPrefabIndex] = this;
			activatedSpawnBehaviourOptionsDict.Remove(activatedOrder);
			activatedOrder = activatedSpawnBehaviourOptionsDict.Count - 1;
			activatedSpawnBehaviourOptionsDict.Add(activatedOrder, this);
		}

		public override void OnDeleted ()
		{
			base.OnDeleted ();
			activatedSpawnBehaviourOptionsDict.Remove(activatedOrder);
			spawnedSpawnBehaviourOptionsDict.Remove(createdOrder);
			if (deleteCurrentBehaviour == DeleteCurrentBehaviour.PreferUseMostRecentActiveThenMostRecentCreated)
			{
				if (!TryMakeCurrentMostRecentInDictionary(activatedSpawnBehaviourOptionsDict))
					TryMakeCurrentMostRecentInDictionary (spawnedSpawnBehaviourOptionsDict);
			}
			else if (deleteCurrentBehaviour == DeleteCurrentBehaviour.PreferUseMostRecentActiveThenLeastRecentCreated)
			{
				if (!TryMakeCurrentMostRecentInDictionary(activatedSpawnBehaviourOptionsDict))
					TryMakeCurrentLeastRecentInDictionary (spawnedSpawnBehaviourOptionsDict);
			}
			else if (deleteCurrentBehaviour == DeleteCurrentBehaviour.PreferUseLeastRecentActiveThenMostRecentCreated)
			{
				if (!TryMakeCurrentLeastRecentInDictionary(activatedSpawnBehaviourOptionsDict))
					TryMakeCurrentMostRecentInDictionary (spawnedSpawnBehaviourOptionsDict);
			}
			else if (deleteCurrentBehaviour == DeleteCurrentBehaviour.PreferUseLeastRecentActiveThenLeastRecentCreated)
			{
				if (!TryMakeCurrentLeastRecentInDictionary(activatedSpawnBehaviourOptionsDict))
					TryMakeCurrentLeastRecentInDictionary (spawnedSpawnBehaviourOptionsDict);
			}
			else if (deleteCurrentBehaviour == DeleteCurrentBehaviour.UseMostRecentCreated)
			{
				TryMakeCurrentMostRecentInDictionary (spawnedSpawnBehaviourOptionsDict);
			}
			else// if (deleteCurrentBehaviour == DeleteCurrentBehaviour.UseLeastRecentCreated)
			{
				TryMakeCurrentLeastRecentInDictionary (spawnedSpawnBehaviourOptionsDict);
			}
		}

		bool TryMakeCurrentLeastRecentInDictionary (SortedDictionary<int, SpawnBehaviourOption> dict)
		{
			if (dict.Count == 0)
				return false;
			SpawnBehaviourOption spawnBehaviourOption;
			int spawnBehaviourOptionIndex = 0;
			while (!dict.TryGetValue(spawnBehaviourOptionIndex, out spawnBehaviourOption))
			{
				spawnBehaviourOptionIndex ++;
				if (spawnBehaviourOptionIndex > mostRecentCreatedOrder)
					return false;
			}
			spawnBehaviourOption.MakeCurrent ();
			return true;
		}

		bool TryMakeCurrentMostRecentInDictionary (SortedDictionary<int, SpawnBehaviourOption> dict)
		{
			if (dict.Count == 0)
				return false;
			SpawnBehaviourOption spawnBehaviourOption;
			int spawnBehaviourOptionIndex = mostRecentCreatedOrder;
			while (!dict.TryGetValue(spawnBehaviourOptionIndex, out spawnBehaviourOption))
			{
				spawnBehaviourOptionIndex --;
				if (spawnBehaviourOptionIndex == -1)
					return false;
			}
			spawnBehaviourOption.MakeCurrent ();
			return true;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetActivatedOrderOfData ();
			SetCreatedOrderOfData ();
			SetOptionPrefabIndexOfData ();
			SetSelfAndAllDefaultChildNamesOfData ();
		}

		void SetActivatedOrderOfData ()
		{
			_Data.activatedOrder = activatedOrder;
		}

		void SetActivatedOrderFromData ()
		{
			activatedOrder = _Data.activatedOrder;
		}

		void SetCreatedOrderOfData ()
		{
			_Data.createdOrder = createdOrder;
		}

		void SetCreatedOrderFromData ()
		{
			createdOrder = _Data.createdOrder;
			if (createdOrder > mostRecentCreatedOrder)
				mostRecentCreatedOrder = createdOrder;
		}

		void SetOptionPrefabIndexOfData ()
		{
			_Data.optionPrefabIndex = optionPrefabIndex;
		}

		void SetOptionPrefabIndexFromData ()
		{
			optionPrefabIndex = _Data.optionPrefabIndex;
		}

		void SetSelfAndAllDefaultChildNamesOfData ()
		{
			_Data.selfAndAllDefaultChildNames = new string[selfAndAllDefaultChildren.Length];
			for (int i = 0; i < selfAndAllDefaultChildren.Length; i ++)
			{
				Option child = selfAndAllDefaultChildren[i];
				if (Exists(child))
					_Data.selfAndAllDefaultChildNames[i] = child.name;
				else
					_Data.selfAndAllDefaultChildNames[i] = null;
			}
		}

		void SetSelfAndAllDefaultChildNamesFromData ()
		{
			List<Option> selfAndAllDefaultChildren = new List<Option>();
			for (int i = 0; i < _Data.selfAndAllDefaultChildNames.Length; i ++)
			{
				string name = _Data.selfAndAllDefaultChildNames[i];
				if (name != null)
					selfAndAllDefaultChildren.Add(EternityEngine.GetOption(name));
				else
					selfAndAllDefaultChildren.Add(null);
			}
			this.selfAndAllDefaultChildren = selfAndAllDefaultChildren.ToArray();
		}

		public enum DeleteCurrentBehaviour
		{
			PreferUseMostRecentActiveThenMostRecentCreated,
			PreferUseMostRecentActiveThenLeastRecentCreated,
			PreferUseLeastRecentActiveThenMostRecentCreated,
			PreferUseLeastRecentActiveThenLeastRecentCreated,
			UseMostRecentCreated,
			UseLeastRecentCreated
		}

		[Serializable]
		public class Data : ToggleChildrenOption.Data
		{
			public string[] selfAndAllDefaultChildNames = new string[0];
			public int optionPrefabIndex;
			public int activatedOrder;
			public int createdOrder;

			public override object MakeAsset ()
			{
				SpawnBehaviourOption spawnBehaviourOption = ObjectPool.instance.SpawnComponent<SpawnBehaviourOption>(EternityEngine.instance.spawnBehaviourOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (spawnBehaviourOption);
				spawnBehaviourOption.Init ();
				return spawnBehaviourOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				SpawnBehaviourOption spawnBehaviourOption = (SpawnBehaviourOption) asset;
				spawnBehaviourOption._Data = this;
				spawnBehaviourOption.SetActivatedOrderFromData ();
				spawnBehaviourOption.SetCreatedOrderFromData ();
				spawnBehaviourOption.SetOptionPrefabIndexFromData ();
				spawnBehaviourOption.SetSelfAndAllDefaultChildNamesFromData ();
			}
		}
	}
}