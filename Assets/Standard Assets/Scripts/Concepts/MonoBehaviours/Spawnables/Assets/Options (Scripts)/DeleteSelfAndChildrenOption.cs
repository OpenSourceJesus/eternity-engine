using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class DeleteSelfAndChildrenOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}

		public override void OnActivate (EternityEngine.Hand hand)
		{
			Delete ();
			DeleteChildren ();
			// base.OnActivate (hand);
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
		}

		[Serializable]
		public class Data : Option.Data
		{
			public override object MakeAsset ()
			{
				DeleteSelfAndChildrenOption deleteSelfAndChildrenOption = ObjectPool.instance.SpawnComponent<DeleteSelfAndChildrenOption>(EternityEngine.instance.deleteSelfAndChildrenOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (deleteSelfAndChildrenOption);
				// if (!deleteSelfAndChildrenOption.isInitialized)
				// 	deleteSelfAndChildrenOption.Init ();
				return deleteSelfAndChildrenOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				DeleteSelfAndChildrenOption deleteSelfAndChildrenOption = (DeleteSelfAndChildrenOption) asset;
				deleteSelfAndChildrenOption._Data = this;
			}
		}
	}
}