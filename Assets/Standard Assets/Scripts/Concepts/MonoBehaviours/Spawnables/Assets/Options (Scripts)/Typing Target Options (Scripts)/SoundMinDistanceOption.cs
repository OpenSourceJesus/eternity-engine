using System;

namespace _EternityEngine
{
	public class SoundMinDistanceOption : TypingTargetOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public SoundFileInstanceOption soundFileInstanceOption;

		public override bool SetValue (string value)
		{
			if (base.SetValue(value))
			{
				if (Exists(soundFileInstanceOption))
				{
					float? minDistance = GetNumberValue();
					if (minDistance != null)
						soundFileInstanceOption.SetMinDistance ((float) minDistance);
				}
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetSoundFileInstanceOptionNameOfData ();
		}

		void SetSoundFileInstanceOptionNameOfData ()
		{
			if (Exists(soundFileInstanceOption))
				_Data.soundFileInstanceOptionName = soundFileInstanceOption.name;
		}

		void SetSoundFileInstanceOptionNameFromData ()
		{
			if (_Data.soundFileInstanceOptionName != null)
				soundFileInstanceOption = EternityEngine.GetOption<SoundFileInstanceOption>(_Data.soundFileInstanceOptionName);
		}

		[Serializable]
		public class Data : TypingTargetOption.Data
		{
			public string soundFileInstanceOptionName = null;

			public override object MakeAsset ()
			{
				SoundMinDistanceOption soundMinDistanceOption = ObjectPool.instance.SpawnComponent<SoundMinDistanceOption>(EternityEngine.instance.soundMinDistanceOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (soundMinDistanceOption);
				soundMinDistanceOption.Init ();
				return soundMinDistanceOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				SoundMinDistanceOption soundMinDistanceOption = (SoundMinDistanceOption) asset;
				soundMinDistanceOption._Data = this;
				soundMinDistanceOption.SetSoundFileInstanceOptionNameFromData ();
			}
		}
	}
}
