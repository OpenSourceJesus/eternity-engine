using System;
using UnityEngine;

namespace _EternityEngine
{
	public class ContactOffsetOption : TypingTargetOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}

		public override bool SetValue (string value)
		{
			if (base.SetValue(value))
			{
				float? contactOffset = GetNumberValue();
				if (contactOffset != null)
					Physics.defaultContactOffset = (float) contactOffset;
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
		}

		[Serializable]
		public class Data : TypingTargetOption.Data
		{
			public override object MakeAsset ()
			{
				ContactOffsetOption contactOffsetOption = ObjectPool.instance.SpawnComponent<ContactOffsetOption>(EternityEngine.instance.contactOffsetOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (contactOffsetOption);
				contactOffsetOption.Init ();
				return contactOffsetOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				ContactOffsetOption contactOffsetOption = (ContactOffsetOption) asset;
				contactOffsetOption._Data = this;
			}
		}
	}
}