using System;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class TypingOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public char charToType;

		public override void OnActivate (EternityEngine.Hand hand)
		{
			if (Exists(TypingTargetOption.currentActive))
			{
				ActivationBehaviourOption.ActivateChildrenIfShould (TriggerType.TypeCharacter, false, this);
				TypingTargetOption.currentActive.AddText ("" + charToType);
				ActivationBehaviourOption.ActivateChildrenIfShould (TriggerType.TypeCharacter, true, this);
			}
			// base.OnActivate (hand);
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetCharToTypeOfData ();
		}

		void SetCharToTypeOfData ()
		{
			_Data.charToType = charToType;
		}

		void SetCharToTypeFromData ()
		{
			charToType = _Data.charToType;
		}

		[Serializable]
		public class Data : Option.Data
		{
			public char charToType;

			public override object MakeAsset ()
			{
				TypingOption typingOption = ObjectPool.instance.SpawnComponent<TypingOption>(EternityEngine.instance.typingOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (typingOption);
				typingOption.Init ();
				return typingOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				TypingOption typingOption = (TypingOption) asset;
				typingOption._Data = this;
				typingOption.SetCharToTypeFromData ();
			}
		}
	}
}