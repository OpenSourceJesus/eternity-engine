using System;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class DuplicateAndTransformMeshFacesOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public Option modelFileInstanceOptionsParent;
		public List<ModelFileInstanceOption> modelFileInstanceOptions = new List<ModelFileInstanceOption>();
		public TransformOption fromTransformOption;
		public TransformOption toTransformOption;

		public override void Init ()
		{
			base.Init ();
			fromTransformOption.Init (EternityEngine.instance.actOnMeshVerticesFromTrs);
			toTransformOption.Init (EternityEngine.instance.actOnMeshVerticesToTrs);
			modelFileInstanceOptionsParent.onAddChild += OnAddChild;
			modelFileInstanceOptionsParent.onAddChild += OnRemoveChild;
		}

		public override void OnDeleted ()
		{
			modelFileInstanceOptionsParent.onAddChild -= OnAddChild;
			modelFileInstanceOptionsParent.onRemoveChild -= OnRemoveChild;
			base.OnDeleted ();
		}

		void OnAddChild (Option child)
		{
			ModelFileInstanceOption modelFileInstanceOption = child as ModelFileInstanceOption;
			if (modelFileInstanceOption != null)
				modelFileInstanceOptions.Add(modelFileInstanceOption);
		}
		
		void OnRemoveChild (Option child)
		{
			ModelFileInstanceOption modelFileInstanceOption = child as ModelFileInstanceOption;
			if (modelFileInstanceOption != null)
				modelFileInstanceOptions.Remove(modelFileInstanceOption);
		}

		public override void OnActivate (EternityEngine.Hand hand)
		{
			for (int i = 0; i < modelFileInstanceOptions.Count; i ++)
			{
				ModelFileInstanceOption modelFileInstanceOption = modelFileInstanceOptions[i];
				PhysicsObject3D physicsObject = modelFileInstanceOption.physicsObject;
				Mesh mesh = physicsObject.meshFilter.mesh;
				List<Vector3> vertices = new List<Vector3>();
				mesh.GetVertices(vertices);
				List<int> triangles = new List<int>(mesh.GetTriangles(0));
				List<int> addedTriangles = new List<int>();
				for (int i2 = 0; i2 < triangles.Count; i2 += 3)
				{
					int vertexIndex1 = triangles[i2];
					int vertexIndex2 = triangles[i2 + 1];
					int vertexIndex3 = triangles[i2 + 2];
					Transform physicsObjectTrs = physicsObject.trs;
					Vector3 vertex1 = physicsObjectTrs.TransformPoint(vertices[vertexIndex1]);
					Vector3 vertex2 = physicsObjectTrs.TransformPoint(vertices[vertexIndex2]);
					Vector3 vertex3 = physicsObjectTrs.TransformPoint(vertices[vertexIndex3]);
					Shape3D.Face face = new Shape3D.Face(new Vector3[] { vertex1, vertex2, vertex3 });
					Transform fromTrs = fromTransformOption.referenceTrs;
					float fromTrsSize = fromTrs.lossyScale.x;
					if (face.GetDistanceSqr(fromTrs.position) < fromTrsSize * fromTrsSize)
					{
						Transform toTrs = toTransformOption.referenceTrs;
						Vector3 positionOffset = toTrs.position - fromTrs.position;
						Quaternion rotation = Quaternion.Euler(QuaternionExtensions.GetDeltaAngles(fromTrs.eulerAngles, toTrs.eulerAngles));
						float toTrsSize = toTrs.lossyScale.x;
						vertex1 = vertex1 + (rotation * positionOffset * (toTrsSize / fromTrsSize));
						vertex2 = vertex2 + (rotation * positionOffset * (toTrsSize / fromTrsSize));
						vertex3 = vertex3 + (rotation * positionOffset * (toTrsSize / fromTrsSize));
						vertices.AddRange(new Vector3[] { vertex1, vertex2, vertex3 });
						int vertexCount = vertices.Count;
						triangles.AddRange(new int[] { vertexCount - 1, vertexCount - 2, vertexCount - 3 });
					}
				}
				mesh.SetVertices(vertices);
				mesh.SetTriangles(triangles, 0);
			}
			// base.OnActivate (hand);
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetModelFileInstanceOptionsParentNameOfData ();
			SetModelFileInstanceOptionsNamesOfData ();
			SetFromTransformOptionNameOfData ();
			SetToTransformOptionNameOfData ();
		}
		
		void SetModelFileInstanceOptionsParentNameOfData ()
		{
			if (Exists(modelFileInstanceOptionsParent))
				_Data.modelFileInstanceOptionsParentName = modelFileInstanceOptionsParent.name;
		}

		void SetModelFileInstanceOptionsParentNameFromData ()
		{
			if (_Data.modelFileInstanceOptionsParentName != null)
				modelFileInstanceOptionsParent = EternityEngine.GetOption(_Data.modelFileInstanceOptionsParentName);
		}

		void SetModelFileInstanceOptionsNamesOfData ()
		{
			_Data.modelFileInstanceOptionsNames = new string[modelFileInstanceOptions.Count];
			for (int i = 0; i < modelFileInstanceOptions.Count; i ++)
			{
				ModelFileInstanceOption modelFileInstanceOption = modelFileInstanceOptions[i];
				if (Exists(modelFileInstanceOption))
					_Data.modelFileInstanceOptionsNames[i] = modelFileInstanceOption.name;
			}
		}

		void SetModelFileInstanceOptionsNamesFromData ()
		{
			for (int i = 0; i < _Data.modelFileInstanceOptionsNames.Length; i ++)
			{
				string parentName = _Data.modelFileInstanceOptionsNames[i];
				modelFileInstanceOptions.Add(EternityEngine.GetOption<ModelFileInstanceOption>(parentName));
			}
		}
		
		void SetFromTransformOptionNameOfData ()
		{
			if (Exists(fromTransformOption))
				_Data.fromTransformOptionName = fromTransformOption.name;
		}

		void SetFromTransformOptionNameFromData ()
		{
			if (_Data.fromTransformOptionName != null)
				fromTransformOption = EternityEngine.GetOption<TransformOption>(_Data.fromTransformOptionName);
		}
		
		void SetToTransformOptionNameOfData ()
		{
			if (Exists(fromTransformOption))
				_Data.toTransformOptionName = toTransformOption.name;
		}

		void SetToTransformOptionNameFromData ()
		{
			if (_Data.toTransformOptionName != null)
				toTransformOption = EternityEngine.GetOption<TransformOption>(_Data.toTransformOptionName);
		}

		[Serializable]
		public class Data : Option.Data
		{
			public string[] modelFileInstanceOptionsNames = new string[0];
			public string modelFileInstanceOptionsParentName = null;
			public string fromTransformOptionName = null;
			public string toTransformOptionName = null;

			public override object MakeAsset ()
			{
				DuplicateAndTransformMeshFacesOption duplicateMeshFacesOption = ObjectPool.instance.SpawnComponent<DuplicateAndTransformMeshFacesOption>(EternityEngine.instance.duplicateAndTransformMeshFacesOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (duplicateMeshFacesOption);
				duplicateMeshFacesOption.Init ();
				return duplicateMeshFacesOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				DuplicateAndTransformMeshFacesOption duplicateMeshFacesOption = (DuplicateAndTransformMeshFacesOption) asset;
				duplicateMeshFacesOption._Data = this;
				duplicateMeshFacesOption.SetModelFileInstanceOptionsParentNameFromData ();
				duplicateMeshFacesOption.SetModelFileInstanceOptionsNamesFromData ();
				duplicateMeshFacesOption.SetFromTransformOptionNameFromData ();
				duplicateMeshFacesOption.SetToTransformOptionNameFromData ();
			}
		}
	}
}