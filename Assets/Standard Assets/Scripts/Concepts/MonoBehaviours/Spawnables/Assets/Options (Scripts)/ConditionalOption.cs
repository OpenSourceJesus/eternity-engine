using System;
using Extensions;
using UnityEngine;

namespace _EternityEngine
{
	public class ConditionalOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public ComparisonType comparisonType;
		public TooManyChildrenBehaviour tooManyChildrenBehaviour;
		public Option trueOption;
		public Option falseOption;
		public Option value1OptionParent;
		public Option value2OptionParent;
		Option value1Option;
		Option value2Option;

		public override void Init ()
		{
			base.Init ();
			value1OptionParent.onAddChild += (Option child) => { OnAddChild (value1OptionParent, child); };
			value1OptionParent.onRemoveChild += (Option child) => { OnRemoveChild (value1OptionParent, child); };
			value1OptionParent.onAboutToAddChild += OnAboutToAddChild_Value1;
			value2OptionParent.onAddChild += (Option child) => { OnAddChild (value2OptionParent, child); };
			value2OptionParent.onRemoveChild += (Option child) => { OnRemoveChild (value2OptionParent, child); };
			value2OptionParent.onAboutToAddChild += OnAboutToAddChild_Value2;
		}

		public override void OnActivate (EternityEngine.Hand hand)
		{
			bool isTrue = false;
			if (value1Option != null)
			{
				if (value2Option != null)
				{
					string value1 = value1Option.GetValue();
					string value2 = value2Option.GetValue();
					float? numberValue1 = TypingTargetOption.GetNumberValue(value1);
					float? numberValue2 = TypingTargetOption.GetNumberValue(value2);
					if (numberValue1 != null)
					{
						if (numberValue2 != null)
						{
							if (comparisonType == ComparisonType.EqualTo)
								isTrue = numberValue1 == numberValue2;
							else if (comparisonType == ComparisonType.NotEqualTo)
								isTrue = numberValue1 != numberValue2;
							else if (comparisonType == ComparisonType.GreaterThan)
								isTrue = numberValue1 > numberValue2;
							else if (comparisonType == ComparisonType.LessThan)
								isTrue = numberValue1 < numberValue2;
							else if (comparisonType == ComparisonType.GreaterThanOrEqualTo)
								isTrue = numberValue1 >= numberValue2;
							else// if (comparisonType == ComparisonType.LessThanOrEqualTo)
								isTrue = numberValue1 <= numberValue2;
						}
						else if (value1Option.valueType == value2Option.valueType)
						{
							EnumOption enumOption1 = value1Option as EnumOption;
							if (enumOption1 != null)
							{
								EnumOption enumOption2 = (EnumOption) value2Option;
								print(enumOption1.GetValue());
								if (comparisonType == ComparisonType.EqualTo)
									isTrue = enumOption1.GetValue() == enumOption2.GetValue();
								else if (comparisonType == ComparisonType.NotEqualTo)
									isTrue = enumOption1.GetValue() != enumOption2.GetValue();
							}
							else
							{
								Vector3Option vector3Option1 = value1Option as Vector3Option;
								if (vector3Option1 != null)
								{
									Vector3Option vector3Option2 = (Vector3Option) value2Option;
									if (comparisonType == ComparisonType.EqualTo)
										isTrue = vector3Option1.value == vector3Option2.value;
									else if (comparisonType == ComparisonType.NotEqualTo)
										isTrue = vector3Option1.value != vector3Option2.value;
								}
								else
								{
									ColorOption colorOption1 = value1Option as ColorOption;
									if (enumOption1 != null)
									{
										ColorOption colorOption2 = (ColorOption) value2Option;
										if (comparisonType == ComparisonType.EqualTo)
											isTrue = colorOption1.value == colorOption2.value;
										else if (comparisonType == ComparisonType.NotEqualTo)
											isTrue = colorOption1.value != colorOption2.value;
									}
									else
									{
										TransformOption transformOption1 = value1Option as TransformOption;
										if (transformOption1 != null)
										{
											TransformOption transformOption2 = (TransformOption) value2Option;
											if (comparisonType == ComparisonType.EqualTo)
												isTrue = transformOption1.positionOption.value == transformOption2.positionOption.value && transformOption1.rotationOption.value == transformOption2.rotationOption.value && transformOption1.sizeOption.value == transformOption2.sizeOption.value;
											else if (comparisonType == ComparisonType.NotEqualTo)
												isTrue = transformOption1.positionOption.value != transformOption2.positionOption.value || transformOption1.rotationOption.value != transformOption2.rotationOption.value || transformOption1.sizeOption.value != transformOption2.sizeOption.value;
										}
									}
								}
							}
						}
					}
					else if (value1Option.valueType == value2Option.valueType)
					{
						if (value1Option.valueType == ValueType.Bool || value1Option.valueType == ValueType.Text)
						{
							if (comparisonType == ComparisonType.EqualTo)
								isTrue = value1 == value2;
							else if (comparisonType == ComparisonType.NotEqualTo)
								isTrue = value1 != value2;
						}
						else
						{
							EnumOption enumOption1 = value1Option as EnumOption;
							if (enumOption1 != null)
							{
								EnumOption enumOption2 = (EnumOption) value2Option;
								print(enumOption1.GetValue());
								if (comparisonType == ComparisonType.EqualTo)
									isTrue = enumOption1.GetValue() == enumOption2.GetValue();
								else if (comparisonType == ComparisonType.NotEqualTo)
									isTrue = enumOption1.GetValue() != enumOption2.GetValue();
							}
							else
							{
								Vector3Option vector3Option1 = value1Option as Vector3Option;
								if (vector3Option1 != null)
								{
									Vector3Option vector3Option2 = (Vector3Option) value2Option;
									if (comparisonType == ComparisonType.EqualTo)
										isTrue = vector3Option1.value == vector3Option2.value;
									else if (comparisonType == ComparisonType.NotEqualTo)
										isTrue = vector3Option1.value != vector3Option2.value;
								}
								else
								{
									ColorOption colorOption1 = value1Option as ColorOption;
									if (enumOption1 != null)
									{
										ColorOption colorOption2 = (ColorOption) value2Option;
										if (comparisonType == ComparisonType.EqualTo)
											isTrue = colorOption1.value == colorOption2.value;
										else if (comparisonType == ComparisonType.NotEqualTo)
											isTrue = colorOption1.value != colorOption2.value;
									}
									else
									{
										TransformOption transformOption1 = value1Option as TransformOption;
										if (transformOption1 != null)
										{
											TransformOption transformOption2 = (TransformOption) value2Option;
											if (comparisonType == ComparisonType.EqualTo)
												isTrue = transformOption1.positionOption.value == transformOption2.positionOption.value && transformOption1.rotationOption.value == transformOption2.rotationOption.value && transformOption1.sizeOption.value == transformOption2.sizeOption.value;
											else if (comparisonType == ComparisonType.NotEqualTo)
												isTrue = transformOption1.positionOption.value != transformOption2.positionOption.value || transformOption1.rotationOption.value != transformOption2.rotationOption.value || transformOption1.sizeOption.value != transformOption2.sizeOption.value;
										}
									}
								}
							}
						}
					}
				}
				else
				{
					if (comparisonType == ComparisonType.EqualTo)
						isTrue = value1Option == value2Option;
					else// if (comparisonType == ComparisonType.NotEqualTo)
						isTrue = value1Option != value2Option;
				}
			}
			else
			{
				if (comparisonType == ComparisonType.EqualTo)
					isTrue = value1Option == value2Option;
				else// if (comparisonType == ComparisonType.NotEqualTo)
					isTrue = value1Option != value2Option;
			}
			if (isTrue)
				trueOption.Activate (null);
			else
				falseOption.Activate (null);
			// base.OnActivate (hand);
		}

		void OnAddChild (Option parent, Option child)
		{
			if (parent.children.Count > 1)
			{
				if (tooManyChildrenBehaviour == TooManyChildrenBehaviour.UnparentOldChildren)
				{
					while (parent.children.Count > 1)
						parent.RemoveChild (parent.children[0]);
				}
			}
			if (parent == value1OptionParent)
				value1Option = child;
			else
				value2Option = child;
			SetActivatable (ShouldBeActivatable());
		}

		void OnRemoveChild (Option parent, Option child)
		{
			if (parent.children.Count == 0)
			{
				if (parent == value1OptionParent)
					value1Option = null;
				else
					value2Option = null;
				SetActivatable (false);
			}
			else
			{
				ApplyTooManyChildrenBehaviour (parent);
				SetActivatable (ShouldBeActivatable());
			}
		}

		bool OnAboutToAddChild_Value1 (Option child)
		{
			return tooManyChildrenBehaviour != TooManyChildrenBehaviour.Disallow || value1OptionParent.children.Count == 0;
		}

		bool OnAboutToAddChild_Value2 (Option child)
		{
			return tooManyChildrenBehaviour != TooManyChildrenBehaviour.Disallow || value2OptionParent.children.Count == 0;
		}

		public void ApplyTooManyChildrenBehaviour (TooManyChildrenBehaviour tooManyChildrenBehaviour)
		{
			this.tooManyChildrenBehaviour = tooManyChildrenBehaviour;
			ApplyTooManyChildrenBehaviour ();
		}

		void ApplyTooManyChildrenBehaviour ()
		{
			ApplyTooManyChildrenBehaviour (value1OptionParent);
			ApplyTooManyChildrenBehaviour (value2OptionParent);
		}

		void ApplyTooManyChildrenBehaviour (Option parent)
		{
			if (parent.children.Count < 2)
				return;
			if (tooManyChildrenBehaviour == TooManyChildrenBehaviour.UseMostRecentChild)
			{
				if (parent == value1OptionParent)
					value1Option = value1OptionParent.children[value1OptionParent.children.Count - 1];
				else
					value2Option = value2OptionParent.children[value2OptionParent.children.Count - 1];
			}
			else if (tooManyChildrenBehaviour == TooManyChildrenBehaviour.UseLeastRecentChild)
			{
				if (parent == value1OptionParent)
					value1Option = value1OptionParent.children[0];
				else
					value2Option = value2OptionParent.children[0];
			}
			else if (tooManyChildrenBehaviour == TooManyChildrenBehaviour.UnparentOldChildren)
			{
				while (parent.children.Count > 1)
					parent.RemoveChild (parent.children[0]);
				if (parent == value1OptionParent)
					value1Option = value1OptionParent.children[0];
				else
					value2Option = value2OptionParent.children[0];
			}
		}

		bool ShouldBeActivatable ()
		{
			bool output = true;
			if (value1Option != null)
			{
				if (value2Option != null)
				{
					float? numberValue1 = TypingTargetOption.GetNumberValue(value1Option.GetValue());
					float? numberValue2 = TypingTargetOption.GetNumberValue(value2Option.GetValue());
					if (numberValue1 != null)
					{
						if (numberValue2 == null && comparisonType != ComparisonType.EqualTo && comparisonType != ComparisonType.NotEqualTo)
							output = false;
					}
					else if (comparisonType != ComparisonType.EqualTo && comparisonType != ComparisonType.NotEqualTo)
						output = false;
				}
				else if (comparisonType != ComparisonType.EqualTo && comparisonType != ComparisonType.NotEqualTo)
					output = false;
			}
			else if (comparisonType != ComparisonType.EqualTo && comparisonType != ComparisonType.NotEqualTo)
				output = false;
			return output;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetComparisonTypeOfData ();
			SetTooManyChildrenBehaviourOfData ();
			SetTrueOptionNameOfData ();
			SetFalseOptionNameOfData ();
			SetValue1OptionParentNameOfData ();
			SetValue2OptionParentNameOfData ();
			SetValue1OptionNameOfData ();
			SetValue2OptionNameOfData ();
		}

		void SetComparisonTypeOfData ()
		{
			_Data.comparisonType = comparisonType;
		}

		void SetComparisonTypeFromData ()
		{
			comparisonType = _Data.comparisonType;
		}

		void SetTooManyChildrenBehaviourOfData ()
		{
			_Data.tooManyChildrenBehaviour = tooManyChildrenBehaviour;
		}

		void SetTooManyChildrenBehaviourFromData ()
		{
			tooManyChildrenBehaviour = _Data.tooManyChildrenBehaviour;
		}

		void SetTrueOptionNameOfData ()
		{
			if (Exists(trueOption))
				_Data.trueOptionName = trueOption.name;
		}

		void SetTrueOptionNameFromData ()
		{
			if (_Data.trueOptionName != null)
				trueOption = EternityEngine.GetOption(_Data.trueOptionName);
		}

		void SetFalseOptionNameOfData ()
		{
			if (Exists(falseOption))
				_Data.falseOptionName = falseOption.name;
		}

		void SetFalseOptionNameFromData ()
		{
			if (_Data.falseOptionName != null)
				falseOption = EternityEngine.GetOption(_Data.falseOptionName);
		}

		void SetValue1OptionParentNameOfData ()
		{
			if (Exists(value1OptionParent))
				_Data.value1OptionParentName = value1OptionParent.name;
		}

		void SetValue1OptionParentNameFromData ()
		{
			if (_Data.value1OptionParentName != null)
				value1OptionParent = EternityEngine.GetOption(_Data.value1OptionParentName);
		}

		void SetValue2OptionParentNameOfData ()
		{
			if (Exists(value2OptionParent))
				_Data.value2OptionParentName = value2OptionParent.name;
		}

		void SetValue2OptionParentNameFromData ()
		{
			if (_Data.value2OptionParentName != null)
				value2OptionParent = EternityEngine.GetOption(_Data.value2OptionParentName);
		}

		void SetValue1OptionNameOfData ()
		{
			if (Exists(value1Option))
				_Data.value1OptionName = value1Option.name;
		}

		void SetValue1OptionNameFromData ()
		{
			if (_Data.value1OptionName != null)
				value1Option = EternityEngine.GetOption(_Data.value1OptionName);
		}

		void SetValue2OptionNameOfData ()
		{
			if (Exists(value2Option))
				_Data.value2OptionName = value2Option.name;
		}

		void SetValue2OptionNameFromData ()
		{
			if (_Data.value2OptionName != null)
				value2Option = EternityEngine.GetOption(_Data.value2OptionName);
		}

		public enum TooManyChildrenBehaviour
		{
			Disallow,
			UseMostRecentChild,
			UseLeastRecentChild,
			UnparentOldChildren
		}

		public enum ComparisonType
		{
			EqualTo,
			NotEqualTo,
			GreaterThan,
			LessThan,
			GreaterThanOrEqualTo,
			LessThanOrEqualTo
		}

		[Serializable]
		public class Data : Option.Data
		{
			public ComparisonType comparisonType;
			public TooManyChildrenBehaviour tooManyChildrenBehaviour;
			public string trueOptionName = null;
			public string falseOptionName = null;
			public string value1OptionParentName = null;
			public string value2OptionParentName = null;
			public string value1OptionName = null;
			public string value2OptionName = null;

			public override object MakeAsset ()
			{
				ConditionalOption conditionalOption = ObjectPool.instance.SpawnComponent<ConditionalOption>(EternityEngine.instance.conditionalOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (conditionalOption);
				conditionalOption.Init ();
				return conditionalOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				ConditionalOption conditionalOption = (ConditionalOption) asset;
				conditionalOption._Data = this;
				conditionalOption.SetComparisonTypeFromData ();
				conditionalOption.SetTooManyChildrenBehaviourFromData ();
				conditionalOption.SetTrueOptionNameFromData ();
				conditionalOption.SetFalseOptionNameFromData ();
				conditionalOption.SetValue1OptionParentNameFromData ();
				conditionalOption.SetValue2OptionParentNameFromData ();
				conditionalOption.SetValue1OptionNameFromData ();
				conditionalOption.SetValue2OptionNameFromData ();
			}
		}
	}
}