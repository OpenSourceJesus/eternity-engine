using System;

namespace _EternityEngine
{
	public class ConstrainTypeOption : EnumOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public ModelFileInstanceOption modelFileInstanceOption;

		public override bool SetFlagValue (int flagIndex, bool value)
		{
			bool output = base.SetFlagValue(flagIndex, value);
			if (output && Exists(modelFileInstanceOption))
				modelFileInstanceOption.SetConstrainType ((_Rigidbody.ConstrainType) Enum.ToObject(enumType, value));
			return output;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetModelFileInstanceOptionNameOfData ();
		}

		void SetModelFileInstanceOptionNameOfData ()
		{
			if (Exists(modelFileInstanceOption))
				_Data.modelFileInstanceOptionName = modelFileInstanceOption.name;
		}

		void SetModelFileInstanceOptionNameFromData ()
		{
			if (_Data.modelFileInstanceOptionName != null)
				modelFileInstanceOption = EternityEngine.GetOption<ModelFileInstanceOption>(_Data.modelFileInstanceOptionName);
		}

		[Serializable]
		public class Data : EnumOption.Data
		{
			public string modelFileInstanceOptionName = null;

			public override object MakeAsset ()
			{
				ConstrainTypeOption constrainTypeOption = ObjectPool.instance.SpawnComponent<ConstrainTypeOption>(EternityEngine.instance.constrainTypeOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (constrainTypeOption);
				constrainTypeOption.Init ();
				return constrainTypeOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				ConstrainTypeOption constrainTypeOption = (ConstrainTypeOption) asset;
				constrainTypeOption._Data = this;
				constrainTypeOption.SetModelFileInstanceOptionNameFromData ();
			}
		}
	}
}