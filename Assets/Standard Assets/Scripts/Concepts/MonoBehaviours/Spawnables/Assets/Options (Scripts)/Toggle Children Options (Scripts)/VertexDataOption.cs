
using System;
using UnityEngine;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class VertexDataOption : ToggleChildrenOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public ModelFileInstanceOption modelFileInstanceOption;
		public uint vertexIndex;
		public MeshFilter referenceMeshFilter;

		public void Init (ModelFileInstanceOption modelFileInstanceOption, uint vertexIndex)
		{
			this.modelFileInstanceOption = modelFileInstanceOption;
			this.vertexIndex = vertexIndex;
		}

		public override void Init ()
		{
			base.Init ();
			referenceMeshFilter = modelFileInstanceOption.physicsObject.meshFilter;
		}

		public override void OnDeleted ()
		{
			base.OnDeleted ();
			List<Vector3> vertices = new List<Vector3>();
			referenceMeshFilter.mesh.GetVertices(vertices);
			vertices.RemoveAt((int) vertexIndex);
			referenceMeshFilter.mesh.SetVertices(vertices);
		}

		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetModelFileInstanceOptionNameOfData ();
			SetVertexIndexOfData ();
		}

		void SetModelFileInstanceOptionNameOfData ()
		{
			if (Exists(modelFileInstanceOption))
				_Data.modelFileInstanceOption = modelFileInstanceOption.name;
		}

		void SetModelFileInstanceOptionNameFromData ()
		{
			if (_Data.modelFileInstanceOption != null)
				modelFileInstanceOption = EternityEngine.GetOption<ModelFileInstanceOption>(_Data.modelFileInstanceOption);
		}

		void SetVertexIndexOfData ()
		{
			_Data.vertexIndex = vertexIndex;
		}

		void SetVertexIndexFromData ()
		{
			vertexIndex = _Data.vertexIndex;
		}
		
		[Serializable]
		public class Data : ToggleChildrenOption.Data
		{
			public string modelFileInstanceOption = null;
			public uint vertexIndex;

			public override object MakeAsset ()
			{
				VertexDataOption vertexDataOption = ObjectPool.instance.SpawnComponent<VertexDataOption>(EternityEngine.instance.vertexDataOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (vertexDataOption);
				vertexDataOption.Init ();
				return vertexDataOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				VertexDataOption vertexDataOption = (VertexDataOption) asset;
				vertexDataOption._Data = this;
				vertexDataOption.SetModelFileInstanceOptionNameFromData ();
				vertexDataOption.SetVertexIndexFromData ();
			}
		}
	}
}