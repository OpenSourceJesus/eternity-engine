using System;

namespace _EternityEngine
{
	public class SetVector3ZOption : TypingTargetOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public Vector3Option vector3Option;

		public override bool SetValue (string value)
		{
			if (base.SetValue(value))
			{
				if (Exists(vector3Option))
				{
					float? z = GetNumberValue();
					if (z != null)
						vector3Option.SetZ ((float) z);
				}
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetVector3OptionNameOfData ();
		}

		void SetVector3OptionNameOfData ()
		{
			if (Exists(vector3Option))
				_Data.vector3OptionName = vector3Option.name;
		}

		void SetVector3OptionNameFromData ()
		{
			if (_Data.vector3OptionName != null)
				vector3Option = EternityEngine.GetOption<Vector3Option>(_Data.vector3OptionName);
		}

		[Serializable]
		public class Data : TypingTargetOption.Data
		{
			public string vector3OptionName = null;

			public override object MakeAsset ()
			{
				SetVector3ZOption setVector3ZOption = ObjectPool.instance.SpawnComponent<SetVector3ZOption>(EternityEngine.instance.setVector3ZOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (setVector3ZOption);
				setVector3ZOption.Init ();
				return setVector3ZOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				SetVector3ZOption setVector3ZOption = (SetVector3ZOption) asset;
				setVector3ZOption._Data = this;
				setVector3ZOption.SetVector3OptionNameFromData ();
			}
		}
	}
}