
using System;

namespace _EternityEngine
{
	public class BouncinessCombineModeOption : EnumOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public ModelFileInstanceOption modelFileInstanceOption;

		public override bool SetValue (string value)
		{
			if (base.SetValue(value))
			{
				if (Exists(modelFileInstanceOption))
					modelFileInstanceOption.SetBouncinessCombineMode ((_PhysicsMaterial.ValueCombineMode) Enum.ToObject(enumType, value));
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetModelFileInstanceOptionNameOfData ();
		}

		void SetModelFileInstanceOptionNameOfData ()
		{
			if (Exists(modelFileInstanceOption))
				_Data.modelFileInstanceOptionName = modelFileInstanceOption.name;
		}

		void SetModelFileInstanceOptionNameFromData ()
		{
			if (_Data.modelFileInstanceOptionName != null)
				modelFileInstanceOption = EternityEngine.GetOption<ModelFileInstanceOption>(_Data.modelFileInstanceOptionName);
		}

		[Serializable]
		public class Data : EnumOption.Data
		{
			public string modelFileInstanceOptionName = null;

			public override object MakeAsset ()
			{
				BouncinessCombineModeOption bouncinessCombineModeOption = ObjectPool.instance.SpawnComponent<BouncinessCombineModeOption>(EternityEngine.instance.bouncinessCombineModeOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (bouncinessCombineModeOption);
				bouncinessCombineModeOption.Init ();
				return bouncinessCombineModeOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				BouncinessCombineModeOption bouncinessCombineModeOption = (BouncinessCombineModeOption) asset;
				bouncinessCombineModeOption._Data = this;
				bouncinessCombineModeOption.SetModelFileInstanceOptionNameFromData ();
			}
		}
	}
}
