using System;

namespace _EternityEngine
{
	public class InstrumentGraphicsAreVisibleOption : BoolOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public InstrumentOption instrumentOption;

		public override bool SetValue (bool value)
		{
			bool output = base.SetValue(value);
			if (output && Exists(instrumentOption))
				instrumentOption.SetGraphicsVisible (value);
			return output;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetInstrumentOptionNameOfData ();
		}

		void SetInstrumentOptionNameOfData ()
		{
			if (Exists(instrumentOption))
				_Data.instrumentOptionName = instrumentOption.name;
		}

		void SetInstrumentOptionNameFromData ()
		{
			if (_Data.instrumentOptionName != null)
				instrumentOption = EternityEngine.GetOption<InstrumentOption>(_Data.instrumentOptionName);
		}

		[Serializable]
		public class Data : BoolOption.Data
		{
			public string instrumentOptionName = null;

			public override object MakeAsset ()
			{
				InstrumentGraphicsAreVisibleOption instrumentRadiusOption = ObjectPool.instance.SpawnComponent<InstrumentGraphicsAreVisibleOption>(EternityEngine.instance.instrumentGraphicsAreVisibleOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (instrumentRadiusOption);
				instrumentRadiusOption.Init ();
				return instrumentRadiusOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				InstrumentGraphicsAreVisibleOption instrumentRadiusOption = (InstrumentGraphicsAreVisibleOption) asset;
				instrumentRadiusOption._Data = this;
				instrumentRadiusOption.SetInstrumentOptionNameFromData ();
			}
		}
	}
}