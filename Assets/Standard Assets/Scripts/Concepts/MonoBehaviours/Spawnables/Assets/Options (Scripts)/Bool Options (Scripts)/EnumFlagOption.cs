using System;
using Extensions;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class EnumFlagOption : BoolOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public EnumOption enumOption;
		public int flagIndex;

		public override void Init ()
		{
			base.Init ();
			if (Exists(enumOption))
			{
				EnumFlagOption[] flagOptions;
				KeyValuePair<int, bool> keyValuePair = new KeyValuePair<int, bool>(flagIndex, value);
				if (enumOption.flagOptionsDict.TryGetValue(keyValuePair, out flagOptions))
					enumOption.flagOptionsDict[keyValuePair] = flagOptions.Add(this);
				else
					enumOption.flagOptionsDict.Add(keyValuePair, new EnumFlagOption[] { this });
			}
		}

		public override bool SetValue (bool value)
		{
			bool output = base.SetValue(value);
			if (output && Exists(enumOption))
			{
				KeyValuePair<int, bool> keyValuePair = new KeyValuePair<int, bool>(flagIndex, !value);
#if UNITY_EDITOR
				print(enumOption.flagOptionsDict.ContainsKey(keyValuePair));
#endif
				enumOption.flagOptionsDict[keyValuePair] = enumOption.flagOptionsDict[keyValuePair].Remove(this);
				keyValuePair = new KeyValuePair<int, bool>(flagIndex, value);
				EnumFlagOption[] flagOptions;
				if (enumOption.flagOptionsDict.TryGetValue(keyValuePair, out flagOptions))
					enumOption.flagOptionsDict[keyValuePair] = flagOptions.Add(this);
				else
					enumOption.flagOptionsDict.Add(keyValuePair, new EnumFlagOption[] { this });
				enumOption.SetFlagValue (flagIndex, value);
			}
			return output;
		}

		public override void OnDeleted ()
		{
			base.OnDeleted ();
			if (Exists(enumOption))
			{
				KeyValuePair<int, bool> keyValuePair = new KeyValuePair<int, bool>(flagIndex, value);
				enumOption.flagOptionsDict[keyValuePair] = enumOption.flagOptionsDict[keyValuePair].Remove(this);
			}
		}

		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetEnumOptionNameOfData ();
			SetFlagIndexOfData ();
		}

		void SetEnumOptionNameOfData ()
		{
			if (Exists(enumOption))
				_Data.enumOptionName = enumOption.name;
		}

		void SetEnumOptionNameFromData ()
		{
			if (_Data.enumOptionName != null)
				enumOption = EternityEngine.GetOption<EnumOption>(_Data.enumOptionName);
		}

		void SetFlagIndexOfData ()
		{
			_Data.flagIndex = flagIndex;
		}

		void SetFlagIndexFromData ()
		{
			flagIndex = _Data.flagIndex;
		}

		[Serializable]
		public class Data : BoolOption.Data
		{
			public string enumOptionName = null;
			public int flagIndex;

			public override object MakeAsset ()
			{
				EnumFlagOption enumFlagOption = ObjectPool.instance.SpawnComponent<EnumFlagOption>(EternityEngine.instance.enumFlagOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (enumFlagOption);
				enumFlagOption.Init ();
				return enumFlagOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				EnumFlagOption enumFlagOption = (EnumFlagOption) asset;
				enumFlagOption._Data = this;
				enumFlagOption.SetEnumOptionNameFromData ();
				enumFlagOption.SetFlagIndexFromData ();
			}
		}
	}
}