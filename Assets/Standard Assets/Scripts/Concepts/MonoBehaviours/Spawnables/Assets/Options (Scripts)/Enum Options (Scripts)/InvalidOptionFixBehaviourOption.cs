using System;

namespace _EternityEngine
{
	public class InvalidOptionFixBehaviourOption : EnumOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public AutomationOption automationOption;

		public override bool SetValue (int value)
		{
			if (base.SetValue(value))
			{
				if (Exists(automationOption))
					automationOption.invalidOptionFixBehaviour = (AutomationSequence.InvalidOptionFixBehaviour) Enum.ToObject(enumType, value);
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetAutomationOptionNameOfData ();
		}

		void SetAutomationOptionNameOfData ()
		{
			if (Exists(automationOption))
				_Data.automationOptionName = automationOption.name;
		}

		void SetAutomationOptionNameFromData ()
		{
			if (_Data.automationOptionName != null)
				automationOption = EternityEngine.GetOption<AutomationOption>(_Data.automationOptionName);
		}

		[Serializable]
		public class Data : EnumOption.Data
		{
			public string automationOptionName = null;

			public override object MakeAsset ()
			{
				InvalidOptionFixBehaviourOption invalidOptionFixBehaviourOption = ObjectPool.instance.SpawnComponent<InvalidOptionFixBehaviourOption>(EternityEngine.instance.invalidOptionFixBehaviourOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (invalidOptionFixBehaviourOption);
				// if (!invalidOptionFixBehaviourOption.isInitialized)
				// 	invalidOptionFixBehaviourOption.Init ();
				return invalidOptionFixBehaviourOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				InvalidOptionFixBehaviourOption invalidOptionFixBehaviourOption = (InvalidOptionFixBehaviourOption) asset;
				invalidOptionFixBehaviourOption._Data = this;
				invalidOptionFixBehaviourOption.SetAutomationOptionNameFromData ();
			}
		}
	}
}