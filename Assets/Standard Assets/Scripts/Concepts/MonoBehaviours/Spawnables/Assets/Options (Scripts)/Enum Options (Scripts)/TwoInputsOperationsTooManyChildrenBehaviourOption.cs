using System;

namespace _EternityEngine
{
	public class TwoInputsOperationsTooManyChildrenBehaviourOption : EnumOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public TwoInputsOperationsOption twoInputsOperationsTooManyChildrenBehaviourOption;

		public override bool SetValue (int value)
		{
			if (base.SetValue(value))
			{
				if (Exists(twoInputsOperationsTooManyChildrenBehaviourOption))
					twoInputsOperationsTooManyChildrenBehaviourOption.tooManyChildrenBehaviour = (TwoInputsOperationsOption.TooManyChildrenBehaviour) Enum.ToObject(enumType, value);
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetTwoInputsOperationsOptionNameOfData ();
		}

		void SetTwoInputsOperationsOptionNameOfData ()
		{
			if (Exists(twoInputsOperationsTooManyChildrenBehaviourOption))
				_Data.twoInputsOperationsTooManyChildrenBehaviourOptionName = twoInputsOperationsTooManyChildrenBehaviourOption.name;
		}

		void SetTwoInputsOperationsOptionNameFromData ()
		{
			if (_Data.twoInputsOperationsTooManyChildrenBehaviourOptionName != null)
				twoInputsOperationsTooManyChildrenBehaviourOption = EternityEngine.GetOption<TwoInputsOperationsOption>(_Data.twoInputsOperationsTooManyChildrenBehaviourOptionName);
		}

		[Serializable]
		public class Data : EnumOption.Data
		{
			public string twoInputsOperationsTooManyChildrenBehaviourOptionName = null;

			public override object MakeAsset ()
			{
				TwoInputsOperationsTooManyChildrenBehaviourOption arithmeticTooManyChildrenBehaviourOption = ObjectPool.instance.SpawnComponent<TwoInputsOperationsTooManyChildrenBehaviourOption>(EternityEngine.instance.twoInputsOperationsTooManyChildrenBehaviourOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (arithmeticTooManyChildrenBehaviourOption);
				arithmeticTooManyChildrenBehaviourOption.Init ();
				return arithmeticTooManyChildrenBehaviourOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				TwoInputsOperationsTooManyChildrenBehaviourOption arithmeticTooManyChildrenBehaviourOption = (TwoInputsOperationsTooManyChildrenBehaviourOption) asset;
				arithmeticTooManyChildrenBehaviourOption._Data = this;
				arithmeticTooManyChildrenBehaviourOption.SetTwoInputsOperationsOptionNameFromData ();
			}
		}
	}
}