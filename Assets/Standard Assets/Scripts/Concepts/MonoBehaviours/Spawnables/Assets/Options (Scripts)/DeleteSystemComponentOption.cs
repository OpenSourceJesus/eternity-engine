using System;
using System.IO;
using Extensions;

namespace _EternityEngine
{
	public class DeleteSystemComponentOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public SystemComponentOption systemComponentOption;

		// public override void Init ()
		// {
		// 	base.Init ();
		// 	if (Exists(systemComponentOption))
		// 		systemComponentOption.defaultChildren = systemComponentOption.defaultChildren.Add(this);
		// }

		public override void OnActivate (EternityEngine.Hand hand)
		{
			// systemComponentOption.Delete ();
			if (Exists(systemComponentOption))
				File.Delete(systemComponentOption.path);
			// base.OnActivate (hand);
		}

		// public override void OnDeleted ()
		// {
		// 	base.OnDeleted ();
		// 	systemComponentOption.defaultChildren = systemComponentOption.defaultChildren.Remove(this);
		// }
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetSystemComponentOptionNameOfData ();
		}

		void SetSystemComponentOptionNameOfData ()
		{
			if (Exists(systemComponentOption))
				_Data.systemComponentOptionName = systemComponentOption.name;
		}

		void SetSystemComponentOptionNameFromData ()
		{
			if (_Data.systemComponentOptionName != null)
				systemComponentOption = EternityEngine.GetOption<SystemComponentOption>(_Data.systemComponentOptionName);
		}

		[Serializable]
		public class Data : Option.Data
		{
			public string systemComponentOptionName = null;

			public override object MakeAsset ()
			{
				DeleteSystemComponentOption deleteSystemComponentOption = ObjectPool.instance.SpawnComponent<DeleteSystemComponentOption>(EternityEngine.instance.deleteSystemComponentOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (deleteSystemComponentOption);
				deleteSystemComponentOption.Init ();
				return deleteSystemComponentOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				DeleteSystemComponentOption deleteSystemComponentOption = (DeleteSystemComponentOption) asset;
				deleteSystemComponentOption._Data = this;
				deleteSystemComponentOption.SetSystemComponentOptionNameFromData ();
			}
		}
	}
}