using System;

namespace _EternityEngine
{
	public class RedOption : TypingTargetOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public ColorOption colorOption;

		public override bool SetValue (string value)
		{
			if (base.SetValue(value))
			{
				if (Exists(colorOption))
				{
					float? r = GetNumberValue();
					if (r != null)
						colorOption.SetR ((float) r);
				}
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetColorOptionNameOfData ();
		}

		void SetColorOptionNameOfData ()
		{
			if (Exists(colorOption))
				_Data.colorOptionName = colorOption.name;
		}

		void SetColorOptionNameFromData ()
		{
			if (_Data.colorOptionName != null)
				colorOption = EternityEngine.GetOption<ColorOption>(_Data.colorOptionName);
		}

		[Serializable]
		public class Data : TypingTargetOption.Data
		{
			public string colorOptionName = null;

			public override object MakeAsset ()
			{
				RedOption redOption = ObjectPool.instance.SpawnComponent<RedOption>(EternityEngine.instance.redOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (redOption);
				redOption.Init ();
				return redOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				RedOption redOption = (RedOption) asset;
				redOption._Data = this;
				redOption.SetColorOptionNameFromData ();
			}
		}
	}
}