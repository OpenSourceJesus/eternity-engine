using System;
using Extensions;
using UnityEngine;

namespace _EternityEngine
{
	public class EnumValueOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public EnumOption enumOption;
		public int value;

		public override void Init ()
		{
			base.Init ();
			if (Exists(enumOption))
			{
				EnumValueOption[] valueOptions;
				if (enumOption.valueOptionsDict.TryGetValue(value, out valueOptions))
					enumOption.valueOptionsDict[value] = valueOptions.Add(this);
				else
					enumOption.valueOptionsDict.Add(value, new EnumValueOption[] { this });
			}
		}

		public override void OnActivate (EternityEngine.Hand hand)
		{
			if (Exists(enumOption))
				enumOption.SetValue (value);
			// base.OnActivate (hand);
		}

		public override void OnDeleted ()
		{
			base.OnDeleted ();
			if (Exists(enumOption))
				enumOption.valueOptionsDict[value] = enumOption.valueOptionsDict[value].Remove(this);
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetEnumOptionNameOfData ();
			SetValueOfData ();
		}

		void SetEnumOptionNameOfData ()
		{
			if (Exists(enumOption))
				_Data.enumOptionName = enumOption.name;
		}

		void SetEnumOptionNameFromData ()
		{
			if (_Data.enumOptionName != null)
				enumOption = EternityEngine.GetOption<EnumOption>(_Data.enumOptionName);
		}

		void SetValueOfData ()
		{
			_Data.value = (uint) value;
		}

		void SetValueFromData ()
		{
			value = (int) _Data.value;
		}

		[Serializable]
		public class Data : Option.Data
		{
			public string enumOptionName = null;
			public uint value;
			
			public override object MakeAsset ()
			{
				EnumValueOption enumValueOption = ObjectPool.instance.SpawnComponent<EnumValueOption>(EternityEngine.instance.enumValueOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (enumValueOption);
				enumValueOption.Init ();
				return enumValueOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				EnumValueOption enumValueOption = (EnumValueOption) asset;
				enumValueOption._Data = this;
				enumValueOption.SetEnumOptionNameFromData ();
				enumValueOption.SetValueFromData ();
			}
		}
	}
}