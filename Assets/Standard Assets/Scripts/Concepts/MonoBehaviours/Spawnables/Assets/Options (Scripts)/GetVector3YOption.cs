using System;

namespace _EternityEngine
{
	public class GetVector3YOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public override string NameToValueSeparator
		{
			get
			{
				return ": ";
			}
		}
		public Vector3Option vector3Option;

		public override void OnActivate (EternityEngine.Hand hand)
		{
			if (Exists(vector3Option))
			{
				vector3Option.UpdateValue ();
				SetValue ("" + vector3Option.value.y);
			}
			// base.OnActivate (hand);
		}

		public override bool SetValue (string value)
		{
			if (base.SetValue(value))
			{
				if (Exists(vector3Option))
				{
					float? y = TypingTargetOption.GetNumberValue(GetValue());
					if (y != null)
						vector3Option.SetY ((float) y);
				}
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetVector3OptionNameOfData ();
		}

		void SetVector3OptionNameOfData ()
		{
			if (Exists(vector3Option))
				_Data.vector3OptionName = vector3Option.name;
		}

		void SetVector3OptionNameFromData ()
		{
			if (_Data.vector3OptionName != null)
				vector3Option = EternityEngine.GetOption<Vector3Option>(_Data.vector3OptionName);
		}

		[Serializable]
		public class Data : Option.Data
		{
			public string vector3OptionName = null;

			public override object MakeAsset ()
			{
				GetVector3YOption getVector3YOption = ObjectPool.instance.SpawnComponent<GetVector3YOption>(EternityEngine.instance.getVector3YOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (getVector3YOption);
				getVector3YOption.Init ();
				return getVector3YOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				GetVector3YOption getVector3YOption = (GetVector3YOption) asset;
				getVector3YOption._Data = this;
				getVector3YOption.SetVector3OptionNameFromData ();
			}
		}
	}
}