using System;

namespace _EternityEngine
{
	public class LightTypeOption : EnumOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public LightOption lightOption;

		public override bool SetValue (int value)
		{
			if (base.SetValue(value))
			{
				if (Exists(lightOption))
					lightOption.SetType ((LightOption.Type) Enum.ToObject(enumType, value));
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetLightOptionNameOfData ();
		}

		void SetLightOptionNameOfData ()
		{
			if (Exists(lightOption))
				_Data.lightOptionName = lightOption.name;
		}

		void SetLightOptionNameFromData ()
		{
			if (_Data.lightOptionName != null)
				lightOption = EternityEngine.GetOption<LightOption>(_Data.lightOptionName);
		}

		[Serializable]
		public class Data : EnumOption.Data
		{
			public string lightOptionName = null;

			public override object MakeAsset ()
			{
				LightTypeOption lightTypeOption = ObjectPool.instance.SpawnComponent<LightTypeOption>(EternityEngine.instance.lightTypeOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (lightTypeOption);
				lightTypeOption.Init ();
				return lightTypeOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				LightTypeOption lightTypeOption = (LightTypeOption) asset;
				lightTypeOption._Data = this;
				lightTypeOption.SetLightOptionNameFromData ();
			}
		}
	}
}