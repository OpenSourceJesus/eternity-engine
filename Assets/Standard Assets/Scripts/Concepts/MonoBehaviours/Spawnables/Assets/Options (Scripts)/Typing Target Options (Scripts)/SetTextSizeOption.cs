using System;
using UnityEngine;

namespace _EternityEngine
{
	public class SetTextSizeOption : TypingTargetOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}

		public override bool SetValue (string value)
		{
			if (base.SetValue(value))
			{
				float? textSize = GetNumberValue();
				if (textSize != null)
				{
					for (int i = 0; i < children.Count; i ++)
					{
						Option child = children[i];
						child.textTrs.localScale = Vector3.one * (float) textSize;
					}
				}
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
		}

		[Serializable]
		public class Data : TypingTargetOption.Data
		{
			public override object MakeAsset ()
			{
				SetTextSizeOption setTextSizeOption = ObjectPool.instance.SpawnComponent<SetTextSizeOption>(EternityEngine.instance.setTextSizeOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (setTextSizeOption);
				setTextSizeOption.Init ();
				return setTextSizeOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				SetTextSizeOption setTextSizeOption = (SetTextSizeOption) asset;
				setTextSizeOption._Data = this;
			}
		}
	}
}
