using System;
using Extensions;

namespace _EternityEngine
{
	public class MakeFileInstanceOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public FileOption fileOption;

		// public override void Init ()
		// {
		// 	base.Init ();
		// 	fileOption.defaultChildren = fileOption.defaultChildren.Add(this);
		// }

		public override void OnActivate (EternityEngine.Hand hand)
		{
			if (Exists(fileOption))
				fileOption.MakeFileInstance ();
			// base.OnActivate (hand);
		}

		// public override void OnDeleted ()
		// {
		// 	base.OnDeleted ();
		// 	fileOption.defaultChildren = fileOption.defaultChildren.Remove(this);
		// }
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetFileOptionNameOfData ();
		}

		void SetFileOptionNameOfData ()
		{
			_Data.fileOptionName = fileOption.name;
		}

		void SetFileOptionNameFromData ()
		{
			if (_Data.fileOptionName != null)
				fileOption = EternityEngine.GetOption<FileOption>(_Data.fileOptionName);
		}

		[Serializable]
		public class Data : Option.Data
		{
			public string fileOptionName = null;

			public override object MakeAsset ()
			{
				MakeFileInstanceOption makeFileInstanceOption = ObjectPool.instance.SpawnComponent<MakeFileInstanceOption>(EternityEngine.instance.makeFileInstanceOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (makeFileInstanceOption);
				makeFileInstanceOption.Init ();
				return makeFileInstanceOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				MakeFileInstanceOption makeFileInstanceOption = (MakeFileInstanceOption) asset;
				makeFileInstanceOption._Data = this;
				makeFileInstanceOption.SetFileOptionNameFromData ();
			}
		}
	}
}