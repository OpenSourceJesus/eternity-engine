using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class RenameOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public Option renameChildrenParent;
		[HideInInspector]
		public string renameTo = "";

		public override void OnActivate (EternityEngine.Hand hand)
		{
			int renameToOccuranceCount = 1;
			for (int i = 0; i < renameChildrenParent.children.Count; i ++)
			{
				Option option = renameChildrenParent.children[i];
				if (!option.name.Equals(renameTo))
				{
					ActivationBehaviourOption.ActivateChildrenIfShould (TriggerType.NameChange, false, option);
					if (EternityEngine.instance.optionNamesDict.ContainsValue(renameTo))
					{
						ActivationBehaviourOption.ActivateChildrenIfShould (TriggerType.InvalidNameChangeAttempt, false, option);
						while (EternityEngine.instance.optionNamesDict.ContainsValue(renameTo + " (" + renameToOccuranceCount + ")"))
							renameToOccuranceCount ++;
						renameTo += " (" + renameToOccuranceCount + ")";
						ActivationBehaviourOption.ActivateChildrenIfShould (TriggerType.InvalidNameChangeAttempt, true, option);
					}
					EternityEngine.instance.optionNamesDict[option] = renameTo;
					string nameAndValue = renameTo;
					string value = option.GetValue();
					if (value != null)
						nameAndValue += option.NameToValueSeparator + value;
					option.text.text = nameAndValue;
					option.name = renameTo;
					ActivationBehaviourOption.ActivateChildrenIfShould (TriggerType.NameChange, true, option);
				}
			}
			// base.OnActivate (hand);
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetRenameChildrenParentNameOfData ();
		}

		void SetRenameChildrenParentNameOfData ()
		{
			if (Exists(renameChildrenParent))
				_Data.renameChildrenParentName = renameChildrenParent.name;
		}

		void SetRenameChildrenParentNameFromData ()
		{
			if (_Data.renameChildrenParentName != null)
				renameChildrenParent = EternityEngine.GetOption(_Data.renameChildrenParentName);
		}

		[Serializable]
		public class Data : Option.Data
		{
			public string renameChildrenParentName = null;

			public override object MakeAsset ()
			{
				RenameOption renameOption = ObjectPool.instance.SpawnComponent<RenameOption>(EternityEngine.instance.renameOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (renameOption);
				renameOption.Init ();
				return renameOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				RenameOption renameOption = (RenameOption) asset;
				renameOption._Data = this;
				renameOption.SetRenameChildrenParentNameFromData ();
			}
		}
	}
}