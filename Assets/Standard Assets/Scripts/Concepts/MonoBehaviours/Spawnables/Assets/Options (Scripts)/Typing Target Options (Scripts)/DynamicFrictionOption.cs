using System;

namespace _EternityEngine
{
	public class DynamicFrictionOption : TypingTargetOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public ModelFileInstanceOption modelFileInstanceOption;

		public override bool SetValue (string value)
		{
			if (base.SetValue(value))
			{
				if (Exists(modelFileInstanceOption))
				{
					float? dynamicFriction = GetNumberValue();
					if (dynamicFriction != null)
						modelFileInstanceOption.SetDynamicFriction ((float) dynamicFriction);
				}
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetModelFileInstanceOptionNameOfData ();
		}

		void SetModelFileInstanceOptionNameOfData ()
		{
			if (Exists(modelFileInstanceOption))
				_Data.modelFileInstanceOptionName = modelFileInstanceOption.name;
		}

		void SetModelFileInstanceOptionNameFromData ()
		{
			if (_Data.modelFileInstanceOptionName != null)
				modelFileInstanceOption = EternityEngine.GetOption<ModelFileInstanceOption>(_Data.modelFileInstanceOptionName);
		}

		[Serializable]
		public class Data : TypingTargetOption.Data
		{
			public string modelFileInstanceOptionName = null;

			public override object MakeAsset ()
			{
				DynamicFrictionOption dynamicFrictionOption = ObjectPool.instance.SpawnComponent<DynamicFrictionOption>(EternityEngine.instance.dynamicFrictionOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (dynamicFrictionOption);
				dynamicFrictionOption.Init ();
				return dynamicFrictionOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				DynamicFrictionOption dynamicFrictionOption = (DynamicFrictionOption) asset;
				dynamicFrictionOption._Data = this;
				dynamicFrictionOption.SetModelFileInstanceOptionNameFromData ();
			}
		}
	}
}