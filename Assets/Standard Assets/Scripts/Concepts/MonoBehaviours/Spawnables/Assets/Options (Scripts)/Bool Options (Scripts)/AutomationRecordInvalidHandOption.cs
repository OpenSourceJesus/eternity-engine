using System;

namespace _EternityEngine
{
	public class AutomationRecordInvalidHandOption : BoolOption
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public AutomationOption automationOption;

		public override bool SetValue (bool value)
		{
			if (base.SetValue(value))
			{
				if (Exists(automationOption))
					automationOption.recordInvalidHand = value;
				return true;
			}
			else
				return false;
		}
		
		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
			SetAutomationOptionNameOfData ();
		}

		void SetAutomationOptionNameOfData ()
		{
			if (Exists(automationOption))
				_Data.automationOptionName = automationOption.name;
		}

		void SetAutomationOptionNameFromData ()
		{
			if (_Data.automationOptionName != null)
				automationOption = EternityEngine.GetOption<AutomationOption>(_Data.automationOptionName);
		}

		[Serializable]
		public class Data : BoolOption.Data
		{
			public string automationOptionName = null;

			public override object MakeAsset ()
			{
				AutomationRecordInvalidHandOption automationRecordInvalidHandOption = ObjectPool.instance.SpawnComponent<AutomationRecordInvalidHandOption>(EternityEngine.instance.automationRecordInvalidHandOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (automationRecordInvalidHandOption);
				automationRecordInvalidHandOption.Init ();
				return automationRecordInvalidHandOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				AutomationRecordInvalidHandOption automationRecordInvalidHandOption = (AutomationRecordInvalidHandOption) asset;
				automationRecordInvalidHandOption._Data = this;
				automationRecordInvalidHandOption.SetAutomationOptionNameFromData ();
			}
		}
	}
}