using System;
using Extensions;
using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	public class TypingTargetOption : Option
	{
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public FloatRange validNumberRange;
		public static TypingTargetOption currentActive;
		// public static int typingCursorLocation;
		// public static char[] DIGITS = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };

		public override void OnActivate (EternityEngine.Hand hand)
		{
			ValueType? previousType = null;
			if (currentActive != null)
				previousType = currentActive.valueType;
			SetCurrentActive ();
			if (previousType == null || valueType != previousType)
			{
				if (valueType != ValueType.Text)
				{
					Option[] typingOptions = EternityEngine.instance.typingOptionsDict[ValueType.Text].options;
					for (int i = 0; i < typingOptions.Length; i ++)
					{
						Option option = typingOptions[i];
						option.SetActivatable (false);
					}
				}
				Option[] _typingOptions = EternityEngine.instance.typingOptionsDict[valueType].options;
				for (int i = 0; i < _typingOptions.Length; i ++)
				{
					Option option = _typingOptions[i];
					option.SetActivatable (true);
				}
			}
			// base.OnActivate (hand);
		}

		// public override bool SetValue (string value)
		// {
		// 	bool output = base.SetValue(value);
		// 	if (output)
		// 		typingCursorLocation = value.Length;
		// 	return output;
		// }

		public void AddText (string text)
		{
			string value = GetValue();
			// if (typingCursorLocation < value.Length)
			// 	SetValue (value.Insert(typingCursorLocation, text));
			// else
				SetValue (value + text);
			// if (valueType == ValueType.Integer)
			// {
			// 	// float? value = GetNumberValue();
			// 	// if (value == null)
			// 	// {
			// 	// 	this.text.color = Color.red;
			// 	// 	return;
			// 	// }
			// 	// else
			// 	// 	this.text.color = Color.black;
			// }
			// else if (valueType == ValueType.Float)
			// {
				
			// }
		}

		public static float? GetNumberValue (string text)
		{
			float output = 0;
			// while (!float.TryParse(text, out output))
			// {
			// 	int indexOfLeftParenthesis = text.IndexOf("(");
			// 	if (indexOfLeftParenthesis != -1)
			// 	{
			// 		if (indexOfLeftParenthesis == text.Length - 1)
			// 			return null;
			// 		text = GetSimplifiedExponentOperation(text, indexOfLeftParenthesis + 1);
			// 		if (text == null)
			// 			return null;
					
			// 	}
			// 	else
			// 	{
					
			// 	}
			// }
			if (!float.TryParse(text, out output))
				return null;
			else
				return output;
		}

		public float? GetNumberValue ()
		{
			float? numberValue = GetNumberValue(GetValue());
			if (numberValue != null)
				return Mathf.Clamp((float) numberValue, validNumberRange.min, validNumberRange.max);
			else
				return null;
		}

		// TreeNode<MathFloatExpression> GetExpressionTree ()
		// {
		// 	throw new NotImplementedException();
		// 	// TreeNode<MathFloatExpression> output = new TreeNode<MathFloatExpression>(null);
		// 	// return output;
		// }

		// public static string GetSimplifiedExponentOperation (string text, int startIndex = 0)
		// {
		// 	int indexOfExponent = text.IndexOf("^", startIndex);
		// 	if (indexOfExponent != -1)
		// 	{
		// 		if (indexOfExponent == 0 || indexOfExponent == text.Length - 1)
		// 			return null;
		// 		int firstNumberIndex = indexOfExponent - 1;
		// 		string currentNumberStr = "";
		// 		while (firstNumberIndex >= 0)
		// 		{
		// 			char c = text[firstNumberIndex];
		// 			if (DIGITS.Contains(c) || c == '.' || c == '-')
		// 			{
		// 				currentNumberStr = c + currentNumberStr;
		// 				firstNumberIndex --;
		// 			}
		// 			else
		// 				break;
		// 		}
		// 		float firstNumber = float.Parse(currentNumberStr);
		// 		int secondNumberIndex = indexOfExponent + 1;
		// 		currentNumberStr = "";
		// 		while (secondNumberIndex < text.Length)
		// 		{
		// 			char c = text[secondNumberIndex];
		// 			if (DIGITS.Contains(c) || c == '.' || c == '-')
		// 			{
		// 				currentNumberStr += c;
		// 				secondNumberIndex ++;
		// 			}
		// 			else
		// 				break;
		// 		}
		// 		float secondNumber = float.Parse(currentNumberStr);
		// 		text = text.RemoveStartEnd(firstNumberIndex, secondNumberIndex);
		// 		text = text.Insert(firstNumberIndex, "" + Mathf.Pow(firstNumber, secondNumber));
		// 	}
		// 	return text;
		// }

		// public void DeleteText ()
		// {
		// 	string value = GetValue();
		// 	if (typingCursorLocation < value.Length)
		// 		SetValue (value.Remove(typingCursorLocation, 1));
		// }

		public void BackspaceText ()
		{
			// if (typingCursorLocation > 0)
			// {
				string value = GetValue();
			// 	SetValue (value.Remove(typingCursorLocation - 1, 1));
			// }
			if (value.Length > 0)
				SetValue (value.Remove(value.Length - 1, 1));
		}

		public void SetCurrentActive ()
		{
			bool changedTypingTarget = currentActive != this;
			if (changedTypingTarget)
				ActivationBehaviourOption.ActivateChildrenIfShould (TriggerType.ChangeTypingTarget, false, this);
			currentActive = this;
			// typingCursorLocation = GetValue().Length;
			if (changedTypingTarget)
				ActivationBehaviourOption.ActivateChildrenIfShould (TriggerType.ChangeTypingTarget, true, this);
		}

		public override void InitData ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
		}

		[Serializable]
		public class Data : Option.Data
		{
			public override object MakeAsset ()
			{
				TypingTargetOption typingTargetOption = ObjectPool.instance.SpawnComponent<TypingTargetOption>(EternityEngine.instance.typingTargetOptionPrefab.prefabIndex, parent:EternityEngine.instance.sceneTrs);
				Apply (typingTargetOption);
				typingTargetOption.Init ();
				return typingTargetOption;
			}

			public override void Apply (Asset asset)
			{
				base.Apply (asset);
				TypingTargetOption typingTargetOption = (TypingTargetOption) asset;
				typingTargetOption._Data = this;
			}
		}
	}
}