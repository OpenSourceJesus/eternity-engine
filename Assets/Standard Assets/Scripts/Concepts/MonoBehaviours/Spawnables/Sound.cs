﻿using UnityEngine;

namespace _EternityEngine
{
	public class Sound : Spawnable
	{
		public AudioSource audioSource;
		public ObjectPool.DelayedDespawn delayedDespawn;
	}
}