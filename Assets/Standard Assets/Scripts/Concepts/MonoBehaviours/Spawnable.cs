﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace _EternityEngine
{
	[ExecuteInEditMode]
	public class Spawnable : MonoBehaviour, ISpawnable
	{
		public Transform trs;
		public int prefabIndex;
		public int PrefabIndex
		{
			get
			{
				return prefabIndex;
			}
		}

// #if UNITY_EDITOR
// 		public virtual void Start ()
// 		{
// 			if (!Application.isPlaying)
// 			{
// 				if (trs == null)
// 					trs = GetComponent<Transform>();
// 			}
// 		}
// #endif
	}
}