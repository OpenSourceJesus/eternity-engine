using System;
using UnityEngine;
using _EternityEngine;
using Oculus.Platform;

[Serializable]
public class Purchase
{
    public string name;
    public float usDollars;
    public string sku;

    public void StartCheckout ()
    {
		IAP.LaunchCheckoutFlow(sku).OnComplete(OnCheckoutComplete);
    }

	void OnCheckoutComplete (Message<Oculus.Platform.Models.Purchase> message)
	{
		if (message.IsError)
		{
			MonoBehaviour.print("Error: " + message.GetError().Message);
			return;
		}
		Oculus.Platform.Models.Purchase purchase = message.GetPurchase();
		MonoBehaviour.print("Purchased " + purchase.Sku);
		ApplyEffect ();
	}

	public virtual void ApplyEffect ()
	{
	}
}