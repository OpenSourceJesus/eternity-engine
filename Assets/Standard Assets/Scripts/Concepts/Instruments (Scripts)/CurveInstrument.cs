using System;
using UnityEngine;
using System.Collections.Generic;

namespace _EternityEngine
{
	[Serializable]
	public class CurveInstrument : Instrument
	{
		public float turnRate;
		public bool blockableAfterMinLength;

		public override void UpdateGraphics (EternityEngine.Hand hand)
		{
			base.UpdateGraphics (hand);
			EternityEngine.InstrumentInfo instrumentInfo = EternityEngine.InstrumentInfo.GetInstrumentInfo(hand, index);
			List<Vector3> positions = new List<Vector3>();
			Transform otherHandTrs;
			if (hand.isLeftHand)
				otherHandTrs = EternityEngine.instance.rightHand.trs;
			else
				otherHandTrs = EternityEngine.instance.leftHand.trs;
			Vector3 position = hand.trs.position;
			positions.Add(position);
			float sampleSeparation = length.max / (sampleCount - 1);
			Vector3 moveDirection = hand.trs.forward;
			float distanceRemaining = length.max;
			for (int i = 0; i < sampleCount - 1; i ++)
			{
				moveDirection = Vector3.RotateTowards(moveDirection, otherHandTrs.position - position, turnRate * sampleSeparation * Mathf.Deg2Rad, 0);
				position += moveDirection * sampleSeparation;
				positions.Add(position);
				distanceRemaining -= sampleSeparation;
				if (blockableAfterMinLength && length.max - distanceRemaining > length.min)
				{
					Option[] options = GetSelectedOptions(hand, position);
					if (options.Length > 0)
						break;
				}
			}
			LineRenderer lineRenderer = instrumentInfo.lineRenderer;
			lineRenderer.positionCount = positions.Count;
			lineRenderer.SetPositions(positions.ToArray());
		}

		public override Option[] GetSelectedOptions (EternityEngine.Hand hand)
		{
			EternityEngine.InstrumentInfo instrumentInfo = EternityEngine.InstrumentInfo.GetInstrumentInfo(hand, index);
			LineRenderer lineRenderer = instrumentInfo.lineRenderer;
			List<Option> output = new List<Option>();
			Vector3[] positions = new Vector3[lineRenderer.positionCount];
			lineRenderer.GetPositions(positions);
			for (int i = 0; i < lineRenderer.positionCount; i ++)
			{
				Vector3 position = positions[i];
				Option[] options = GetSelectedOptions(hand, position);
				for (int i2 = 0; i2 < options.Length; i2 ++)
				{
					Option option = options[i2];
					if (!output.Contains(option))
						output.Add(option);
				}
			}
			return output.ToArray();
		}
	}
}