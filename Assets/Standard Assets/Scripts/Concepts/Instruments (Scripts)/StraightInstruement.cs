using System;
using UnityEngine;
using System.Collections.Generic;

namespace _EternityEngine
{
	[Serializable]
	public class StraightInstrument : Instrument
	{
		[NonSerialized]
		RaycastHit hit;

		public override void UpdateGraphics (EternityEngine.Hand hand)
		{
			base.UpdateGraphics (hand);
			EternityEngine.InstrumentInfo instrumentInfo = EternityEngine.InstrumentInfo.GetInstrumentInfo(hand, index);
			Transform trs = instrumentInfo.trs;
			if (Physics.SphereCast(trs.position, radius, trs.forward, out hit, length.max, LayerMask.GetMask("Default")))
				trs.localScale = Vector3.one * hit.distance;
			else
				trs.localScale = Vector3.one * length.max;
		}

		public override Option[] GetSelectedOptions (EternityEngine.Hand hand)
		{
			EternityEngine.InstrumentInfo instrumentInfo = EternityEngine.InstrumentInfo.GetInstrumentInfo(hand, index);
			Transform trs = instrumentInfo.trs;
			if (!trs.gameObject.activeInHierarchy || hit.collider == null)
				return new Option[0];
			else
				return new Option[] { hit.collider.GetComponent<Option>() };
		}
	}
}