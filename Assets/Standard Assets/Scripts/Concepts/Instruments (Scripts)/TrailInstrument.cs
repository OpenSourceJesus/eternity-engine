using System;
using UnityEngine;
using System.Collections.Generic;

namespace _EternityEngine
{
	[Serializable]
	public class TrailInstrument : Instrument
	{
		public float disappearDelay;
		public bool resetDisappearDelayAtMinLength;
		public bool resetDisappearDelayAtMaxLength;
		[HideInInspector]
		[NonSerialized]
		public LineRenderer temporaryLineRenderer;
		List<Point> points = new List<Point>();
		float lineRendererLength;

		public override void UpdateGraphics (EternityEngine.Hand hand)
		{
			EternityEngine.InstrumentInfo instrumentInfo = EternityEngine.InstrumentInfo.GetInstrumentInfo(hand, index);
			LineRenderer lineRenderer = instrumentInfo.lineRenderer;
			base.UpdateGraphics (hand);
			float distanceToPreviousPoint = (hand.trs.position - hand.previousPosition).magnitude;
			points.Add(new Point(_Vector3.FromVec3(hand.trs.position), Time.time, distanceToPreviousPoint));
			lineRendererLength += distanceToPreviousPoint;
			float distanceOvershoot = lineRendererLength - length.max;
			if (distanceOvershoot >= 0)
			{
				while (distanceOvershoot > 0 && points.Count > 1)
				{
					Vector3 toSecondPoint = points[1].position.ToVec3() - points[0].position.ToVec3();
					Vector3 offset = Vector3.ClampMagnitude(toSecondPoint, distanceOvershoot);
					points[0].position += offset;
					distanceOvershoot -= offset.magnitude;
					if (distanceOvershoot >= 0 && !TryToRemoveTailPoint())
						break;
				}
			}
			if (hand.triggerInput)
			{
				if (!hand.previousTriggerInput)
				{
					temporaryLineRenderer = GameManager.Instantiate(lineRenderer, hand.trs);
					Transform temporaryLineRendererTrs = temporaryLineRenderer.GetComponent<Transform>();
					temporaryLineRendererTrs.SetParent(EternityEngine.instance.sceneTrs);
					temporaryLineRendererTrs.position = Vector3.zero;
					temporaryLineRendererTrs.eulerAngles = Vector3.zero;
					temporaryLineRenderer.useWorldSpace = false;
				}
			}
			else if (hand.previousTriggerInput && temporaryLineRenderer != null)
				GameManager.DestroyImmediate (temporaryLineRenderer.gameObject);
			if (!((lineRendererLength <= length.min && resetDisappearDelayAtMinLength) || (lineRendererLength >= length.max && resetDisappearDelayAtMaxLength)))
			{
				while (points.Count > 1 && Time.time - points[0].time > disappearDelay)
				{
					if (!TryToRemoveTailPoint())
						break;
				}
			}
			Vector3[] positions = new Vector3[points.Count];
			for (int i = 0; i < points.Count; i ++)
			{
				Point point = points[i];
				positions[i] = point.position.ToVec3();
			}
			lineRenderer.positionCount = points.Count;
			lineRenderer.SetPositions(positions);
		}

		bool TryToRemoveTailPoint ()
		{
			float distanceToPreviousPoint = points[0].distanceToPreviousPoint;
			if (lineRendererLength - distanceToPreviousPoint >= length.min)
			{
				lineRendererLength -= distanceToPreviousPoint;
				points.RemoveAt(0);
				return true;
			}
			return false;
		}

		public override Option[] GetSelectedOptions (EternityEngine.Hand hand)
		{
			EternityEngine.InstrumentInfo instrumentInfo = EternityEngine.InstrumentInfo.GetInstrumentInfo(hand, index);
			LineRenderer lineRenderer = instrumentInfo.lineRenderer;
			List<Option> output = new List<Option>();
			float distance = 0;
			int pointIndex = 1;
			List<Point> _points = new List<Point>(points);
			_points.Reverse();
			Point previousPoint = _points[0];
			float sampleSeparation = lineRendererLength / (sampleCount - 1);
			for (int i = 0; i < sampleCount; i ++)
			{
				float sampleDistance = sampleSeparation * i;
				for (pointIndex = pointIndex; pointIndex < _points.Count; pointIndex ++)
				{
					Point point = _points[pointIndex];
					float distanceToPreviousPoint = previousPoint.distanceToPreviousPoint;
					distance += distanceToPreviousPoint;
					float distanceOvershoot = distance - sampleDistance;
					if (distanceOvershoot >= 0)
					{
						LineSegment3D lineSegment = new LineSegment3D(previousPoint.position.ToVec3(), point.position.ToVec3());
						Option[] options = GetSelectedOptions(hand, lineSegment.GetPointWithDirectedDistance(distanceToPreviousPoint - distanceOvershoot));
						for (int i2 = 0; i2 < options.Length; i2 ++)
						{
							Option option = options[i2];
							if (!output.Contains(option))
								output.Add(option);
						}
						previousPoint = point;
						pointIndex ++;
						break;
					}
					previousPoint = point;
				}
			}
			return output.ToArray();
		}

		[Serializable]
		public class Point
		{
			public _Vector3 position;
			public float time;
			public float distanceToPreviousPoint;

			public Point (_Vector3 position, float time, float distanceToPreviousPoint)
			{
				this.position = position;
				this.time = time;
				this.distanceToPreviousPoint = distanceToPreviousPoint;
			}
		}
	}
}