using System;
using UnityEngine;

[Serializable]
public struct _Mesh
{
	public _Vector3[] vertices;
	public int[] triangles;
	public _Color[] colors;
	public _Vector2[] uvs;
	public _Vector3[] normals;
	public _Vector4[] tangents;

	public Mesh ToMesh ()
	{
		throw new NotImplementedException(); 
	}

	public static _Mesh FromMesh (Mesh mesh)
	{
		throw new NotImplementedException(); 
	}
}