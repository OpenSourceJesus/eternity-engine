using System;
using UnityEngine;

namespace _EternityEngine
{
	[Serializable]
	public struct _ContactPoint
	{
		public _Vector3 point;
		public _Vector3 normal;

		public _ContactPoint (_Vector3 point, _Vector3 normal)
		{
			this.point = point;
			this.normal = normal;
		}
		
		public static _ContactPoint FromContactPoint (ContactPoint contactPoint)
		{
			_ContactPoint output = new _ContactPoint();
			output.point = _Vector3.FromVec3(contactPoint.point);
			return output;
		}
	}
}