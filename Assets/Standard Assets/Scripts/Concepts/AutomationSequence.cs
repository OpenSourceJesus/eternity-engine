using System;
using Extensions;
using UnityEngine;
using System.Collections.Generic;

namespace _EternityEngine
{
	[Serializable]
	public struct AutomationSequence
	{
		public List<SequenceEntry> sequenceEntries;
		public float timeMultiplier;

		public AutomationSequence (List<SequenceEntry> sequenceEntries, float timeMultiplier)
		{
			this.sequenceEntries = sequenceEntries;
			this.timeMultiplier = timeMultiplier;
		}

		public enum InvalidOptionFixBehaviour
		{
			DontFix,
			Spawn,
			DeleteSequenceEntry
		}

		public enum InvalidOptionContinueSequenceBehaviour
		{
			Stop,
			Continue,
			Restart,
			Pause
		}

		[Serializable]
		public struct SequenceEntry
		{
			public OptionEntry activateOptionStart;
			public OptionEntry parentOption;
			public OptionEntry childOption;
			public OptionEntry inputOrientationOption;
			public InputBoolOption.Type inputType;
			public EternityEngine.Hand.Type madeByHandType;
			public bool addChildAfterRecord;
			public bool removeChildAfterRecord;
			public float time;

			public SequenceEntry (OptionEntry activateOptionStart, OptionEntry parentOption, OptionEntry childOption, OptionEntry inputOrientationOption, InputBoolOption.Type inputType, EternityEngine.Hand.Type madeByHandType, bool addChildAfterRecord, bool removeChildAfterRecord, float time)
			{
				this.activateOptionStart = activateOptionStart;
				this.parentOption = parentOption;
				this.childOption = childOption;
				this.inputOrientationOption = inputOrientationOption;
				this.inputType = inputType;
				this.madeByHandType = madeByHandType;
				this.addChildAfterRecord = addChildAfterRecord;
				this.removeChildAfterRecord = removeChildAfterRecord;
				this.time = time;
			}

			[Serializable]
			public struct OptionEntry
			{
				public Option.Data data;
				public bool spawnIfInvalid;

				public OptionEntry (Option.Data data, bool spawnIfInvalid)
				{
					this.data = data;
					this.spawnIfInvalid = spawnIfInvalid;
				}

				public Option GetOption ()
				{
					for (int i = 0; i < EternityEngine.instance.optionNamesDict.Count; i ++)
					{
						Option option = EternityEngine.instance.optionNamesDict.keys[i];
						if (option.name == data.name)
							return option;
					}
					if (spawnIfInvalid)
						return (Option) data.MakeAsset();
					else
						return null;
				}
			}
		}
	}
}