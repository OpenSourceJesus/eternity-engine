using System;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class ByteRange : Range<byte>
{
	public ByteRange (byte min, byte max) : base (min, max)
	{
	}

	public bool DoesIntersect (ByteRange byteRange, bool containsMinAndMax = true)
	{
		if (containsMinAndMax)
			return (min >= byteRange.min && min <= byteRange.max) || (byteRange.min >= min && byteRange.min <= max) || (max <= byteRange.max && max >= byteRange.min) || (byteRange.max <= max && byteRange.max >= min);
		else
			return (min > byteRange.min && min < byteRange.max) || (byteRange.min > min && byteRange.min < max) || (max < byteRange.max && max > byteRange.min) || (byteRange.max < max && byteRange.max > min);
	}

	public bool Contains (byte f, bool containsMinAndMax = true)
	{
		return Contains(f, containsMinAndMax, containsMinAndMax);
	}

	public bool Contains (byte f, bool containsMin = true, bool containsMax = true)
	{
		bool greaterThanMin = min < f;
		if (containsMin)
			greaterThanMin |= min == f;
		bool lessThanMax = f < max;
		if (containsMax)
			lessThanMax |= f == max;
		return greaterThanMin && lessThanMax;
	}

	public bool GetIntersectionRange (ByteRange byteRange, out ByteRange intersectionRange, bool containsMinAndMax = true)
	{
		intersectionRange = null;
		if (DoesIntersect(byteRange, containsMinAndMax))
			intersectionRange = new ByteRange((byte) Mathf.Max(min, byteRange.min), (byte) Mathf.Min(max, byteRange.max));
		return intersectionRange != null;
	}
}