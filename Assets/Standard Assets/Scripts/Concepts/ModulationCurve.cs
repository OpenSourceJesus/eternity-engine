using System;
using UnityEngine;
using System.Collections.Generic;

[Serializable]
public struct ModulationCurve
{
	[NonSerialized]
	public AnimationCurve normalizedValueOverTimeCurve;
	public List<KeyframeData> keyframeDatas;

	public void RemakeAnimationCurve ()
	{
		Keyframe[] keyframes = new Keyframe[keyframeDatas.Count];
		for (int i = 0; i < keyframeDatas.Count; i ++)
		{
			KeyframeData keyframeData = keyframeDatas[i];
			keyframes[i] = keyframeData.ToKeyframe();
		}
		normalizedValueOverTimeCurve.keys = keyframes;
	}

	[Serializable]
	public struct KeyframeData
	{
		public float time;
		public float value;
		public WeightedMode weightedMode;
		public float inTangent;
		public float inWeight;
		public float outTangent;
		public float outWeight;

		public static KeyframeData FromKeyframe (Keyframe keyframe)
		{
			KeyframeData output = new KeyframeData();
			output.time = keyframe.time;
			output.value = keyframe.value;
			output.inTangent = keyframe.inTangent;
			output.outTangent = keyframe.outTangent;
			output.inWeight = keyframe.inWeight;
			output.outWeight = keyframe.outWeight;
			output.weightedMode = keyframe.weightedMode;
			return output;
		}

		public Keyframe ToKeyframe ()
		{
			Keyframe output = new Keyframe(time, value, inTangent, outTangent, inWeight, outWeight);
			output.weightedMode = weightedMode;
			return output;
		}
	}
}