using System;
using System.Text.RegularExpressions;

namespace _EternityEngine
{ 
	[Serializable]
	public struct _Regex
	{
		public string pattern;
		public RegexOptions options;
		public ByteRange characterRange;
		public bool startFromCharacterRangeMin;
	}
}