using System;
using Extensions;
using UnityEngine;

namespace _EternityEngine
{
	[Serializable]
	public struct _PhysicsMaterial
	{
		public float staticFriction;
		public float dynamicFriction;
		public ValueCombineMode frictionCombineMode;
		public float bounciness;
		public ValueCombineMode bouncinessCombineMode;

		public _PhysicsMaterial (float staticFriction, float dynamicFriction, ValueCombineMode frictionCombineMode, float bounciness, ValueCombineMode bouncinessCombineMode)
		{
			this.staticFriction = staticFriction;
			this.dynamicFriction = dynamicFriction;
			this.frictionCombineMode = frictionCombineMode;
			this.bounciness = bounciness;
			this.bouncinessCombineMode = bouncinessCombineMode;
		}

		public PhysicsMaterial ToPhysicsMat ()
		{
			PhysicsMaterial output = new PhysicsMaterial();
			output.staticFriction = staticFriction;
			output.dynamicFriction = dynamicFriction;
			output.frictionCombine = (PhysicsMaterialCombine) Enum.ToObject(typeof(PhysicsMaterialCombine), frictionCombineMode.GetHashCode());
			output.bounciness = bounciness;
			output.bounceCombine = (PhysicsMaterialCombine) Enum.ToObject(typeof(PhysicsMaterialCombine), bouncinessCombineMode.GetHashCode());
			return output;
		}

		public enum ValueCombineMode
		{
			Average,
			Minimum,
			Multiply,
			Maximum
		}
	}
}