using System;
using UnityEngine;

namespace _EternityEngine
{ 
	[Serializable]
	public struct CodeCommand
	{
		public string name;
		[Multiline]
		public string contents;
		[Multiline]
		public string extraMembers;
		[Multiline]
		public string usingStatements;
	}
}