#if OCULUS_STORE
using System;
using Oculus.Platform;

namespace _EternityEngine
{
	[Serializable]
	public class SaveCapability : Purchase
	{
		public override void ApplyEffect ()
		{
			GameManager.SetModifierActive ("Can save", true);
		}
	}
}
#endif