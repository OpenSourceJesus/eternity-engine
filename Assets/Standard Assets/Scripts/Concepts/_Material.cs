using System;
using System.IO;
using Extensions;
using UnityEngine;
using System.Collections.Generic;

namespace _EternityEngine
{
	[Serializable]
	public struct _Material
	{
		public string shaderName;
		public string fileText;
		public FileGUID[] fileGUIDs;

		public _Material (string folderPath, string shaderName)
		{
			this.shaderName = shaderName;
			List<string> filePaths = new List<string>(Directory.GetFiles(folderPath));
			List<FileGUID> _fileGUIDs = new List<FileGUID>();
			fileText = "";
			for (int i = 0; i < filePaths.Count; i ++)
			{
				string filePath = filePaths[i];
				filePaths.RemoveAt(i);
				i --;
				for (int i2 = 0; i2 < MaterialExtensions.IMAGE_FILE_EXTENSIONS.Length; i2 ++)
				{
					string imageFileExtension = MaterialExtensions.IMAGE_FILE_EXTENSIONS[i2];
					if (filePath.EndsWith(imageFileExtension + MaterialExtensions.META_FILE_EXTENSION))
					{
						string fileText = File.ReadAllText(filePath);
						string[] fileLines = fileText.Split(new string[] { "\n" }, StringSplitOptions.None);
						for (int i3 = 0; i3 < fileLines.Length; i3 ++)
						{
							string fileLine = fileLines[i3];
							int indexOfGuidIndicator = fileLine.IndexOf(MaterialExtensions.GUID_INDICATOR);
							if (indexOfGuidIndicator != -1)
							{
								string guid = fileLine.Substring(indexOfGuidIndicator + MaterialExtensions.GUID_INDICATOR.Length);
								filePath = filePath.Remove(filePath.Length - MaterialExtensions.META_FILE_EXTENSION.Length);
								_fileGUIDs.Add(new FileGUID(File.ReadAllBytes(filePath), guid));
								break;
							}
						}
					}
				}
			}
			fileGUIDs = _fileGUIDs.ToArray();
			for (int i = 0; i < filePaths.Count; i ++)
			{
				string filePath = filePaths[i];
				filePaths.RemoveAt(i);
				i --;
				if (filePath.EndsWith(MaterialExtensions.MATERIAL_FILE_EXTENSION))
				{
					fileText = File.ReadAllText(filePath);
					return;
					// string[] fileLines = fileText.Split(new string[] { "\n" }, StringSplitOptions.None);
					// for (int i2 = 0; i2 < fileLines.Length; i2 ++)
					// {
					// 	string fileLine = fileLines[i2];
					// 	int indexOfTexturesIndicator = fileLine.IndexOf(MaterialExtensions.TEXTURES_INDICATOR);
					// 	if (indexOfTexturesIndicator != -1)
					// 	{
					// 		i2 ++;
					// 		for (int i3 = i2; i3 < fileLines.Length; i3 += 4)
					// 		{
					// 			string fileLine1 = fileLines[i3];
					// 			int indexOfProertyIndicator = fileLine1.IndexOf(MaterialExtensions.PROPERTY_INDICATOR);
					// 			if (indexOfProertyIndicator != -1)
					// 			{
					// 				string fileLine2 = fileLines[i3 + 1];
					// 				int indexOfGuidIndicator = fileLine2.IndexOf(MaterialExtensions.GUID_INDICATOR);
					// 				string guid = fileLine2.Substring(indexOfGuidIndicator + MaterialExtensions.GUID_INDICATOR.Length);
					// 				guid = guid.RemoveStartAt(",");
					// 				i2 += 4;
					// 			}
					// 			else
					// 				break;
					// 		}
					// 	}
					// }
				}
			}
		}

		public static Material ToMat (string folderPath, string shaderName)
		{
			return MaterialExtensions.RecreateMaterial(folderPath, Shader.Find(shaderName));
		}

		public Material ToMat ()
		{
			try
			{
				Dictionary<string, Texture2D> textureGuids = new Dictionary<string, Texture2D>();
				for (int i = 0; i < fileGUIDs.Length; i ++)
				{
					FileGUID fileGUID = fileGUIDs[i];
					Texture2D texture = new Texture2D(1, 1);
					ImageConversion.LoadImage(texture, fileGUID.fileData);
					textureGuids.Add(fileGUID.guid, texture);
				}
				Material material = new Material(Shader.Find(shaderName));
				string[] fileLines = fileText.Split(new string[] { "\n" }, StringSplitOptions.None);
				for (int i = 0; i < fileLines.Length; i ++)
				{
					string fileLine = fileLines[i];
					fileLine = fileLine.TrimStart();
					int indexOfShaderKeywordsIndicator = fileLine.IndexOf(MaterialExtensions.SHADER_KEYWORDS_INDICATOR);
					if (indexOfShaderKeywordsIndicator != -1)
					{
						fileLine = fileLine.Substring(indexOfShaderKeywordsIndicator + MaterialExtensions.SHADER_KEYWORDS_INDICATOR.Length);
						material.shaderKeywords = fileLine.Split(new string[] { " " }, StringSplitOptions.None);
					}
					else
					{
						bool doubleSidedGI = false;
						if (MaterialExtensions.GetBool(fileLine, MaterialExtensions.DOUBLE_SIDED_GI_INDICATOR, out doubleSidedGI))
							material.doubleSidedGI = doubleSidedGI;
						else
						{
							string name = "";
							if (MaterialExtensions.GetString(fileLine, MaterialExtensions.NAME_INDICATOR, out name))
								material.name = name;
							else
							{
								int renderQueue = 0;
								if (MaterialExtensions.GetInt(fileLine, MaterialExtensions.RENDER_QUEUE_INDICATOR, out renderQueue))
									material.renderQueue = renderQueue;
								else
								{
									bool enableInstancing = false;
									if (MaterialExtensions.GetBool(fileLine, MaterialExtensions.ENABLE_INSTANCING_INDICATOR, out enableInstancing))
										material.enableInstancing = enableInstancing;
									else
									{
										MaterialGlobalIlluminationFlags globalIlluminationFlags = default(MaterialGlobalIlluminationFlags);
										if (MaterialExtensions.GetEnum<MaterialGlobalIlluminationFlags>(fileLine, MaterialExtensions.GLOBAL_ILLUMINATION_FLAGS_INDICATOR, out globalIlluminationFlags))
											material.globalIlluminationFlags = globalIlluminationFlags;
										else
										{
											int indexOfTexturesIndicator = fileLine.IndexOf(MaterialExtensions.TEXTURES_INDICATOR);
											if (indexOfTexturesIndicator != -1)
											{
												i ++;
												for (int i2 = i; i2 < fileLines.Length; i2 += 4)
												{
													string fileLine1 = fileLines[i2];
													int indexOfProertyIndicator = fileLine1.IndexOf(MaterialExtensions.PROPERTY_INDICATOR);
													if (indexOfProertyIndicator != -1)
													{
														fileLine1 = fileLine1.Substring(indexOfProertyIndicator + MaterialExtensions.PROPERTY_INDICATOR.Length);
														fileLine1 = fileLine1.Remove(fileLine1.Length - 1);
														string fileLine2 = fileLines[i2 + 1];
														int indexOfGuidIndicator = fileLine2.IndexOf(MaterialExtensions.GUID_INDICATOR);
														string guid = fileLine2.Substring(indexOfGuidIndicator + MaterialExtensions.GUID_INDICATOR.Length);
														guid = guid.RemoveStartAt(",");
														material.SetTexture(fileLine1, textureGuids[guid]);
														string fileLine3 = fileLines[i2 + 2];
														float x = float.Parse(fileLine3.SubstringStartEnd(fileLine3.IndexOf(MaterialExtensions.X_INDICATOR), fileLine3.IndexOf(",") - 1));
														float y = float.Parse(fileLine3.SubstringStartEnd(fileLine3.IndexOf(MaterialExtensions.Y_INDICATOR), fileLine3.IndexOf("}") - 1));
														material.SetTextureScale(fileLine1, new Vector2(x, y));
														string fileLine4 = fileLines[i2 + 3];
														x = float.Parse(fileLine4.SubstringStartEnd(fileLine4.IndexOf(MaterialExtensions.X_INDICATOR), fileLine4.IndexOf(",") - 1));
														y = float.Parse(fileLine4.SubstringStartEnd(fileLine4.IndexOf(MaterialExtensions.Y_INDICATOR), fileLine4.IndexOf("}") - 1));
														material.SetTextureOffset(fileLine1, new Vector2(x, y));
														i += 4;
													}
													else
														break;
												}
											}
											else
											{
												int indexOfFloatsIndicator = fileLine.IndexOf(MaterialExtensions.FLOATS_INDICATOR);
												if (indexOfTexturesIndicator != -1)
												{
													i ++;
													for (int i2 = i; i2 < fileLines.Length; i2 ++)
													{
														string fileLine1 = fileLines[i2];
														int indexOfProertyIndicator = fileLine1.IndexOf(MaterialExtensions.PROPERTY_INDICATOR);
														if (indexOfProertyIndicator != -1)
														{
															string nameAndValueSeparator = ": ";
															int indexOfNameAndValueSeparator = fileLine1.IndexOf(nameAndValueSeparator);
															string floatName = fileLine1.SubstringStartEnd(indexOfProertyIndicator + MaterialExtensions.PROPERTY_INDICATOR.Length, indexOfNameAndValueSeparator - 1);
															material.SetFloat(floatName, float.Parse(fileLine1.Substring(indexOfNameAndValueSeparator + nameAndValueSeparator.Length)));
															i ++;
														}
														else
															break;
													}
												}
												else
												{
													int indexOfColorsIndicator = fileLine.IndexOf(MaterialExtensions.COLORS_INDICATOR);
													if (indexOfColorsIndicator != -1)
													{
														i ++;
														for (int i2 = i; i2 < fileLines.Length; i2 ++)
														{
															string fileLine1 = fileLines[i2];
															int indexOfProertyIndicator = fileLine1.IndexOf(MaterialExtensions.PROPERTY_INDICATOR);
															if (indexOfProertyIndicator != -1)
															{
																string nameAndValueSeparator = ": ";
																int indexOfNameAndValueSeparator = fileLine1.IndexOf(nameAndValueSeparator);
																string colorName = fileLine1.SubstringStartEnd(indexOfProertyIndicator + MaterialExtensions.PROPERTY_INDICATOR.Length, indexOfNameAndValueSeparator - 1);
																int indexOfRedIndicator = fileLine1.IndexOf(MaterialExtensions.RED_INDICATOR);
																float r = float.Parse(fileLine1.SubstringStartEnd(indexOfRedIndicator + MaterialExtensions.RED_INDICATOR.Length, fileLine1.IndexOf(",") - 1));
																int indexOfGreenIndicator = fileLine1.IndexOf(MaterialExtensions.GREEN_INDICATOR);
																float g = float.Parse(fileLine1.SubstringStartEnd(indexOfGreenIndicator + MaterialExtensions.GREEN_INDICATOR.Length, fileLine1.IndexOf(",") - 1));
																int indexOfBlueIndicator = fileLine1.IndexOf(MaterialExtensions.BLUE_INDICATOR);
																float b = float.Parse(fileLine1.SubstringStartEnd(indexOfBlueIndicator + MaterialExtensions.BLUE_INDICATOR.Length, fileLine1.IndexOf(",") - 1));
																int indexOfAlphaIndicator = fileLine1.IndexOf(MaterialExtensions.ALPHA_INDICATOR);
																float a = float.Parse(fileLine1.SubstringStartEnd(indexOfAlphaIndicator + MaterialExtensions.ALPHA_INDICATOR.Length, fileLine1.IndexOf("}") - 1));
																material.SetColor(colorName, new Color(r, g, b, a));
																i ++;
															}
															else
																break;
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
				return material;
			}
			catch (Exception e)
			{
				return null;
			}
		}

		[Serializable]
		public struct FileGUID
		{
			public byte[] fileData;
			public string guid;

			public FileGUID (byte[] fileData, string guid)
			{
				this.fileData = fileData;
				this.guid = guid;
			}
		}
	}
}