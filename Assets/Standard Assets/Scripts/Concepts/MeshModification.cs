using System;
using UnityEngine;
using System.Collections.Generic;

[Serializable]
public struct MeshModification
{
	public SerializableDictionary<int, VertexData> changeVertexDatasDict;
	public SerializableDictionary<int, int[]> changeTrianglesDict;
	public int[] removeVertices;
	public int[] removeTriangles;
	public SerializableDictionary<int, VertexData> addVertexDatasDict;
	public SerializableDictionary<int, int> addTrianglesDict;

	public void Apply (Mesh mesh)
	{
		List<Vector3> vertices = new List<Vector3>();
		List<int> triangles = new List<int>();
		List<Color> colors = new List<Color>();
		List<Vector2> uvs = new List<Vector2>();
		List<Vector3> normals = new List<Vector3>();
		List<Vector4> tangents = new List<Vector4>();
		mesh.GetVertices(vertices);
		mesh.GetTriangles(triangles, 0);
		mesh.GetColors(colors);
		mesh.GetUVs(0, uvs);
		mesh.GetNormals(normals);
		mesh.GetTangents(tangents);
		for (int i = 0; i < changeVertexDatasDict.keys.Count; i ++)
		{
			int vertexIndex = changeVertexDatasDict.keys[i];
			VertexData vertexData = changeVertexDatasDict.values[i];
			vertices[i] = vertexData.position.ToVec3();
			colors[i] = vertexData.color.ToColor();
			uvs[i] = vertexData.uv.ToVec2();
		}
		for (int i = 0; i < removeVertices.Length; i ++)
		{
			int removeVertex = removeVertices[i];
			vertices.RemoveAt(removeVertex);
		}
		for (int i = 0; i < changeVertexDatasDict.keys.Count; i ++)
		{
			int vertexIndex = changeVertexDatasDict.keys[i];
			VertexData vertexData = changeVertexDatasDict.values[i];
			vertices.Insert(vertexIndex, vertexData.position.ToVec3());
			colors.Insert(vertexIndex, vertexData.color.ToColor());
			uvs.Insert(vertexIndex, vertexData.uv.ToVec2());
			normals.Insert(vertexIndex, vertexData.normal.ToVec3());
			tangents.Insert(vertexIndex, vertexData.tangent.ToVec4());
		}
		mesh.SetVertices(vertices);
		mesh.SetTriangles(triangles, 0);
		mesh.SetColors(colors);
		mesh.SetNormals(normals);
		mesh.SetTangents(tangents);
	}

	[Serializable]
	public struct VertexData
	{
		public _Vector3 position;
		public _Color color;
		public _Vector2 uv;
		public _Vector3 normal;
		public _Vector4 tangent;
	}
}