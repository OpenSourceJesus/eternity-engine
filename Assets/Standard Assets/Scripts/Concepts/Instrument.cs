using System;
using UnityEngine;
using System.Collections.Generic;

namespace _EternityEngine
{
	[Serializable]
	public class Instrument
	{
		public int index;
		public FloatRange length;
		public float radius;
		public int sampleCount;
		public bool showMinAndMaxLength;
		public bool showSamples;

		public virtual void UpdateGraphics (EternityEngine.Hand hand)
		{
			EternityEngine.InstrumentInfo instrumentInfo = EternityEngine.InstrumentInfo.GetInstrumentInfo(hand, index);
			instrumentInfo.lineRenderer.widthMultiplier = radius * 2;
			// instrumentInfo.trs.localScale = Vector3.one * radius * 2;
		}

		public virtual Option[] GetSelectedOptions (EternityEngine.Hand hand)
		{
			throw new NotImplementedException();
		}

		public virtual Option[] GetSelectedOptions (EternityEngine.Hand hand, Vector3 position)
		{
			SortedDictionary<float, Option> selectedOptionsDict = new SortedDictionary<float, Option>();
			for (int i = 0; i < Option.instances.Count; i ++)
			{
				Option option = Option.instances[i];
				Bounds optionBounds = option.collider.bounds;
				float distanceToPositionSqr = (position - optionBounds.center).sqrMagnitude;
				if (distanceToPositionSqr <= (optionBounds.extents.x + radius) * (optionBounds.extents.x + radius))
					selectedOptionsDict[Mathf.Sqrt(distanceToPositionSqr) - optionBounds.extents.x] = option;
			}
			Option[] output = new Option[selectedOptionsDict.Count];
			selectedOptionsDict.Values.CopyTo(output, 0);
			return output;
		}
	}
}