using System;
using UnityEngine;

namespace _EternityEngine
{
	[Serializable]
	public struct _Collision
	{
		public _ContactPoint[] contactPoints;

		public _Collision (_ContactPoint[] contactPoints)
		{
			this.contactPoints = contactPoints;
		}
		
		public static _Collision FromCollision (Collision coll)
		{
			_Collision output = new _Collision();
			ContactPoint[] contactPoints = new ContactPoint[coll.contactCount];
			coll.GetContacts(contactPoints);
			output.contactPoints = new _ContactPoint[contactPoints.Length];
			for (int i = 0; i < contactPoints.Length; i ++)
			{
				ContactPoint contactPoint = contactPoints[i];
				output.contactPoints[i] = _ContactPoint.FromContactPoint(contactPoint);
			}
			return output;
		}
	}
}